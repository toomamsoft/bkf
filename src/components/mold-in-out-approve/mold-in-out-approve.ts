import { Component, Input } from '@angular/core';
import { AlertController, Events } from 'ionic-angular';
import { SubMoldInOutApproveModel } from '../../models/package/SubMoldInOutApproveModel';
import { BasePage } from '../../shared/basepage';

/**
 * Generated class for the MoldInOutApproveComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'mold-in-out-approve',
  templateUrl: 'mold-in-out-approve.html'
})
export class MoldInOutApproveComponent extends BasePage {

  @Input() taggedApprove: SubMoldInOutApproveModel[];

  constructor(
    private alertCtrl: AlertController,
    public events : Events
  ) {
    super(events)
    console.log('Hello MoldInOutApproveComponent Component');
  }

  async viewRemark(remark) {
    this.alertCtrl.create({
      title: 'Remark',
      subTitle: remark,
      buttons: ['close']
    }).present();
  }

}
