import { NgModule } from '@angular/core';
import { AccordionMenuComponent } from './accordion-menu/accordion-menu';
import { IonicPageModule } from 'ionic-angular';
import { AccordionListComponent } from './accordion-list/accordion-list';
import { MoldInOutApproveComponent } from './mold-in-out-approve/mold-in-out-approve';

@NgModule({
	declarations: [
		AccordionMenuComponent,
		AccordionListComponent,
    MoldInOutApproveComponent
	],
	imports: [IonicPageModule.forChild('')],
	exports: [
		AccordionMenuComponent,
		AccordionListComponent,
    MoldInOutApproveComponent
	]
})
export class ComponentsModule {}
