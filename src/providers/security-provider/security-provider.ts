import { CONSTANT } from './../../shared/constant';
import { Injectable } from '@angular/core';
import { App, AlertController, Platform, MenuController } from 'ionic-angular';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';
import { StorageProvider } from '../storage-provider/storage-provider';

const MINUTES_UNITL_AUTO_LOGOUT = 60 // in mins
const CHECK_INTERVAL = 10000 // in ms
const STORE_KEY =  'lastAction';

/*
  Generated class for the SecurityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SecurityProvider {

  public getLastAction() {
    return parseInt(localStorage.getItem(STORE_KEY));
  }
  public setLastAction(lastAction: number) {
    localStorage.setItem(STORE_KEY, lastAction.toString());
  }

  constructor(
    private app: App,
    private alertCtrl: AlertController,
    private platform: Platform,
    private menuCtrl: MenuController,
    private _storageProvider: StorageProvider 
    ) {
    console.log('Hello SecurityProvider Provider');
    this.platform.ready().then(() => {
      this.check();
      this.initListener();
      this.initInterval();
      localStorage.setItem(STORE_KEY,Date.now().toString());
    });
  }

  async initListener() {
    document.body.addEventListener('click', () => this.reset());
    document.body.addEventListener('keydown',() => this.reset());
    document.body.addEventListener('keyup',() => this.reset());
    document.body.addEventListener('keypress',() => this.reset());
    document.body.addEventListener('touchstart', () => this.reset())
  }

  async reset() {
    this.setLastAction(Date.now());
  }

  async initInterval() {
    setInterval(() => {
      this.check();
    }, CHECK_INTERVAL);
  }

  async check() {
    let nav = this.app.getActiveNavs()[0];
    let activeView = nav.getActive();
    if((activeView.name !== 'LoginPage') && (activeView.name !=='ActiveMobilePage'))
    {
      const now = Date.now();
      const timeleft = this.getLastAction() + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
      const diff = timeleft - now;
      const isTimeout = diff < 0;

      if (isTimeout)  {
        this._storageProvider.removeKey(STORE_KEY);

        console.log('activeView:::',activeView)
        this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%session.timeout%'] : ENG['%session.timeout%'],//'Session Timeout',
          buttons: [{ 
            text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%'],
            handler: ()=>{
              // activeView.getNav().popAll();
              // const views: any[] = this.app.getActiveNavs();
              // console.log('authen view:::',views)
              this._storageProvider.clear();
              this.menuCtrl.enable(false, 'authenticateduser');
              this.menuCtrl.enable(false, 'authenticatedadmin');
              this.menuCtrl.enable(false, 'authenticatedmaker');
              this.app.getActiveNav().setRoot('LoginPage');
            }}
          ]
        }).present();
      }
    }
  }

}
