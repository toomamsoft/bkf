import { CONSTANT } from './../../shared/constant';
import { LoginModel } from './../../models/LoginModel';
import { Injectable } from '@angular/core';
import { BaseProvider } from '../base-provider/base-provider';
import { AlertController, LoadingController, MenuController, App } from 'ionic-angular';
import { Utilities } from '../../shared/utilities';
import { OperationBaseModel } from '../../models/package/OperationBaseModel';
import { AuthenticationAdapterProvider } from '../adapter/authentication-adapter/authentication-adapter';
import { AuthenticateUserAdapterModel } from '../../models/package/AuthenticateUserAdapterModel';
import { StorageProvider } from '../storage-provider/storage-provider';

import * as ENG from '../../assets/i18n/en.json';
import * as THAI from '../../assets/i18n/th.json';
import { SettingAdapterModel } from '../../models/package/SettingAdapterModel';

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider extends BaseProvider {
  public loginModel: LoginModel = new LoginModel();
  public loginModel2: LoginModel = new LoginModel();

  constructor(
    public app: App,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public _authenticateAdapterProvider: AuthenticationAdapterProvider,
    public _storageProvider: StorageProvider
  ) {
    super(alertCtrl,loadingCtrl);
    console.log('Hello LoginProvider Provider');
  }

  async authenticateUser() {
    try {
      let response: any = await this._authenticateAdapterProvider.authenticateUser(this.loginModel);
      console.log(`authenticateUser ::: `, response);
      this.loginModel.authenticateUser = Utilities.mapJsontoModel(response.ResponseData, AuthenticateUserAdapterModel);
    }catch (err)
    {
      this.loginModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async activeMobile() {
    try {
      let response: any = await this._authenticateAdapterProvider.activeMobile(this.loginModel);
      console.log(`activeMobile ::: `, response);
      this.loginModel.authenticateUser = Utilities.mapJsontoModel(response.ResponseData, AuthenticateUserAdapterModel);
    }catch (err)
    {
      this.loginModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async deviceAllow() {
    try {
      let response: any = await this._authenticateAdapterProvider.deviceAllow();
      console.log(`deviceAllow ::: `, response);
      this.loginModel.authenticateUser = Utilities.mapJsontoModel(response.ResponseData, AuthenticateUserAdapterModel);
    }catch (err)
    {
      this.loginModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async settingConfig() {
    try {
      let response: any = await this._authenticateAdapterProvider.settingConfig();
      console.log(`settingConfig ::: `, response);
      this.loginModel.setting = Utilities.mapJsontoModel(response.ResponseData, SettingAdapterModel);
    }catch (err)
    {
      this.loginModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async authenticateLogout()
  {
    this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
      message: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.logout%'] : ENG['%message.logout%'],
      buttons:[{
        text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.no%'] : ENG['%btn.logout.no%'],
        handler: () => {
          console.log('cancel')
        }
      },{
        text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.yes%'] : ENG['%btn.logout.yes%'],
        handler: () => {
          this._storageProvider.clear();
          this.menuCtrl.enable(false, 'authenticateduser');
          this.menuCtrl.enable(false, 'authenticatedadmin');
          this.menuCtrl.enable(false, 'authenticatedmaker');
          this.app.getActiveNav().setRoot(`LoginPage`);
        }
      }]
    }).present();
  }

}