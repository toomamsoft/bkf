import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';
import { OperationBaseModel } from '../../models/package/OperationBaseModel';
import { CONSTANT } from '../../shared/constant';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';

@Injectable()
export class BaseProvider {
  public loading;

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {
    console.log('Hello BaseProvider Provider');
  }

  mappingErrorCodeToErrorMessage(err) {
    let errMapping = Object.create(err);
    if (CONSTANT.APP_LANGUAGE == 'th') {
      if(!!errMapping.errorCode) {
        errMapping.errorMsg = THAI["%" + errMapping.errorCode + "%"] || errMapping.errorMsg;
      } else {
        errMapping.errorMsg = THAI[errMapping.errorMsg] || errMapping.errorMsg;
      }
    } else {
      if(!!errMapping.errorCode) {
        errMapping.errorMsg = ENG["%" + errMapping.errorCode + "%"] || errMapping.errorMsg;
      } else {
        errMapping.errorMsg = ENG[errMapping.errorMsg] || errMapping.errorMsg;
      }
    }
    return errMapping;
  }

  submitErrorEvent(operationBaseModel: OperationBaseModel) {
    console.log('submitErrorEvent operationBaseModel', operationBaseModel)
    if (operationBaseModel ) {
      let errorMapping = this.mappingErrorCodeToErrorMessage(operationBaseModel);
      let alert = this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%'+ errorMapping.errorStatus +'%'] : ENG['%'+ errorMapping.errorStatus +'%'] || errorMapping.errorMsg || errorMapping.errorName,
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
        }]
      });
      alert.present();
    }
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%loading%'] : ENG['%loading%']
    });

    this.loading.present();
  }

  dismissLoading() {
    this.loading.dismiss();
  }
}
