import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Connection } from '../../BKFConnection/connection/Connection';
import { CONSTANT } from '../../shared/constant';
import { LoginModel } from '../../models/LoginModel';
import { LoadingController } from 'ionic-angular';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';

@Injectable()
export class ConnectionUtilProvider {
  public userModel: LoginModel = new LoginModel();

  constructor(
    private _connection: Connection,
    private _http: HttpClient,
    private _loadingCtrl: LoadingController
  ) {
    console.log('Hello ConnectionUtilProvider Provider');
  }

  getConnection(): ConnectionMobile {
    return this._connection.getConnection(CONSTANT.SERVER, this._http);
  }

  async invokeAdapter(endpoint: string, param: any) {
    console.log(`[invokeAdapter][` + endpoint + `] Param ::: `, param);
    let loading = this._loadingCtrl.create({
      content: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%loading%'] : ENG['%loading%']
    });

    loading.present();
    try {
      
      const response: any = await this.getConnection().getResourceRequest(endpoint, param, this.userModel);
      console.log(`[RESPONSE][` + endpoint + `] Response ::: `, response);
      if (response.Status && response.Status == "SuccessWithWarning") {
        console.log(`[SUCCESS WITH WARNING][` + endpoint + `] ::: `, response);
        return response;
      } else if (response.Status && response.Status != "Failure") {
        console.log(`[SUCCESS][` + endpoint + `] ::: `, response);
        return response;
      } else {
        console.log('invokeAdapter else')
        throw response;
      }
    } catch (err) {
      console.log(`[ERROR] catch [ ` + endpoint + `] ::: `, err);
      throw err;
    } finally 
    {
      loading.dismiss()
    }
  }

}
