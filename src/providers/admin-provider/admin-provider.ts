import { AdminAdapterModel } from './../../models/package/AdminAdapterModel';
import { AdminAdapterProvider } from './../adapter/admin-adapter/admin-adapter';
import { AdminModel } from './../../models/AdminModel';
import { BaseProvider } from './../base-provider/base-provider';
import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';
import { OperationBaseModel } from '../../models/package/OperationBaseModel';
import { Utilities } from '../../shared/utilities';

/*
  Generated class for the AdminProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AdminProvider extends BaseProvider {

  public adminModel: AdminModel = new AdminModel();
  public adminModel2: AdminModel = new AdminModel();

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private _adminAdapterProvider: AdminAdapterProvider
  ) {
    super(alertCtrl,loadingCtrl)
    console.log('Hello AdminProvider Provider');
  }

  async getListUserBKF() {
    try {
      let response: any = await this._adminAdapterProvider.getListUserBKF();
      console.log(`getListUserBKF ::: `, response);
      this.adminModel.getListBKF = Utilities.mapJsontoModel(response.ResponseData, AdminAdapterModel);
    }catch (err)
    {
      this.adminModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async getListUserMold() {
    try {
      let response: any = await this._adminAdapterProvider.getListUserMold();
      console.log(`getLisUserMold ::: `, response);
      this.adminModel.getListMold = Utilities.mapJsontoModel(response.ResponseData, AdminAdapterModel);
    }catch (err)
    {
      this.adminModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async getConfig() {
    try {
      let response: any = await this._adminAdapterProvider.getConfig();
      console.log(`getConfig ::: `, response);
      this.adminModel.getConfig = Utilities.mapJsontoModel(response.ResponseData, AdminAdapterModel);
    }catch (err)
    {
      this.adminModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async getUpdateUserBKF() {
    try {
      let response: any = await this._adminAdapterProvider.getUpdateUserBKF(this.adminModel);
      console.log(`getUpdateUserBKF ::: `, response);
      this.adminModel.statement = Utilities.mapJsontoModel(response.ResponseData, AdminAdapterModel);
    }catch (err)
    {
      this.adminModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async getUpdateUserMaker() {
    try {
      let response: any = await this._adminAdapterProvider.getUpdateUserMaker(this.adminModel);
      console.log(`getUpdateUserMaker ::: `, response);
      this.adminModel.statement = Utilities.mapJsontoModel(response.ResponseData, AdminAdapterModel);
    }catch (err)
    {
      this.adminModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async getUpdateConfig() {
    try {
      let response: any = await this._adminAdapterProvider.getUpdateConfig(this.adminModel);
      console.log(`getUpdateConfig ::: `, response);
      this.adminModel.statement = Utilities.mapJsontoModel(response.ResponseData, AdminAdapterModel);
    }catch (err)
    {
      this.adminModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }


}
