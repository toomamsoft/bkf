import { BaseProvider } from './../base-provider/base-provider';
import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';
import { MoldmakerAdapterProvider } from './../adapter/moldmaker-adapter/moldmaker-adapter';
import { MoldMakerModel } from './../../models/MoldMakerModel';
import { Utilities } from '../../shared/utilities';
import { MoldMakerAdapterModel } from '../../models/package/MoldMakerAdapterModel';
import { OperationBaseModel } from '../../models/package/OperationBaseModel';

/*
  Generated class for the MoldmakerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MoldmakerProvider extends BaseProvider {
  public moldmakerModel: MoldMakerModel = new MoldMakerModel()
  public moldmakerModel2: MoldMakerModel = new MoldMakerModel();
  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private _moldmakerAdapterProvider: MoldmakerAdapterProvider
  ) {
    super(alertCtrl, loadingCtrl)
    console.log('Hello MoldmakerProvider Provider');
  }

  async getListMold() {
    try {
      let response: any = await this._moldmakerAdapterProvider.getListMold();
      console.log(`getListMold ::: `, response);
      this.moldmakerModel.getListMode = Utilities.mapJsontoModel(response.ResponseData, MoldMakerAdapterModel);
    }catch (err)
    {
      this.moldmakerModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async getViewMoldFromBKF() {
    try {
      let response: any = await this._moldmakerAdapterProvider.getViewMoldFromBKF(this.moldmakerModel);
      console.log(`getViewMoldFromBKF ::: `, response);
      this.moldmakerModel.getMoldBKF = Utilities.mapJsontoModel(response.ResponseData, MoldMakerAdapterModel);
    }catch (err)
    {
      this.moldmakerModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async getReceiveMold() {
    try {
      let response: any = await this._moldmakerAdapterProvider.getReceiveMold(this.moldmakerModel);
      console.log(`getReceiveMold ::: `, response);
      this.moldmakerModel.getMoldBKF = Utilities.mapJsontoModel(response.ResponseData, MoldMakerAdapterModel);
    }catch (err)
    {
      this.moldmakerModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async getMoldMakerTransaction() {
    try {
      let response: any = await this._moldmakerAdapterProvider.getMoldMakerTransaction(this.moldmakerModel);
      console.log(`getMoldMakerTransaction ::: `, response);
      this.moldmakerModel.saveData = Utilities.mapJsontoModel(response.ResponseData, MoldMakerAdapterModel);
    }catch (err)
    {
      this.moldmakerModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }


  async getMoldMakerHistoryDetail(){
    try {
      let response: any = await this._moldmakerAdapterProvider.getMoldMakerHistoryDetail(this.moldmakerModel);
      console.log(`getMoldMakerHistoryDetail ::: `, response);
      this.moldmakerModel.getHistoryDetail = Utilities.mapJsontoModel(response.ResponseData, MoldMakerAdapterModel);
    }catch (err)
    {
      this.moldmakerModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async getBKFViewHistory() {
    try {
      let response: any = await this._moldmakerAdapterProvider.getBKFViewHistory(this.moldmakerModel);
      console.log(`getBKFViewHistory ::: `, response);
      this.moldmakerModel.bkfview = Utilities.mapJsontoModel(response.ResponseData, MoldMakerAdapterModel);
    }catch (err)
    {
      this.moldmakerModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

}
