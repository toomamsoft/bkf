import { CONSTANT } from '../../../shared/constant';
import { Injectable } from '@angular/core';
import { ConnectionUtilProvider } from '../../connection-util/connection-util';
import { AdminModel } from '../../../models/AdminModel';
/*
  Generated class for the AdminAdapterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AdminAdapterProvider {

  constructor(
    private _connectionUtil: ConnectionUtilProvider
    ) {
    console.log('Hello AdminAdapterProvider Provider');
  }

  async getListUserBKF() {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.ADMIN_GET_USER_BKF_ADAPTER, param);
  }

  async getListUserMold() {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.ADMIN_GET_USER_MOLD_ADAPTER, param);
  }

  async getConfig() {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.ADMIN_GET_CONFIG_ADAPTER, param);
  }

  async getCreateUserBKF(inputObj) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          userid: '',
          firstname: '',
          lastname: '',
          position: ''
        }
      }
    }];
    return this._connectionUtil.invokeAdapter(CONSTANT.ADMIN_GET_CONFIG_ADAPTER, param);
  }

  async getUpdateUserBKF(inputObj: AdminModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          empid: inputObj.empid,
          username : inputObj.username || '',
          password : inputObj.password || '',
          firstname: inputObj.firstname || '',
          lastname: inputObj.lastname || '', 
          position: inputObj.position || '',
          usergroup: inputObj.usergroup || '',
          usertype: inputObj.usertype || '',
          flag: inputObj.flag
        }
      }
    }];
    return this._connectionUtil.invokeAdapter(CONSTANT.ADMIN_GET_UPDATE_USER_BKF_ADAPTER, param);
  }

  async getUpdateUserMaker(inputObj: AdminModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          empid: inputObj.empid,
          username : inputObj.username || '',
          password : inputObj.password || '',
          firstname: inputObj.firstname || '',
          lastname: inputObj.lastname || '', 
          position: inputObj.position || '',
          usergroup: inputObj.usergroup || '',
          usertype: inputObj.usertype || '',
          flag: inputObj.flag
        }
      }
    }];
    return this._connectionUtil.invokeAdapter(CONSTANT.ADMIN_GET_UPDATE_USER_MAKER_ADAPTER, param);
  }

  async getUpdateConfig(inputObj: AdminModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          survey: inputObj.imgSurvey,
          moldinout: inputObj.imgInOut,
          moldmaker: inputObj.imgMaker,
          iso: inputObj.isoNumber
        }
      }
    }];
    return this._connectionUtil.invokeAdapter(CONSTANT.ADMIN_GET_UPDATE_CONFIG_ADAPTER, param);
  }
}
