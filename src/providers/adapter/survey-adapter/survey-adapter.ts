import { Injectable } from '@angular/core';
import { ConnectionUtilProvider } from '../../connection-util/connection-util';
import { CONSTANT } from '../../../shared/constant';
import { SurveyModel } from '../../../models/SurveyModel';
import { Utilities } from '../../../shared/utilities';
import { StorageProvider } from '../../storage-provider/storage-provider';

@Injectable()
export class SurveyAdapterProvider {

  constructor(private _connectionUtil: ConnectionUtilProvider, private _storageProvider: StorageProvider) {
    console.log('Hello SurveyAdapterProvider Provider');
  }

  async getLocationSurvey()
  {
    const params = [{
      "parameter": {
        "inboxParam":
        {
          
        }
      }
    }];
    return this._connectionUtil.invokeAdapter(CONSTANT.SURVEY_LOCATION_ADAPTER, params);
  }

  async getHistorySurvey(inputObj: SurveyModel)
  {
    const params = [{
      "parameter":
      {
        "inboxParam":
        {
          "MoldID" :  inputObj.MoldId
        }
      }
    }];
    return this._connectionUtil.invokeAdapter(CONSTANT.SURVEY_HISTORY_ADAPTER, params);
  }

  async getHistoryDetail(inputObj: SurveyModel)
  {
    const params = [{
      "parameter":
      {
        "inboxParam":
        {
          "MoldID" :  inputObj.MoldId,
          "RunNo" :  inputObj.RunNo
        }
      }
    }];
    return this._connectionUtil.invokeAdapter(CONSTANT.SURVEY_HISOTRY_DETAIL_ADAPTER, params);
  }

  async getSearchSurvey(inputObj: SurveyModel)
  {
    const params = [{
      "parameter":
      {
        "inboxParam":
        {
          "startDate" : inputObj.From,
          "endDate" : inputObj.To,
          "MoldId": inputObj.MoldId
        }
      }
    }];
    return this._connectionUtil.invokeAdapter(CONSTANT.SURVEY_SARCH_ADAPTER, params);
  }

  async saveDataMold(inputObj: SurveyModel)
  {
    const params = [{
      "parameter":
      {
        "inboxParam":
        {
          "moldid" :  inputObj.MoldId,
          "assetid" :  inputObj.AssetId,
          "custno" :  inputObj.CustNo,
          "partno" :  inputObj.PartNo,
          "partname" :  inputObj.PartName,
          "plate" :  inputObj.Plate,
          "location" :  inputObj.Location,
          "remark" :  inputObj.Remark,
          "empid": await this._storageProvider.getKey(CONSTANT.USER_ID),
          "imei" :  await Utilities.IMEI(),
          "photo" : inputObj.Images
        }
      }
    }];
    return this._connectionUtil.invokeAdapter(CONSTANT.SURVEY_SAVE_ADAPTER, params)
  }

}
