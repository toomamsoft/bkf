import { Injectable } from '@angular/core';
import { ConnectionUtilProvider } from '../../connection-util/connection-util';
import { StorageProvider } from '../../storage-provider/storage-provider';
import { CONSTANT } from '../../../shared/constant';
import { MessageModel } from '../../../models/MessageModel';

/*
  Generated class for the NotificationAdapterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class  MessageAdapterProvider  {

  constructor(
    private _connectionUtil: ConnectionUtilProvider, 
    private _storageProvider: StorageProvider) {
    console.log('Hello NotificationAdapterProvider Provider');
  }

  async getMessageDetail() {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          empid: await this._storageProvider.getKey(CONSTANT.USER_ID)
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MESSAGE_GETMESSGE_ADAPTER, param);
  }

  async readMessage(inputObj: MessageModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          makerid: await this._storageProvider.getKey(CONSTANT.USER_ID)
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MESSAGE_READ_MESSGE_ADAPTER, param);
  }

}
