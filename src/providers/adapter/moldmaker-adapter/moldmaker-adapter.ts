import { CONSTANT } from '../../../shared/constant';
import { Injectable } from '@angular/core';
import { ConnectionUtilProvider } from '../../connection-util/connection-util';
// import { Utilities } from '../../../shared/utilities';
import { StorageProvider } from '../../storage-provider/storage-provider';
import { MoldMakerModel } from '../../../models/MoldMakerModel';
import { Utilities } from '../../../shared/utilities';

/*
  Generated class for the MoldmakerAdapterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MoldmakerAdapterProvider {

  constructor(
    private _connectionUtil: ConnectionUtilProvider,
    private _storageProvider: StorageProvider
  ) {
    console.log('Hello MoldmakerAdapterProvider Provider');
  }

  async getListMold() {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          makerid: await this._storageProvider.getKey(CONSTANT.USER_ID)
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDMAKER_GETLISTMOLD_ADAPTER, param);
  }

  async getViewMoldFromBKF(inputObj: MoldMakerModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          jobid: inputObj.JobId
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDMAKER_HISTORY_ADAPTER, param);
  }

  async getReceiveMold(inputObj: MoldMakerModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          jobid: inputObj.JobId
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDMAKER_RECEIVE_ADAPTER, param);
  }

  async getMoldMakerTransaction(inputObj: MoldMakerModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          'imei': await Utilities.IMEI(),
          'jobid': inputObj.JobId,
          'makerid': await this._storageProvider.getKey(CONSTANT.USER_ID),
          'remark': inputObj.Remark,
          'status': inputObj.Status,
          'photo': inputObj.Images
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDMAKER_SAVE_TRANSACTION_ADAPTER, param);
  }

  async getMoldMakerHistoryDetail(inputObj: MoldMakerModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          'jobid': inputObj.JobId,
          'runno': inputObj.RunNo,
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDMAKER_HISTORY_DETAIL_ADAPTER, param);
  }

  async getBKFViewHistory(inputObj: MoldMakerModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          jobid: inputObj.JobId
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDMAKER_BKFVIEW_ADAPTER, param);
  }
}
