import { CONSTANT } from '../../../shared/constant';
import { Injectable } from '@angular/core';
import { ConnectionUtilProvider } from '../../connection-util/connection-util';
import { LoginModel } from '../../../models/LoginModel';
import { Utilities } from '../../../shared/utilities';
import { StorageProvider } from '../../storage-provider/storage-provider';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthenticationAdapterProvider {

  constructor(
    private _connectionUtil: ConnectionUtilProvider,
    private _storageProvider: StorageProvider
  ) {
    console.log('Hello AuthenticationAdapterProvider Provider');
  }

  async authenticateUser(inputObj: LoginModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          username: inputObj.username,
          password: inputObj.password,
          token: await this._storageProvider.getKey(CONSTANT.FCM_TOKEN),//localStorage.getItem(CONSTANT.FCM_TOKEN),
          imei: await Utilities.IMEI()
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.AUTHENTICATION_AUTHENTICATE_ADAPTER, param);
  }

  async deviceAllow () {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          imei: await Utilities.IMEI()
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.AUTHENTICATION_DEVICE_ALLOW_ADAPTER, param);

  }

  async activeMobile(inputObj: LoginModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          password: inputObj.keyActive,
          imei: await Utilities.IMEI()
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.AUTHENTICATION_ACTIVE_ADAPTER, param);
  }

  async settingConfig() {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {

        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.AUTHENtICATION_CONFIG, param);
  }

}
