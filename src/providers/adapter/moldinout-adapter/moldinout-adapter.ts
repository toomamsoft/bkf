import { CONSTANT } from '../../../shared/constant';
import { Injectable } from '@angular/core';
import { ConnectionUtilProvider } from '../../connection-util/connection-util';
import { MoldInOutModel } from '../../../models/MoldInOutModel';
import { Utilities } from '../../../shared/utilities';
import { StorageProvider } from '../../storage-provider/storage-provider';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MoldInoutAdapterProvider {

  constructor(
    private _connectionUtil: ConnectionUtilProvider,
    private _storageProvider: StorageProvider
  ) {
    console.log('Hello MoldInoutAdapterProvider Provider');
  }

  async getSetting() {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {

        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDINOUT_SETTING_ADAPTER, param);
  }

  async getNewMoldOut(inputObj: MoldInOutModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          "moldid": inputObj.MoldId,
          "jobid": inputObj.JobId
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDINOUT_MASTER_ADAPTER, param);
  }

  async getMoldOutHistory(inputObj: MoldInOutModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          "moldid": "",
          "jobid": ""
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDINOUT_HISTORY_ADAPTER, param);
  }

  async getApprove(inputObj: MoldInOutModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          "jobid": inputObj.JobId,
          "approvetype": ''
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDINOUT_APPROVE_ADAPTER, param);
  }

  async saveDataNewMold(inputObj: MoldInOutModel){
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          'JobId': '',
          'Mold_ID': inputObj.MoldId,
          'MakerId': inputObj.MakerId,
          'PlanOut': inputObj.PlanOut,
          'PlanIn': inputObj.PlanIn,
          'ReasonId': inputObj.ReasonId,
          'Remark_new': inputObj.Remark || '',
          'ConfirmOrCancel': null,
          'Remark_out': '', 
          'ConfirmOutDate': null,
          'Remark_out_Transport': '', 
          'CurrentStaus': 'new',
          'CurrentStausRemark': '',
          'EmpID': await this._storageProvider.getKey(CONSTANT.USER_ID),
          'CreateDate': '',
          'IMEI': await Utilities.IMEI(),
          'Printed': '',
          'flag': 'inprogress',
          'MoldMakerStaus': '',
          'Custno': inputObj.CustNo,
          'PartNo': inputObj.PartNo,
          'PartName': inputObj.PartName,
          'Photo': inputObj.Images
        }
      }
    }];

    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDINOUT_SAVE_NEW_ADAPTER, param);
  }

  async ApproveOrRejectOut(inputObj: MoldInOutModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          'JobId': inputObj.JobId,
          'EmpID': await this._storageProvider.getKey(CONSTANT.USER_ID),
          'ApproveType': inputObj.ApproveType,
          'Status': inputObj.ApproveStatus,
          'Remark': inputObj.ApproveRemark
        }
      }
    }]
    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDINOUT_APPROVE_REJECT_OUT, param);
  }

  async ConfirmOrRejectOut(inputObj: MoldInOutModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          'JobId': inputObj.JobId,
          'ConfirmOrCancel': inputObj.ConfirmStatus,
          'Remark_out': inputObj.ConfirmStatus == 'N' ? inputObj.ConfirmRemark : '',
          'Remark_out_Transport': inputObj.ConfirmStatus == 'Y' ? inputObj.ConfirmRemark : '',
          'CurrentStaus': inputObj.ConfirmStatus == 'Y' ? 'ConfirmOut' : 'Completed',
          'MoldMakerStaus': '',
          'EmpID': await this._storageProvider.getKey(CONSTANT.USER_ID),
          'IMEI' : await Utilities.IMEI(),
          'Photo' : inputObj.Images,
        }
      }
    }]
    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDINOUT_APPROVE_CONFIRMOUT, param);
  }

  async moldSearch(inputObj: MoldInOutModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          moldid: inputObj.MoldId
        }
      }
    }]
    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDINOUT_SEARCH_ADAPTER, param);
  }

  async moldStatus(inputObj: MoldInOutModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          id: inputObj.statusId
        }
      }
    }]
    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDINOUT_STATUS_ADAPTER, param);
  }

  async getWaitingApprove() {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {

        }
      }
    }]
    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDINOUT_GET_WAITING, param);
  }

  async ConfirmIn(inputObj: MoldInOutModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          'JobId': inputObj.JobId,
          'confirmorreject': inputObj.ConfirmStatus,
          'remark_in': inputObj.ConfirmRemark,
          'IMEI' : await Utilities.IMEI(),
          'Photo' : inputObj.Images,
        }
      }
    }]
    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDINOUT_APPROVE_CONFIRMIN, param);
  }

  async ApproveOrRejectIn(inputObj: MoldInOutModel) {
    let param: any = null;
    param = [{
      "parameter":
      {
        "inboxParam":
        {
          'JobId': inputObj.JobId,
          'EmpID': await this._storageProvider.getKey(CONSTANT.USER_ID),
          'Status': inputObj.ApproveStatus,
          'Remark': inputObj.ApproveRemark
        }
      }
    }]
    return this._connectionUtil.invokeAdapter(CONSTANT.MOLDINOUT_APPROVE_REJECT_IN, param);
  }

}
