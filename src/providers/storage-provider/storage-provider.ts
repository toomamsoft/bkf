import { CONSTANT } from './../../shared/constant';
import { Injectable } from '@angular/core';

/*
  Generated class for the StorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageProvider {

  constructor() {
    console.log('Hello StorageProvider Provider');
  }

  async getKey(key: string) {
    return new Promise(async (resolve, reject) => {
      try {
        let value = null;
        value = localStorage.getItem(key);
        return resolve(value);
      } catch (e) {
        console.log('getKey error', e);
        return resolve(null);
      }
    });
  }

  async setKey(key: string, value: any) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = true;
        localStorage.setItem(key, value);
        return resolve(result);
      } catch (e) {
        return reject(e);
      }
    });
  }

  async removeKey(key: string) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = true;
        localStorage.removeItem(key);
        return resolve(result);
      } catch (e) {
        return reject(e);
      }
    });
  }

  async clear() {
    const ignore = {}
    ignore[CONSTANT.ACTIVE_MOBILE] = await this.getKey(CONSTANT.ACTIVE_MOBILE);
    return new Promise(async (resolve, reject) => {
      try {
        let result = null;
        localStorage.clear();
        localStorage.setItem(CONSTANT.ACTIVE_MOBILE,ignore[CONSTANT.ACTIVE_MOBILE])
        return resolve(result);
      } catch (e) {
        return reject(e);
      }
    });
  }

}
