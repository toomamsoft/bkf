import { CONSTANT } from './../../shared/constant';
import { AlertController,LoadingController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
// import { File } from '@ionic-native/file';
import { Utilities } from '../../shared/utilities';

/*
  Generated class for the ScanProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ScanProvider {

  constructor(
    private barcodeScanner: BarcodeScanner,
    private camera: Camera,
    private imagePicker: ImagePicker,
    // private file: File,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController
  ) {
    console.log('Hello ScanProvider Provider');
  }

  public async scan()
  {
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false, // iOS and Android
      showFlipCameraButton: true, // iOS and Android
      showTorchButton: true, // iOS and Android
      torchOn: false, // Android, launch with the torch switched on (if available)
      prompt: 'Place a barcode inside the scan area', // Android
        // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
      resultDisplayDuration: 500,
      //formats: 'QR_CODE,PDF_417', // default: all but PDF_417 and RSS_EXPANDED
        // Android only (portrait|landscape), default unset so it rotates with the device
      orientation: 'portrait',
      disableAnimations: true, // iOS
      disableSuccessBeep: false // iOS
    };
    return new Promise(resolve => {
      this.barcodeScanner.scan(options)
      .then((data) => {
        resolve(data.text)
      }).catch((error) => {
        this.showMessageError(error);
        resolve('')
      });
    }) 
  }

  public async getImageFromCamere() {
    let loading = this.loadingCtrl.create({

    });
    loading.present();
    const options = {
      quality: 100,
      allowEdit: false,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: true,
      correctOrientation: true
    };

    return new Promise(resolve => {
      this.camera.getPicture(options).then(async b64str => {
        let b64resize = await Utilities.resizeImage('data:image/jpeg;base64,'+b64str)
        loading.dismiss();
        resolve(b64resize);
      }).catch((error) => { 
        if(error != 'No Image Selected'){
          this.showMessageError(error);
        }
        loading.dismiss();
        resolve(error);
      })
    })

    // return new Promise(resolve => {
    //   this.camera.getPicture(options).then(async fileUri => {
    //     var imagePath = fileUri.substr(0, fileUri.lastIndexOf('/') + 1);
    //     var imageName = fileUri.substr(fileUri.lastIndexOf('/') + 1);
    //     let fileImages = await this.file.readAsDataURL(imagePath, imageName).then((b64str) => {
    //       return b64str
    //     })
    //     resolve(await Utilities.getBase64Image(fileImages))
    //   }).catch((error) => {
    //     if(error != 'No Image Selected'){
    //       this.showMessageError(error);
    //     }
    //     resolve(error)
    //   })
    // })
  }

  public async getImageFormAlbum(limit:number = 5)
  {
    let base64Images:any = [];
    const options = {
      quality: 100,
      maximumImagesCount: limit,
      height: CONSTANT.HEIGHT,
      width: CONSTANT.WIDTH,
      outputType: 1
    }

    return new Promise(resolve => {
      this.imagePicker.getPictures(options)
      .then( async (results) => {
        for (var i = 0; i < results.length; i++) {
          base64Images.push('data:image/jpeg;base64,'+results[i]);
        }
        resolve(base64Images)
      }).catch((error) => {
        this.showMessageError(error);
        resolve(error)
      });
    })
  }

  private showMessageError(msg: string) {
    const alert = this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: 'Attention!',
      subTitle: msg,
      buttons: ['Close']
    });
    alert.present();
  }
}
