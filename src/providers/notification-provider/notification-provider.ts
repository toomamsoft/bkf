import { Injectable } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Platform } from 'ionic-angular';

@Injectable()
export class NotificationProvider {
  constructor(
    private localNotification: LocalNotifications,
    private platform: Platform
  ) {
    console.log('Hello NotificationProvider Provider');
  }

  async showNotification(text: string = '') {
    this.localNotification.schedule({
      text: text,
      sound: this.platform.is('ios') ? `file://beep.caf` : `file://song.mp3`
    })
  }
}
