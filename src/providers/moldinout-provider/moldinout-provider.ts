import { MoldInOutModel } from './../../models/MoldInOutModel';
import { Injectable } from '@angular/core';
import { BaseProvider } from '../base-provider/base-provider';
import { AlertController, LoadingController } from 'ionic-angular';
import { Utilities } from '../../shared/utilities';
import { OperationBaseModel } from '../../models/package/OperationBaseModel';
import { MoldInoutAdapterProvider } from '../adapter/moldinout-adapter/moldinout-adapter'
import { MoldInOutAdapterModel } from '../../models/package/MoldInOutAdapterModel';

/*
  Generated class for the MoldinoutProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MoldinoutProvider extends BaseProvider {
  public moldInOutModel: MoldInOutModel = new MoldInOutModel();
  public moldInOutModel2: MoldInOutModel = new MoldInOutModel();

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private _moldInoutAdapterProvider: MoldInoutAdapterProvider
  ) {
    super(alertCtrl, loadingCtrl)
    console.log('Hello MoldinoutProvider Provider');
  }

  async getSetting() {
    try {
      let response: any = await this._moldInoutAdapterProvider.getSetting();
      console.log(`getSetting ::: `, response);
      this.moldInOutModel.getSetting = Utilities.mapJsontoModel(response.ResponseData, MoldInOutAdapterModel);
    } catch (err) {
      this.moldInOutModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }

  async getNewMoldOut() {
    try {
      let response: any = await this._moldInoutAdapterProvider.getNewMoldOut(this.moldInOutModel);
      console.log(`getNewMoldOut ::: `, response);
      this.moldInOutModel.getNewMoldInOut = Utilities.mapJsontoModel(response.ResponseData, MoldInOutAdapterModel);
    } catch (err) {
      this.moldInOutModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }

  async saveDataNewMold() {
    try {
      let response: any = await this._moldInoutAdapterProvider.saveDataNewMold(this.moldInOutModel);
      console.log(`saveDataNewMold ::: `, response);
      this.moldInOutModel.getNewMoldInOut = Utilities.mapJsontoModel(response.ResponseData, MoldInOutAdapterModel);
    } catch (err) {
      this.moldInOutModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }

  async getApprove() {
    try {
      let response: any = await this._moldInoutAdapterProvider.getApprove(this.moldInOutModel);
      console.log(`getApprove ::: `, response);
      this.moldInOutModel.getApprove = Utilities.mapJsontoModel(response.ResponseData, MoldInOutAdapterModel);
    } catch (err) {
      this.moldInOutModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }

  async ApproveOrRejectOut() {
    try {
      let response: any = await this._moldInoutAdapterProvider.ApproveOrRejectOut(this.moldInOutModel);
      console.log(`ApproveOrRejectOut ::: `, response);
      this.moldInOutModel.statment = Utilities.mapJsontoModel(response.ResponseData, MoldInOutAdapterModel);
    } catch (err) {
      this.moldInOutModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }

  async ConfirmOrRejectOut() {
    try {
      let response: any = await this._moldInoutAdapterProvider.ConfirmOrRejectOut(this.moldInOutModel);
      console.log(`ConfirmOrRejectOut ::: `, response);
      this.moldInOutModel.statment = Utilities.mapJsontoModel(response.ResponseData, MoldInOutAdapterModel);
    } catch (err) {
      this.moldInOutModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }

  async moldSearch() {
    try {
      let response: any = await this._moldInoutAdapterProvider.moldSearch(this.moldInOutModel);
      console.log(`moldSearch ::: `, response);
      this.moldInOutModel.getSearch = Utilities.mapJsontoModel(response.ResponseData, MoldInOutAdapterModel);
    } catch (err) {
      this.moldInOutModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }

  async moldStatus() {
    try {
      let response: any = await this._moldInoutAdapterProvider.moldStatus(this.moldInOutModel);
      console.log(`moldStatus ::: `, response);
      this.moldInOutModel.getStatus = Utilities.mapJsontoModel(response.ResponseData, MoldInOutAdapterModel);
    } catch (err) {
      this.moldInOutModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }

  async getWaitingApprove() {
    try {
      let response: any = await this._moldInoutAdapterProvider.getWaitingApprove();
      console.log(`getWaitingApprove ::: `, response);
      this.moldInOutModel.getWaiting = Utilities.mapJsontoModel(response.ResponseData, MoldInOutAdapterModel);
    } catch (err) {
      this.moldInOutModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }//MOLDINOUT_APPROVE_CONFIRMIN

  async ConfirmIn() {
    try {
      let response: any = await this._moldInoutAdapterProvider.ConfirmIn(this.moldInOutModel);
      console.log(`ConfirmIn ::: `, response);
      this.moldInOutModel.statment = Utilities.mapJsontoModel(response.ResponseData, MoldInOutAdapterModel);
    } catch (err) {
      this.moldInOutModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }

  async ApproveOrRejectIn() {
    try {
      let response: any = await this._moldInoutAdapterProvider.ApproveOrRejectIn(this.moldInOutModel);
      console.log(`ApproveOrRejectIn ::: `, response);
      this.moldInOutModel.statment = Utilities.mapJsontoModel(response.ResponseData, MoldInOutAdapterModel);
    } catch (err) {
      this.moldInOutModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }

}
