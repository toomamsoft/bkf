import { MessageAdapterModel } from './../../models/package/MessageAdapterModel';
import { Injectable } from '@angular/core';
import { BaseProvider } from '../base-provider/base-provider';
import { AlertController, LoadingController } from 'ionic-angular';
import { MessageAdapterProvider } from '../adapter/message-adapter/message-adapter';
import { Utilities } from '../../shared/utilities';
import { OperationBaseModel } from '../../models/package/OperationBaseModel';
import { MessageModel } from '../../models/MessageModel';

/*
  Generated class for the MessageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MessageProvider extends BaseProvider {

  public messageModel: MessageModel = new MessageModel();
  public messageModel2: MessageModel = new MessageModel();

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private _messageAdapterProvider: MessageAdapterProvider
    ) {
      super(alertCtrl,loadingCtrl)
      console.log('Hello MessageProvider Provider');
  }

  async getMessageDetail() {
    try {
      let response: any = await this._messageAdapterProvider.getMessageDetail();
      console.log(`getMessageDetail ::: `, response);
      this.messageModel.getMessageDetail = Utilities.mapJsontoModel(response.ResponseData, MessageAdapterModel);
    } catch (err) {
      this.messageModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }

  async getReadMessage() {
    try {
      let response: any = await this._messageAdapterProvider.readMessage(this.messageModel);
      console.log(`getReadMessage ::: `, response);
      this.messageModel.readMessage = Utilities.mapJsontoModel(response.ResponseData, MessageAdapterModel);
    } catch (err) {
      this.messageModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
    }
  }

}
