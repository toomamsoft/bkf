import { Injectable } from '@angular/core';
import { BaseProvider } from '../base-provider/base-provider';
import { AlertController, LoadingController } from 'ionic-angular';
import { Utilities } from '../../shared/utilities';
import { OperationBaseModel } from '../../models/package/OperationBaseModel';
import { SurveyModel } from '../../models/SurveyModel';
import { SurveyAdapterProvider } from '../adapter/survey-adapter/survey-adapter';
import { HistorySurveyAdapterModel } from '../../models/package/HistorySurveyAdapterModel';
import { HistoryDetailSurveyAdapderMoldel } from '../../models/package/HistoryDetailSurveyAdapderMoldel';
import { LocationSurveyAdapterModel } from '../../models/package/LocationSurveyAdapterModel';
import { SurveySaveMoldModel } from '../../models/package/SurveySaveMoldModel';

@Injectable()
export class SurveyProvider extends BaseProvider {
  public surVeyModel: SurveyModel = new SurveyModel();
  public surVeyModel2: SurveyModel = new SurveyModel();

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private _survey: SurveyAdapterProvider
  ) {
    super(alertCtrl,loadingCtrl)
    console.log('Hello SurveyProvider Provider');
  }

  async getLocationSurvey() {
    try {
      let response: any = await this._survey.getLocationSurvey();
      console.log(`getLocationSurvey ::: `, response);
      this.surVeyModel.locationDescription = Utilities.mapJsontoModel(response.ResponseData.MoldLocation, LocationSurveyAdapterModel);
    } catch (err) {
      this.surVeyModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    } 
  }

  async getMoldHistory() {
    try {
      let response: any = await this._survey.getHistorySurvey(this.surVeyModel);
      console.log(`getHistorySurvey ::: `, response);
      this.surVeyModel.surveyMoldHistory = Utilities.mapJsontoModel(response.ResponseData, HistorySurveyAdapterModel);
    } catch (err) {
      this.surVeyModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async getSearchSurvey() {
    try {
      let response: any = await this._survey.getSearchSurvey(this.surVeyModel);
      console.log(`getSearchSurvey ::: `, response);
      this.surVeyModel.surveyMoldHistory = Utilities.mapJsontoModel(response.ResponseData, HistorySurveyAdapterModel);
    } catch (err) {
      this.surVeyModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async getMoldHistoryDetail()
  {
    try {
      let response: any = await this._survey.getHistoryDetail(this.surVeyModel);
      console.log(`getHistoryDetailSurvey ::: `, response);
      this.surVeyModel.surveyMoldHistoryDetail = Utilities.mapJsontoModel(response.ResponseData, HistoryDetailSurveyAdapderMoldel);
    } catch (err) {
      this.surVeyModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

  async saveDataMold()
  {
    try
    {
      let response: any = await this._survey.saveDataMold(this.surVeyModel)
      console.log(`saveDataMold ::: `, response);
      this.surVeyModel.surveyMoldSave = Utilities.mapJsontoModel(response.ResponseData, SurveySaveMoldModel);
    }
    catch (err)
    {
      this.surVeyModel2 = Utilities.mapJsontoModel(err, OperationBaseModel);
      throw err;
    }
  }

}
