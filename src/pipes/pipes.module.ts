import { NgModule } from '@angular/core';
import { DatetimePipe } from './datetime/datetime';
@NgModule({
	declarations: [DatetimePipe],
	imports: [],
	exports: [DatetimePipe]
})
export class PipesModule {}
