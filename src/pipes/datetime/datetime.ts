import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the DatetimePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'datetime',
})
export class DatetimePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(date: string, format: string) {

    let inputDate = new Date( (date.replace(/T,Z/g, ' ').trim()) );

    let ThaiDay = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์']
    // let shortThaiMonth = [
    //     'ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
    //     'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'
    // ];
    // let longThaiMonth = [
    //     'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
    //     'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
    // ];

    let longEngMonth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let shortEngMonth = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];


    let longMonth = [];
    let shortMonth = [];
    let yearNumber = 0;

    longMonth = longEngMonth;
    shortMonth = shortEngMonth;
    yearNumber = 0;

    let dataDate = [
      inputDate.getDay(), inputDate.getDate(), inputDate.getMonth(), inputDate.getFullYear(), inputDate.getHours(), inputDate.getMinutes(), inputDate.getSeconds()
    ];

    let outputDateFull = [
      'วัน ' + ThaiDay[dataDate[0]],
      'ที่ ' + dataDate[1],
      'เดือน ' + longMonth[dataDate[2]],
      'พ.ศ. ' + (dataDate[3] + yearNumber)
    ];

    let outputDateShort = [
        dataDate[1],
        shortMonth[dataDate[2]],
        dataDate[3] + yearNumber
    ];
    let outputDateMedium = [
        dataDate[1],
        longMonth[dataDate[2]],
        dataDate[3] + yearNumber
    ];
    let outputTime = [
        this.checkTwoDigit(dataDate[4]),
        this.checkTwoDigit(dataDate[5]),
        this.checkTwoDigit(dataDate[6])
    ]
    let outputTimeNoSec = [
        this.checkTwoDigit(dataDate[4]),
        this.checkTwoDigit(dataDate[5])
    ]

    let returnDate: string;
    returnDate = outputDateMedium.join(" ");
    if (format == 'full') {
      returnDate = outputDateFull.join(" ");
    }
    if (format == 'medium') {
        returnDate = outputDateMedium.join(" ");
    }
    if (format == 'short') {
        returnDate = outputDateShort.join(" ");
    }
    if (format == "dateAndTime") {
        returnDate = outputDateShort.join(" ") + " - " + outputTime.join(":");
    }
    if (format == "dateAndTimeNoSec") {
        returnDate = outputDateShort.join(" ") + " : " + outputTimeNoSec.join(".");
    }
    if (format == 'dateTimeComma')
    {
        returnDate = outputDateShort.join(" ") + ", " + outputTime.join(":");
    }

    if( format == 'time') {
        returnDate = outputTimeNoSec.join(":")
    }

    if(format == 'dateNow')
    {
        let dteNow = new Date();
        let date =  dteNow.getDate() - inputDate.getDate();
        let month = dteNow.getMonth() - inputDate.getMonth();
        let year = dteNow.getFullYear() - inputDate.getFullYear();

        if((date + month + year) === 0)
        {
            returnDate = 'Today';
        }else if ((date + month + year) == 1)
        {
            returnDate = 'Yesterday';
        }else
        {
            returnDate = outputDateMedium.join(" ");
        }
    }

    return returnDate;
  }

  checkTwoDigit(date){
    let dataFormat = '';
    if(date.toString().length < 2){
        dataFormat = '0'+date.toString() 
    }else{
        dataFormat = date;
    }
    return dataFormat;
  }
}
