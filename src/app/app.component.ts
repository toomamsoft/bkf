import { Component } from '@angular/core';
import { Platform, App, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { ActiveMobilePage } from '../pages/active-mobile/active-mobile';
import { MainPage } from '../pages/main/main';
import { StorageProvider } from '../providers/storage-provider/storage-provider';
import { LoginProvider } from '../providers/login-provider/login-provider';
import { LoginModel } from '../models/LoginModel';
import { CONSTANT } from './../shared/constant';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  loginModel: LoginModel
  rootPage: any = ActiveMobilePage;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public app: App,
    // private ionicApp: IonicApp,
    public toastCtrl: ToastController,
    public _loginProvider: LoginProvider,
    public _storageProvider: StorageProvider
    ) {
      this.deviceAllow();
      platform.ready().then(async () => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        // CONSTANT.APP_LANGUAGE = (await this._storageProvider.getKey(CONSTANT.LANGURAGE)).toString() == null ? 'en' : (await this._storageProvider.getKey(CONSTANT.LANGURAGE)).toString();
        statusBar.styleDefault();
        splashScreen.hide();
        this.registerBackButton();
      });
  }

  async registerBackButton()
  {
    var lastTimeBackPress = 0;
    var timePeriodToExit =  2000;
    this.platform.registerBackButtonAction((fn, priority) => {
      // priority=100;
      console.log('getActiveNavs::::',this.app.getActiveNavs())
      let nav = this.app.getActiveNavs()[0];
      // let activeModal = this.ionicApp._modalPortal.getActive() ||
      //   this.ionicApp._toastPortal.getActive() ||
      //   this.ionicApp._overlayPortal.getActive();
      // console.log('activeModal:::',activeModal)
      // if (activeModal) {
      //   activeModal.dismiss();
      //   return;
      // }

      let activeView = nav.getActive();
      console.log('navControl:::',activeView);
      
      if(new Date().getTime() - lastTimeBackPress < timePeriodToExit)
      {
        this.platform.exitApp();
      }else{
        let toast = this.toastCtrl.create({
          message:'Press back again to exit App',
          duration: 2000,
          position:'bottom'
        });
        toast.present();
        lastTimeBackPress = new Date().getTime();
      }
    });
  }

  // async deviceAllow() {
  //   this.loginModel = this._loginProvider.loginModel;
  //   this._storageProvider.getKey(CONSTANT.ACTIVE_MOBILE).then(async val => {
  //     if(val) {
  //       if(await this._storageProvider.getKey(CONSTANT.USER_TYPE)) {
  //         this.rootPage = MainPage;
  //       }else{
  //         this.rootPage = LoginPage;
  //       }
  //     } else {
  //       await this._loginProvider.deviceAllow().catch( () => { this._loginProvider.submitErrorEvent(this._loginProvider.loginModel2); });
  //       if(this.loginModel.authenticateUser.deviceAllow.length > 0){
  //         this._storageProvider.setKey(CONSTANT.ACTIVE_MOBILE, 'true');
  //         this.rootPage = LoginPage;
  //       }
  //     }
  //   }).catch(() => {
  //     this.rootPage = ActiveMobilePage;
  //   })  
  // }
  async deviceAllow() {
    
    this.loginModel = this._loginProvider.loginModel;
    try {
      await this._loginProvider.deviceAllow();
      if(this.loginModel.authenticateUser.deviceAllow.length > 0) {
        this._storageProvider.getKey(CONSTANT.ACTIVE_MOBILE).then(async val => {
          if(val) {
            if(await this._storageProvider.getKey(CONSTANT.USER_TYPE)) {
              this.rootPage = MainPage;
            }else{
              this.rootPage = LoginPage;
            }
          } else {
            await this._loginProvider.deviceAllow().catch( () => { this._loginProvider.submitErrorEvent(this._loginProvider.loginModel2); });
            if(this.loginModel.authenticateUser.deviceAllow.length > 0){
              this._storageProvider.setKey(CONSTANT.ACTIVE_MOBILE, 'true');
              this.rootPage = LoginPage;
            }
          }
        }).catch(() => {
          this.rootPage = ActiveMobilePage;
        })  
      }
    }catch(e) {
      this._loginProvider.submitErrorEvent(this._loginProvider.loginModel2)
    }
  }
}

