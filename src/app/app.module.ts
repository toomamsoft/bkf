import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { AndroidPermissions } from '@ionic-native/android-permissions';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Geolocation } from '@ionic-native/geolocation';
import { ImagePicker } from '@ionic-native/image-picker';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Network } from '@ionic-native/network';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ActiveMobilePageModule } from '../pages/active-mobile/active-mobile.module';
import { BkfMainPageModule } from '../pages/bkf-main/bkf-main.module';
import { LoginPageModule } from '../pages/login/login.module';
import { MainPageModule } from '../pages/main/main.module';
import { DrawingImagePageModule } from '../pages/drawing-image/drawing-image.module'
import { PipesModule } from '../pipes/pipes.module';
import { ComponentsModule } from '../components/components.module';
import { DirectivesModule } from '../directives/directives.module';

import { TranslateModule,TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { HttpClientModule, HttpClient } from '@angular/common/http'

import { BaseProvider } from '../providers/base-provider/base-provider';
import { Connection } from '../BKFConnection/connection/Connection';
import { ConnectionUtilProvider } from '../providers/connection-util/connection-util';

import { SurveyAdapterProvider } from '../providers/adapter/survey-adapter/survey-adapter';
import { SurveyProvider } from '../providers/survey-provider/survey-provider';
import { NotificationProvider } from '../providers/notification-provider/notification-provider';
import { AuthenticationAdapterProvider } from '../providers/adapter/authentication-adapter/authentication-adapter';
import { LoginProvider } from '../providers/login-provider/login-provider';
import { ScanProvider } from '../providers/scan/scan';
import { MoldInoutAdapterProvider } from '../providers/adapter/moldinout-adapter/moldinout-adapter';
import { MoldinoutProvider } from '../providers/moldinout-provider/moldinout-provider';
import { SecurityProvider } from '../providers/security-provider/security-provider';
import { StorageProvider } from '../providers/storage-provider/storage-provider';
import { MoldmakerAdapterProvider } from '../providers/adapter/moldmaker-adapter/moldmaker-adapter';
import { MoldmakerProvider } from '../providers/moldmaker-provider/moldmaker-provider';
import { MessageAdapterProvider } from '../providers/adapter/message-adapter/message-adapter';
import { MessageProvider } from '../providers/message-provider/message-provider';
import { AdminAdapterProvider } from '../providers/adapter/admin-adapter/admin-adapter';
import { AdminProvider } from '../providers/admin-provider/admin-provider';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      backButtonIcon: 'ios-arrow-back',
      menuType: 'push',
      swipeBackEnabled: false,
      iconMode: 'ios'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ActiveMobilePageModule,
    BkfMainPageModule,
    DrawingImagePageModule,
    LoginPageModule,
    MainPageModule,
    ComponentsModule,
    DirectivesModule,
    PipesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    AndroidPermissions,
    BarcodeScanner,
    Camera,
    File,
    Geolocation,
    ImagePicker,
    LocalNotifications,
    LocationAccuracy,
    Network,
    PhotoViewer,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BaseProvider,
    Connection,
    ConnectionUtilProvider,
    SurveyAdapterProvider,
    SurveyProvider,
    NotificationProvider,
    AuthenticationAdapterProvider,
    LoginProvider,
    ScanProvider,
    MoldInoutAdapterProvider,
    MoldinoutProvider,
    { provide: HttpClientModule, useClass: SecurityProvider, useValue: false },
    StorageProvider,
    MoldmakerAdapterProvider,
    MoldmakerProvider,
    MessageAdapterProvider,
    MessageProvider,
    AdminAdapterProvider,
    AdminProvider,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
