import { MockConnection } from './MockConnection';
import { NodeConnection } from './NodeConnection';
import { HttpClient } from '@angular/common/http';

export class Connection {

    private connectionMobile: ConnectionMobile;

    public getConnection(connection: string, http: HttpClient): ConnectionMobile {

        if (connection == null) {
            return null;
        }
        
        if (!this.connectionMobile) {
            if ("MOCK" === connection) {
                this.connectionMobile = new MockConnection();
            } else if ("NODE" === connection) {
                this.connectionMobile = new NodeConnection(http);
            }
        }
        return this.connectionMobile;
    }

}