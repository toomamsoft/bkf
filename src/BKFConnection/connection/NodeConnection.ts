import { LoginModel } from './../../models/LoginModel';
import { NodeConnectionUtils } from './ConnectionUtil';
// import { HttpParams } from '@angular/common/http';
import { CONSTANT } from '../../shared/constant';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';


export class NodeConnection extends NodeConnectionUtils implements ConnectionMobile {

    getResourceRequest(endpoint: string, parameters: any, userModel: LoginModel): Promise<{}> {
        // console.log('Node Connection!!', userModel);
        // console.log(`Node Endpoint::: `, endpoint, parameters);

        let key, keyAdapterParam;

        for (key in parameters[0].parameter) {
            if (key.match(/Param/)) {
                keyAdapterParam = key;
            }
        }
        // prepare endpoint API match to tomcat mock server
        let NodeEndpoint;//, funcNames = endpoint.split("/");

        NodeEndpoint = endpoint;
        // console.log(`NodeEndpoint ::: `, CONSTANT.IPAddress + NodeEndpoint);
        let param = parameters[0].parameter[keyAdapterParam];
        // prepare query string
        // const nodeParameters = new HttpParams(param);
        // console.log(`NodeParameters ::: `, nodeParameters.toString());

        return new Promise(async (resolve, reject) => {
            try {
                this.http.post(CONSTANT.IPAddress + NodeEndpoint, param).timeout(CONSTANT.TIMEOUT).subscribe(
                    (response) => {
                        resolve(response);
                    },
                    (err) => {
                        resolve(err);
                    });
            } catch (err) {
                reject(err);
            }
        })
    }
}
