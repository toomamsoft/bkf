import { MOCK } from '../../mock/mockdata'

export class MockConnection implements ConnectionMobile {

    getResourceRequest(endpoint: string, parameters: any): Promise<{}> {
        console.log('%c ::[Mock Connection!!]:: ', 'background: #222; color: #fff; font-size:20px;');
        console.log(`Mock Endpoint::: `, endpoint, parameters);

        let key, keyAdapterParam;

        for (key in parameters[0].parameter) {
            if (key.match(/Param/)) {
                keyAdapterParam = key;
            }
        }
        // prepare endpoint API match to tomcat mock server
        let NodeEndpoint;//, funcNames = endpoint.split("/");

        NodeEndpoint = endpoint;
        console.log(`NodeEndpoint ::: `, NodeEndpoint);
        let param = parameters[0].parameter[keyAdapterParam];
        // prepare query string
        console.log(`MockParameters ::: `, param);


        return new Promise((resolve, reject) => {
            if (!endpoint.includes('_')) {
                if (MOCK[endpoint]) {
                    resolve(MOCK[endpoint]);
                } else {
                    reject(MOCK["mockError"]);
                }
            }
            else {
                reject(MOCK["mockError"]);
            }
        })
    }
}