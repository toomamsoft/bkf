import { SubAdminGetListUserBKF } from './package/SubAdminGetListUserBKF';
import { AdminAdapterModel } from './package/AdminAdapterModel';
import { JsonObject } from "json2typescript";
import { OperationBaseModel } from "./package/OperationBaseModel";


@JsonObject("AdminModel")
export class AdminModel extends OperationBaseModel {
    getListBKF: AdminAdapterModel = new AdminAdapterModel();
    getListMold: AdminAdapterModel = new AdminAdapterModel();
    getConfig: AdminAdapterModel = new AdminAdapterModel();
    statement: AdminAdapterModel = new AdminAdapterModel();

    imgSurvey: number;
    imgInOut: number;
    imgMaker: number;
    isoNumber: string;


    username: string;
    password: string;
    empid: string;
    firstname: string;
    lastname: string;
    position: string;
    usergroup: string;
    usertype: string;
    flag: string;
    
    bkfSelected: SubAdminGetListUserBKF;
}
