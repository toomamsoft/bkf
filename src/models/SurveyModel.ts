import { SurveySaveMoldModel } from './package/SurveySaveMoldModel';
import { JsonObject } from "json2typescript";
import { OperationBaseModel } from "./package/OperationBaseModel";
import { LocationSurveyAdapterModel } from "./package/LocationSurveyAdapterModel";
import { HistorySurveyAdapterModel } from "./package/HistorySurveyAdapterModel";
import { HistoryDetailSurveyAdapderMoldel } from "./package/HistoryDetailSurveyAdapderMoldel";
// import { SearchSurveyAdapterModel } from "./package/SearchSurveyAdapterModel";

@JsonObject("SurveyModel")
export class SurveyModel extends OperationBaseModel {
     // from api
     locationDescription: LocationSurveyAdapterModel = new LocationSurveyAdapterModel();
     surveyMoldHistory: HistorySurveyAdapterModel = new HistorySurveyAdapterModel();
     surveyMoldHistoryDetail: HistoryDetailSurveyAdapderMoldel = new HistoryDetailSurveyAdapderMoldel();
     surveyMoldSave: SurveySaveMoldModel = new SurveySaveMoldModel();

     // surveyMoldHistoryDetail: 

     // from html
     MoldId: string;
     AssetId:string;
     CustNo: string;
     PartNo: string;
     PartName: string;
     Plate: string;
     Location: string;
     Remark: string;
     IMEI: string;
     RunNo: string;
     From: string;
     To: string;
     Images: object[];
}