import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';


@JsonObject("SubMoldMakerHistoryDetailModel")
export class SubMoldMakerHistoryDetailModel {
    @JsonProperty(CONSTANT.SubMoldMakerHistoryDetail.JOBID, String, true)
    jobId: string = undefined;
    
    @JsonProperty(CONSTANT.SubMoldMakerHistoryDetail.REMARK, String, true)
    remark: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerHistoryDetail.STATUS, String, true)
    status: string = undefined;

}