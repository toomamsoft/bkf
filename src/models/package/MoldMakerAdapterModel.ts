import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';
import { SubGetListMoldModel } from './SubGetListMoldModel';
import { SubMoldMakerFormBKFModel } from './SubMoldMakerFormBKFModel';
import { SubMoldMakerHistoryModel } from './SubMoldMakerHistoryModel';
import { SubMoldBKFVIEWHistoryModel } from './SubMoldBKFVIEWHistoryModel';
import { SubMoldMakerHistoryDetailModel } from './SubMoldMakerHistoryDetailModel';
import { SubPhotoModel } from './SubPhotoModel';


@JsonObject("MoldMakerAdapterModel")
export class MoldMakerAdapterModel {
    
    @JsonProperty(CONSTANT.SurveySaveMoldModel.STATUS, Number, true)
    Status: string = undefined;

    @JsonProperty(CONSTANT.SurveySaveMoldModel.MESSAGE, String, true)
    Message: string = undefined;

    @JsonProperty(CONSTANT.MoldMakerAdapterModel.LIST_MOLD, [SubGetListMoldModel], true)
    getListMold: SubGetListMoldModel[] = undefined;

    @JsonProperty(CONSTANT.MoldMakerAdapterModel.HISTORY, [SubMoldMakerHistoryModel], true)
    history: SubMoldMakerHistoryModel[] = undefined;

    @JsonProperty(CONSTANT.MoldMakerAdapterModel.HISTORY_DETAIL, [SubMoldMakerHistoryDetailModel], true)
    historyDetail: SubMoldMakerHistoryDetailModel[] = undefined;

    @JsonProperty(CONSTANT.MoldMakerAdapterModel.HISTORY_IMAGE, [SubPhotoModel], true)
    historyImage: SubPhotoModel[] = undefined;

    @JsonProperty(CONSTANT.MoldMakerAdapterModel.HISTORY_BKF, [SubMoldMakerFormBKFModel], true)
    bkfview: SubMoldMakerFormBKFModel[] = undefined;

    @JsonProperty(CONSTANT.MoldMakerAdapterModel.VIEW_BY_BKF, [SubMoldBKFVIEWHistoryModel], true)
    bkfviewHistory: SubMoldBKFVIEWHistoryModel[] = undefined;
}