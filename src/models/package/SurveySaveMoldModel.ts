import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';


@JsonObject("SurveySaveMoldModel")
export class SurveySaveMoldModel {
    @JsonProperty(CONSTANT.SurveySaveMoldModel.STATUS, Number, true)
    Status: string = undefined;

    @JsonProperty(CONSTANT.SurveySaveMoldModel.MESSAGE, String, true)
    Message: string = undefined;
}