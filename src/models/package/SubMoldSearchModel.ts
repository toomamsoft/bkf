import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("SubMoldSearchModel")
export class SubMoldSearchModel {
    @JsonProperty(CONSTANT.SurveySubSearchModel.MOLDID, String, true)
    MoldId: string = undefined;

    @JsonProperty(CONSTANT.SurveySubSearchModel.ASSETID, String, true)
    AssetId: string = undefined;

    @JsonProperty(CONSTANT.SurveySubSearchModel.PLATE, String, true)
    Plate: string = undefined;

    @JsonProperty(CONSTANT.SurveySubSearchModel.FULLNAME, String, true)
    FullName: string = undefined;

    @JsonProperty(CONSTANT.SurveySubSearchModel.CREATEDATE, String, true)
    CreateDate: string = undefined;

    @JsonProperty(CONSTANT.SurveySubSearchModel.RUNNO, String, true)
    RunNo: string = undefined;

}