import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';


@JsonObject("SubMoldInOutStatusModel")
export class SubMoldInOutStatusModel {
    @JsonProperty(CONSTANT.SubMoldInOutStatus.JOBID, String, true)
    jobId: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutStatus.MOLDID, String, true)
    moldId: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutStatus.PLANIN, String, true)
    planIn: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutStatus.PLANOUT, String, true)
    planOut: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutStatus.FULLNAME, String, true)
    name: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutStatus.MAKERNAME, String, true)
    makerName: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutStatus.REASON, String, true)
    reson: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutStatus.STATUS, String, true)
    status: string = undefined;
}