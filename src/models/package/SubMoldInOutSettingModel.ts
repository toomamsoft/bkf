import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';


@JsonObject("SubMoldInOutSetting")
export class SubMoldInOutSetting {
    @JsonProperty(CONSTANT.SubMoldInoutConfig.ID, String, true)
    id: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInoutConfig.DESCIRPTION, String, true)
    description: string = undefined;

}