
import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("SubMessageModel")
export class SubMessageModel {

    @JsonProperty(CONSTANT.SubMessageModel.MESSAGEID, String, true)
    messageId: string = undefined;

    @JsonProperty(CONSTANT.SubMessageModel.SUBJECT, String, true)
    messageSubject: string = undefined;

    @JsonProperty(CONSTANT.SubMessageModel.DESCRIPTION, String, true)
    messageDescription: string = undefined;

    @JsonProperty(CONSTANT.SubMessageModel.FLAG, String, true)
    readStatus: string = undefined;

    @JsonProperty(CONSTANT.SubMessageModel.MESSAGE_DATE, String, true)
    messageDate: string = undefined;

    @JsonProperty(CONSTANT.SubMessageModel.JOBID, String, true)
    jobId: string = undefined;

    @JsonProperty(CONSTANT.SubMessageModel.MOLDID, String, true)
    moldId: string = undefined;

    @JsonProperty(CONSTANT.SubMessageModel.STATUS, String, true)
    status: string = undefined;
}