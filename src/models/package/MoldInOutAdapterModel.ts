import { SubMoldInOutApproveModel } from './SubMoldInOutApproveModel';
import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';
import { SubNewMoldInOut } from './SubNewMoldInOut';
import { SubMoldInOutSetting } from './SubMoldInOutSettingModel'
import { SubMoldInOutImageMoldel } from './SubMoldInOutImageMoldel';
import { SubMoldInOutSearchModel } from './SubMoldInOutSearchModel';
import { SubMoldInOutStatusModel } from './SubMoldInOutStatusModel';
import { SubMoldInOutWatingModel } from './SubMoldInOutWatingModel';

@JsonObject("MoldInOutAdapterModel")
export class MoldInOutAdapterModel {

    @JsonProperty(CONSTANT.SurveySaveMoldModel.STATUS, Number, true)
    Status: string = undefined;

    @JsonProperty(CONSTANT.SurveySaveMoldModel.MESSAGE, String, true)
    Message: string = undefined;

    @JsonProperty(CONSTANT.SurveySaveMoldModel.PDF, String, true)
    pdf: string = undefined;
    
    @JsonProperty(CONSTANT.MoldInoutAdapterModel.SETTING_MOLDMAKER, [SubMoldInOutSetting], true)
    makerId: SubMoldInOutSetting[] = undefined;

    @JsonProperty(CONSTANT.MoldInoutAdapterModel.MOLDINOUT_SEARCH, [SubMoldInOutSearchModel], true)
    moldSearch: SubMoldInOutSearchModel[] = undefined;

    @JsonProperty(CONSTANT.MoldInoutAdapterModel.MOLDINOUT_STATUS, [SubMoldInOutStatusModel], true)
    moldStatus: SubMoldInOutStatusModel[] = undefined;

    @JsonProperty(CONSTANT.MoldInoutAdapterModel.SETTING_REASON, [SubMoldInOutSetting], true)
    reason: SubMoldInOutSetting[] = undefined;

    @JsonProperty(CONSTANT.MoldInoutAdapterModel.NEWMOLDINOUT, [SubNewMoldInOut], true)
    inoutMold: SubNewMoldInOut[] = undefined;

    @JsonProperty(CONSTANT.MoldInoutAdapterModel.MOLDINOUT_IMAGEOUT, [SubMoldInOutImageMoldel], true)
    imagesOut: SubMoldInOutImageMoldel[] = undefined;

    @JsonProperty(CONSTANT.MoldInoutAdapterModel.MOLDINOUT_IMAGEIN, [SubMoldInOutImageMoldel], true)
    imagesIn: SubMoldInOutImageMoldel[] = undefined;

    @JsonProperty(CONSTANT.MoldInoutAdapterModel.MOLDINOUT_IMAGETRANSPORT, [SubMoldInOutImageMoldel], true)
    imagesTransport: SubMoldInOutImageMoldel[] = undefined;

    @JsonProperty(CONSTANT.MoldInoutAdapterModel.MOLDINOUT_APPROVE_IN, [SubMoldInOutApproveModel], true)
    approveIn: SubMoldInOutApproveModel[] = undefined;

    @JsonProperty(CONSTANT.MoldInoutAdapterModel.MOLDINOUT_APPROVE_OUT, [SubMoldInOutApproveModel], true)
    approveOut: SubMoldInOutApproveModel[] = undefined;

    @JsonProperty(CONSTANT.MoldInoutAdapterModel.MOLDINOUT_WAITNG, [SubMoldInOutWatingModel], true)
    getWaiting: SubMoldInOutWatingModel[] = undefined;
}