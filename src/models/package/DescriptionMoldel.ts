import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("DescriptionMoldel")
export class DescriptionMoldel {

    @JsonProperty(CONSTANT.SurveySubHistoryDatailModel.SUBDESCRIPTION.ASSETID, String, true)
    AssetId: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryDatailModel.SUBDESCRIPTION.CUSTNO, String, true)
    CustNo: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryDatailModel.SUBDESCRIPTION.PARTNO, String, true)
    PartNo: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryDatailModel.SUBDESCRIPTION.PARTNAME, String, true)
    PartName: string = undefined;
}