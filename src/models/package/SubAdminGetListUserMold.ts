
import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("SubAdminGetListUserMold")
export class SubAdminGetListUserMold {

    @JsonProperty(CONSTANT.SubAdminGetListMold.EMPID, String, true)
    empId: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetListMold.FIRSTNAME, String, true)
    fullName: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetListMold.USERTYPE, String, true)
    userType: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetListMold.USERNAME, String, true)
    username: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetListMold.PASSWORD, String, true)
    password: string = undefined;

}
