import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';


@JsonObject("SubMoldInOutSearchModel")
export class SubMoldInOutSearchModel {
    @JsonProperty(CONSTANT.SubMoldInOutSearch.JOBID, String, true)
    jobId: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutSearch.MOLDID, String, true)
    moldId: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutSearch.CUSTNO, String, true)
    custNo: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutSearch.PARTNO, String, true)
    partNo: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutSearch.PARTNAME, String, true)
    partName: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutSearch.MAKERNAME, String, true)
    makerName: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutSearch.STATUS, String, true)
    status: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutSearch.CONFIRMOUT_DATE, String, true)
    confirmDate: string = undefined;

}
