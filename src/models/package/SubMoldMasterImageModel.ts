import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("SubMoldMasterImageModel")
export class SubMoldMasterImageModel {
    @JsonProperty(CONSTANT.SurveySubHistoryDetailImageModel.PHOTO, String, true)
    Photo: string = undefined;
}