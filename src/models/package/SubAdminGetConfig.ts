import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("SubAdminGetConfig")
export class SubAdminGetConfig {

    @JsonProperty(CONSTANT.SubAdminGetConfig.SURVEY, String, true)
    survey: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetConfig.MOLDINOUT, String, true)
    inout: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetConfig.MOLDMAKER, String, true)
    mold: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetConfig.ISONUMBER, String, true)
    iso: string = undefined;

}
