import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("SubMoldMasterModel")
export class SubMoldMasterModel {
    @JsonProperty(CONSTANT.SurveySubMasterModel.MOLDID, String, true)
    MoldId: string = undefined;

    @JsonProperty(CONSTANT.SurveySubMasterModel.ASSETID, String, true)
    AssetId: string = undefined;

    @JsonProperty(CONSTANT.SurveySubMasterModel.CUSTNO, String, true)
    CustNo: string = undefined;

    @JsonProperty(CONSTANT.SurveySubMasterModel.PARTNO, String, true)
    PartNo: string = undefined;

    @JsonProperty(CONSTANT.SurveySubMasterModel.PARTNAME, String, true)
    PartName: string = undefined;

}