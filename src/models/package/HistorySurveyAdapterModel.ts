import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';
import { SubMoldHistoryModel } from './SubMoldHistoryModel'
import { SubMoldMasterModel } from './SubMoldMasterModel';
import { SubMoldSearchModel } from './SubMoldSearchModel';

@JsonObject("HistorySurveyAdapterModel")
export class HistorySurveyAdapterModel {
    @JsonProperty(CONSTANT.HistorySurveyModel.MOLDMASTER, [SubMoldMasterModel], true)
    master: SubMoldMasterModel[] = undefined;

    @JsonProperty(CONSTANT.HistorySurveyModel.MOLDHISTORY, [SubMoldHistoryModel], true)
    history: SubMoldHistoryModel[] = undefined;

    @JsonProperty(CONSTANT.HistorySurveyModel.MOLDSEARCH, [SubMoldSearchModel], true)
    search: SubMoldSearchModel[] = undefined;

}