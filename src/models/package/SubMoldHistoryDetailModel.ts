import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';
import { DescriptionMoldel } from './DescriptionMoldel';

@JsonObject("SubMoldHistoryDetailModel")
export class SubMoldHistoryDetailModel {
    @JsonProperty(CONSTANT.SurveySubHistoryDatailModel.MOLDID, String, true)
    MoldId: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryDatailModel.LOCATION, String, true)
    location: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryDatailModel.PLATE, String, true)
    Plate: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryDatailModel.REMARK, String, true)
    Remark: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryDatailModel.FULLNAME, String, true)
    FullName: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryDatailModel.CREATEDATE, String, true)
    CreateDate: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryDatailModel.RUNNO, String, true)
    RunNo: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryDatailModel.DESCRIPTION, [DescriptionMoldel], true)
    Description: DescriptionMoldel[] = undefined;
}