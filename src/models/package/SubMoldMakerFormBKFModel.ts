import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';
import { SubPhotoModel } from './SubPhotoModel';


@JsonObject("SubMoldMakerFormBKFModel")
export class SubMoldMakerFormBKFModel {
    
    @JsonProperty(CONSTANT.SubMoldMakerBKFVIEW.JOBID, String, true)
    jobId: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerBKFVIEW.MOLDID, String, true)
    moldId: string = undefined;
    
    @JsonProperty(CONSTANT.SubMoldMakerBKFVIEW.CONFIRMDATE, String, true)
    confirmDate: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerBKFVIEW.PLANINT, String, true)
    planIn: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerBKFVIEW.PLANOUT, String, true)
    planOut: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerBKFVIEW.REMARK, String, true)
    remark: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerBKFVIEW.REASON, String, true)
    reason: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerBKFVIEW.PHOTO, [SubPhotoModel], true)
    photo: SubPhotoModel[] = undefined;
}