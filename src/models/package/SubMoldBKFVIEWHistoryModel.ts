import { SubPhotoModel } from './SubPhotoModel';
import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';


@JsonObject("SubMoldBKFVIEWHistoryModel")
export class SubMoldBKFVIEWHistoryModel {

    @JsonProperty(CONSTANT.SubMoldBKFVIEWHistoryModel.JOBID, String, true)
    jobId: string = undefined;

    @JsonProperty(CONSTANT.SubMoldBKFVIEWHistoryModel.MOLDID, String, true)
    moldId: string = undefined;

    @JsonProperty(CONSTANT.SubMoldBKFVIEWHistoryModel.CREATEDATE, String, true)
    createDate: string = undefined;

    @JsonProperty(CONSTANT.SubMoldBKFVIEWHistoryModel.REMARK, String, true)
    remark: string = undefined;

    @JsonProperty(CONSTANT.SubMoldBKFVIEWHistoryModel.STAUTS, String, true)
    status: string = undefined;
    
    @JsonProperty(CONSTANT.SubMoldBKFVIEWHistoryModel.PHOTO, [SubPhotoModel], true)
    photo: SubPhotoModel[] = undefined;
}