import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("SubAdminGetListUserBKF")
export class SubAdminGetListUserBKF {
    @JsonProperty(CONSTANT.SubAdminGetListBKF.EMPID, String, true)
    empId: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetListBKF.FIRSTNAME, String, true)
    firstName: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetListBKF.LASTNAME, String, true)
    lastName: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetListBKF.USERGROUP, String, true)
    userGroup: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetListBKF.USERTYPE, String, true)
    userType: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetListBKF.USERNAME, String, true)
    username: string = undefined;

    @JsonProperty(CONSTANT.SubAdminGetListBKF.PASSWORD, String, true)
    password: string = undefined;
}
