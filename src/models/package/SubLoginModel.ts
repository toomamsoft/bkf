import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("SubLoginModel")
export class SubLoginModel {
    @JsonProperty(CONSTANT.AuthenticateUserModel.FULL_NAME, String, true)
    fullName: string = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.USER_ID, String, true)
    userId: string = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.IMAGES_SURVEY, String, true)
    imageSurvey: string = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.IMAGES_MOLDINOUT, String, true)
    imageInOut: string = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.USER_TYPE, String, true)
    userType: string = undefined;

}