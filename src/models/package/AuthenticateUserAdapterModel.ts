import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';
import { SubLoginModel } from './SubLoginModel';
import { SubDeviceAllowMoldel } from './SubDeviceAllowMoldel';

@JsonObject("AuthenticateUserAdapterModel")
export class AuthenticateUserAdapterModel {

    @JsonProperty(CONSTANT.AuthenticateUserModel.USER_ID, String, true)
    UserId: string = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.USER_TYPE, String, true)
    UserType: string = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.FULL_NAME, String, true)
    FullName: string = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.IMEI, String, true)
    IMEI: string = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.IMAGES_SURVEY, String, true)
    ImageSurvey: string = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.IMAGES_MOLDINOUT, String, true)
    ImageMoldInOut: string = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.USER_LOGIN, [SubLoginModel], true)
    userLogin: SubLoginModel[] = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.DEVICE_ALLOW, [SubDeviceAllowMoldel], true)
    deviceAllow: SubDeviceAllowMoldel[] = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.STATUS, String, true)
    status: string = undefined;
}