import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';


@JsonObject("SubMoldHistoryModel")
export class SubMoldHistoryModel {
    @JsonProperty(CONSTANT.SurveySubHistoryModel.MOLDID, String, true)
    MoldId: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryModel.ASSETID, String, true)
    AssetId: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryModel.CUSTNO, String, true)
    CustNo: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryModel.PARTNO, String, true)
    PartNo: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryModel.PARTNAME, String, true)
    PartName: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryModel.LOCATION, String, true)
    Location: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryModel.PLATE, Number, true)
    Plate: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryModel.REMARK, String, true)
    Remark: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryModel.FULLNAME, String, true)
    FullName: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryModel.CREATEDATE, String, true)
    CreateDate: string = undefined;

    @JsonProperty(CONSTANT.SurveySubHistoryModel.RUNNO, String, true)
    RunNo: string = undefined;
}