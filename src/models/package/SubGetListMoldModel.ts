import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("SubGetListMoldModel")
export class SubGetListMoldModel {
    @JsonProperty(CONSTANT.SubGetListMoldModel.ACTUAL, String, true)
    actual: string = undefined;

    @JsonProperty(CONSTANT.SubGetListMoldModel.CUSTNO, String, true)
    custno: string = undefined;

    @JsonProperty(CONSTANT.SubGetListMoldModel.JOBID, String, true)
    jobid: string = undefined;

    @JsonProperty(CONSTANT.SubGetListMoldModel.MAKERMOLDID, String, true)
    makerid: string = undefined;

    @JsonProperty(CONSTANT.SubGetListMoldModel.MOLDID, String, true)
    moldid: string = undefined;

    @JsonProperty(CONSTANT.SubGetListMoldModel.PARTNAME, String, true)
    partname: string = undefined;

    @JsonProperty(CONSTANT.SubGetListMoldModel.PARTNO, String, true)
    partno: string = undefined;

    @JsonProperty(CONSTANT.SubGetListMoldModel.PLANIN, String, true)
    planin: string = undefined;

    @JsonProperty(CONSTANT.SubGetListMoldModel.PLANOUT, String, true)
    planout: string = undefined;

    @JsonProperty(CONSTANT.SubGetListMoldModel.REMARK, String, true)
    remark: string = undefined;

    @JsonProperty(CONSTANT.SubGetListMoldModel.STATUS, String, true)
    status: string = undefined;
}