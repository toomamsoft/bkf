import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("SubDeviceAllowMoldel")
export class SubDeviceAllowMoldel {
    @JsonProperty(CONSTANT.AuthenticateUserModel.IMEI, String, true)
    IMEI: string = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.STATUS, String, true)
    status: string = undefined;


}