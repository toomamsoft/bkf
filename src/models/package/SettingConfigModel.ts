import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("SettingConfigModel")
export class SettingConfigModel {

    @JsonProperty(CONSTANT.AuthenticateUserModel.IMAGES_SURVEY, String, true)
    imageSurvey: string = undefined;

    @JsonProperty(CONSTANT.AuthenticateUserModel.IMAGES_MOLDINOUT, String, true)
    imageInOut: string = undefined;
}