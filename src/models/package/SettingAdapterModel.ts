import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';
import { SettingConfigModel } from './SettingConfigModel';

@JsonObject("SettingAdapterModel")
export class SettingAdapterModel {

    @JsonProperty(CONSTANT.SettingModel.CONFIG, [SettingConfigModel], true)
    config: SettingConfigModel[] = undefined;

}