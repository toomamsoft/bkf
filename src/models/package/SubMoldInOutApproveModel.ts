import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("SubMoldInOutApproveModel")
export class SubMoldInOutApproveModel {
    @JsonProperty(CONSTANT.SubMoldInOutApprove.JOBID, String, true)
    jobId: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutApprove.EMPID, String, true)
    empId: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutApprove.TYPE, String, true)
    approveType: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutApprove.STATUS, String, true)
    status: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutApprove.DATE, String, true)
    date: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutApprove.REMARK, String, true)
    remark: string = undefined;
    
    @JsonProperty(CONSTANT.SubMoldInOutApprove.NAME, String, true)
    fullName: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInOutApprove.USER_TYPE, String, true)
    userType: string = undefined;
}