import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';
import { SubAdminGetListUserBKF } from './SubAdminGetListUserBKF';
import { SubAdminGetListUserMold } from './SubAdminGetListUserMold';
import { SubAdminGetConfig } from './SubAdminGetConfig';

@JsonObject("AdminAdapterModel")
export class AdminAdapterModel {
    @JsonProperty(CONSTANT.SurveySaveMoldModel.STATUS, Number, true)
    Status: string = undefined;

    @JsonProperty(CONSTANT.SurveySaveMoldModel.MESSAGE, String, true)
    Message: string = undefined;

    @JsonProperty(CONSTANT.AdminAdapterModel.GET_USER_BKF, [SubAdminGetListUserBKF], true)
    getListBKF: SubAdminGetListUserBKF[] = undefined;

    @JsonProperty(CONSTANT.AdminAdapterModel.GET_USER_MOLD, [SubAdminGetListUserMold], true)
    getListMold: SubAdminGetListUserMold[] = undefined;

    @JsonProperty(CONSTANT.AdminAdapterModel.GET_CONFIG, [SubAdminGetConfig], true)
    getConfig: SubAdminGetConfig[] = undefined;
}