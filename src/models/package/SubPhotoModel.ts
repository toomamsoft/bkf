import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';


@JsonObject("SubPhotoModel")
export class SubPhotoModel {
    @JsonProperty(CONSTANT.SubMoldMakerBKFVIEW.SubPhoto.PHOTONAME, String, true)
    photo: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerBKFVIEW.SubPhoto.LATITUDE, String, true)
    latitude: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerBKFVIEW.SubPhoto.LONGITUDE, String, true)
    longitude: string = undefined;

}