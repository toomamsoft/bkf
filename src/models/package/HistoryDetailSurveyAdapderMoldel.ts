import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';
import { SubMoldMasterImageModel } from './SubMoldMasterImageModel'
import { SubMoldHistoryDetailModel } from './SubMoldHistoryDetailModel';

@JsonObject("HistoryDetailSurveyAdapderMoldel")
export class HistoryDetailSurveyAdapderMoldel {

    @JsonProperty(CONSTANT.HistorySurveyDetailModel.HISTORY, [SubMoldHistoryDetailModel], true)
    historyDetail: SubMoldHistoryDetailModel[] = undefined;

    @JsonProperty(CONSTANT.HistorySurveyDetailModel.IMAGES, [SubMoldMasterImageModel], true)
    images: SubMoldMasterImageModel[] = undefined;

}