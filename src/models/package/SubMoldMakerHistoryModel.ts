import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';


@JsonObject("SubMoldMakerHistoryModel")
export class SubMoldMakerHistoryModel {
    @JsonProperty(CONSTANT.SubMoldMakerHistory.JOBID, String, true)
    jobId: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerHistory.MOLDID, String, true)
    moldid: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerHistory.ID, String, true)
    id: string = undefined;
    
    @JsonProperty(CONSTANT.SubMoldMakerHistory.CREATEDATE, String, true)
    createdate: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerHistory.REMARK, String, true)
    remark: string = undefined;

    @JsonProperty(CONSTANT.SubMoldMakerHistory.STATUS, String, true)
    status: string = undefined;
}