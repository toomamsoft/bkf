import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';
import { SubMoldSearchModel } from './SubMoldSearchModel';

@JsonObject("SearchSurveyAdapterModel")
export class SearchSurveyAdapterModel
{
    @JsonProperty(CONSTANT.HistorySurveyModel.MOLDSEARCH, [SubMoldSearchModel], true)
    search: [SubMoldSearchModel] = undefined
}