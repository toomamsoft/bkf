import { JsonObject, JsonProperty, Any } from "json2typescript";

@JsonObject("OperationBaseModel")
export class OperationBaseModel {

    @JsonProperty("status", Any, true)
    errorStatus: any = undefined;

    @JsonProperty("number", Any, true)
    errornNumber: any = undefined;

    @JsonProperty("code", Any, true)
    errorCode: any = undefined;

    @JsonProperty("name", Any, true)
    errorName: any = undefined;

    @JsonProperty("message", Any, true)
    errorMsg: any = undefined;
}