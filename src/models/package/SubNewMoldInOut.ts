import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';
import { DescriptionMoldel } from './DescriptionMoldel';


@JsonObject("SubNewMoldInOut")
export class SubNewMoldInOut {

    @JsonProperty(CONSTANT.SubNewMoldInOut.DESCRIPTION, [DescriptionMoldel], true)
    description: DescriptionMoldel[] = undefined;
    
    @JsonProperty(CONSTANT.SubNewMoldInOut.MOLDID, String, true)
    moldId: string = undefined;
    
    @JsonProperty(CONSTANT.SubNewMoldInOut.JOBID, String, true)
    jobId: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.MAKERMOLD, String, true)
    makerMold: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.MAKERID, String, true)
    makerId: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.PLANOUT, String, true)
    planOut: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.PLANIN, String, true)
    planIn: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.REASON, String, true)
    reason: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.NAMECREATE, String, true)
    nameCreate: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.REMARKNEW, String, true)
    remarkNew: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.CONFIRMDATE, String, true)
    confirmDate: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.NAMECONFIRMOUT, String, true)
    nameConfirmout: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.REMARKOUT, String, true)
    remarkOut: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.STATUS, String, true)
    status: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.PRINT, String, true)
    print: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.PDF, String, true)
    pdf: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.FLAG, String, true)
    flag: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.MAKERSTATUS, String, true)
    makerStatus: string = undefined;

    @JsonProperty(CONSTANT.SubNewMoldInOut.REMARKIN, String, true)
    remarkIn: string = undefined;

}