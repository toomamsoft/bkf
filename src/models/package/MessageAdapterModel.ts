import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';
import { SubMessageModel } from './SubMessageModel';

@JsonObject("MessageAdapterModel")
export class MessageAdapterModel {

    @JsonProperty(CONSTANT.SurveySaveMoldModel.STATUS, Number, true)
    Status: string = undefined;

    @JsonProperty(CONSTANT.SurveySaveMoldModel.MESSAGE, String, true)
    Message: string = undefined;

    @JsonProperty(CONSTANT.MessageAdapterModel.MESSAGE, [SubMessageModel], true)
    detail: SubMessageModel[] = undefined;
    
}