import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';


@JsonObject("SubMoldInOutWatingModel")
export class SubMoldInOutWatingModel {
    
    @JsonProperty(CONSTANT.SubMoldInoutWatingModel.JOBID, String, true)
    jobid: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInoutWatingModel.MOLDID, String, true)
    moldid: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInoutWatingModel.CUSNO, String, true)
    custno: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInoutWatingModel.PARTNO, String, true)
    partno: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInoutWatingModel.PARTNAME, String, true)
    partname: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInoutWatingModel.CREATEDATE, String, true)
    createdate: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInoutWatingModel.STATUS, String, true)
    status: string = undefined;
   
}