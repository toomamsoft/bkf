import {JsonObject, JsonProperty} from 'json2typescript';
import { CONSTANT } from '../../shared/constant';


@JsonObject("SubMoldInOutImageMoldel")
export class SubMoldInOutImageMoldel {
    @JsonProperty(CONSTANT.SubMoldInoutImages.IMAGES_TYPE, String, true)
    ImageType: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInoutImages.LATITUDE, String, true)
    Latiture: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInoutImages.LONGITUDE, String, true)
    Longiture: string = undefined;

    @JsonProperty(CONSTANT.SubMoldInoutImages.IMAGES_NAME, String, true)
    PhotoName: string = undefined;

}
