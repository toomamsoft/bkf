import { JsonObject, JsonProperty } from 'json2typescript';
import { CONSTANT } from '../../shared/constant';

@JsonObject("LocationSurveyAdapterModel")
export class LocationSurveyAdapterModel {

    @JsonProperty(CONSTANT.LocationModel.LOCATION_ID, String, true)
    LocationID: string = undefined;

    @JsonProperty(CONSTANT.LocationModel.LOCATION_DESCRIPTION, String, true)
    LocationDescription: string = undefined;

}