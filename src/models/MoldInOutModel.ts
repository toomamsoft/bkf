import { MoldInOutAdapterModel } from './package/MoldInOutAdapterModel';
import { JsonObject } from "json2typescript";
import { OperationBaseModel } from "./package/OperationBaseModel";


@JsonObject("MoldInOutModel")
export class MoldInOutModel extends OperationBaseModel {

    getNewMoldInOut: MoldInOutAdapterModel = new MoldInOutAdapterModel();
    getSetting: MoldInOutAdapterModel = new MoldInOutAdapterModel();
    getWaiting: MoldInOutAdapterModel = new MoldInOutAdapterModel();
    getSearch: MoldInOutAdapterModel = new MoldInOutAdapterModel();
    getStatus: MoldInOutAdapterModel = new MoldInOutAdapterModel();
    getApprove: MoldInOutAdapterModel = new MoldInOutAdapterModel();
    statment: MoldInOutAdapterModel = new MoldInOutAdapterModel();

    //form html
    MoldId: string;
    JobId: string;
    MoldMaker: string;
    AssetId: string;
    CustNo: string;
    PartNo: string;
    PartName: string;
    Remark: string;
    MakerId: string;
    ReasonId: string;
    PlanIn: string;
    PlanOut: string;
    Images:Array<{photo:string, latitude:number, longitude: number}> = [];
    statusId: string;

    ApproveType: string;
    ApproveRemark: string;
    ApproveStatus: string;

    ConfirmStatus: string;
    ConfirmRemark: string;


    ImageSelectedName: string;
    ImageSelectedIndex: number;
}