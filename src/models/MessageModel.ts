import { MessageAdapterModel } from './package/MessageAdapterModel';
import { JsonObject } from "json2typescript";
import { OperationBaseModel } from "./package/OperationBaseModel";

@JsonObject("MessageModel")
export class MessageModel extends OperationBaseModel {

    getMessageDetail: MessageAdapterModel = new MessageAdapterModel();
    readMessage: MessageAdapterModel = new MessageAdapterModel();

}