import { MoldMakerAdapterModel } from './package/MoldMakerAdapterModel';
import { JsonObject } from "json2typescript";
import { OperationBaseModel } from "./package/OperationBaseModel";


@JsonObject("MoldMakerModel")
export class MoldMakerModel extends OperationBaseModel {
    getListMode: MoldMakerAdapterModel = new MoldMakerAdapterModel();
    getMoldBKF: MoldMakerAdapterModel = new MoldMakerAdapterModel();
    getHistoryDetail: MoldMakerAdapterModel = new MoldMakerAdapterModel();
    bkfview: MoldMakerAdapterModel = new MoldMakerAdapterModel();
    saveData: MoldMakerAdapterModel = new MoldMakerAdapterModel();

    JobId: string;
    RunNo: string;
    MoldId: string;
    AssetId: string;
    CustNo: string;
    PartNo: string;
    PartName: string;
    Images:Array<{photo:string, latitude:number, longitude: number}> = [];
    Remark: string;
    Status: string;
}