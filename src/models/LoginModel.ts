// import { Utilities } from './../shared/utilities';
// import { CONSTATT } from './../shared/constant';
import { JsonObject } from "json2typescript";
import { OperationBaseModel } from "./package/OperationBaseModel";
import { AuthenticateUserAdapterModel } from "./package/AuthenticateUserAdapterModel";
import { SettingAdapterModel } from "./package/SettingAdapterModel";
// import { MOCK } from "../mock/mockdata";

@JsonObject("LoginModel")
export class LoginModel extends OperationBaseModel {
    authenticateUser: AuthenticateUserAdapterModel = new AuthenticateUserAdapterModel();
    setting: SettingAdapterModel = new SettingAdapterModel()

    //form html
    username:string;
    password:string;

    keyActive: string;
}
