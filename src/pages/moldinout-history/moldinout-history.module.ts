import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldinoutHistoryPage } from './moldinout-history';
import { ComponentsModule } from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoldinoutHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(MoldinoutHistoryPage),
    ComponentsModule,
    DirectivesModule,
    PipesModule,
    TranslateModule
  ],
  entryComponents: [
    MoldinoutHistoryPage
  ]
})
export class MoldinoutHistoryPageModule {}
