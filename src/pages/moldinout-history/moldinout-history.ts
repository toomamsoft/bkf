import { BasePage } from './../../shared/basepage';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, Events, AlertController, ActionSheetController, Platform } from 'ionic-angular';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { MoldMakerModel } from '../../models/MoldMakerModel';
import { MoldmakerProvider } from '../../providers/moldmaker-provider/moldmaker-provider';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Utilities } from '../../shared/utilities';
import { StorageProvider } from '../../providers/storage-provider/storage-provider';
import { CONSTANT } from '../../shared/constant';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';
import { ScanProvider } from '../../providers/scan/scan';

/**
 * Generated class for the MoldinoutHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moldinout-history',
  templateUrl: 'moldinout-history.html',
})
export class MoldinoutHistoryPage extends BasePage implements OnInit, OnDestroy {

  public moldinoutModel: MoldInOutModel;
  public moldMakerModel: MoldMakerModel;
  public Mode: string = 'Out'
  public form: FormGroup;
  public limit: number = 5;
  public Latitude:number = 0.0000000000000000;
  public Longitude:number = 0.0000000000000000;
  public approveOut: boolean = false;
  public approveIn: boolean = false;
  public confirmOut: boolean = false;
  public confirmIn: boolean = false;
  public IP: string = CONSTANT.IPAddress;

  constructor(
    public navCtrl: NavController,
    public events: Events,
    private alertCtrl: AlertController,
    // private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    private actionsheetCtrl: ActionSheetController,
    private _moldinoutProvider: MoldinoutProvider,
    private _moldmakerProvider: MoldmakerProvider,
    private _storageProvider: StorageProvider,
    private _scanProvider: ScanProvider,
    private platform: Platform
    ) {
      super(events);
      this.form = this.formBuilder.group({
        MoldId: [{value: null, disabled: true}],
        MoldMaker: [{value: null, disabled: true}],
        AssetId: [{value: null, disabled: true}],
        CustNo: [{value: null, disabled: true}],
        PartNo: [{value: null, disabled: true}],
        PartName: [{value: null, disabled: true}],
        Remark: [{value: null, disabled: true}],
        MakerId: [{value: null, disabled: true}],
        ReasonId: [{value: null, disabled: true}],
        PlanIn: [{value: null, disabled: true}],
        PlanOut: [{value: null, disabled: true}]
      })
  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad MoldinoutHistoryPage');
    try {
      await Promise.all([this._moldinoutProvider.getSetting(), this._moldinoutProvider.getApprove()]);
      let AssetId:any = '';
      let CustNo:any = '';
      let PartNo:any = '';
      let PartName:any = '';
      this.form.controls['MoldId'].setValue(this.moldinoutModel.MoldId);
      this.form.controls['MoldMaker'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].makerMold);
      for(let i = 0; i < this.moldinoutModel.getNewMoldInOut.inoutMold[0].description.length; i++){
        if(i == 0) {
          AssetId = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].AssetId;
          CustNo = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].CustNo;
          PartNo = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartNo;
          PartName = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartName;
        } else {
          AssetId += '\n'+ this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].AssetId;
          CustNo += '\n'+ this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].CustNo;
          PartNo += '\n'+ this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartNo;
          PartName += '\n'+ this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartName;
        }
      }
      this.form.controls['AssetId'].setValue(AssetId);
      this.form.controls['CustNo'].setValue(CustNo);
      this.form.controls['PartNo'].setValue(PartNo);
      this.form.controls['PartName'].setValue(PartName);
      this.form.controls['Remark'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].remarkNew);
      this.form.controls['MakerId'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].makerId);
      this.form.controls['ReasonId'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].reason);
      this.form.controls['PlanIn'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].planIn);
      this.form.controls['PlanOut'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].planIn);

      if(this.moldinoutModel.getNewMoldInOut.inoutMold[0].makerStatus == 'Completed') {
        this.Mode = 'In'
      }

      if( this.moldinoutModel.getApprove.approveIn.filter(val => {return val.status === 'Waiting'}).length > 0 || this.moldinoutModel.getNewMoldInOut.inoutMold[0].status == 'Completed') {
        this.approveIn = true
      }

      if( this.moldinoutModel.getApprove.approveOut.filter(val => {return val.status === 'Waiting'}).length > 0 &&
          this.moldinoutModel.getApprove.approveOut.filter(val => {return val.status === 'Reject'}).length <= 0 &&
          this.moldinoutModel.getApprove.approveOut.filter(val => {return val.status === 'ApproveOut'}).length <= this.moldinoutModel.getApprove.approveOut.length ) {
        this.approveOut = true;
      }

      if( this.moldinoutModel.getNewMoldInOut.inoutMold[0].status === 'ApproveOut' && 
          this.moldinoutModel.getApprove.approveOut.filter(val => {return val.status === 'ApproveOut'}).length <= this.moldinoutModel.getApprove.approveOut.length ){
        this.confirmOut = true;
      }

      console.log(this.approveIn, this.approveOut, this.confirmOut)
    } catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }
  }

  async ngOnInit() {
    this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
    this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
  }

  async ngOnDestroy() {
    this._moldinoutProvider.moldInOutModel = new MoldInOutModel();
  }

  async tapViewPhoto(photo) {
    // let photo = this.IP + 'inout/showImage?filename='+imageBase64;
    let view = `${this.IP}/inout/showImage?filename='=${photo}`;
    console.log(photo)
    Utilities.viewImages(view);
  }

  async viewPdf(){
    try {
      await Utilities.viewFilePDF(this.moldinoutModel.getNewMoldInOut.inoutMold[0].pdf);
    }catch (e) {
      console.log(e)
    }
  }

  async openLaunchNavigator(latitude, longitude){
    await Utilities.openGoogleMapApplication(latitude, longitude);
  }

  async viewHistory() {
    this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
    this.moldMakerModel.JobId = this.moldinoutModel.getNewMoldInOut.inoutMold[0].jobId;
    this.navCtrl.push('MoldinoutViewMoldmakerPage')
  }

  async presentActionSheet() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Albums',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Camera',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {            
            this.openCamera();
          }
        },
        {
          text: 'Album',
          icon: !this.platform.is('ios') ? 'albums' : null,
          handler: () => {
            this.getImageFromGallery();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  async openCamera() {
    if(this.moldinoutModel.Images.length >= this.limit)
    {
      let alert = this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.photo%'] : ENG['%message.photo%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
        }]
      });
      alert.present();
      return;
    }

    await this._scanProvider.getImageFromCamere().then(val => {
      if(val && val != 'No Image Selected' && val != 'cordova_not_available') {
        this.moldinoutModel.Images.push({photo: val.toString(), latitude: this.Latitude, longitude: this.Longitude })
      }
    });
  }

  async getImageFromGallery() {
    let images: any;
    if(this.moldinoutModel.Images.length >= this.limit)
    {
      let alert = this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.photo%'] : ENG['%message.photo%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
        }]
      });
      alert.present();
      return;
    }

    await this._scanProvider.getImageFormAlbum(this.limit - this.moldinoutModel.Images.length).then(val => {
      if(val && val != 'cordova_not_available') {
        images = val;
        for(let i = 0; i < images.length; i++){
          this.moldinoutModel.Images.push({photo: images[i], latitude: this.Latitude, longitude: this.Longitude});
        }
      }
    });
  }

  async deletePhoto(index)
  {
    let confirm = this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
      subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.delete.image%'] : ENG['%message.delete.image%'],
      buttons: [
        {
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.no%'] : ENG['%btn.logout.no%'],
          handler: () => {
            console.log('Disagree clicked');
          }
        }, {
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.yes%'] : ENG['%btn.logout.yes%'],
          handler: () => {
            this.moldinoutModel.Images.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  async approveTypeOut() {
    let CheckApprove: boolean = false;
    for(let i = 0; i < this.moldinoutModel.getApprove.approveOut.length; i++)
    {
      if(this.moldinoutModel.getApprove.approveOut[i].userType == await this._storageProvider.getKey(CONSTANT.USER_TYPE) )
      { 
        if(i != 0){
          let tmp = this.moldinoutModel.getApprove.approveOut[i-1];
          if(tmp.status == 'ApproveOut')
          {              
            if(this.moldinoutModel.getApprove.approveOut[i].status == 'Waiting')
            {
              CheckApprove = true;
              this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveOut[i].jobId;
              this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
              this.moldinoutModel.ApproveType = 'out';
              this.moldinoutModel.ApproveStatus = 'ApproveOut';
            }else{
              CheckApprove = false;
            }
          }
        }else{
          let tmp = this.moldinoutModel.getApprove.approveOut[i];
          if(tmp.status == 'ApproveOut')
          {
            CheckApprove = false;
          }else if (tmp.status == 'Waiting'){
            CheckApprove = true;
            this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveOut[i].jobId;
            this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
            this.moldinoutModel.ApproveType = 'out';
            this.moldinoutModel.ApproveStatus = 'ApproveOut';
          }
        }
      }
    }

    if(!CheckApprove) {
      this.alertCtrl.create({
        title: 'แจ้งเตือน',
        message: 'คุณไม่มีสิทธิในการอนุมัติ',
        buttons: ["OK"]
      }).present();
      return;
    }

    this.navCtrl.push('MoldinoutApprovePage');

    // let modal = this.modalCtrl.create('MoldinoutApprovePage', null, { cssClass: 'test-modal' } );
    // modal.present();

    // modal.onWillDismiss( (val) => {

    // })

  }

  async rejectTypeOut() {
    let CheckApprove: boolean = false;
    for(let i = 0; i < this.moldinoutModel.getApprove.approveOut.length; i++)
    {
      if(this.moldinoutModel.getApprove.approveOut[i].userType == await this._storageProvider.getKey(CONSTANT.USER_TYPE) )
      { 
        if(i != 0){
          let tmp = this.moldinoutModel.getApprove.approveOut[i-1];
          if( tmp.status == 'ApproveOut' || tmp.status == 'Reject' )
          {              
            if(this.moldinoutModel.getApprove.approveOut[i].status == 'Waiting')
            {
              CheckApprove = true;
              this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveOut[i].jobId;
              this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
              this.moldinoutModel.ApproveType = 'out';
              this.moldinoutModel.ApproveStatus = 'Reject';
            }else{
              CheckApprove = false;
            }
          }
        }else{
          let tmp = this.moldinoutModel.getApprove.approveOut[i];
          if( tmp.status == 'ApproveOut' || tmp.status == 'Reject' )
          {
            CheckApprove = false;
          }else if (tmp.status == 'Waiting'){
            CheckApprove = true;
            this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveOut[i].jobId;
            this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
            this.moldinoutModel.ApproveType = 'out';
            this.moldinoutModel.ApproveStatus = 'Reject';
          }
        }
      }
    }

    if(!CheckApprove) {
      this.alertCtrl.create({
        title: 'แจ้งเตือน',
        message: 'คุณไม่มีสิทธิในการอนุมัติ',
        buttons: ["OK"]
      }).present();
      return;
    }
    this.navCtrl.push('MoldinoutApprovePage');
    // let modal = this.modalCtrl.create('MoldinoutApprovePage');
    // modal.present();
  }

  async rejectConfirmOut() {
    this.moldinoutModel.ConfirmStatus = 'N'
    this.navCtrl.push('MoldinoutConfirmInPage');
  }

  async approveTypeIn() {
    let CheckApprove: boolean = false;
    for(let i = 0; i < this.moldinoutModel.getApprove.approveIn.length; i++) {
      if( this.moldinoutModel.getApprove.approveIn[i].userType == await this._storageProvider.getKey(CONSTANT.USER_TYPE) )
      {
        if(i != 0){
          let tmp = this.moldinoutModel.getApprove.approveIn[i-1];
          if(tmp.status == 'ApproveIn' || tmp.status == 'Reject')
          {              
            if(this.moldinoutModel.getApprove.approveIn[i].status == 'Waiting')
            {
              CheckApprove = true;
              this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveIn[i].jobId;
              this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
              this.moldinoutModel.ApproveType = 'in';
              this.moldinoutModel.ApproveStatus = 'ApproveIn';
            }else{
              CheckApprove = false;
            }
          }
        }else{
          let tmp = this.moldinoutModel.getApprove.approveIn[i];
          if(tmp.status == 'ApproveIn' || tmp.status == 'Reject')
          {
            CheckApprove = false;
          }else if (tmp.status == 'Waiting'){
            CheckApprove = true;
            this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveIn[i].jobId;
            this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
            this.moldinoutModel.ApproveType = 'in';
            this.moldinoutModel.ApproveStatus = 'ApproveIn';
          }
        }
      }
    }

    if(!CheckApprove) {
      this.alertCtrl.create({
        title: 'แจ้งเตือน',
        message: 'คุณไม่มีสิทธิในการอนุมัติ',
        buttons: ["OK"]
      }).present();
      return;
    }

    this.navCtrl.push('MoldinoutApprovePage');
    // let modal = this.modalCtrl.create('MoldinoutApprovePage', null, { cssClass: 'test-modal' } );
    // modal.present();
  }

  async rejectTypeIn() {
    let CheckApprove: boolean = false;
    for(let i = 0; i < this.moldinoutModel.getApprove.approveIn.length; i++) {
      if( this.moldinoutModel.getApprove.approveIn[i].userType == await this._storageProvider.getKey(CONSTANT.USER_TYPE) )
      {
        if(i != 0){
          let tmp = this.moldinoutModel.getApprove.approveIn[i-1];

          if(tmp.status == 'ApproveIn' || tmp.status == 'Reject')
          {              
            if(this.moldinoutModel.getApprove.approveIn[i].status == 'Waiting')
            {
              CheckApprove = true;             
              this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveIn[i].jobId;
              this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
              this.moldinoutModel.ApproveType = 'in';
              this.moldinoutModel.ApproveStatus = 'Reject';
            }else{
              CheckApprove = false;
            }
          }
        }else{
          let tmp = this.moldinoutModel.getApprove.approveIn[i];
          if(tmp.status == 'ApproveIn' || tmp.status == 'Reject')
          {
            CheckApprove = false;
          }else if (tmp.status == 'Waiting'){
            CheckApprove = true;
            this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveIn[i].jobId;
            this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
            this.moldinoutModel.ApproveType = 'in';
            this.moldinoutModel.ApproveStatus = 'Reject';
          }
        }
      }
    }
    
    if(!CheckApprove) {
      this.alertCtrl.create({
        title: 'แจ้งเตือน',
        message: 'คุณไม่มีสิทธิในการอนุมัติ',
        buttons: ["OK"]
      }).present();
      return;
    }

    this.navCtrl.push('MoldinoutApprovePage');
    // let modal = this.modalCtrl.create('MoldinoutApprovePage', null, { cssClass: 'test-modal' } );
    // modal.present();
  }

  async ConfirmOut() {
    this.navCtrl.push('MoldinoutConfirmOutPage')
  }

  async ConfirmIn() {
    if(this.moldinoutModel.Images.length <= 0)  {
      this.alertCtrl.create({
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: 'จำนวนรูปภาพไม่ครบ กรุณาตรวจสอบอีกครั้ง',
        buttons: ['OK']
      }).present();
      return
    }
    this.navCtrl.push('MoldinoutConfirmInPage');
  }

}
