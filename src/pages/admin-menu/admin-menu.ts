import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { LoginProvider } from '../../providers/login-provider/login-provider';

/**
 * Generated class for the AdminMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-menu',
  templateUrl: 'admin-menu.html',
})
export class AdminMenuPage extends BasePage implements OnInit {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public events: Events,
    private _loginProvider: LoginProvider
    ) {
      super(events)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminMenuPage');
  }

  async ngOnInit() { 

  }

  async onLogOut() {
    this._loginProvider.authenticateLogout();
  }

}
