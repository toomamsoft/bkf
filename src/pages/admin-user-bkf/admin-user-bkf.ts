import { SubAdminGetListUserBKF } from './../../models/package/SubAdminGetListUserBKF';
import { AdminModel } from './../../models/AdminModel';
import { BasePage } from './../../shared/basepage';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { AdminProvider } from '../../providers/admin-provider/admin-provider';

/**
 * Generated class for the AdminUserBkfPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-user-bkf',
  templateUrl: 'admin-user-bkf.html',
})
export class AdminUserBkfPage extends BasePage implements OnInit, OnDestroy {
  public adminModel: AdminModel;
  constructor(
    public navCtrl: NavController, 
    public events: Events,
    private _adminProvider: AdminProvider) {
    super(events)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminUserBkfPage');
  }

  ngOnInit() {
    this.adminModel = this._adminProvider.adminModel;
  }

  ngOnDestroy() {
    this._adminProvider.adminModel = new AdminModel();
  }

  async ionViewDidEnter(){
    try {
      await this._adminProvider.getListUserBKF();
    }catch( ex ) {
      this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
    }
  }

  viewHistory(itemSelected: SubAdminGetListUserBKF) {
    this.adminModel.bkfSelected = itemSelected;
    this.navCtrl.push('AdminEditUserPage');
  }

}
