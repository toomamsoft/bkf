import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminUserBkfPage } from './admin-user-bkf';

@NgModule({
  declarations: [
    AdminUserBkfPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminUserBkfPage),
  ],
  entryComponents:[
    AdminUserBkfPage
  ]
})
export class AdminUserBkfPageModule {}
