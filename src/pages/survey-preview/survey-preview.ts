import { CONSTANT } from './../../shared/constant';
import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { SurveyProvider } from '../../providers/survey-provider/survey-provider';
import { SurveyModel } from '../../models/SurveyModel';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Utilities } from '../../shared/utilities';
import { LocationSurveyAdapterModel } from '../../models/package/LocationSurveyAdapterModel';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';

/**
 * Generated class for the SurveyPreviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-survey-preview',
  templateUrl: 'survey-preview.html',
})
export class SurveyPreviewPage extends BasePage implements OnInit {

  public dataLocation: LocationSurveyAdapterModel;
  public surveyModel: SurveyModel;
  public form: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public events: Events,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public _surveyProvider: SurveyProvider
    ) {
    super(events);

    this.form = this.formBuilder.group({
      Location:[{value: null, disabled: true}],
      MoldId: [{value: null, disabled: true}],
      AssetId: [{value: null, disabled: true}],
      CustNo: [{value: null, disabled: true}],
      PartNo: [{value: null, disabled: true}],
      PartName: [{value: null, disabled: true}],
      Plate: [{value: null, disabled: true}],
      Creator: [{value: null, disabled: true}],
      Remark: [{value: null, disabled: true}]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SurveyPreviewPage');
  }

  async ngOnInit() {
    await Promise.all([this.surveyModel = this._surveyProvider.surVeyModel]) ;
    this.dataLocation = this.surveyModel.locationDescription;
    this.form.controls['Location'].setValue(this.surveyModel.Location);
    this.form.controls['MoldId'].setValue(this.surveyModel.MoldId);
    this.form.controls['AssetId'].setValue(this.surveyModel.AssetId);
    this.form.controls['CustNo'].setValue(this.surveyModel.CustNo);
    this.form.controls['PartNo'].setValue(this.surveyModel.PartNo);
    this.form.controls['PartName'].setValue(this.surveyModel.PartName);
    this.form.controls['Plate'].setValue(this.surveyModel.Plate);
    this.form.controls['Creator'].setValue(localStorage.getItem(CONSTANT.USER_NAME));
    this.form.controls['Remark'].setValue(this.surveyModel.Remark);
  }

  async saveData()
  {
    try {
      await this._surveyProvider.saveDataMold();

      if(this.surveyModel.surveyMoldSave)
      {
        this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
            handler: () =>{
              this.popToPage('SurveyMenuPage', this.navCtrl);
            }
          }]
        }).present();
      }

    } catch (ex) {
      this._surveyProvider.submitErrorEvent(this._surveyProvider.surVeyModel2);
    }
  }

  async backToPage()
  {
    this.navCtrl.pop()
  }

  async viewImages(imageUrl: string = '', title: string = '') {
    Utilities.viewImages(imageUrl,title);
  }
}
