import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurveyPreviewPage } from './survey-preview';
import { ComponentsModule } from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SurveyPreviewPage,
  ],
  imports: [
    IonicPageModule.forChild(SurveyPreviewPage),
    ComponentsModule,
    DirectivesModule,
    PipesModule,
    TranslateModule
  ],
  entryComponents: [
    SurveyPreviewPage
  ]
})
export class SurveyPreviewPageModule {}
