import { FormGroup, FormBuilder } from '@angular/forms';
import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { SurveyModel } from '../../models/SurveyModel';
import { SurveyProvider } from '../../providers/survey-provider/survey-provider';
import { Utilities } from '../../shared/utilities';
import { CONSTANT } from '../../shared/constant';

/**
 * Generated class for the SurveyHistoryDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-survey-history-detail',
  templateUrl: 'survey-history-detail.html',
})
export class SurveyHistoryDetailPage extends BasePage implements OnInit {
  public form: FormGroup;
  public surveyModel: SurveyModel;
  public IP: string = CONSTANT.IPAddress;

  constructor(
    public navCtrl: NavController, 
    public events: Events,
    private formBuilder: FormBuilder,
    public _surveyProvider: SurveyProvider,
    ) {
    super(events);

    this.form = this.formBuilder.group({
      Location: [{value: null, disabled: true}],
      MoldId: [{value: null, disabled: true}],
      AssetId: [{value: null, disabled: true}],
      CustNo: [{value: null, disabled: true}],
      PartNo: [{value: null, disabled: true}],
      PartName: [{value: null, disabled: true}],
      Plate: [{value: null, disabled: true}],
      Creator: [{value: null, disabled: true}],
      Remark: [{value: null, disabled: true}]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SurveyHistoryDetailPage');
  }

  async ngOnInit() {
    let AssetId:any = '';
    let CustNo:any = '';
    let PartNo:any = '';
    let PartName:any = '';
    try {
      this.surveyModel = this._surveyProvider.surVeyModel;
      await this._surveyProvider.getMoldHistoryDetail()

      this.form.controls['Location'].setValue(this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].location);
      this.form.controls['MoldId'].setValue(this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].MoldId);
      for(let i = 0; i < this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description.length; i++){
        if(i == 0) {
          AssetId = this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].AssetId;
          CustNo = this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].CustNo;
          PartNo = this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].PartNo;
          PartName = this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].PartName;
        } else {
          AssetId += '\n'+ this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].AssetId;
          CustNo += '\n'+ this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].CustNo;
          PartNo += '\n'+ this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].PartNo;
          PartName += '\n'+ this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].PartName;
        }
      }
      this.form.controls['AssetId'].setValue(AssetId);
      this.form.controls['CustNo'].setValue(CustNo);
      this.form.controls['PartNo'].setValue(PartNo);
      this.form.controls['PartName'].setValue(PartName);
      this.form.controls['Plate'].setValue(this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Plate);
      this.form.controls['Remark'].setValue(this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Remark);
      this.form.controls['Creator'].setValue(this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].FullName);
    }catch (ex) {
      this._surveyProvider.submitErrorEvent(this._surveyProvider.surVeyModel2);
    }
  }

  async viewImages(imageUrl: string = '', title: string = '') {
    let view = `${this.IP}/survey/showImage?filename='=${imageUrl}`;
    Utilities.viewImages(view,title);
  }

}
