import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurveyHistoryDetailPage } from './survey-history-detail';
import { ComponentsModule } from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SurveyHistoryDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SurveyHistoryDetailPage),
    ComponentsModule,
    DirectivesModule,
    PipesModule,
    TranslateModule
  ],
  entryComponents: [
    SurveyHistoryDetailPage
  ]
})
export class SurveyHistoryDetailPageModule {}
