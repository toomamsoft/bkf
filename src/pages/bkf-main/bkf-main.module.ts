import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BkfMainPage } from './bkf-main';

@NgModule({
  declarations: [
    BkfMainPage,
  ],
  imports: [
    IonicPageModule.forChild(BkfMainPage),
  ],
})
export class BkfMainPageModule {}
