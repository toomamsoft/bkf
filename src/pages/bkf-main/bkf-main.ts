import { CONSTANT } from './../../shared/constant';
import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { MessageProvider } from '../../providers/message-provider/message-provider';
import { MessageModel } from '../../models/MessageModel';
import { LoginProvider } from '../../providers/login-provider/login-provider';
import { StorageProvider } from '../../providers/storage-provider/storage-provider';
import { Utilities } from '../../shared/utilities';

/**
 * Generated class for the BkfMainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bkf-main',
  templateUrl: 'bkf-main.html',
})
export class BkfMainPage extends BasePage implements OnInit {
  public messageModel: MessageModel;
  public countMessage: number = 0;
  public fullName:any = '';
  public platformVersion: any = '';
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public events: Events,
    private _messageProvider: MessageProvider,
    private _loginProvider: LoginProvider,
    private _storageProvider: StorageProvider
    ) {
      super(events);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BkfMainPage');
  }

  async ionViewDidEnter(){
    try {
      this.fullName = await this._storageProvider.getKey(CONSTANT.USER_NAME);
      this.platformVersion = await Utilities.getAppVersion();
      this.messageModel = this._messageProvider.messageModel;
      await this._messageProvider.getMessageDetail();
      this.countMessage = this.messageModel.getMessageDetail.detail.length;
    } catch (ex) {
      console.log(ex)
    }
  }

  async ngOnInit() {
    this.messageModel = this._messageProvider.messageModel;
  }

  async onNotification() {
    this.navCtrl.push('NotificationPage')
  }

  async onLogout() {
    this._loginProvider.authenticateLogout();
  }

}
