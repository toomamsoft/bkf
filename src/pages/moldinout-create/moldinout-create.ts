import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BasePage } from './../../shared/basepage';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, ActionSheetController, Platform } from 'ionic-angular';
import { Geolocation, GeolocationOptions } from '@ionic-native/geolocation';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider'
import { ScanProvider } from '../../providers/scan/scan';
import { StorageProvider } from '../../providers/storage-provider/storage-provider';
import { CONSTANT } from '../../shared/constant';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';
import { Utilities } from '../../shared/utilities';
/**
 * Generated class for the MoldinoutCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
const gpsOptions : GeolocationOptions = {
  enableHighAccuracy : true
};

@IonicPage()
@Component({
  selector: 'page-moldinout-create',
  templateUrl: 'moldinout-create.html',
})
export class MoldinoutCreatePage extends BasePage implements OnInit, OnDestroy {
  public Mode: string = 'Out';
  public limit: number = 5;
  public form: FormGroup;
  public Latitude:number = 0.0000000000000000;
  public Longitude:number = 0.0000000000000000;
  public moldinoutModel: MoldInOutModel;
  public someMaxDateFromComponent = (new Date((new Date().getFullYear() + 1)+'-12-31')).toISOString().replace('Z','')
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public events: Events,
    private platform: Platform,
    private alertCtrl: AlertController,
    private actionsheetCtrl: ActionSheetController,
    private formBuilder: FormBuilder,
    private geolocation: Geolocation,
    private _moldinoutProvider: MoldinoutProvider,
    private _scanProvider: ScanProvider,
    private _storageProvider: StorageProvider
    ) {
      super(events);
      this.form = this.formBuilder.group({
        MoldId: [{value: null, disabled: true}, [Validators.required]],
        MoldMaker: [{value: null, disabled: true}],
        AssetId: [{value: null, disabled: true}],
        CustNo: [{value: null, disabled: true}],
        PartNo: [{value: null, disabled: true}],
        PartName: [{value: null, disabled: true}],
        Remark:[null],
        MakerId: ['',[Validators.required]],
        ReasonId: ['',[Validators.required]],
        PlanIn: ['',[Validators.required]],
        PlanOut: ['',[Validators.required]]
      })
  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad MoldinoutCreatePage');
  }

  async ngOnInit() {
    let AssetId:any = '';
    let CustNo:any = '';
    let PartNo:any = '';
    let PartName:any = '';
    this.limit = Number(await this._storageProvider.getKey(CONSTANT.IMAGE_MOLDINOUT));
    this.moldinoutModel = this._moldinoutProvider.moldInOutModel;

    // this.locationAccuracy.canRequest().then((canRequest: boolean) => {

    //   if(canRequest) {
    //     // the accuracy option will be ignored by iOS
    //     this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_LOW_POWER).then(
    //       () => {
    //         console.log('requesting location permissions success');
    //       },
    //       error => console.log('Error requesting location permissions', error)
    //     );
    //   }
    // }).catch(error => {
    //   const alert = this.alertCtrl.create({
    //     title: 'Attention!',
    //     subTitle: error,
    //     buttons: ['Close']
    //   });
    //   alert.present();
    // });
    await Utilities.accessGPS();

    this.geolocation.getCurrentPosition(gpsOptions).then(resp => {
      console.log('requesting geolocation:::', resp);
      this.Latitude = resp.coords.latitude;
      this.Longitude = resp.coords.longitude;
    });   
    try {
      await this._moldinoutProvider.getSetting();
      this.form.controls['MoldId'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].moldId);
      this.form.controls['MoldMaker'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].makerMold)
      for(let i = 0; i < this.moldinoutModel.getNewMoldInOut.inoutMold[0].description.length; i++){
        if(i == 0) {
          AssetId = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].AssetId;
          CustNo = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].CustNo;
          PartNo = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartNo;
          PartName = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartName;
        } else {
          AssetId += '\n'+ this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].AssetId;
          CustNo += '\n'+ this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].CustNo;
          PartNo += '\n'+ this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartNo;
          PartName += '\n'+ this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartName;
        }
      }
      this.form.controls['AssetId'].setValue(AssetId);
      this.form.controls['CustNo'].setValue(CustNo);
      this.form.controls['PartNo'].setValue(PartNo);
      this.form.controls['PartName'].setValue(PartName);
    } catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel);
    }
  }

  async ngOnDestroy() {
    this._moldinoutProvider.moldInOutModel = new MoldInOutModel();
  }

  async deletePhoto(index)
  {
    let confirm = this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
      subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.delete.image%'] : ENG['%message.delete.image%'],
      buttons: [
        {
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.no%'] : ENG['%btn.logout.no%'],
          handler: () => {
            console.log('Disagree clicked');
          }
        }, {
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.yes%'] : ENG['%btn.logout.yes%'],
          handler: () => {
            this.moldinoutModel.Images.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  async tapViewPhoto(imageBase64) {
    Utilities.viewImages(imageBase64);
  }

  async openLaunchNavigator(latitude, longitude) {

  }

  async presentActionSheet() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Albums',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Camera',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {            
            this.openCamera();
          }
        },
        {
          text: 'Album',
          icon: !this.platform.is('ios') ? 'albums' : null,
          handler: () => {
            this.getImageFromGallery();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  async openCamera() {
    if(this.moldinoutModel.Images.length >= this.limit)
    {
      let alert = this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.photo%'] : ENG['%message.photo%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
        }]
      });
      alert.present();
      return;
    }

    await this._scanProvider.getImageFromCamere().then(val => {
      if(val && val != 'No Image Selected' && val != 'cordova_not_available') {
        this.moldinoutModel.Images.push({photo: val.toString(), latitude: this.Latitude, longitude: this.Longitude })
      }
    });
  }

  async getImageFromGallery() {
    let images: any;
    if(this.moldinoutModel.Images.length >= this.limit)
    {
      let alert = this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.photo%'] : ENG['%message.photo%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
        }]
      });
      alert.present();
      return;
    }

    await this._scanProvider.getImageFormAlbum(this.limit - this.moldinoutModel.Images.length).then(val => {
      if(val && val != 'cordova_not_available') {
        images = val;
        for(let i = 0; i < images.length; i++){
          this.moldinoutModel.Images.push({photo: images[i], latitude: this.Latitude, longitude: this.Longitude});
        }
      }
    });
  }

  async preview() {

    this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
    this.moldinoutModel.MoldMaker = this.form.controls['MoldMaker'].value;
    this.moldinoutModel.Remark = this.form.controls['Remark'].value;
    this.moldinoutModel.MakerId = this.form.controls['MakerId'].value; 
    this.moldinoutModel.ReasonId = this.form.controls['ReasonId'].value; 
    this.moldinoutModel.PlanIn = this.form.controls['PlanIn'].value;
    this.moldinoutModel.PlanOut = this.form.controls['PlanOut'].value;

    if(this.moldinoutModel.Images.length <= 0) {
      this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%title.title%'] : ENG['%title.title%'],
        subTitle: 'จำนวนรูปภาพควรมากกว่า 1 รูป',//CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
          handler: () =>{
            return;
          }
        }]
      }).present();
    }else {
      this.navCtrl.push('MoldinoutPreviewPage');
    }
  }

  async cancelTransaction() {
    this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: 'แจ้งเตือน',
      subTitle: 'คุณต้องการยกเลิกรายการนี้ใช่ไหม',
      buttons: [{
        text: 'ยกลเลิก',
        handler: () => {
          console.log('cancel click')
        }
      },{
        text: 'ยืนยัน',
        handler: () => {
          this.navCtrl.pop();
        }
      }]
    }).present();
  }
}
