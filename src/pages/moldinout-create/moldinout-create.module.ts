import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldinoutCreatePage } from './moldinout-create';
import { ComponentsModule } from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoldinoutCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(MoldinoutCreatePage),
    ComponentsModule,
    DirectivesModule,
    TranslateModule
  ],
  entryComponents: [
    MoldinoutCreatePage
  ]
})
export class MoldinoutCreatePageModule {}
