import { SubMoldInOutSearchModel } from './../../models/package/SubMoldInOutSearchModel';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, Events, AlertController } from 'ionic-angular';
import { BasePage } from '../../shared/basepage';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';
import { CONSTANT } from '../../shared/constant';
import { ScanProvider } from '../../providers/scan/scan';

/**
 * Generated class for the MoldinoutSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moldinout-search',
  templateUrl: 'moldinout-search.html',
})
export class MoldinoutSearchPage extends BasePage implements OnInit, OnDestroy {
  public form: FormGroup;
  public molInOutModel: MoldInOutModel;
  public dataResult: SubMoldInOutSearchModel[];
  constructor(
    public navCtrl: NavController,
    public events: Events,
    private alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    private _scanProvider: ScanProvider,
    private _moldinoutProvider: MoldinoutProvider
    ) {
      super(events);

      this.form = this.formBuilder.group({
        MoldId: [null, [Validators.required]],
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoldinoutSearchPage');
  }

  async ngOnInit() {
    this.molInOutModel = this._moldinoutProvider.moldInOutModel;
  }

  async ngOnDestroy() {
    this._moldinoutProvider.moldInOutModel = new MoldInOutModel();
  }

  async scan() {
    this.form.controls['MoldId'].setValue(await this._scanProvider.scan());
    if (this.form.controls['MoldId'].value == '') {
      return;
    }
    this.molInOutModel.MoldId = this.form.controls['MoldId'].value;
    this.onSearchHistory();
  }

  async onSearchByMold() {
    if (this.form.controls['MoldId'].value == '') {
      let alert = this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.serach.mold.error%'] : ENG['%message.serach.mold.error%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
        }]
      });
      alert.present();
      return;
    }

    this.molInOutModel.MoldId = this.form.controls['MoldId'].value;
    this.onSearchHistory()
  }

  async onSearchHistory() {
    try {
      await this._moldinoutProvider.moldSearch();
      if (this.molInOutModel.getSearch.moldSearch.length <= 0) {
        let alert = this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.notfound%'] : ENG['%message.notfound%'],
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
          }]
        });
        alert.present();
      }
      this.dataResult = this.molInOutModel.getSearch.moldSearch;
    }catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }
  }

  async viewHistory(itemSelected) {
    this._moldinoutProvider.moldInOutModel = new MoldInOutModel();
    this.molInOutModel = this._moldinoutProvider.moldInOutModel;

    this.molInOutModel.MoldId = itemSelected.moldId;
    this.molInOutModel.JobId = itemSelected.jobId;
    
    try {
      await this._moldinoutProvider.getNewMoldOut();
      this.navCtrl.push('MoldinoutHistoryPage');
    }catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }
  }
}