import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldinoutSearchPage } from './moldinout-search';

@NgModule({
  declarations: [
    MoldinoutSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(MoldinoutSearchPage),
  ],
  entryComponents: [
    MoldinoutSearchPage
  ]
})
export class MoldinoutSearchPageModule {}
