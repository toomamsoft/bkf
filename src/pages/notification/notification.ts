import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { MessageModel } from '../../models/MessageModel';
import { MessageProvider } from '../../providers/message-provider/message-provider';
import { Utilities } from '../../shared/utilities';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';
/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage extends BasePage implements OnInit {
  public messageModel: MessageModel;
  public moldinoutModel: MoldInOutModel;
  public TransactionsMessage: { messageDate: string, transaction: { MessageTime: string,  MoldId: string, JobId: string, messageID: string, messageSubject: string, messageDescription: string, readStatus: string }[] }[] = [];
  constructor(
    public navCtrl: NavController, 
    public events: Events,
    private _messageProvider: MessageProvider,
    private _moldinoutProvider: MoldinoutProvider
    ) {
      super(events)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
  }

  async ngOnInit() {
    this.messageModel = this._messageProvider.messageModel;
    this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
  }

  async ionViewDidEnter(){
    await this.LoadDate();
  }

  async LoadDate() {
    try {
      this.messageModel = this._messageProvider.messageModel;
      await this._messageProvider.getMessageDetail();
      this.TransactionsMessage = this.messageModel.getMessageDetail.detail.reduce((filtered, option) => {
        const position = filtered.findIndex(val => {
          const firstDate = new Date(Date.parse(val.messageDate.replace('Z','')));
          const secondDate = new Date(Date.parse(option.messageDate.replace('Z','')));
          return Utilities.formatDateDDMMYYYY(firstDate) === Utilities.formatDateDDMMYYYY(secondDate);
        });
  
        if (position == -1) {
          const obj = { 
            messageDate: option.messageDate,
            transaction: [{
              MessageTime: option.messageDate,
              JobId: option.jobId,
              MoldId: option.moldId,
              messageID: option.messageId,
              messageSubject: option.messageSubject,
              messageDescription: option.messageDescription,
              readStatus: option.readStatus
            }]
          }
          filtered.push(obj)
        }else{
          const obj = {
            MessageTime: option.messageDate,
            JobId: option.jobId,
            MoldId: option.moldId,
            messageID: option.messageId,
            messageSubject: option.messageSubject,
            messageDescription: option.messageDescription,
            readStatus: option.readStatus
          }
          filtered[position].transaction.push(obj)
        }
        return filtered;
      },[]);
    } catch (ex) {
      this._messageProvider.submitErrorEvent(this._messageProvider.messageModel2);
    }
  }

  async readMessage(itemSelected: { MessageTime: string,  MoldId: string, JobId: string,messageID: string ,messageSubject: string, messageDescription: string, readStatus: string }) {
    try {
      this.moldinoutModel.JobId = itemSelected.JobId;
      this.moldinoutModel.MoldId = itemSelected.MoldId;
      await this._moldinoutProvider.getNewMoldOut();
      this.navCtrl.push('MoldinoutHistoryPage');
    }catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }
  }
}
