import { MoldinoutMenuPage } from './../moldinout-menu/moldinout-menu';
import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events, MenuController, AlertController } from 'ionic-angular';
import { CONSTANT } from '../../shared/constant';
import { TranslateService } from '@ngx-translate/core';
import { LoginProvider } from '../../providers/login-provider/login-provider';
import { StorageProvider } from '../../providers/storage-provider/storage-provider';
import { ScanProvider } from '../../providers/scan/scan';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';
import { MessageProvider } from '../../providers/message-provider/message-provider';
/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage extends BasePage implements OnInit {
  rootPage: any;
  selectedMenu: any;
  public USER_NAME: any = '';
  public USER_TYPE: any = '';

  itemMenu:any =[
    {
      name: 'Fix Asset Survey',
      sub: [{
        title: 'Survey',
        icon: 'analytics',
        component: 'SurveyMenuPage/SurveyCreatePage'
      },{
        title: 'History',
        icon: 'time',
        component: 'SurveyMenuPage/SurveyHistoryPage'
      }]
    },
    {
      name: 'Mold In/Out',
      sub: [{
        title: 'Request Mold Out',
        icon: 'git-pull-request',
        component: 'MoldinoutMenuPage/MoldinoutCreatePage'
      },{
        title: 'Search',
        icon: 'search',
        component: 'MoldinoutMenuPage/MoldinoutSearchPage'
      },{
        title: 'Status',
        icon: 'pulse',
        component: 'MoldinoutMenuPage/MoldinoutStatusPage'
      }]
    }
  ];

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public events: Events,
    private menuCtrl: MenuController,
    public translate: TranslateService,
    private _loginProvider: LoginProvider,
    private _storageProvider: StorageProvider,
    private _moldinoutProvider: MoldinoutProvider,
    private _scanProvider: ScanProvider,
    private _messageProvider: MessageProvider
    ) {
    super(events)
    this.intitialLanguage(this.translate);
  }

  async ngOnInit()
  {
    
  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad MainPage');
    this.USER_TYPE = await this._storageProvider.getKey(CONSTANT.USER_TYPE);
    this.USER_NAME = await this._storageProvider.getKey(CONSTANT.USER_NAME);
    this.menuCtrl.swipeEnable(false);
    if( this.USER_TYPE == 'Admin' ) {
      this.menuCtrl.enable(false, 'authenticateduser');
      this.menuCtrl.enable(true, 'authenticatedadmin');
      this.menuCtrl.enable(false, 'authenticatedmaker');
      this.rootPage = 'AdminMenuPage';
    } else if( this.USER_TYPE == 'MoldMaker') {
      this.menuCtrl.enable(false, 'authenticateduser');
      this.menuCtrl.enable(false, 'authenticatedadmin');
      this.menuCtrl.enable(true, 'authenticatedmaker');
      this.rootPage = 'MakerMenuPage';
    } else {
      this.menuCtrl.enable(true, 'authenticateduser');
      this.menuCtrl.enable(false, 'authenticatedadmin');
      this.menuCtrl.enable(false, 'authenticatedmaker');
      this.rootPage = 'BkfMainPage';
    }
  }

  async setLang()
  {
    let lang = CONSTANT.APP_LANGUAGE == 'th' ? 'en' : 'th';
    await this.setLanguage(lang);
    this.intitialLanguage(this.translate);
  }

  async openPage(page:string)
  {
    let pages: string[] = page.split(/\//g);
    console.log(pages);
    this.navCtrl.push(pages[0]).then(() => {
      if(pages[1] !== 'MoldinoutCreatePage'){
        this.navCtrl.push(pages[1]);
      }else {
        new MoldinoutMenuPage(this.navCtrl,this.events, this.alertCtrl, this._loginProvider, this._moldinoutProvider, this._scanProvider, this._messageProvider, this._storageProvider).scanQr()
      }
    });
  }

  async onLogout()
  {
    this._loginProvider.authenticateLogout();
  }

}
