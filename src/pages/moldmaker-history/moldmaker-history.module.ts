import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldmakerHistoryPage } from './moldmaker-history';
import { ComponentsModule } from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoldmakerHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(MoldmakerHistoryPage),
    ComponentsModule,
    DirectivesModule,
    PipesModule,
    TranslateModule
  ],
  entryComponents: [
    MoldmakerHistoryPage
  ]
})
export class MoldmakerHistoryPageModule {}
