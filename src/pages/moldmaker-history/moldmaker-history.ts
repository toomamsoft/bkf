import { FormGroup, FormBuilder } from '@angular/forms';
import { BasePage } from './../../shared/basepage';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, Events, AlertController } from 'ionic-angular';
import { MoldMakerModel } from '../../models/MoldMakerModel';
import { Utilities } from '../../shared/utilities';
import { MoldmakerProvider } from '../../providers/moldmaker-provider/moldmaker-provider';
import { CONSTANT } from '../../shared/constant';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';
import { ScanProvider } from '../../providers/scan/scan';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation, GeolocationOptions } from '@ionic-native/geolocation';
import { SubMoldMakerHistoryModel } from '../../models/package/SubMoldMakerHistoryModel';

/**
 * Generated class for the MoldmakerHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
const gpsOptions : GeolocationOptions = {
  enableHighAccuracy : true
};
@IonicPage()
@Component({
  selector: 'page-moldmaker-history',
  templateUrl: 'moldmaker-history.html',
})
export class MoldmakerHistoryPage extends BasePage implements OnInit, OnDestroy {
  public moldmakerModel : MoldMakerModel;
  public form: FormGroup;
  public limit:number = 5;
  public Latitude: number = 0.0000000; 
  public Longitude: number = 0.0000000;
  public Mode: string = 'Current'
  constructor(
    public navCtrl: NavController,
    public events: Events,
    private locationAccuracy: LocationAccuracy,
    private geolocation: Geolocation,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private _moldmakerProvider : MoldmakerProvider,
    private _scanProvider: ScanProvider
  ) {
      super(events);
      this.form = this.formBuilder.group({
        Remark: ['']
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoldmakerHistoryPage');
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {

      if(canRequest) {
        // the accuracy option will be ignored by iOS
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => {
            this.geolocation.getCurrentPosition(gpsOptions).then(resp => {
              console.log('requesting geolocation:::', resp);
              this.Latitude = resp.coords.latitude;
              this.Longitude = resp.coords.longitude;
            });        
          },
          error => console.log('Error requesting location permissions', error)
        );
      }
    }).catch(error => {
      const alert = this.alertCtrl.create({
        title: 'Attention!',
        subTitle: error,
        buttons: ['Close']
      });
      alert.present();
    });
  }

  async ngOnInit() {
    this.moldmakerModel = this._moldmakerProvider.moldmakerModel;
  }

  async ngOnDestroy() {
    this._moldmakerProvider.moldmakerModel = new MoldMakerModel();
    this.moldmakerModel = this._moldmakerProvider.moldmakerModel;
  }

  async deletePhoto(index) {
    let confirm = this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
      subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.delete.image%'] : ENG['%message.delete.image%'],
      buttons: [
        {
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.no%'] : ENG['%btn.logout.no%'],
          handler: () => {
            console.log('Disagree clicked');
          }
        }, {
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.yes%'] : ENG['%btn.logout.yes%'],
          handler: () => {
            this.moldmakerModel.Images.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  async tapViewPhoto(photo) {
    await Utilities.viewImages(photo);
  }

  async getImageFromCamera() {
    if(this.moldmakerModel.Images.length >= this.limit) {
      let alert = this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.photo%'] : ENG['%message.photo%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
        }]
      });
      alert.present();
      return;
    }

    await this._scanProvider.getImageFromCamere().then(val => {
      if(val && val != 'No Image Selected' && val != 'cordova_not_available') {
        this.moldmakerModel.Images.push({photo: val.toString(), latitude: this.Latitude, longitude: this.Longitude })
      }
    });
  }

  async getImageFromAlbum() {
    let images: any;
    if(this.moldmakerModel.Images.length >= this.limit)
    {
      let alert = this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.photo%'] : ENG['%message.photo%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
        }]
      });
      alert.present();
      return;
    }

    await this._scanProvider.getImageFormAlbum(this.limit - this.moldmakerModel.Images.length).then(val => {
      if(val && val != 'cordova_not_available') {
        images = val;
        for(let i = 0; i < images.length; i++){
          this.moldmakerModel.Images.push({photo: images[i], latitude: this.Latitude, longitude: this.Longitude});
        }
      }
    });
  }

  async onCompleted() {
    try {
      this.moldmakerModel.Remark = this.form.controls['Remark'].value;
      this.moldmakerModel.Status = 'Completed'
      await this._moldmakerProvider.getMoldMakerTransaction();
      if(this.moldmakerModel.saveData.Status)
      {
        this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
            handler: () =>{
              this.popToPage('MakerMenuPage', this.navCtrl);
            }
          }]
        }).present();
      }
    }catch (e){
      this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2)
    }
  }

  async onInprogress() {
    try {
      this.moldmakerModel.Remark = this.form.controls['Remark'].value;
      this.moldmakerModel.Status = 'Inprogress'
      await this._moldmakerProvider.getMoldMakerTransaction();
      if(this.moldmakerModel.saveData.Status)
      {
        this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
            handler: () =>{
              this.popToPage('MakerMenuPage', this.navCtrl);
            }
          }]
        }).present();
      }
    }catch (e){
      this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2)
    }
  }

  async openLaunchNavigator(latitude, longitude) {

  }

  async viewHistory(itemSelected : SubMoldMakerHistoryModel) {
    this.moldmakerModel.JobId = itemSelected.jobId;
    this.moldmakerModel.RunNo = itemSelected.id;
    try { 
      await this._moldmakerProvider.getMoldMakerHistoryDetail()
      this.navCtrl.push('MoldmakerHistoryDetailPage');
    }catch(ex) {
      this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
    }    
  }
}
