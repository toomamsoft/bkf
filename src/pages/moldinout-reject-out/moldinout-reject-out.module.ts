import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldinoutRejectOutPage } from './moldinout-reject-out';

@NgModule({
  declarations: [
    MoldinoutRejectOutPage,
  ],
  imports: [
    IonicPageModule.forChild(MoldinoutRejectOutPage),
  ],
})
export class MoldinoutRejectOutPageModule {}
