import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { BasePage } from '../../shared/basepage';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { LoginModel } from '../../models/LoginModel';
import { LoginProvider } from '../../providers/login-provider/login-provider';
import { ScanProvider } from '../../providers/scan/scan';
import { StorageProvider } from '../../providers/storage-provider/storage-provider';
import { CONSTANT } from '../../shared/constant';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/th.json';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage extends BasePage implements OnInit {
  public formLogin: FormGroup;
  public loginModel: LoginModel;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public events: Events,
    public formBuilder: FormBuilder,
    public translate: TranslateService,
    private alertCtrl: AlertController,
    private _loginProvider: LoginProvider,
    private _scanProvider: ScanProvider,
    private _storageProvider: StorageProvider
    ) {
      super(events)
      this.intitialLanguage(this.translate);
      
      this.formLogin = this.formBuilder.group({
        username: [null, [Validators.required, Validators.maxLength(16)]],
        password: [null, [Validators.required, Validators.maxLength(16)]]
      })
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  async ngOnInit()
  {
    this.loginModel = this._loginProvider.loginModel;
  }

  async onLogin()
  {
    try {
      this.loginModel.username = this.formLogin.controls['username'].value;
      this.loginModel.password = this.formLogin.controls['password'].value

      await Promise.all([this._loginProvider.authenticateUser(), this._loginProvider.settingConfig()]);
      if(this.loginModel.authenticateUser.userLogin.length > 0)
      {
        this._storageProvider.setKey(CONSTANT.USER_ID, this.loginModel.authenticateUser.userLogin[0].userId);
        this._storageProvider.setKey(CONSTANT.USER_NAME, this.loginModel.authenticateUser.userLogin[0].fullName);
        this._storageProvider.setKey(CONSTANT.USER_TYPE, this.loginModel.authenticateUser.userLogin[0].userType);
        this._storageProvider.setKey(CONSTANT.IMAGE_MOLDINOUT, this.loginModel.setting.config[0].imageInOut);
        this._storageProvider.setKey(CONSTANT.IMAGE_MOLDSERVEY, this.loginModel.setting.config[0].imageInOut);
        this.navCtrl.setRoot("MainPage");
      }else {
        let alert = this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%error.login%'] : ENG['%error.login%'],
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
          }]
        });
        alert.present();
      }
    }catch (ex) {
      this._loginProvider.submitErrorEvent(this._loginProvider.loginModel2);
    }
  }

  async scan() {
    this.formLogin.controls['username'].setValue(await this._scanProvider.scan());
  }
}
