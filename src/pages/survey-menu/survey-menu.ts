import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { LoginProvider } from '../../providers/login-provider/login-provider';
import { MessageModel } from '../../models/MessageModel';
import { MessageProvider } from '../../providers/message-provider/message-provider';
import { Utilities } from '../../shared/utilities';
import { StorageProvider } from '../../providers/storage-provider/storage-provider';
import { CONSTANT } from '../../shared/constant';

/**
 * Generated class for the SurveyMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-survey-menu',
  templateUrl: 'survey-menu.html',
})
export class SurveyMenuPage extends BasePage implements OnInit {
  public messageModel: MessageModel;
  public countMessage: number = 0;
  public fullName:any = '';
  public platformVersion: any = '';

  constructor(
    public navCtrl: NavController,
    public events: Events,
    private _loginProvider: LoginProvider,
    private _messageProvider: MessageProvider,
    private _storageProvider: StorageProvider
    ) {
      super(events)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SurveyMenuPage');
  }

  async ngOnInit() {
    this.messageModel = this._messageProvider.messageModel;
  }

  async ionViewDidEnter(){
    try {
      this.fullName = await this._storageProvider.getKey(CONSTANT.USER_NAME);
      this.platformVersion = await Utilities.getAppVersion();
      this.messageModel = this._messageProvider.messageModel;
      await this._messageProvider.getMessageDetail();
      this.countMessage = this.messageModel.getMessageDetail.detail.length;
    } catch (ex) {
      console.log(ex)
    }
  }

  async onNotification() {
    this.navCtrl.push('NotificationPage')
  }

  async onLogout()
  {
    this._loginProvider.authenticateLogout();
  }

}
