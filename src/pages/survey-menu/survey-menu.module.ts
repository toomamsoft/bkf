import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurveyMenuPage } from './survey-menu';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SurveyMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(SurveyMenuPage),
    TranslateModule
  ],
  entryComponents: [
    SurveyMenuPage
  ]
})
export class SurveyMenuPageModule {}
