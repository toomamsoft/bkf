import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldmakerReceivePage } from './moldmaker-receive';
import { ComponentsModule } from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoldmakerReceivePage,
  ],
  imports: [
    IonicPageModule.forChild(MoldmakerReceivePage),
    ComponentsModule,
    DirectivesModule,
    PipesModule,
    TranslateModule
  ],
  entryComponents: [
    MoldmakerReceivePage
  ]
})
export class MoldmakerReceivePageModule {}
