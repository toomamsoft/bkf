import { Utilities } from './../../shared/utilities';
import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events, AlertController } from 'ionic-angular';
import { MoldMakerModel } from '../../models/MoldMakerModel';
import { MoldmakerProvider } from '../../providers/moldmaker-provider/moldmaker-provider';

/**
 * Generated class for the MoldmakerReceivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moldmaker-receive',
  templateUrl: 'moldmaker-receive.html',
})
export class MoldmakerReceivePage extends BasePage implements OnInit {
  public moldmakerModel : MoldMakerModel;
  constructor(
    public navCtrl: NavController,
    public events: Events,
    private alertCtrl: AlertController,
    private _moldmakerProvider : MoldmakerProvider
    ) {
      super(events);
  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad MoldmakerReceivePage');
  }

  async ngOnInit() {
    this.moldmakerModel = this._moldmakerProvider.moldmakerModel;
  }

  async onReceive() {
    try {
      await this._moldmakerProvider.getReceiveMold()
      if(this.moldmakerModel.getMoldBKF.Status) {
        this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: '',
          subTitle: '',
          buttons: [
            {
              text: 'Close',
              handler: ()=> {
                this.popToPage('MakerMenuPage', this.navCtrl)
              }
            }]
        }).present();
      }
    }catch (ex) {
      this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
    }
    
  }

  async backToPage() {
    this.navCtrl.pop();
  }

  async viewImages(photo) {
    Utilities.viewImages(photo)
  }

}
