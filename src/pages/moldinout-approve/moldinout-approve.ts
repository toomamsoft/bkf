import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events, AlertController, ViewController } from 'ionic-angular';
import { BasePage } from '../../shared/basepage';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { CONSTANT } from '../../shared/constant';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';

/**
 * Generated class for the MoldinoutApprovePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moldinout-approve',
  templateUrl: 'moldinout-approve.html',
})
export class MoldinoutApprovePage extends BasePage implements OnInit {
  public moldinoutModel: MoldInOutModel;
  public form: FormGroup;

  constructor(public navCtrl: NavController,
    public events: Events,
    private alertCtrl: AlertController,
    private viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    private _moldinoutProvider: MoldinoutProvider
    ) {
    super(events);
    this.form = this.formBuilder.group({
      Remark: ['']
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoldinoutApprovePage');
  }

  async ngOnInit() {
    this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
  }

  async ConfirmSaveData()
  {
    this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%title.confirm%'] : ENG['%title.confirm%'],
      subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
      buttons: [{
        text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.cancel%'] : ENG['%btn.cancel%'],
        handler:() =>{
          console.log('Click Cancel')
        }
      },
      {
        text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.confirm%'] : ENG['%btn.confirm%'],
        handler:() =>{
          this.saveData();
        }
      }]
    }).present();
  }

  async saveData() {
    try {
      this.moldinoutModel.ApproveRemark = this.form.controls['Remark'].value;

      if(this.moldinoutModel.ApproveType == 'out') {
        await this._moldinoutProvider.ApproveOrRejectOut();
      }else {
        await this._moldinoutProvider.ApproveOrRejectIn();
      }
      
      if(this.moldinoutModel.statment.Status)
      {
        this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
            handler: () =>{
              this.viewCtrl.dismiss().then( () => {
                this.popToPage('MoldinoutMenuPage', this.navCtrl);
              })
            }
          }]
        }).present();
      }
    } catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }
  }

  async onCancel() {
    this.viewCtrl.dismiss();
  }

}
