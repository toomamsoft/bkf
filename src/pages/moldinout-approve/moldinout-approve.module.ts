import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldinoutApprovePage } from './moldinout-approve';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoldinoutApprovePage,
  ],
  imports: [
    IonicPageModule.forChild(MoldinoutApprovePage),
    DirectivesModule,
    PipesModule,
    TranslateModule
  ],
  entryComponents: [
    MoldinoutApprovePage
  ]
})
export class MoldinoutApprovePageModule {}
