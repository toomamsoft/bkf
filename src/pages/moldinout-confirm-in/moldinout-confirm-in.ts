import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CONSTANT } from '../../shared/constant';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';

/**
 * Generated class for the MoldinoutConfirmInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moldinout-confirm-in',
  templateUrl: 'moldinout-confirm-in.html',
})
export class MoldinoutConfirmInPage extends BasePage implements OnInit {
  public moldinoutModel: MoldInOutModel
  public form: FormGroup;
  constructor(
    public navCtrl: NavController,
    public events: Events,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private _moldinoutProvider: MoldinoutProvider
    ) {
      super(events);
      this.form = this.formBuilder.group({
        Remark: ['']
      })
  }

  async ngOnInit() {
    this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoldinoutConfirmInPage');
  }

  async onConfirmIn() {
    try {
      this.moldinoutModel.ConfirmRemark = this.form.controls['Remark'].value;
      if(this.moldinoutModel.ConfirmStatus == 'N') {
        await this._moldinoutProvider.ConfirmOrRejectOut();
      } else {
        this.moldinoutModel.ConfirmStatus = 'Y';
        await this._moldinoutProvider.ConfirmIn();
      }
  
      this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
          handler: () =>{
            this.popToPage('MoldinoutMenuPage', this.navCtrl);
          }
        }]
      }).present();
    } catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }
  }

  async backPage() {
    this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
      subTitle: 'คุณต้องการยกเลิกรายการนี้ใช่ไหม',
      buttons: [{
        text: 'ยกลเลิก',
        handler: () => {
          console.log('cancel click')
        }
      },{
        text: 'ยืนยัน',
        handler: () => {
          this.popToPage('MoldinoutMenuPage', this.navCtrl);
        }
      }]
    }).present();
  }

}
