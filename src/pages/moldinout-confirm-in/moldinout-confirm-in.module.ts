import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldinoutConfirmInPage } from './moldinout-confirm-in';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoldinoutConfirmInPage,
  ],
  imports: [
    IonicPageModule.forChild(MoldinoutConfirmInPage),
    TranslateModule
  ],
  entryComponents: [
    MoldinoutConfirmInPage
  ]
})
export class MoldinoutConfirmInPageModule {}
