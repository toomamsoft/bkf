import { CONSTANT } from './../../shared/constant';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { BasePage } from '../../shared/basepage';
import { SurveyModel } from '../../models/SurveyModel';
import { SurveyProvider } from '../../providers/survey-provider/survey-provider';
import { SubMoldHistoryModel } from '../../models/package/SubMoldHistoryModel';
import { LocationSurveyAdapterModel } from '../../models/package/LocationSurveyAdapterModel';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ScanProvider } from '../../providers/scan/scan';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';
import { Utilities } from '../../shared/utilities';
import { StorageProvider } from '../../providers/storage-provider/storage-provider'

/**
 * Generated class for the SurveyCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-survey-create',
  templateUrl: 'survey-create.html',
})
export class SurveyCreatePage extends BasePage implements OnInit, OnDestroy{

  public surveyModel: SurveyModel;
  public Mode: string = 'Transaction'
  public dataLocation: LocationSurveyAdapterModel;
  public formTransaction: FormGroup;
  public formMold: FormGroup;
  private limitImage: number = 5;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public events: Events,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public _surveyProvider: SurveyProvider,
    public _scanProvider: ScanProvider,
    public _storageProvider: StorageProvider
    ) {
      super(events);
      this.formMold = this.formBuilder.group({
        Location: ['', [Validators.required, Validators.minLength(1)]],
        MoldId: ['', [Validators.required, Validators.minLength(8)]]
      })
      
      this.setFormTransaction();
  }
  async ngOnInit() {
    await Promise.all([this.surveyModel = this._surveyProvider.surVeyModel]);
    this.getLocation();
    this.limitImage = Number(await this._storageProvider.getKey(CONSTANT.IMAGE_MOLDSERVEY))
  }

  async ngOnDestroy() {
    this._surveyProvider.surVeyModel = new SurveyModel();
  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad SurveyCreatePage');
  }

  async getLocation() {
    try
    {
      await Promise.all([this._surveyProvider.getLocationSurvey()])
      this.dataLocation = this.surveyModel.locationDescription;
      this.surveyModel.Images = [];
    }
    catch (ex)
    {
      this._surveyProvider.submitErrorEvent(this._surveyProvider.surVeyModel2);
    } 
  }

  async getMoldMaster() {
    try {
      if(this.formMold.controls['MoldId'].value.length >= 8)
      {
        this.surveyModel.MoldId = this.formMold.controls['MoldId'].value;
        await Promise.all([this._surveyProvider.getMoldHistory()]);

        if (this.surveyModel.surveyMoldHistory.master.length > 0)
        {
          this.formTransaction.controls['AssetId'].setValue(this.surveyModel.surveyMoldHistory.master[0].AssetId || this.surveyModel.AssetId);
          this.formTransaction.controls['CustNo'].setValue(this.surveyModel.surveyMoldHistory.master[0].CustNo || this.surveyModel.CustNo);
          this.formTransaction.controls['PartNo'].setValue(this.surveyModel.surveyMoldHistory.master[0].PartNo || this.surveyModel.PartNo);
          this.formTransaction.controls['PartName'].setValue(this.surveyModel.surveyMoldHistory.master[0].PartName || this.surveyModel.PartName);
        }else if (this.surveyModel.surveyMoldHistory.master.length <= 0) {
          this.setFormTransaction();
          let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
            subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%error.moldmaster%'] : ENG['%error.moldmaster%'],
            buttons: [{
              text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
            }]
          });
          alert.present();
        }

        if(this.surveyModel.surveyMoldHistory.history.length > 0)
        {
          let alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
            subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.moldmaster%'] : ENG['%message.moldmaster%'],
            buttons: [{
              text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
            }]
          });
          alert.present();
        }
      }
    }
    catch (error) {
      this._surveyProvider.submitErrorEvent(this._surveyProvider.surVeyModel2);
    }
  }

  async preview() {
    if(this.formMold.controls['Location'].value === '')
    {
      let alert = this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%error.location%'] : ENG['%error.location%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
        }]
      });
      alert.present();
      return;
    }
    this.surveyModel.Location = this.formMold.controls['Location'].value;
    this.surveyModel.AssetId = this.formTransaction.controls['AssetId'].value;
    this.surveyModel.CustNo = this.formTransaction.controls['CustNo'].value;
    this.surveyModel.PartNo = this.formTransaction.controls['PartNo'].value;
    this.surveyModel.PartName = this.formTransaction.controls['PartName'].value;
    this.surveyModel.Remark = this.formTransaction.controls['Remark'].value;
    this.surveyModel.Plate = this.formTransaction.controls['NoPlate'].value === true ? 'No' : 'Yes';
    
    this.navCtrl.push('SurveyPreviewPage');
  }

  async reset() {
    this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
      subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.reset%'] : ENG['%message.reset%'],
      buttons: [{
        text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.no%'] : ENG['%btn.logout.no%'],
        handler: ()=>{
          console.log('Close Reset')
        }
      },{
        text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.yes%'] : ENG['%btn.logout.yes%'],
        handler: ()=>{
          this._surveyProvider.surVeyModel = new SurveyModel();
          this.surveyModel = this._surveyProvider.surVeyModel;
          this.formMold = this.formBuilder.group({
            Location: ['', [Validators.required]],
            MoldId: ['', [Validators.required, Validators.minLength(8)]]
          });
          this.formTransaction = this.formBuilder.group({
            AssetId: [null],
            CustNo: ['', [Validators.required]],
            PartNo: ['', [Validators.required]],
            PartName: [null],
            Plate: [false],
            NoPlate: [true],
            Remark: [null]
          });
          this.getLocation();
        }
      }]
    }).present()
  }

  async viewHistory(Items: SubMoldHistoryModel) {
    this.surveyModel.MoldId = Items.MoldId;
    this.surveyModel.RunNo = Items.RunNo;
    this.navCtrl.push('SurveyHistoryDetailPage');
  }

  async onChangePlate() {
    if(this.formTransaction.controls['NoPlate'].value) {
      this.formTransaction.controls['NoPlate'].setValue(false);
      this.formTransaction.controls['Plate'].setValue(true);
    }else if(!this.formTransaction.controls['Plate'].value && !this.formTransaction.controls['NoPlate'].value)
    {
      this.formTransaction.controls['Plate'].setValue(true);
    }
  }

  async onChangeNoPlate() {
    if(this.formTransaction.controls['Plate'].value) {
      this.formTransaction.controls['NoPlate'].setValue(true);
      this.formTransaction.controls['Plate'].setValue(false);
    }else if(!this.formTransaction.controls['Plate'].value && !this.formTransaction.controls['NoPlate'].value)
    {
      this.formTransaction.controls['NoPlate'].setValue(true);
    }
  }

  async scanQr() {
    try {
      let txtScanQR: string;
      await this._scanProvider.scan().then(val => {
        txtScanQR = val.toString();
      });
      if(txtScanQR){
        this.surveyModel.MoldId = txtScanQR.split(/\|/)[0] || '';
        this.surveyModel.AssetId = txtScanQR.split(/\|/)[1] || '';
        this.surveyModel.CustNo = txtScanQR.split(/\|/)[2] || '';
        this.surveyModel.PartNo = txtScanQR.split(/\|/)[3] || '';
        this.surveyModel.PartName = txtScanQR.split(/\|/)[4] || '';
        this.formMold.controls['MoldId'].setValue(txtScanQR.split(/\|/)[0] || '');
        this.getMoldMaster();
      }
    } catch (ex) {
      console.log(ex);
    }
  }

  async getImageFromCamera() {

    if(this.surveyModel.Images.length >= this.limitImage)
    {
      let alert = this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.photo%'] : ENG['%message.photo%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
        }]
      });
      alert.present();
      return;
    }

    await this._scanProvider.getImageFromCamere().then(val => {
      if(val && val != 'No Image Selected' && val != 'cordova_not_available') {
        this.surveyModel.Images.push(val);
      }
    });
  }

  async getImageFromAlbum() {
    let images: any;
    if(this.surveyModel.Images.length >= this.limitImage)
    {
      let alert = this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.photo%'] : ENG['%message.photo%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
        }]
      });
      alert.present();
      return;
    }

    await this._scanProvider.getImageFormAlbum(this.limitImage - this.surveyModel.Images.length).then(val => {
      if(val && val != 'cordova_not_available') {
        images = val;
        for(let i = 0; i < images.length; i++){
          this.surveyModel.Images.push(images[i]);
        }
      }
    });
  }

  async deletePhoto(index) {
    let confirm = this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
      subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.delete.image%'] : ENG['%message.delete.image%'],
      buttons: [
        {
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.no%'] : ENG['%btn.logout.no%'],
          handler: () => {
            console.log('Disagree clicked');
          }
        }, {
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.yes%'] : ENG['%btn.logout.yes%'],
          handler: () => {
            this.surveyModel.Images.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  async viewImages(imageUrl: string = '', title: string = '') {
    Utilities.viewImages(imageUrl,title);
  }

  async setFormTransaction()
  {
    this.formTransaction = this.formBuilder.group({
      AssetId: [null],
      CustNo: ['', [Validators.required]],
      PartNo: ['', [Validators.required]],
      PartName: [null],
      Plate: [false],
      NoPlate: [true],
      Remark: [null]
    });
  }
}
