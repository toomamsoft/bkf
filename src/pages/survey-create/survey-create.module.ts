import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurveyCreatePage } from './survey-create';
import { ComponentsModule } from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SurveyCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(SurveyCreatePage),
    DirectivesModule,
    PipesModule,
    ComponentsModule,
    TranslateModule
  ],
  entryComponents: [
    SurveyCreatePage,
  ]
})
export class SurveyCreatePageModule {}
