import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActiveMobilePage } from './active-mobile';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ActiveMobilePage,
  ],
  imports: [
    IonicPageModule.forChild(ActiveMobilePage),
    TranslateModule
  ],
  entryComponents: [
    ActiveMobilePage
  ]
})
export class ActiveMobilePageModule {}
