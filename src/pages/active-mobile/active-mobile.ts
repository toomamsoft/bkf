import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { Utilities } from '../../shared/utilities';
import { StorageProvider } from '../../providers/storage-provider/storage-provider';
import { LoginProvider } from '../../providers/login-provider/login-provider';
import { LoginModel } from '../../models/LoginModel';
import { CONSTANT } from '../../shared/constant';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the ActiveMobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-active-mobile',
  templateUrl: 'active-mobile.html',
})
export class ActiveMobilePage extends BasePage implements OnInit {
  public loginModel: LoginModel;
  public IMEI: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public events: Events,
    public translate: TranslateService,
    public alertCtrl: AlertController,
    private storageProvider: StorageProvider,
    private _loginProvider: LoginProvider
    ) {
      super(events);
      this.intitialLanguage(this.translate);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActiveMobilePage');
  }

  async ngOnInit() { 
    // this.setLanguage(CONSTANT.APP_LANGUAGE);
    this.IMEI = await Utilities.IMEI();
    this.loginModel = this._loginProvider.loginModel;
  }

  async ActiveMobile() {
    try {
      await this._loginProvider.deviceAllow();
      if(this.loginModel.authenticateUser.deviceAllow.length > 0) {
        this.navCtrl.setRoot('LoginPage');
      }else {
        this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: '',
          inputs: [{
            type: 'password',
            name: 'keyActive',
          }],
          buttons: [{
            text: 'Cancel',
            handler: () => {
              console.log('Cancel')
            }
          }, {
            text: 'Active',
            handler: (data) => {
              this.loginModel.keyActive = data.keyActive;
              this.SendActiveMobile();
            }
          }]
        }).present();
      }
    } catch (ex) {
      this._loginProvider.submitErrorEvent(this._loginProvider.loginModel2);
    }
  }

  async SendActiveMobile() {
    try {
      if(this.loginModel.keyActive  == '') {
        this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.active.password%'] : ENG['%message.active.password%'],
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
          }]
        }).present();
        return;
      }
      await this._loginProvider.activeMobile();
      if(this.loginModel.authenticateUser.status == 'Active') {
        this.storageProvider.setKey(CONSTANT.ACTIVE_MOBILE, 'ACTIVE');
        this.navCtrl.push('LoginPage');
      }else {
        this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.active.error%'] : ENG['%message.active.error%'],
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
          }]
        }).present();
      }
    } catch (ex) {
      this._loginProvider.submitErrorEvent(this._loginProvider.loginModel2);
    }
  }

}
