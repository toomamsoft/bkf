import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldinoutWaitingForApprovePage } from './moldinout-waiting-for-approve';

@NgModule({
  declarations: [
    MoldinoutWaitingForApprovePage,
  ],
  imports: [
    IonicPageModule.forChild(MoldinoutWaitingForApprovePage),
  ],
  entryComponents: [
    MoldinoutWaitingForApprovePage
  ]
})
export class MoldinoutWaitingForApprovePageModule {}
