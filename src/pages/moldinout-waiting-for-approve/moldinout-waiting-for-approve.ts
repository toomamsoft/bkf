import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';
import { SubMoldInOutWatingModel } from '../../models/package/SubMoldInOutWatingModel';

/**
 * Generated class for the MoldinoutWaitingForApprovePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moldinout-waiting-for-approve',
  templateUrl: 'moldinout-waiting-for-approve.html',
})
export class MoldinoutWaitingForApprovePage extends BasePage implements OnInit {
  public moldinoutModel: MoldInOutModel;
  constructor(public navCtrl: NavController,
    public events: Events,
    private _moldinoutProvider: MoldinoutProvider
    ) {
      super(events);
  }

  async ionViewDidEnter() {
    console.log('ionViewDidLoad MoldinoutWaitingForApprovePage');
    try {
      await this._moldinoutProvider.getWaitingApprove();
    }catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }
  }

  async ngOnInit() {
    this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
  }

  async viewHistory(itemSelected: SubMoldInOutWatingModel) {
    try {
      this.moldinoutModel.JobId = itemSelected.jobid;
      this.moldinoutModel.MoldId = itemSelected.moldid;
      await this._moldinoutProvider.getNewMoldOut();
      this.navCtrl.push('MoldinoutHistoryPage');
    }catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }
  }
}
