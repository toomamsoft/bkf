import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminUserMoldPage } from './admin-user-mold';

@NgModule({
  declarations: [
    AdminUserMoldPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminUserMoldPage),
  ],
  entryComponents: [
    AdminUserMoldPage
  ]
})
export class AdminUserMoldPageModule {}
