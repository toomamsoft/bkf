import { SubAdminGetListUserMold } from './../../models/package/SubAdminGetListUserMold';
import { BasePage } from './../../shared/basepage';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { AdminProvider } from '../../providers/admin-provider/admin-provider';
import { AdminModel } from '../../models/AdminModel';

/**
 * Generated class for the AdminUserMoldPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-user-mold',
  templateUrl: 'admin-user-mold.html',
})
export class AdminUserMoldPage extends BasePage implements OnInit, OnDestroy {

  public adminModel: AdminModel;

  constructor(
    public navCtrl: NavController, 
    public events: Events,
    private _adminProvider: AdminProvider) {
    super(events)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminUserMoldPage');
  }

  ngOnInit() {
    this.adminModel = this._adminProvider.adminModel;
  }

  ngOnDestroy() {
    this._adminProvider.adminModel = new AdminModel();
  }

  async ionViewDidEnter(){
    try {
      await this._adminProvider.getListUserMold();
    }catch (ex) {
      this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
    }
  }

  viewHistory(itemSelected: SubAdminGetListUserMold) {
    // this.adminModel.bkfSelected = itemSelected;
    // this.navCtrl.push('AdminEditUserPage');
  }
}
