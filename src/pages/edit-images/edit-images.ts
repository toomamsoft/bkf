import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, ActionSheetController, Platform } from 'ionic-angular';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';

/**
 * Generated class for the EditImagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-images',
  templateUrl: 'edit-images.html',
})
export class EditImagesPage implements OnInit {
  public order: boolean = false;
  public  moldinoutModel: MoldInOutModel;
  constructor(
    public navCtrl: NavController,
    private platform: Platform,
    private actionsheetCtrl : ActionSheetController,
    private _moldinoutProvider: MoldinoutProvider
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditImagesPage');
  }

  async ngOnInit() {
    this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
  }

  presentActionSheet() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Albums',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'จัดเรียงรูปภาพ',
          icon: !this.platform.is('ios') ? 'albums' : null,
          handler: () => {
            this.order = true;
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  reorderItems(indexes) {
    let element = this.moldinoutModel.Images[indexes.from];
    this.moldinoutModel.Images.splice(indexes.from, 1);
    this.moldinoutModel.Images.splice(indexes.to, 0, element);
  }

  selectImage(imageSelected:{photo:string, latitude:number, longitude: number}, index) {
    this.moldinoutModel.ImageSelectedName = imageSelected.photo;
    this.moldinoutModel.ImageSelectedIndex = index;
    this.navCtrl.push('DrawingImagePage');
  }

}
