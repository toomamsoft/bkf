import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditImagesPage } from './edit-images';

@NgModule({
  declarations: [
    EditImagesPage,
  ],
  imports: [
    IonicPageModule.forChild(EditImagesPage),
  ],
  entryComponents: [
    EditImagesPage
  ]
})
export class EditImagesPageModule {}
