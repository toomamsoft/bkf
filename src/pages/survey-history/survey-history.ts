import { BasePage } from './../../shared/basepage';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { SurveyProvider } from '../../providers/survey-provider/survey-provider';
import { SurveyModel } from '../../models/SurveyModel';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Utilities } from '../../shared/utilities';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';
import { CONSTANT } from '../../shared/constant';
import { ScanProvider } from '../../providers/scan/scan';
import { SubMoldSearchModel } from './../../models/package/SubMoldSearchModel';

/**
 * Generated class for the SurveyHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-survey-history',
  templateUrl: 'survey-history.html',
})
export class SurveyHistoryPage extends BasePage implements OnInit, OnDestroy {

  public form: FormGroup;
  public surveyModel: SurveyModel;
  public Mode: string = 'formMold'

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public event: Events,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public _surveyProvider: SurveyProvider,
    public _scanProvider: ScanProvider
    ) {
      super(event)

      this.form = this.formBuilder.group({
        MoldId: [''],
        fromDate: [null, [Validators.required]],
        toDate : [null, [Validators.required]]
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SurveyHistoryPage');
  }

  async ngOnInit() {
    await Promise.all([this.surveyModel = this._surveyProvider.surVeyModel]);
    this.form.controls['fromDate'].setValue(Utilities.getCurrentDateTimeMySql());
    this.form.controls['toDate'].setValue(Utilities.getCurrentDateTimeMySql())
  }

  async ngOnDestroy() {
    this._surveyProvider.surVeyModel = new SurveyModel();
  }

  async onSearchByMold()
  {
    if (this.form.controls['MoldId'].value == '') {
      let alert = this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.serach.mold.error%'] : ENG['%message.serach.mold.error%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
        }]
      });
      alert.present();
      return;
    }

    this.surveyModel.MoldId = this.form.controls['MoldId'].value;
    this.surveyModel.From = '';
    this.surveyModel.To = '';

    this.onSearchHistory()
  }

  async onSearchByDate() {
    this.surveyModel.MoldId = '';
    this.surveyModel.From = this.form.controls['fromDate'].value;
    this.surveyModel.To = this.form.controls['toDate'].value;
    this.onSearchHistory();
  }

  async scan() {
    this.form.controls['MoldId'].setValue(await this._scanProvider.scan());
    if (this.form.controls['MoldId'].value == '') {
      return;
    }
    this.surveyModel.MoldId = this.form.controls['MoldId'].value;
    this.surveyModel.From = '';
    this.surveyModel.To = '';
    this.onSearchHistory();
  }

  async onSearchHistory()
  {
    try {
      await Promise.all([this._surveyProvider.getSearchSurvey()]);

      if (this.surveyModel.surveyMoldHistory.search.length <= 0) {
        let alert = this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.notfound%'] : ENG['%message.notfound%'],
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
          }]
        });
        alert.present();
      }
    } catch (ex) {
      this._surveyProvider.submitErrorEvent(this._surveyProvider.surVeyModel2);
    }
  }

  async viewHistory(Items: SubMoldSearchModel) {
    this.surveyModel.MoldId = Items.MoldId;
    this.surveyModel.RunNo = Items.RunNo;
    this.navCtrl.push('SurveyHistoryDetailPage');
  }

}
