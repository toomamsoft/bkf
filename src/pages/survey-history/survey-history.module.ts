import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurveyHistoryPage } from './survey-history';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SurveyHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(SurveyHistoryPage),
    PipesModule,
    TranslateModule
  ],
  entryComponents:[
    SurveyHistoryPage
  ]
})
export class SurveyHistoryPageModule {}
