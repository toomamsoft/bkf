import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldinoutConfirmOutPage } from './moldinout-confirm-out';
import { PipesModule } from '../../pipes/pipes.module';
import { DirectivesModule } from '../../directives/directives.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoldinoutConfirmOutPage,
  ],
  imports: [
    IonicPageModule.forChild(MoldinoutConfirmOutPage),
    DirectivesModule,
    PipesModule,
    TranslateModule
  ],
  entryComponents: [
    MoldinoutConfirmOutPage
  ]
})
export class MoldinoutConfirmOutPageModule {}
