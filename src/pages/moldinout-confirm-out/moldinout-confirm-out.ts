import { ScanProvider } from './../../providers/scan/scan';
import { CONSTANT } from './../../shared/constant';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events, AlertController } from 'ionic-angular';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation, GeolocationOptions } from '@ionic-native/geolocation';
import { BasePage } from '../../shared/basepage';
import { Utilities } from '../../shared/utilities';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';
import { StorageProvider } from '../../providers/storage-provider/storage-provider';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';


/**
 * Generated class for the MoldinoutConfirmOutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
const gpsOptions : GeolocationOptions = {
  enableHighAccuracy : true
};

@IonicPage()
@Component({
  selector: 'page-moldinout-confirm-out',
  templateUrl: 'moldinout-confirm-out.html',
})
export class MoldinoutConfirmOutPage extends BasePage implements OnInit {

  public form: FormGroup;
  public moldinoutModel: MoldInOutModel;
  public FullName: any = ''
  public CreateDate: string = Utilities.getCurrentDateTimeMySql() || ''; 
  Latitude: number = 0.000000000000000;
  Longitude: number = 0.000000000000000;
  tmpFile1: any = [];
  tmpFile2: any = [];
  tmpFile3: any = [];
  tmpFile4: any = [];

  constructor(
      public navCtrl: NavController,
      public events: Events,
      private locationAccuracy: LocationAccuracy,
      private geolocation: Geolocation,
      private alertCtrl: AlertController,
      private formBuilder: FormBuilder,
      private _moldinoutProvider: MoldinoutProvider,
      private _storageProvider: StorageProvider,
      private _scanProvider: ScanProvider
    ) {
      super(events);
      this.form = this.formBuilder.group({
        Remark: ['']
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoldinoutConfirmOutPage');
  }

  async ngOnInit() {
    this.FullName = await this._storageProvider.getKey(CONSTANT.USER_NAME);
    this.moldinoutModel = this._moldinoutProvider.moldInOutModel;

    this.locationAccuracy.canRequest().then((canRequest: boolean) => {

      if(canRequest) {
        // the accuracy option will be ignored by iOS
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_LOW_POWER).then(
          () => {
            console.log('requesting location permissions success');
          },
          error => console.log('Error requesting location permissions', error)
        );
      }
    }).catch(error => {
      const alert = this.alertCtrl.create({
        title: 'Attention!',
        subTitle: error,
        buttons: ['Close']
      });
      alert.present();
    });

    this.geolocation.getCurrentPosition(gpsOptions).then(resp => {
      console.log('requesting geolocation:::', resp);
      this.Latitude = resp.coords.latitude;
      this.Longitude = resp.coords.longitude;
    });
  }

  async openCamera(type) {
    await this._scanProvider.getImageFromCamere().then(val => {
      if(val && val != 'No Image Selected' && val != 'cordova_not_available') {
        if(type == '1')
        {
          this.tmpFile1.push({photo: val.toString(), latitude: this.Latitude, longitude: this.Longitude});
        }else if(type == '2')
        {
          this.tmpFile2.push({photo: val.toString(), latitude: this.Latitude, longitude: this.Longitude});
        }else if(type == '3')
        {
          this.tmpFile3.push({photo: val.toString(), latitude: this.Latitude, longitude: this.Longitude});
        }else if(type == '4')
        {
          this.tmpFile4.push({photo: val.toString(), latitude: this.Latitude, longitude: this.Longitude});
        }
      }
    });
  }

  async deletePhoto(type){
    let confirm = this.alertCtrl.create({
      title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
      subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.delete.image%'] : ENG['%message.delete.image%'],
      buttons: [
        {
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.no%'] : ENG['%btn.logout.no%'],
          handler: () => {
            console.log('Disagree clicked');
          }
        }, {
          text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.logout.yes%'] : ENG['%btn.logout.yes%'],
          handler: () => {
            if(type == '1'){
              this.tmpFile1 = []
            }else if(type == '2'){
              this.tmpFile2 = []
            }else if(type == '3'){
              this.tmpFile3 = []
            }else if(type == '4'){
              this.tmpFile4 = []
            }
          }
      }]
    });
    confirm.present();
  }

  async saveData() {
    try {
      this.moldinoutModel.ConfirmRemark = this.form.controls['Remark'].value;
      this.moldinoutModel.ConfirmStatus = 'Y';
      await this._moldinoutProvider.ConfirmOrRejectOut();

      await Utilities.viewFilePDF(this.moldinoutModel.statment.pdf);
      
      this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
          handler: () =>{
            this.popToPage('MoldinoutMenuPage', this.navCtrl);
          }
        }]
      }).present();
    } catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }    
  }

  async ConfirmSaveData()
  {
    this.moldinoutModel.Images = [];
    this.tmpFile4.length > 0 ? this.moldinoutModel.Images.push({photo: this.tmpFile1[0].photo, latitude: this.tmpFile1[0].latitude, longitude: this.tmpFile1[0].longitude}) : '';
    this.tmpFile2.length > 0 ? this.moldinoutModel.Images.push({photo: this.tmpFile2[0].photo, latitude: this.tmpFile2[0].latitude, longitude: this.tmpFile2[0].longitude}) : '';
    this.tmpFile3.length > 0 ? this.moldinoutModel.Images.push({photo: this.tmpFile3[0].photo, latitude: this.tmpFile3[0].latitude, longitude: this.tmpFile3[0].longitude}) : '';
    this.tmpFile4.length > 0 ? this.moldinoutModel.Images.push({photo: this.tmpFile4[0].photo, latitude: this.tmpFile4[0].latitude, longitude: this.tmpFile4[0].longitude}) : '';

    if(this.moldinoutModel.Images.length < 4)
    {
      this.alertCtrl.create({
        title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: 'จำนวนรูปภาพไม่ครบ กรุณาตรวจสอบอีกครั้ง',
        buttons: ['OK']
      }).present();
      return
    }

    this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%title.confirm%'] : ENG['%title.confirm%'],
      subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
      buttons: [{
        text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.cancel%'] : ENG['%btn.cancel%'],
        handler:() =>{
          console.log('Click Cancel')
        }
      },
      {
        text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.confirm%'] : ENG['%btn.confirm%'],
        handler:() =>{
          this.saveData();
        }
      }]
    }).present();
  }

  async backPage() {
    this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
      subTitle: 'คุณต้องการยกเลิกรายการนี้ใช่ไหม',
      buttons: [{
        text: 'ยกลเลิก',
        handler: () => {
          console.log('cancel click')
        }
      },{
        text: 'ยืนยัน',
        handler: () => {
          this.popToPage('MoldinoutMenuPage', this.navCtrl);
        }
      }]
    }).present();
  }

  async tapViewPhoto(imageBase64) {
    Utilities.viewImages(imageBase64);
  }

}
