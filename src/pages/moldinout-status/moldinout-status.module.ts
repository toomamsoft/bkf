import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldinoutStatusPage } from './moldinout-status';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoldinoutStatusPage,
  ],
  imports: [
    IonicPageModule.forChild(MoldinoutStatusPage),
    ComponentsModule,
    PipesModule,
    TranslateModule
  ],
  entryComponents: [
    MoldinoutStatusPage
  ]
})
export class MoldinoutStatusPageModule {}
