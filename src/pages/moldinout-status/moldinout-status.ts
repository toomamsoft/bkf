import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, Events, AlertController } from 'ionic-angular';
import { BasePage } from '../../shared/basepage';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';
import { CONSTANT } from '../../shared/constant';
import { SubMoldInOutStatusModel } from '../../models/package/SubMoldInOutStatusModel';

/**
 * Generated class for the MoldinoutStatusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moldinout-status',
  templateUrl: 'moldinout-status.html',
})
export class MoldinoutStatusPage extends BasePage implements OnInit, OnDestroy {
  public dataStatus: Array<{id:string, description:string}> = [];
  public molInOutModel: MoldInOutModel;
  public dataResult: SubMoldInOutStatusModel[];
  public form: FormGroup;
  constructor(
    public navCtrl: NavController,
    public events: Events,
    private alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    private _moldinoutProvider: MoldinoutProvider
    ) {
      super(events);

      this.form = this.formBuilder.group({
        Status: ['', [Validators.required]]
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoldinoutStatusPage');
  }

  async ngOnInit() {
    this.molInOutModel = this._moldinoutProvider.moldInOutModel;
    // this.dataResult = this.molInOutModel.getStatus;
    this.dataStatus = [{
      id: '', description:'Please select Status'
    },{
      id: '1', description: 'Waiting For Approve Out'
    },{
      id: '2', description: 'Waiting For Tooling Pickup'
    },{
      id: '3', description: 'Processing For Transportation'
    },{
      id: '4', description: 'Tooling Receive Completed'
    },{
      id: '5', description: 'Waiting For Approve In'
    },{
      id: '6', description: 'Tooling Approve Completed'
    },{
      id: '7', description: 'Tooling Reject&Cancel'
    },{
      id: '8', description: 'Tooling Completed'
    }];
  }

  async ngOnDestroy() {
    this._moldinoutProvider.moldInOutModel = new MoldInOutModel();
  }

  async onSelectStatus() {
    try {
      if(this.form.controls['Status'].value == ''){
        return;
      }
      this.dataResult = [];
      this._moldinoutProvider.moldInOutModel = new MoldInOutModel();
      this.molInOutModel = this._moldinoutProvider.moldInOutModel;
      this.molInOutModel.statusId = this.form.controls['Status'].value;
      await this._moldinoutProvider.moldStatus();
      if (this.molInOutModel.getStatus.moldStatus.length <= 0) {
        let alert = this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.notfound%'] : ENG['%message.notfound%'],
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
          }]
        });
        alert.present();
      }else {
        this.dataResult = this.molInOutModel.getStatus.moldStatus;
      }
    } catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }
  }

  async viewHistory(itemSelected: SubMoldInOutStatusModel) {    
    try {
      this._moldinoutProvider.moldInOutModel = new MoldInOutModel();
      this.molInOutModel = this._moldinoutProvider.moldInOutModel;
      this.molInOutModel.MoldId = itemSelected.moldId;
      this.molInOutModel.JobId = itemSelected.jobId;
      await this._moldinoutProvider.getNewMoldOut();
      this.navCtrl.push('MoldinoutHistoryPage');
    }catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }
    
  }

}
