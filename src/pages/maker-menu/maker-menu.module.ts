import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MakerMenuPage } from './maker-menu';

@NgModule({
  declarations: [
    MakerMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(MakerMenuPage),
  ],
})
export class MakerMenuPageModule {}
