import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Refresher, AlertController } from 'ionic-angular';
import { MoldMakerModel } from '../../models/MoldMakerModel';
import { MoldmakerProvider } from '../../providers/moldmaker-provider/moldmaker-provider';
import { ScanProvider } from '../../providers/scan/scan';
import { StorageProvider } from '../../providers/storage-provider/storage-provider';
import { CONSTANT } from '../../shared/constant';

/**
 * Generated class for the MakerMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-maker-menu',
  templateUrl: 'maker-menu.html',
})
export class MakerMenuPage extends BasePage implements OnInit {
  public moldMakerModel: MoldMakerModel;
  public fullName: any = '';
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public events: Events,
    private alertCtrl: AlertController,
    private _moldmakerProvider: MoldmakerProvider,
    private _scanProvider: ScanProvider,
    private _storageProvider : StorageProvider
    ) {
      super(events);
  }

  async ionViewWillEnter() {
    console.log('ionViewDidLoad MakerMenuPage');
    this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
    try {
      await this._moldmakerProvider.getListMold();
    }catch (ex) {
      this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
    }
  }

  async ngOnInit() {
    this.fullName = await this._storageProvider.getKey(CONSTANT.USER_NAME);
    this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
  }

  async doRefresh(event: Refresher)
  {
    try {
      this._moldmakerProvider.moldmakerModel = new MoldMakerModel();
      this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
      await this._moldmakerProvider.getListMold();
    }catch (ex)
    {
      this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
    }finally
    {
      event.complete();
    }
  }

  async viewHistory(jobid) {
    try {
      this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
      this.moldMakerModel.JobId = jobid;
      await this._moldmakerProvider.getViewMoldFromBKF()
      this.navCtrl.push('MoldmakerHistoryPage');
    }catch (e) {
      this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
    }
  }

  async ReceiveMold(jobid, moldid) {
    try {
      let txtScanQR: string;
      await this._scanProvider.scan().then(val => {
        txtScanQR = val.toString();
      });
      if(txtScanQR){
        this.moldMakerModel.MoldId = txtScanQR.split(/\|/)[0] || '';
        this.moldMakerModel.AssetId = txtScanQR.split(/\|/)[1] || '';
        this.moldMakerModel.CustNo = txtScanQR.split(/\|/)[2] || '';
        this.moldMakerModel.PartNo = txtScanQR.split(/\|/)[3] || '';
        this.moldMakerModel.PartName = txtScanQR.split(/\|/)[4] || '';

        if(moldid != this.moldMakerModel.MoldId) {
          this.alertCtrl.create({
            title: 'Alert',
            message: 'Mold ID: '+ this.moldMakerModel.MoldId + '  Not match',
            buttons: ['OK']
          }).present();
          return;
        }else {
          this.moldMakerModel.JobId = jobid;
          await this._moldmakerProvider.getViewMoldFromBKF().catch( () => {
            this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
          });
          this.navCtrl.push('MoldmakerReceivePage');
        }
      }
    } catch (ex) {
      console.log(ex);
    }
  }

}