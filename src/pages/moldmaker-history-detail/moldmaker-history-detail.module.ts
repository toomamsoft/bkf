import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldmakerHistoryDetailPage } from './moldmaker-history-detail';

@NgModule({
  declarations: [
    MoldmakerHistoryDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MoldmakerHistoryDetailPage),
  ],
})
export class MoldmakerHistoryDetailPageModule {}
