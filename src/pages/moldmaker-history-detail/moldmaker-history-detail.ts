import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { BasePage } from '../../shared/basepage';
import { MoldMakerModel } from '../../models/MoldMakerModel';
import { MoldmakerProvider } from '../../providers/moldmaker-provider/moldmaker-provider';
import { Utilities } from '../../shared/utilities';

/**
 * Generated class for the MoldmakerHistoryDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moldmaker-history-detail',
  templateUrl: 'moldmaker-history-detail.html',
})
export class MoldmakerHistoryDetailPage extends BasePage implements OnInit{

  public moldmakerModel : MoldMakerModel;
  
  constructor(public navCtrl: NavController,
    public events: Events,
    private _moldmakerProvider : MoldmakerProvider,
  ) {
    super(events)
  }

  async ngOnInit() {
    this.moldmakerModel = this._moldmakerProvider.moldmakerModel;
    
  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad MoldmakerHistoryDetailPage');
  }

  async viewImages(photo) {
    Utilities.viewImages(photo)
  }

}
