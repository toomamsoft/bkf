import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminUserListPage } from './admin-user-list';

@NgModule({
  declarations: [
    AdminUserListPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminUserListPage),
  ],
  entryComponents: [
    AdminUserListPage
  ]
})
export class AdminUserListPageModule {}
