import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { Utilities } from '../../shared/utilities';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';
import { CONSTANT } from '../../shared/constant';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';

/**
 * Generated class for the MoldinoutPreviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moldinout-preview',
  templateUrl: 'moldinout-preview.html',
})
export class MoldinoutPreviewPage extends BasePage implements OnInit {
  public form: FormGroup;
  public moldinoutModel: MoldInOutModel;
  public Mode: string = 'Out'
  constructor(
    public navCtrl: NavController,
    public events: Events,
    private alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    private _moldinoutProvider: MoldinoutProvider,
    ) {
    super(events);
    this.form = this.formBuilder.group({
      MoldId: [{value: null, disabled: true}, [Validators.required]],
      MoldMaker: [{value: null, disabled: true}],
      AssetId: [{value: null, disabled: true}],
      CustNo: [{value: null, disabled: true}],
      PartNo: [{value: null, disabled: true}],
      PartName: [{value: null, disabled: true}],
      Remark: [{value: null, disabled: true}],
      MakerId: [{value: null, disabled: true}],
      ReasonId: [{value: null, disabled: true}],
      PlanIn: [{value: null, disabled: true}],
      PlanOut: [{value: null, disabled: true}]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoldinoutPreviewPage');
  }
  async ngOnInit() {
    let AssetId:any = '';
    let CustNo:any = '';
    let PartNo:any = '';
    let PartName:any = '';
    this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
    this.form.controls['MoldId'].setValue(this.moldinoutModel.MoldId);
    this.form.controls['MoldMaker'].setValue(this.moldinoutModel.MoldMaker);
    for(let i = 0; i < this.moldinoutModel.getNewMoldInOut.inoutMold[0].description.length; i++){
      if(i == 0) {
        AssetId = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].AssetId;
        CustNo = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].CustNo;
        PartNo = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartNo;
        PartName = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartName;
      } else {
        AssetId += '\n'+ this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].AssetId;
        CustNo += '\n'+ this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].CustNo;
        PartNo += '\n'+ this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartNo;
        PartName += '\n'+ this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartName;
      }
    }
    this.form.controls['AssetId'].setValue(AssetId);
    this.form.controls['CustNo'].setValue(CustNo);
    this.form.controls['PartNo'].setValue(PartNo);
    this.form.controls['PartName'].setValue(PartName);
    this.form.controls['Remark'].setValue(this.moldinoutModel.Remark);
    this.form.controls['MakerId'].setValue(this.moldinoutModel.MakerId);
    this.form.controls['ReasonId'].setValue(this.moldinoutModel.ReasonId);
    this.form.controls['PlanIn'].setValue(this.moldinoutModel.PlanIn);
    this.form.controls['PlanOut'].setValue(this.moldinoutModel.PlanOut);

  }

  async tapViewPhoto(imageBase64) {
    Utilities.viewImages(imageBase64);
  }

  async ConfirmSaveData()
  {
    this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%title.confirm%'] : ENG['%title.confirm%'],
      subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
      buttons: [{
        text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.cancel%'] : ENG['%btn.cancel%'],
        handler:() =>{
          console.log('Click Cancel')
        }
      },
      {
        text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.confirm%'] : ENG['%btn.confirm%'],
        handler:() =>{
          this.saveData();
        }
      }]
    }).present();
  }

  async saveData() {
    try {
      await this._moldinoutProvider.saveDataNewMold();
      this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
          handler: () =>{
            this.popToPage('MoldinoutMenuPage', this.navCtrl);
          }
        }]
      }).present();
    }catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }
  }

  async backToPage() {
    this.navCtrl.pop();
  }

}
