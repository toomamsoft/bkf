import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldinoutPreviewPage } from './moldinout-preview';
import { ComponentsModule } from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoldinoutPreviewPage,
  ],
  imports: [
    IonicPageModule.forChild(MoldinoutPreviewPage),
    ComponentsModule,
    DirectivesModule,
    TranslateModule
  ],
  entryComponents: [
    MoldinoutPreviewPage
  ]
})
export class MoldinoutPreviewPageModule {}
