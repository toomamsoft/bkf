import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events, AlertController } from 'ionic-angular';
import { AdminModel } from '../../models/AdminModel';
import { AdminProvider } from '../../providers/admin-provider/admin-provider';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';
import { CONSTANT } from '../../shared/constant';

/**
 * Generated class for the AdminEditUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-edit-user',
  templateUrl: 'admin-edit-user.html',
})
export class AdminEditUserPage extends BasePage implements OnInit {

  public adminModel: AdminModel;
  public form: FormGroup;

  public position: any = [{id: 1, value: 'Staff'},{id: 2, value: 'Manager'}, {id: 3, value: 'Department'},{id: 4, value: 'Division'}];

  constructor(
    public navCtrl: NavController,
    public events: Events,
    private formBuilder:FormBuilder,
    private alertCtrl: AlertController,
    private _adminProvider: AdminProvider) {
      super(events);
      this.form = this.formBuilder.group({
        EmpID: [{value: null, disabled: true}],
        FirstName: [null, [Validators.required]],
        LastName: [null, [Validators.required]],
        UserType: [null, [Validators.required]],
        UserGroup: [null,],
        Username: [{value: null, disabled: true}],
        Password:[null,[Validators.required]]
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminEditUserPage');
  }

  ngOnInit() {
    this.adminModel = this._adminProvider.adminModel;
    this.form.controls['FirstName'].setValue(this.adminModel.bkfSelected.firstName);
    this.form.controls['LastName'].setValue(this.adminModel.bkfSelected.lastName);
    this.form.controls['Username'].setValue(this.adminModel.bkfSelected.username);
    this.form.controls['Password'].setValue(this.adminModel.bkfSelected.password);
    this.form.controls['EmpID'].setValue(this.adminModel.bkfSelected.empId);
    this.form.controls['UserType'].setValue(this.adminModel.bkfSelected.userType);
    this.form.controls['UserGroup'].setValue(this.adminModel.bkfSelected.userGroup);
  }

  reset() {

  }

  async editAccount() {
    try {
      this.adminModel.empid = this.form.controls['EmpID'].value;
      this.adminModel.firstname = this.form.controls['FirstName'].value;
      this.adminModel.lastname = this.form.controls['LastName'].value;
      this.adminModel.position = this.form.controls['UserType'].value;
      this.adminModel.usergroup = this.form.controls['UserGroup'].value;
      this.adminModel.username = this.form.controls['Username'].value;
      this.adminModel.password = this.form.controls['Password'].value;
      this.adminModel.flag = 'U';
      await this._adminProvider.getUpdateUserBKF();
      this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
          handler: () =>{
            // this.popToPage('SurveyMenuPage', this.navCtrl);
            this.navCtrl.pop();
          }
        }]
      }).present();
    } catch (ex) {
      this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
    }
  }

  async deleteAccount() {
    try {
      this.adminModel.empid = this.form.controls['EmpID'].value;
      this.adminModel.flag = 'D';
      
      this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: 'คุณต้องการลบผู้ใช้งานนี้ใช่ไหม',//CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
        buttons: [{
          text: 'ไม่',
        },{
          text: 'ใช่',//CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
          handler: () =>{
            this.delete();
          }
        }]
      }).present();
    } catch (ex) {
      this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
    }
  }

  async delete() {
    try { 
      await this._adminProvider.getUpdateUserBKF();
      this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.delete.success%'] : ENG['%message.delete.success%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
          handler: () =>{
            this.navCtrl.pop();
          }
        }]
      }).present();

    } catch (ex) {
      this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
    }
  }

}
