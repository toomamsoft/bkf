import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminEditUserPage } from './admin-edit-user';

@NgModule({
  declarations: [
    AdminEditUserPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminEditUserPage),
  ],
  entryComponents:[
    AdminEditUserPage
  ]
})
export class AdminEditUserPageModule {}
