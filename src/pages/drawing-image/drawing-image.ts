import { Component, ViewChild, Renderer, OnInit } from '@angular/core';
import { IonicPage, NavController, Platform, Content } from 'ionic-angular';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';
// import { CONSTANT } from '../../shared/constant';

/**
 * Generated class for the DrawingImagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-drawing-image',
  templateUrl: 'drawing-image.html',
})
export class DrawingImagePage implements OnInit {
  public moldinoutModel:MoldInOutModel;
  @ViewChild('imageCanvas') canvas: any;
  canvasElement: any;
 
  saveX: number;
  saveY: number;
 
  storedImages = [];
 
  // Make Canvas sticky at the top stuff
  @ViewChild(Content) content: Content;
  @ViewChild('fixedContainer') fixedContainer: any;
 
  // Color Stuff
  selectedColor = '#9e2956';
 
  colors = [ '#9e2956', '#c2281d', '#de722f', '#edbf4c', '#5db37e', '#459cde', '#4250ad', '#802fa3' ];
 
  constructor(
    public navCtrl: NavController, 
    public renderer: Renderer, 
    private platform: Platform,
    private _moldinoutProvider: MoldinoutProvider
    ) {
  }

  async ngOnInit() {
    this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
    this.storedImages.push(this.moldinoutModel.ImageSelectedName)
  }
 
  ionViewDidEnter() {
    // https://github.com/ionic-team/ionic/issues/9071#issuecomment-362920591
    // Get the height of the fixed item
    let itemHeight = this.fixedContainer.nativeElement.offsetHeight;
    let scroll = this.content.getScrollElement();
 
    // Add preexisting scroll margin to fixed container size
    itemHeight = Number.parseFloat(scroll.style.marginTop.replace("px", "")) + itemHeight;
    scroll.style.marginTop = itemHeight + 'px';
  }
 
  ionViewDidLoad() {
    // Set the Canvas Element and its size
    this.canvasElement = this.canvas.nativeElement;
    this.canvasElement.width = this.platform.width() + '';
    this.canvasElement.height = 380;
    
    // var canvas = ctx.canvas ;

    let context = this.canvasElement.getContext('2d');
    
    let img = new Image();
    img.onload = () => {
      context.fillStyle = "white";
      context.fillRect(0, 0, this.platform.width(), 350);
      context.drawImage(img, 0, 0, this.platform.width(), 350);
    };
    img.src = this.moldinoutModel.ImageSelectedName;
  }
  selectColor(color) {
    this.selectedColor = color;
  }
  
  startDrawing(ev) {
    var canvasPosition = this.canvasElement.getBoundingClientRect();
  
    this.saveX = ev.touches[0].pageX - canvasPosition.x;
    this.saveY = ev.touches[0].pageY - canvasPosition.y;
  }
  
  moved(ev) {
    var canvasPosition = this.canvasElement.getBoundingClientRect();
  
    let ctx = this.canvasElement.getContext('2d');
    let currentX = ev.touches[0].pageX - canvasPosition.x;
    let currentY = ev.touches[0].pageY - canvasPosition.y;
  
    ctx.lineJoin = 'round';
    ctx.strokeStyle = this.selectedColor;
    ctx.lineWidth = 2;
  
    ctx.beginPath();
    ctx.moveTo(this.saveX, this.saveY);
    ctx.lineTo(currentX, currentY);
    ctx.closePath();
  
    ctx.stroke();
  
    this.saveX = currentX;
    this.saveY = currentY;
  }
  
  
  saveCanvasImage() {
    var dataUrl = this.canvasElement.toDataURL("image/jpeg", 0.98);
  
    let ctx = this.canvasElement.getContext('2d');
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    let element = this.moldinoutModel.Images[this.moldinoutModel.ImageSelectedIndex];
    element.photo = dataUrl;
    this.navCtrl.pop();
    // let ctx = this.canvasElement.getContext('2d');
    // ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
  
    // let name = new Date().getTime() + '.png';
    // let path = this.file.dataDirectory;
    // let options: IWriteOptions = { replace: true };
  
    // var data = dataUrl.split(',')[1];
    // let blob = this.b64toBlob(data, 'image/png');
  
    // this.file.writeFile(path, name, blob, options).then(res => {
    
    // }, err => {
    //   console.log('error: ', err);
    // });
  }
  
  // https://forum.ionicframework.com/t/save-base64-encoded-image-to-specific-filepath/96180/3
  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
  
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
  
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
  
      var byteArray = new Uint8Array(byteNumbers);
  
      byteArrays.push(byteArray);
    }
  
    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

}
