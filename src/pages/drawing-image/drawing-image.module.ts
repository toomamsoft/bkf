import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DrawingImagePage } from './drawing-image';

@NgModule({
  declarations: [
    DrawingImagePage,
  ],
  imports: [
    IonicPageModule.forChild(DrawingImagePage),
  ],
  entryComponents: [
    DrawingImagePage
  ]
})
export class DrawingImagePageModule {}
