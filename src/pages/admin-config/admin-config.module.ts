import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminConfigPage } from './admin-config';

@NgModule({
  declarations: [
    AdminConfigPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminConfigPage),
  ],
  entryComponents: [
    AdminConfigPage
  ]
})
export class AdminConfigPageModule {}
