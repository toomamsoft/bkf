import { BasePage } from './../../shared/basepage';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events, AlertController } from 'ionic-angular';
import { AdminProvider } from '../../providers/admin-provider/admin-provider';
import { AdminModel } from '../../models/AdminModel';
import { CONSTANT } from '../../shared/constant';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';

/**
 * Generated class for the AdminConfigPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-config',
  templateUrl: 'admin-config.html',
})
export class AdminConfigPage extends BasePage implements OnInit {
  public form: FormGroup;
  public adminModel: AdminModel;
  constructor(public navCtrl: NavController,
    public events: Events,
    private formBuilder: FormBuilder,
    private _adminProvider: AdminProvider,
    private alertCtrl: AlertController
    ) {
      super(events)
      this.form = this.formBuilder.group({
        Survey: [0],
        InOut: [0],
        MoldMaker: [0],
        ISO: ['']
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminConfigPage');
  }

  ngOnInit() {
    this.adminModel = this._adminProvider.adminModel;
  }

  async ionViewDidEnter(){
    try {
      await this._adminProvider.getConfig();
      this.form.controls['Survey'].setValue(this.adminModel.getConfig.getConfig[0].survey || 0)
      this.form.controls['InOut'].setValue(this.adminModel.getConfig.getConfig[0].inout || 0)
      this.form.controls['MoldMaker'].setValue(this.adminModel.getConfig.getConfig[0].mold || 0)
      this.form.controls['ISO'].setValue(this.adminModel.getConfig.getConfig[0].iso || '')
    } catch (ex) {
      this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
    }
  }

  reset() {
    this.ionViewDidEnter();
  }

  async edit() {
    try {
      this.adminModel.imgSurvey = this.form.controls['Survey'].value
      this.adminModel.imgInOut = this.form.controls['InOut'].value
      this.adminModel.imgMaker = this.form.controls['MoldMaker'].value
      this.adminModel.isoNumber = this.form.controls['ISO'].value
      await this._adminProvider.getUpdateConfig();
      this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
          handler: () =>{
            // this.popToPage('SurveyMenuPage', this.navCtrl);
            this.navCtrl.pop();
          }
        }]
      }).present();
    }catch (ex) {
      this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
    }
  }

}
