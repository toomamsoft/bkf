import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events, AlertController } from 'ionic-angular';
import { LoginProvider } from '../../providers/login-provider/login-provider';
import { MoldinoutProvider } from '../../providers/moldinout-provider/moldinout-provider';
import { ScanProvider } from '../../providers/scan/scan';
import { MoldInOutModel } from '../../models/MoldInOutModel';
import { MessageModel } from '../../models/MessageModel';
import { MessageProvider } from '../../providers/message-provider/message-provider';
import { StorageProvider } from '../../providers/storage-provider/storage-provider';
import { CONSTANT } from '../../shared/constant';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';
import { Utilities } from '../../shared/utilities';

/**
 * Generated class for the MoldinoutMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moldinout-menu',
  templateUrl: 'moldinout-menu.html',
})
export class MoldinoutMenuPage extends BasePage implements OnInit {
  public moldinoutModel: MoldInOutModel;
  public messageModel: MessageModel;
  public countMessage: number = 0;
  public countApprove: number = 0;
  public fullName:any = '';
  public platformVersion: any = '';

  constructor(
    public navCtrl: NavController,
    public events: Events,
    private alertCtrl: AlertController,
    private _loginProvider: LoginProvider,
    private _moldinoutProvider: MoldinoutProvider,
    public _scanProvider: ScanProvider,
    private _messageProvider: MessageProvider,
    private _storageProvider: StorageProvider
    ) {
      super(events)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoldinoutMenuPage');
  }

  async ngOnInit() {
    this.messageModel = this._messageProvider.messageModel;
    this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
  }

  async ionViewDidEnter(){
    try {
      this.fullName = await this._storageProvider.getKey(CONSTANT.USER_NAME);
      this.platformVersion = await Utilities.getAppVersion();
      this.messageModel = this._messageProvider.messageModel;
      await Promise.all([this._messageProvider.getMessageDetail(), this._moldinoutProvider.getWaitingApprove()]) ;
      this.countMessage = this.messageModel.getMessageDetail.detail.length;
      this.countApprove = this.moldinoutModel.getWaiting.getWaiting.length;
    } catch (ex) {
      console.log(ex)
    }
  }

  async onNotification() {
    this.navCtrl.push('NotificationPage')
  }

  async onLogout()
  {
    this._loginProvider.authenticateLogout();
  }

  async scanQr() {
    this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
    let txtScanQR: string;
    await this._scanProvider.scan().then(val => {
      txtScanQR = val.toString();
    }).catch(error => {
      console.log(error)
    });
      
    if(txtScanQR){
      this.moldinoutModel.JobId = '';
      this.moldinoutModel.MoldId = txtScanQR.split(/\|/)[0] || '';
      this.moldinoutModel.AssetId = txtScanQR.split(/\|/)[1] || '';
      this.moldinoutModel.CustNo = txtScanQR.split(/\|/)[2] || '';
      this.moldinoutModel.PartNo = txtScanQR.split(/\|/)[3] || '';
      this.moldinoutModel.PartName = txtScanQR.split(/\|/)[4] || '';
      
      this.LoadDataMoldInOut();
    }
  }

  async LoadDataMoldInOut() {
    try {

      await this._moldinoutProvider.getNewMoldOut();

      if(this.moldinoutModel.getNewMoldInOut.inoutMold.length > 0){
        if(this.moldinoutModel.getNewMoldInOut.inoutMold[0].jobId !== '') {
         this.alertCtrl.create({
          title: 'Alert',
          subTitle: 'This mold has Transaction',
          buttons:[{
            text: 'Cancel',
            handler: () => {
              console.log('Cancel Click')
            }
          },{
            text: 'Continue',
            handler: () => {
              this.moldinoutModel.MoldId = this.moldinoutModel.getNewMoldInOut.inoutMold[0].moldId;
              this.moldinoutModel.JobId = this.moldinoutModel.getNewMoldInOut.inoutMold[0].jobId;
              this.navCtrl.push('MoldinoutHistoryPage');
            }
          }]
         }).present();
          
        }else {
          this.navCtrl.push('MoldinoutCreatePage');
        }
      }else {
        let alert = this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.title%'] : ENG['%message.title%'],
          subTitle: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%message.notfound%'] : ENG['%message.notfound%'],
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
          }]
        });
        alert.present();
      }
    }catch (ex) {
      this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
    }
  }

}
