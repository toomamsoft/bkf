import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldinoutMenuPage } from './moldinout-menu';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoldinoutMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(MoldinoutMenuPage),
    TranslateModule
  ],
})
export class MoldinoutMenuPageModule {}
