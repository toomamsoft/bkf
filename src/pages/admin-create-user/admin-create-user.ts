import { BasePage } from './../../shared/basepage';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Events, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminModel } from '../../models/AdminModel';
import { AdminProvider } from '../../providers/admin-provider/admin-provider';
import { CONSTANT } from '../../shared/constant';
import * as THAI from '../../assets/i18n/th.json';
import * as ENG from '../../assets/i18n/en.json';

/**
 * Generated class for the AdminCreateUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-create-user',
  templateUrl: 'admin-create-user.html',
})
export class AdminCreateUserPage extends BasePage implements OnInit {
  public adminModel: AdminModel
  public position: any = [{id: 1, value: 'Staff'},{id: 2, value: 'Manager'}, {id: 3, value: 'Department'},{id: 4, value: 'Division'}];
  public form: FormGroup;

  constructor(public navCtrl: NavController,
    public events: Events,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private _adminProvider: AdminProvider) {
    super(events);
    this.form = this.formBuilder.group({
      EmpID: [null, [Validators.required]],
      FirstName: [null, [Validators.required]],
      LastName: [null, [Validators.required]],
      UserType: ['', [Validators.required]],
      UserGroup: [null],
      Username: [null, [Validators.required]],
      Password:[null,[Validators.required]]
    })
  }

  ngOnInit() {
    this.adminModel = this._adminProvider.adminModel;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminCreateUserPage');
  }

  reset() {

  }

  async save() {
    try {
      this.adminModel.empid = this.form.controls['EmpID'].value;
      this.adminModel.firstname = this.form.controls['FirstName'].value;
      this.adminModel.lastname = this.form.controls['LastName'].value;
      this.adminModel.position = this.form.controls['UserType'].value;
      this.adminModel.usergroup = this.form.controls['UserGroup'].value;
      this.adminModel.username = this.form.controls['Username'].value;
      this.adminModel.password = this.form.controls['Password'].value;
      this.adminModel.flag = 'I';

      if(this.adminModel.getListBKF.getListBKF.filter(val => {
        return val.empId == this.adminModel.empid 
      }).length > 0) {
        this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: 'แจ้งเตือน',
          subTitle: 'รหัสพนักงานนี้มีอยู่ในระบบแล้ว กรุราลองใหม่อีกครั้ง',
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
          }]
        }).present();

        return
      }

      if(this.adminModel.getListBKF.getListBKF.filter(val => {
        return val.username == this.adminModel.username 
      }).length > 0) {
        this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: 'แจ้งเตือน',
          subTitle: 'ชื่อผู้ใช้งานนี้มีอยู่ในระบบแล้ว กรุราลองใหม่อีกครั้ง',
          buttons: [{
            text: CONSTANT.APP_LANGUAGE == 'th' ? THAI['%btn.close%'] : ENG['%btn.close%']
          }]
        }).present();

        return
      }

      this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: 'แจ้งเตือน',
        subTitle: 'คุณต้องการเพิ่มพนักงานใช่หรือไม่',
        buttons: [{
          text: 'ไม่'
        },{
          text: 'ใช่',
          handler: () => {
            this.addEmployee()
          }
        }]
      }).present();
    } catch (ex) {
      this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
    }
  }

  async addEmployee() {
    try {
      await this._adminProvider.getUpdateUserBKF();
      this.alertCtrl.create({
        enableBackdropDismiss: false,
        title: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.title%'] : ENG['%message.title%'],
        subTitle: CONSTANT.APP_LANGUAGE == 'th'? THAI['%message.save.success%'] : ENG['%message.save.success%'],
        buttons: [{
          text: CONSTANT.APP_LANGUAGE == 'th'? THAI['%btn.close%'] : ENG['%btn.close%'],
          handler: () =>{
            this.navCtrl.pop();
          }
        }]
      }).present();
    } catch (ex) {
      this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
    }
  }

}
