import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminCreateUserPage } from './admin-create-user';

@NgModule({
  declarations: [
    AdminCreateUserPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminCreateUserPage),
  ],
  entryComponents: [
    AdminCreateUserPage
  ]
})
export class AdminCreateUserPageModule {}
