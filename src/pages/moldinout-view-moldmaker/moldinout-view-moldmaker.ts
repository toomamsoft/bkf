import { BasePage } from './../../shared/basepage';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { MoldMakerModel } from '../../models/MoldMakerModel';
import { MoldmakerProvider } from '../../providers/moldmaker-provider/moldmaker-provider';
import { Utilities } from '../../shared/utilities';
import { CONSTANT } from '../../shared/constant';

/**
 * Generated class for the MoldinoutViewMoldmakerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-moldinout-view-moldmaker',
  templateUrl: 'moldinout-view-moldmaker.html',
})
export class MoldinoutViewMoldmakerPage extends BasePage implements OnInit, OnDestroy {
  public moldMakerModel: MoldMakerModel;
  public IP: string = CONSTANT.IPAddress;
  constructor(public navCtrl: NavController,
    public events: Events,
    private _moldmakerProvider: MoldmakerProvider
    ) {
    super(events)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoldinoutViewMoldmakerPage');
    this.LoadData();
  }

  async ngOnInit() {
    this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
  }

  async ngOnDestroy() {
    this._moldmakerProvider.moldmakerModel = new MoldMakerModel;

  }

  async LoadData() {
    try {
      await this._moldmakerProvider.getBKFViewHistory();
    } catch (ex) {
      this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
    }
  }

  async tapViewPhoto(photo) {
    let view = `${this.IP}/inout/showImage?filename='=${photo}`;
    Utilities.viewImages(view);
  }

  async openLaunchNavigator(latitude, longitude) {
    Utilities.openGoogleMapApplication(latitude, longitude)
  }

}
