import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoldinoutViewMoldmakerPage } from './moldinout-view-moldmaker';
import { ComponentsModule } from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoldinoutViewMoldmakerPage,
  ],
  imports: [
    IonicPageModule.forChild(MoldinoutViewMoldmakerPage),
    ComponentsModule,
    DirectivesModule,
    PipesModule,
    TranslateModule
  ],
  entryComponents: [
    MoldinoutViewMoldmakerPage
  ]
})
export class MoldinoutViewMoldmakerPageModule {}
