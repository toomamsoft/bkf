export interface IToastProvider {
    id: number, 
    title: string,
    message: string,
    ms: number
}