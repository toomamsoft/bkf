export const CONSTANT = {
    SERVER: "NODE",
    APP_LANGUAGE: 'en',
    IPAddress: 'http://172.20.10.7:3000',
    FCM_TOKEN: 'DEVICE_TOKEN',
    USER_TYPE: 'USER_TYPE',
    IMAGE_MOLDINOUT: 'IMAGE_MOLDINOUT',
    USER_ID: 'USER_ID',
    IMAGE_MOLDSERVEY: 'IMAGE_MOLDSERVEY',
    ACTIVE_MOBILE: 'ACTIVE_MOBILE',
    USER_NAME: 'USER_NAME',
    LANGURAGE : 'language',
    TIMEOUT: 120000,
    WIDTH: 600,
    HEIGHT: 800,
    AdminAdapterModel: {
        GET_USER_BKF: 'getListuserBKF',
        GET_USER_MOLD: 'getListuserMold',
        GET_CONFIG: 'getConfig'
    },
    AuthenticateUserModel: {
        USER_ID: 'EmpID',
        USER_TYPE: 'TypeLogin',
        FULL_NAME: 'NameEN',
        IMEI: 'IMEI',
        STATUS: 'Status',
        IMAGES_SURVEY: 'Images',
        IMAGES_MOLDINOUT: 'ImagesInOut',
        USER_LOGIN: 'userLogin',
        DEVICE_ALLOW: 'DeviceAllow'
    },
    SettingModel: {
        CONFIG: 'ConfigApplication'
    },
    LocationModel: {
        LOCATION_ID: `LocationID`,
        LOCATION_DESCRIPTION: `LocationDescription`
    },
    HistorySurveyModel: {
        MOLDLOCATION: 'MoldLocation',
        MOLDMASTER: 'MoldMaster',
        MOLDHISTORY: 'MoldHistory',
        MOLDSEARCH: 'MoldSearch'
    },
    HistorySurveyDetailModel: {
        HISTORY: 'MoldHistoryDetail',
        IMAGES: 'MoldHistoryImages'
    },
    SurveySubMasterModel: {
        MOLDID: 'MoldID',
        ASSETID: 'AssetID',
        CUSTNO: 'CustNo',
        PARTNO: 'PartNo',
        PARTNAME: 'PartName'
    },
    SurveySubSearchModel: {
        MOLDID: 'MoldID',
        ASSETID: 'AssetID',
        PLATE: 'Plage',
        FULLNAME: 'Name',
        CREATEDATE: 'CreateDate',
        RUNNO: 'RunNo'
    },
    SurveySubHistoryModel: {
        MOLDID: 'MoldID',
        ASSETID: 'AssetID',
        CUSTNO: 'CustNo',
        PARTNO: 'PartNo',
        PARTNAME: 'PartName',
        LOCATION: 'LocationDescription',
        PLATE: 'Plate',
        REMARK: 'Remark',
        FULLNAME: 'Names',
        CREATEDATE: 'CreateDate',
        RUNNO: 'RunNo'
    },
    SurveySubHistoryDatailModel: {
        MOLDID: 'MoldID',
        LOCATION: 'LocationDescription',
        PLATE: 'Plate',
        REMARK: 'Remark',
        FULLNAME: 'Names',
        CREATEDATE: 'CreateDate',
        RUNNO: 'RunNo',
        DESCRIPTION:'Discription',
        SUBDESCRIPTION: {
            ASSETID: 'AssetID',
            CUSTNO: 'CustNo',
            PARTNO: 'PartNo',
            PARTNAME: 'PartName'
        }
    },
    SurveySaveMoldModel: {
        STATUS: 'Status',
        MESSAGE: 'Message',
        PDF: 'Pdf'
    },
    SurveySubHistoryDetailImageModel: {
        PHOTO: 'ImagePath'
    },
    MoldInoutAdapterModel: {
        NEWMOLDINOUT :'NewMoldInOut',
        SETTING_MOLDMAKER: 'MoldMaker',
        SETTING_REASON: 'Reason',
        MOLDINOUT_WAITNG: 'GetApproveByWaiting',
        MOLDINOUT_IMAGEOUT: 'ImagesOut',
        MOLDINOUT_IMAGEIN: 'ImagesIn',
        MOLDINOUT_IMAGETRANSPORT: 'ImagesTransport',
        MOLDINOUT_SEARCH: 'MoldInOutSearch',
        MOLDINOUT_STATUS: 'MoldInOutStatus',
        MOLDINOUT_APPROVE_IN: 'approveIn',
        MOLDINOUT_APPROVE_OUT: 'approveOut'
    },
    SubNewMoldInOut: {
        MOLDID: 'MoldId',
        DESCRIPTION:'Discription',
        SUBDESCRIPTION: {
            ASSETID: 'AssetID',
            CUSTNO: 'CustNo',
            PARTNO: 'PartNo',
            PARTNAME: 'PartName'
        },
        JOBID: 'JobId',
        MAKERMOLD: 'MakerMoldID',
        MAKERID: 'MakerId',
        PLANOUT : 'PlanOut',
        PLANIN: 'PlanIn',
        REASON: 'ReasonId',
        NAMECREATE: 'NameTHCreate',
        REMARKNEW: 'RemarkNew',
        CONFIRMDATE: 'ConfirmOutDate',
        NAMECONFIRMOUT: 'NameConfirmOut',
        REMARKOUT: 'RemarkOut',
        STATUS: 'CurrentStatus',
        PRINT: 'Printed',
        PDF: 'PDF',
        FLAG: 'Flag',
        MAKERSTATUS: 'MoldMakerStatus',
        REMARKIN: 'RemarkIn'
    },
    SubMoldInoutWatingModel : {
        JOBID: 'JobId',
        MOLDID: 'MoldId',
        CUSNO: 'CustNo',
        PARTNO: 'PartNo',
        PARTNAME: 'PartName',
        STATUS: 'CurrentStatus',
        CREATEDATE: 'CreateDate'
    },
    SubMoldInoutConfig: {
        ID: 'ID',
        DESCIRPTION: 'Description'
    },
    SubMoldInoutImages: {
        IMAGES_TYPE: 'ImageType',
        LATITUDE: 'Latitude',
        LONGITUDE: 'Longitude',
        IMAGES_NAME: 'PhotoName'
    },
    SubMoldInOutSearch: {
        JOBID: 'JobId',
        MOLDID: 'MoldId',
        CUSTNO: 'CustNo',
        PARTNO: 'PartNo',
        PARTNAME: 'PartName',
        MAKERNAME: 'MakerName',
        STATUS: 'SearchStatus',
        CONFIRMOUT_DATE: 'ConfirmOutDate'
    },
    SubMoldInOutStatus: {
        JOBID: 'JobId',
        MOLDID: 'MoldID',
        PLANIN: 'PlanIn',
        PLANOUT: 'PlanOut',
        FULLNAME: 'FullName',
        MAKERNAME: 'MakerName',
        REASON: 'Reason',
        STATUS: 'DelayStatus'
    },
    SubMoldInOutApprove: {
        JOBID: "JobId",
        EMPID: "EmpID",
        TYPE: "ApproveType",
        STATUS: "Status",
        DATE: "ApproveDate",
        REMARK: "Remark",
        NAME: "NameTH",
        USER_TYPE:"UserType"
    },
    MoldMakerAdapterModel: {
        LIST_MOLD: 'getListMold',
        VIEW_BY_BKF: 'ViewHistoryWithBKF',
        HISTORY: 'history',
        HISTORY_BKF: 'bkfview',
        HISTORY_DETAIL: 'historyDetail',
        HISTORY_IMAGE: 'historyImages'
    },
    SubGetListMoldModel: {
        JOBID: "JobId",
        MOLDID: "MoldID",
        MAKERMOLDID: "MakerMoldId",
        PLANOUT: "PlanOut",
        ACTUAL: "Actual",
        PLANIN: "PlanIn",
        REMARK: "RemarkNew",
        PARTNO: "PartNo",
        CUSTNO: "CustNo",
        PARTNAME: "PartName",
        STATUS: "MoldMakerStaus"
    },
    SubMoldMakerHistory: {
        ID: 'ID',
        MOLDID: 'MoldId',
        JOBID: 'JobID',
        REMARK: 'Remark',
        CREATEDATE:'CreateDate',
        STATUS: 'Status'
    },
    SubMoldMakerHistoryDetail: {
        MOLDID: '',
        JOBID: 'JobID',
        REMARK: 'Remark',
        PHOTO: 'Photo',
        CREATEDATE: '',
        STATUS: 'Status',
        SubPhoto: {
            PHOTONAME : 'PhotoName',
            LATITUDE : 'Latitude',
            LONGITUDE: 'Longitude'
        }
    },
    SubMoldMakerBKFVIEW: {
        JOBID : 'JobId',
        MOLDID: 'MoldId' ,
        PLANOUT: 'PlanOut',
        PLANINT: 'PlanIn',
        REMARK: 'RemarkNew',
        REASON: 'Reason',
        CONFIRMDATE: 'ConfirmOutDate',
        PHOTO: 'Photo',
        SubPhoto: {
            PHOTONAME : 'PhotoName',
            LATITUDE : 'Latitude',
            LONGITUDE: 'Longitude'
        }
    },
    SubMoldBKFVIEWHistoryModel: {
        JOBID : 'JobId',
        MOLDID: 'MoldId',
        REMARK: 'Remark',
        CREATEDATE: 'CreateDate',
        PHOTO: 'Photo',
        STAUTS: 'Status',
        SubPhoto: {
            PHOTONAME : 'PhotoName',
            LATITUDE : 'Latitude',
            LONGITUDE: 'Longitude'
        }
    },
    MessageAdapterModel : {
        MESSAGE: 'notification'
    },
    SubMessageModel: {
        MESSAGEID: 'MessageID',
        SUBJECT: 'MessageSubject',
        DESCRIPTION: 'MessageDescription',
        FLAG: 'ReadStatus',
        MESSAGE_DATE: 'MessageDate',
        JOBID: 'JobId',
        MOLDID: 'MoldId',
        STATUS: 'CurrentStatus'
    },
    SubAdminGetListBKF: {
        EMPID: "empId",
        FIRSTNAME: "firstName",
        LASTNAME: "lastName",
        USERGROUP: "userGroup",
        USERNAME: "username",
        PASSWORD: "password",
        USERTYPE: "userType"
    },
    SubAdminGetListMold: {
        FIRSTNAME: "MakerName",
        EMPID: "EmpID",
        USERNAME: "username",
        PASSWORD: "password",
        USERTYPE: "userType"
    },
    SubAdminGetConfig: {
        SURVEY: "Survey",
        MOLDINOUT: "MoldInOut",
        MOLDMAKER: "MoldMaker",
        ISONUMBER: "ISO"
    },
    AUTHENTICATION_AUTHENTICATE_ADAPTER: '/login/authentication',
    AUTHENTICATION_DEVICE_ALLOW_ADAPTER: '/login/deviceAllow',
    AUTHENTICATION_ACTIVE_ADAPTER: '/login/activeDevice',
    AUTHENtICATION_CONFIG: '/login/config',
    SURVEY_LOCATION_ADAPTER: '/survey/getLocation',
    SURVEY_HISTORY_ADAPTER: '/survey/getMoldHistory',
    SURVEY_HISOTRY_DETAIL_ADAPTER: '/survey/getMoldHistoryDetail',
    SURVEY_SARCH_ADAPTER: '/survey/getMoldSearch',
    SURVEY_SAVE_ADAPTER: '/survey/saveMold',
    MOLDINOUT_SETTING_ADAPTER: '/inout/setting',
    MOLDINOUT_MASTER_ADAPTER: '/inout/newmoldinout',
    MOLDINOUT_HISTORY_ADAPTER: '/inout/getHistory',
    MOLDINOUT_APPROVE_ADAPTER: '/inout/getApprove',
    MOLDINOUT_SAVE_NEW_ADAPTER: '/inout/saveMoldOut',
    MOLDINOUT_APPROVE_REJECT_OUT: '/inout/approveOrReject',
    MOLDINOUT_APPROVE_CONFIRMOUT: '/inout/confirmOut',
    MOLDINOUT_APPROVE_CONFIRMIN : '/inout/confirmIn',
    MOLDINOUT_APPROVE_REJECT_IN: '/inout/approveInOrReject',
    MOLDINOUT_SEARCH_ADAPTER: '/inout/moldSearch',
    MOLDINOUT_STATUS_ADAPTER: '/inout/moldStatus',
    MOLDINOUT_GET_WAITING: '/inout/getApproveByWaiting',
    MOLDMAKER_GETLISTMOLD_ADAPTER: '/maker/getListMold',
    MOLDMAKER_BKFVIEW_ADAPTER: '/maker/bkfview',
    MOLDMAKER_HISTORY_ADAPTER: '/maker/getMoldHistory',
    MOLDMAKER_HISTORY_DETAIL_ADAPTER: '/maker/getMoldHistoryDetail',
    MOLDMAKER_RECEIVE_ADAPTER: '/maker/receive',
    MOLDMAKER_SAVE_TRANSACTION_ADAPTER: '/maker/transaction',
    MESSAGE_GETMESSGE_ADAPTER: '/inbox/notification',
    MESSAGE_READ_MESSGE_ADAPTER: '/inbox/readNotification',

    ADMIN_GET_USER_BKF_ADAPTER: '/admin/getListUserBKF',
    ADMIN_GET_CONFIG_ADAPTER: '/admin/getConfig',
    ADMIN_GET_USER_MOLD_ADAPTER: '/admin/getListUserMold',
    ADMIN_GET_UPDATE_USER_BKF_ADAPTER: '/admin/getUpdateUserBKF',
    ADMIN_GET_UPDATE_USER_MAKER_ADAPTER: '/admin/getUpdateUserMaker',
    ADMIN_GET_UPDATE_CONFIG_ADAPTER: '/admin/getUpdateConfig'
}