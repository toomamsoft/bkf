import { CONSTANT } from './constant';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { JsonConvert, OperationMode, ValueCheckingMode } from "json2typescript";
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';
import { Uid } from '@ionic-native/uid';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { Platform } from 'ionic-angular';
import { FileTransfer } from '@ionic-native/file-transfer';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { LocationAccuracy } from '@ionic-native/location-accuracy';


export class Utilities {

    private static device = new Device();
    private static appVersion = new AppVersion();
    private static photoViewer = new PhotoViewer();
    private static uid = new Uid();
    private static fileOpener = new FileOpener();
    private static platform = new Platform();
    private static file = new File();
    private static fileTranfer = new FileTransfer();
    private static launchNavigator = new LaunchNavigator();
    private static locationAccuracy = new LocationAccuracy();

    public static async IMEI()
    {  
        return new Promise(resolve => {
            if(this.uid != null || undefined){
                resolve(this.uid.IMEI || this.device.serial || '000000000000000')
            }else {
                resolve(this.device.serial || '000000000000000')
            }
        })
        
    }

    public static setItem(key, value)
    {
        localStorage.setItem(key,value);
    }

    public static getItem(key)
    {
        let returnItms: any;
        returnItms = localStorage.getItem(key);

        return returnItms;
    }

    public static getAppVersion()
    {
        return new Promise(resove => {
            this.appVersion.getVersionNumber().then(res => {
                resove(res)
            }).catch((e: string) => {
                resove(`APP NOT ON DEVICE.`);
            });
        })
    }

    public static getDeviceUUID(): string {
        return (this.device.platform) ? this.device.uuid : "b4493d5e-8070-3c12-af42-f052d67dbc11";
    }

    public static padDigits(number, digits) {
        return Array(Math.max(digits - String(number).length + 1, 0)).join('0') + number;
    }

    public static formatDateDDMMYYYY(date: Date, separator?: string) {
        const yyyy = date.getUTCFullYear();
        const mm = Utilities.padDigits((date.getMonth() + 1), 2);
        const dd = Utilities.padDigits(date.getDate(), 2);
        separator = separator ? separator : '';
        return dd + separator + mm + separator + yyyy;
    }

    public static getCurrentDateTimeMySql() {        
        var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
        var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0, 19);
        var mySqlDT = localISOTime;
        return mySqlDT;
    }

    public static mapJsontoModel(json: any, classReference: { new(): any }, ) {
        let jsonConvert: JsonConvert = new JsonConvert();
        jsonConvert.operationMode = OperationMode.ENABLE; // print some debug data
        jsonConvert.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
        jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL; // never allow null
    
        // Map to the country class
        let obj;
    
        try {
          obj = jsonConvert.deserialize(json, classReference);
          return obj;
          //console.log(obj);
          //obj.cities[0].printInfo(); // prints: Basel was founded in -200 and is really beautiful!
        } catch (e) {
          console.log('error mapJsontoModel:::',<Error>e);
          throw e;
        }
    }

    public static async getBase64Image(fileImages) {

        var canvas = document.createElement("canvas");
        var ctx = canvas.getContext("2d");
        var img = new Image();
    
        return new Promise(async (resolve, reject) => {
          
          img.onload = () => {
            canvas.width = CONSTANT.WIDTH;
            canvas.height = CONSTANT.HEIGHT;
    
            ctx.fillStyle = "white";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
    
            let imageBase64Data = canvas.toDataURL("image/jpeg", 0.98);
    
            resolve(imageBase64Data);
          };
          img.src = await fileImages.replace('file:///', '/');
        });
    }

    public static async resizeImage(imageBage64) {
    
        var canvas = document.createElement("canvas");
        var ctx = canvas.getContext("2d");

        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        return new Promise((resolve, reject) => {
            var img = new Image();
            img.onload = function () {
                canvas.width = CONSTANT.WIDTH;
                canvas.height = CONSTANT.HEIGHT;

                ctx.fillStyle = "white";
                ctx.fillRect(0, 0, canvas.width, canvas.height);

                ctx.drawImage(img, 0, 0, CONSTANT.WIDTH, CONSTANT.HEIGHT);
                let imageBase64Data = canvas.toDataURL("image/jpeg", 0.98);
                resolve(imageBase64Data);
            };

            img.src = imageBage64;
        });
    }

    public static async viewImages(imageUrl: string = '', title: string = '') {
        try {
            const options = {
                share: true, // default is false
                closeButton: true, // default is true
                copyToReference: true, // default is false
                headers: '',  // If this is not provided, an exception will be triggered
                piccasoOptions: { } // If this is not provided, an exception will be triggered
            };
            this.photoViewer.show(imageUrl, title, options);
        }catch (ex) {
            console.log('Error viewImages::: ',ex)
        }
    }

    public static async viewFilePDF(filename, type = 'application/pdf') {
        // const options: DocumentViewerOptions = {
        //     title: `${filename}`
        // }
        try {
            let download = `${CONSTANT.IPAddress}/inout/download?filename=${filename}`;
            let path = this.file.dataDirectory;
            const transfer = this.fileTranfer.create();

            transfer.download(download, `${path}${filename}`).then(entry => {
                let url = entry.toURL();
                if(this.platform.is('ios'))
                {
                    // this.documentViewer.viewDocument(url, `${type}`, options)
                }else{
                    this.fileOpener.open(url, `${type}`).then( () => {})
                }
            }).catch(error =>{
                return error
            })
        } catch (ex){
            return ex;
        }
    }

    public static async openGoogleMapApplication(latitude: number = 0.00, longitude: number = 0.00) {
        this.launchNavigator.isAppAvailable(this.launchNavigator.APP.GOOGLE_MAPS).then( (isAvailable)=>{
            var app;
            if(isAvailable){
              app = this.launchNavigator.APP.GOOGLE_MAPS;
            }else
            {
              console.warn("Google Maps not available - falling back to user selection");
              app = this.launchNavigator.APP.USER_SELECT;
            }
            this.launchNavigator.navigate([latitude, longitude], {
              app: app
            })
        }).catch(error => {
            console.log('Error launchNavigator::: ',error)
        })
    }

    public static async accessGPS() {
        this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            if(canRequest) {
                this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then( 
                    () => {
                        console.log('requesting location permissions success');
                    }, (error) => {
                        console.log('Error requesting location permissions', error);
                    })
            }
          }).catch(error => {
            console.log('Error locationAccuracy::: ',error)
          });
    }
}