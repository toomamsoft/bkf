import { Events, NavController } from 'ionic-angular';
import iziToast, { IziToastSettings } from 'izitoast';
import { IToastProvider } from '../interface/toast-interface';
import { TranslateService } from '../../node_modules/@ngx-translate/core';
import { CONSTANT } from './constant';

export class BasePage {
    private iziToastInstance = iziToast;
    private static translate: TranslateService;
    private _lang: string;

    constructor(
        public event: Events
    ) {
        this._lang = CONSTANT.APP_LANGUAGE;
    }

    public intitialLanguage(translate: TranslateService, language = this._lang) {
        translate.setDefaultLang(language);
        BasePage.translate = translate;
    }

    getLanguage() {
        return BasePage.translate;
    }

    setLanguage(lang) {
        console.log('setLanguage', lang);
        CONSTANT.APP_LANGUAGE = lang;
        return new Promise((reslove => {
            this._lang = lang;
            return reslove();
        }))
    }

    public popToPage(page: string, navCtrl: NavController): void {
        const views: any[] = navCtrl.getViews();
        const position = views.findIndex(val => {
            return val.name == page;
        });

        console.log('view:::',views);
        console.log('position:::',position);
        if (position != -1) {
            const viewIndex = views[position].index;
            navCtrl.popTo(navCtrl.getByIndex(viewIndex));
        }
        else {
            navCtrl.setRoot('MainPage');
        }
    }


    private getIZIToastInstance()
    {
        return this.iziToastInstance;
    }

    private getToastOptions(title: string = '', message: string = '', ms: number = 2300, backgroundColor: string = 'dark') {
        const toastOptions: IziToastSettings = {
            // id: null,
            //title: title,
            icon: '',
            message: message,
            timeout: ms,
            close: false,
            progressBar: false,
            position: "topCenter", // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
            // prepare for use
            onOpening: function () { },
            onOpened: function () { },
            onClosing: function () { },
            onClosed: function () { },
            transitionIn: "fadeInDown",
            transitionInMobile: "fadeInDown",
            transitionOut: "fadeOutUp",
            transitionOutMobile: "fadeOutUp",
            animateInside:false,
            backgroundColor: backgroundColor
        }
        return toastOptions;
    }

    private destructuringSection(options: IToastProvider) {
        console.log(options);
        const arrayOption = Object.keys(options).map(key => options[key]);
        console.log(arrayOption);
        return arrayOption;
    }
    
    showSuccessToast(options: IToastProvider) {
        try {
            this.getIZIToastInstance().success(this.getToastOptions(...this.destructuringSection(options)));
        } catch (err) {
            console.error(err);
        }
    }
}