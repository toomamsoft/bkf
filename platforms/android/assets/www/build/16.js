webpackJsonp([16],{

/***/ 445:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoldinoutCreatePageModule", function() { return MoldinoutCreatePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__moldinout_create__ = __webpack_require__(474);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var MoldinoutCreatePageModule = /** @class */ (function () {
    function MoldinoutCreatePageModule() {
    }
    MoldinoutCreatePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_create__["a" /* MoldinoutCreatePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__moldinout_create__["a" /* MoldinoutCreatePage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_create__["a" /* MoldinoutCreatePage */]
            ]
        })
    ], MoldinoutCreatePageModule);
    return MoldinoutCreatePageModule;
}());

//# sourceMappingURL=moldinout-create.module.js.map

/***/ }),

/***/ 474:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldinoutCreatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_MoldInOutModel__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_moldinout_provider_moldinout_provider__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_scan_scan__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__shared_utilities__ = __webpack_require__(16);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};













/**
 * Generated class for the MoldinoutCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var gpsOptions = {
    enableHighAccuracy: true
};
var MoldinoutCreatePage = /** @class */ (function (_super) {
    __extends(MoldinoutCreatePage, _super);
    function MoldinoutCreatePage(navCtrl, navParams, events, platform, alertCtrl, actionsheetCtrl, formBuilder, geolocation, _moldinoutProvider, _scanProvider, _storageProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.events = events;
        _this.platform = platform;
        _this.alertCtrl = alertCtrl;
        _this.actionsheetCtrl = actionsheetCtrl;
        _this.formBuilder = formBuilder;
        _this.geolocation = geolocation;
        _this._moldinoutProvider = _moldinoutProvider;
        _this._scanProvider = _scanProvider;
        _this._storageProvider = _storageProvider;
        _this.Mode = 'Out';
        _this.limit = 5;
        _this.Latitude = 0.0000000000000000;
        _this.Longitude = 0.0000000000000000;
        _this.someMaxDateFromComponent = (new Date((new Date().getFullYear() + 1) + '-12-31')).toISOString().replace('Z', '');
        _this.form = _this.formBuilder.group({
            MoldId: [{ value: null, disabled: true }, [__WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].required]],
            MoldMaker: [{ value: null, disabled: true }],
            AssetId: [{ value: null, disabled: true }],
            CustNo: [{ value: null, disabled: true }],
            PartNo: [{ value: null, disabled: true }],
            PartName: [{ value: null, disabled: true }],
            Remark: [null],
            MakerId: ['', [__WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].required]],
            ReasonId: ['', [__WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].required]],
            PlanIn: ['', [__WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].required]],
            PlanOut: ['', [__WEBPACK_IMPORTED_MODULE_0__angular_forms__["f" /* Validators */].required]]
        });
        return _this;
    }
    MoldinoutCreatePage.prototype.ionViewDidLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log('ionViewDidLoad MoldinoutCreatePage');
                return [2 /*return*/];
            });
        });
    };
    MoldinoutCreatePage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var AssetId, CustNo, PartNo, PartName, _a, _b, i, ex_1;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        AssetId = '';
                        CustNo = '';
                        PartNo = '';
                        PartName = '';
                        _a = this;
                        _b = Number;
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].IMAGE_MOLDINOUT)];
                    case 1:
                        _a.limit = _b.apply(void 0, [_c.sent()]);
                        this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
                        // this.locationAccuracy.canRequest().then((canRequest: boolean) => {
                        //   if(canRequest) {
                        //     // the accuracy option will be ignored by iOS
                        //     this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_LOW_POWER).then(
                        //       () => {
                        //         console.log('requesting location permissions success');
                        //       },
                        //       error => console.log('Error requesting location permissions', error)
                        //     );
                        //   }
                        // }).catch(error => {
                        //   const alert = this.alertCtrl.create({
                        //     title: 'Attention!',
                        //     subTitle: error,
                        //     buttons: ['Close']
                        //   });
                        //   alert.present();
                        // });
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_12__shared_utilities__["a" /* Utilities */].accessGPS()];
                    case 2:
                        // this.locationAccuracy.canRequest().then((canRequest: boolean) => {
                        //   if(canRequest) {
                        //     // the accuracy option will be ignored by iOS
                        //     this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_LOW_POWER).then(
                        //       () => {
                        //         console.log('requesting location permissions success');
                        //       },
                        //       error => console.log('Error requesting location permissions', error)
                        //     );
                        //   }
                        // }).catch(error => {
                        //   const alert = this.alertCtrl.create({
                        //     title: 'Attention!',
                        //     subTitle: error,
                        //     buttons: ['Close']
                        //   });
                        //   alert.present();
                        // });
                        _c.sent();
                        this.geolocation.getCurrentPosition(gpsOptions).then(function (resp) {
                            console.log('requesting geolocation:::', resp);
                            _this.Latitude = resp.coords.latitude;
                            _this.Longitude = resp.coords.longitude;
                        });
                        _c.label = 3;
                    case 3:
                        _c.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, this._moldinoutProvider.getSetting()];
                    case 4:
                        _c.sent();
                        this.form.controls['MoldId'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].moldId);
                        this.form.controls['MoldMaker'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].makerMold);
                        for (i = 0; i < this.moldinoutModel.getNewMoldInOut.inoutMold[0].description.length; i++) {
                            if (i == 0) {
                                AssetId = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].AssetId;
                                CustNo = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].CustNo;
                                PartNo = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartNo;
                                PartName = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartName;
                            }
                            else {
                                AssetId += '\n' + this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].AssetId;
                                CustNo += '\n' + this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].CustNo;
                                PartNo += '\n' + this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartNo;
                                PartName += '\n' + this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartName;
                            }
                        }
                        this.form.controls['AssetId'].setValue(AssetId);
                        this.form.controls['CustNo'].setValue(CustNo);
                        this.form.controls['PartNo'].setValue(PartNo);
                        this.form.controls['PartName'].setValue(PartName);
                        return [3 /*break*/, 6];
                    case 5:
                        ex_1 = _c.sent();
                        this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutCreatePage.prototype.ngOnDestroy = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this._moldinoutProvider.moldInOutModel = new __WEBPACK_IMPORTED_MODULE_5__models_MoldInOutModel__["a" /* MoldInOutModel */]();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutCreatePage.prototype.deletePhoto = function (index) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var confirm;
            return __generator(this, function (_a) {
                confirm = this.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.title%"],
                    subTitle: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.delete.image%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.delete.image%"],
                    buttons: [
                        {
                            text: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%btn.logout.no%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%btn.logout.no%"],
                            handler: function () {
                                console.log('Disagree clicked');
                            }
                        }, {
                            text: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%btn.logout.yes%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%btn.logout.yes%"],
                            handler: function () {
                                _this.moldinoutModel.Images.splice(index, 1);
                            }
                        }
                    ]
                });
                confirm.present();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutCreatePage.prototype.tapViewPhoto = function (imageBase64) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                __WEBPACK_IMPORTED_MODULE_12__shared_utilities__["a" /* Utilities */].viewImages(imageBase64);
                return [2 /*return*/];
            });
        });
    };
    MoldinoutCreatePage.prototype.openLaunchNavigator = function (latitude, longitude) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    MoldinoutCreatePage.prototype.presentActionSheet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var actionSheet;
            return __generator(this, function (_a) {
                actionSheet = this.actionsheetCtrl.create({
                    title: 'Albums',
                    cssClass: 'action-sheets-basic-page',
                    buttons: [
                        {
                            text: 'Camera',
                            role: 'destructive',
                            icon: !this.platform.is('ios') ? 'camera' : null,
                            handler: function () {
                                _this.openCamera();
                            }
                        },
                        {
                            text: 'Album',
                            icon: !this.platform.is('ios') ? 'albums' : null,
                            handler: function () {
                                _this.getImageFromGallery();
                            }
                        },
                        {
                            text: 'Cancel',
                            role: 'cancel',
                            icon: !this.platform.is('ios') ? 'close' : null,
                            handler: function () {
                                console.log('Cancel clicked');
                            }
                        }
                    ]
                });
                actionSheet.present();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutCreatePage.prototype.openCamera = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var alert_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.moldinoutModel.Images.length >= this.limit) {
                            alert_1 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.photo%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.photo%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_1.present();
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this._scanProvider.getImageFromCamere().then(function (val) {
                                if (val && val != 'No Image Selected' && val != 'cordova_not_available') {
                                    _this.moldinoutModel.Images.push({ photo: val.toString(), latitude: _this.Latitude, longitude: _this.Longitude });
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutCreatePage.prototype.getImageFromGallery = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var images, alert_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.moldinoutModel.Images.length >= this.limit) {
                            alert_2 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.photo%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.photo%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_2.present();
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this._scanProvider.getImageFormAlbum(this.limit - this.moldinoutModel.Images.length).then(function (val) {
                                if (val && val != 'cordova_not_available') {
                                    images = val;
                                    for (var i = 0; i < images.length; i++) {
                                        _this.moldinoutModel.Images.push({ photo: images[i], latitude: _this.Latitude, longitude: _this.Longitude });
                                    }
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutCreatePage.prototype.preview = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
                this.moldinoutModel.MoldMaker = this.form.controls['MoldMaker'].value;
                this.moldinoutModel.Remark = this.form.controls['Remark'].value;
                this.moldinoutModel.MakerId = this.form.controls['MakerId'].value;
                this.moldinoutModel.ReasonId = this.form.controls['ReasonId'].value;
                this.moldinoutModel.PlanIn = this.form.controls['PlanIn'].value;
                this.moldinoutModel.PlanOut = this.form.controls['PlanOut'].value;
                if (this.moldinoutModel.Images.length <= 0) {
                    this.alertCtrl.create({
                        enableBackdropDismiss: false,
                        title: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%title.title%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%title.title%"],
                        subTitle: 'จำนวนรูปภาพควรมากกว่า 1 รูป',
                        buttons: [{
                                text: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%btn.close%"],
                                handler: function () {
                                    return;
                                }
                            }]
                    }).present();
                }
                else {
                    this.navCtrl.push('MoldinoutPreviewPage');
                }
                return [2 /*return*/];
            });
        });
    };
    MoldinoutCreatePage.prototype.cancelTransaction = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: 'แจ้งเตือน',
                    subTitle: 'คุณต้องการยกเลิกรายการนี้ใช่ไหม',
                    buttons: [{
                            text: 'ยกลเลิก',
                            handler: function () {
                                console.log('cancel click');
                            }
                        }, {
                            text: 'ยืนยัน',
                            handler: function () {
                                _this.navCtrl.pop();
                            }
                        }]
                }).present();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutCreatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* Component */])({
            selector: 'page-moldinout-create',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-create/moldinout-create.html"*/'<!--\n  Generated template for the MoldinoutCreatePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>Mold In/Out</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]="form">\n    <ion-list>\n      <ion-item>\n        <ion-label fixed>Mold ID</ion-label>\n        <ion-input formControlName="MoldId" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>MoldMakerID</ion-label>\n        <ion-input formControlName="MoldMaker" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>AssetID</ion-label>\n        <ion-textarea formControlName="AssetId" placeholder="AssetID" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>CustNo</ion-label>\n        <ion-textarea formControlName="CustNo" placeholder="CustNo" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>PartNo</ion-label>\n        <ion-textarea formControlName="PartNo" placeholder="PartNo" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>PartName</ion-label>\n        <ion-textarea formControlName="PartName" placeholder="PartName" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label>MoldMaker</ion-label>\n        <ion-select interface="action-sheet" formControlName="MakerId">\n          <ion-option value="">กรุณาเลือก MoldMaker</ion-option>\n          <ion-option *ngFor="let items of moldinoutModel?.getSetting?.makerId" [value]="items?.id">{{ items?.description }}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label>Plan Out</ion-label>\n        <ion-datetime pickerFormat="DD/MM/YYYY HH:mm" displayFormat="DD/MM/YYYY HH:mm" formControlName="PlanOut"></ion-datetime>\n      </ion-item>\n      <ion-item>\n        <ion-label>Plan In</ion-label>\n        <ion-datetime pickerFormat="DD/MM/YYYY HH:mm" displayFormat="DD/MM/YYYY HH:mm" formControlName="PlanIn" [max]="someMaxDateFromComponent"></ion-datetime>\n      </ion-item>\n      <ion-item>\n        <ion-label>Reason</ion-label>\n        <ion-select interface="action-sheet" formControlName="ReasonId">\n          <ion-option value="">กรุณาเลือก MoldMaker</ion-option>\n          <ion-option *ngFor="let items of moldinoutModel?.getSetting?.reason" [value]="items?.id">{{ items?.description }}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Remark</ion-label>\n        <ion-textarea formControlName="Remark" placeholder="Remark" rows="5" autosize></ion-textarea>\n      </ion-item>\n    </ion-list>\n  </form>\n\n  <ion-segment [(ngModel)]="Mode">\n    <ion-segment-button value="Out">\n      <h6>OUT</h6>\n    </ion-segment-button>\n    <ion-segment-button value="In" [disabled]="true">\n      <h6>IN</h6>\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]="Mode">\n    <ion-list *ngSwitchCase="\'Out\'">\n      <accordion-list\n        [title]="\'%label.images%\' | translate"\n        textColor="#FFF"\n        hasMargin="false"\n        headerColor="#A9ADAA"\n        [expanded]="true">\n        <div class="imagesType" *ngIf="moldinoutModel?.Images?.length > 0">\n          <a class="imagesName"></a>\n          <a class="imagesEdit" [navPush]="\'EditImagesPage\'"><ion-icon name="move"></ion-icon> Edit</a>\n        </div>\n        <ion-grid>\n          <ion-row>\n            <ion-col col-6 *ngFor="let photo of moldinoutModel?.Images; let id = index">\n              <ion-card class="card-background-page">\n                <ion-icon name="trash" class="deleteIcon" (click)="deletePhoto(id)"></ion-icon>\n                <img style="height: 150px" [src]="photo?.photo" (tap)="tapViewPhoto(photo?.photo)" alt="" />\n                <ion-card-content>\n                  <p class="gps" (click)="openLaunchNavigator(photo?.latitude, photo?.longitude)">{{ photo?.latitude }}, {{ photo?.longitude }}</p>\n                </ion-card-content>\n              </ion-card>\n            </ion-col>\n            <ion-col col-6 *ngIf="moldinoutModel?.Images?.length < limit">\n              <ion-card class="card-background-page" (click)="presentActionSheet()">\n                <ion-icon name="add" class="addIcon"></ion-icon>\n                <img style="height: 150px" src="../../assets/imgs/images.png" alt=""/>\n                <ion-card-content></ion-card-content>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-grid> \n      </accordion-list>\n      \n    </ion-list>\n\n    <ion-list *ngSwitchCase="\'In\'">\n      \n    </ion-list>\n  </div>\n</ion-content>\n\n<ion-footer>\n  <ion-item no-padding class="btn-setting-fav">\n    <ion-row class="two-col-button">\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 class="cancel-btn" (click)="cancelTransaction()">{{ \'%btn.back%\' | translate }}</button>\n      </ion-col>\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 [disabled]="!form.valid" (click)="preview()">Preview</button>\n      </ion-col>\n    </ion-row>\n  </ion-item>\n</ion-footer>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-create/moldinout-create.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_6__providers_moldinout_provider_moldinout_provider__["a" /* MoldinoutProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_scan_scan__["a" /* ScanProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], MoldinoutCreatePage);
    return MoldinoutCreatePage;
}(__WEBPACK_IMPORTED_MODULE_1__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=moldinout-create.js.map

/***/ })

});
//# sourceMappingURL=16.js.map