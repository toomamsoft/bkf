webpackJsonp([3],{

/***/ 439:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SurveyHistoryDetailPageModule", function() { return SurveyHistoryDetailPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__survey_history_detail__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var SurveyHistoryDetailPageModule = /** @class */ (function () {
    function SurveyHistoryDetailPageModule() {
    }
    SurveyHistoryDetailPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__survey_history_detail__["a" /* SurveyHistoryDetailPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__survey_history_detail__["a" /* SurveyHistoryDetailPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__survey_history_detail__["a" /* SurveyHistoryDetailPage */]
            ]
        })
    ], SurveyHistoryDetailPageModule);
    return SurveyHistoryDetailPageModule;
}());

//# sourceMappingURL=survey-history-detail.module.js.map

/***/ }),

/***/ 468:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyHistoryDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_survey_provider_survey_provider__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_constant__ = __webpack_require__(3);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the SurveyHistoryDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SurveyHistoryDetailPage = /** @class */ (function (_super) {
    __extends(SurveyHistoryDetailPage, _super);
    function SurveyHistoryDetailPage(navCtrl, events, formBuilder, _surveyProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this.formBuilder = formBuilder;
        _this._surveyProvider = _surveyProvider;
        _this.IP = __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].IPAddress;
        _this.form = _this.formBuilder.group({
            Location: [{ value: null, disabled: true }],
            MoldId: [{ value: null, disabled: true }],
            AssetId: [{ value: null, disabled: true }],
            CustNo: [{ value: null, disabled: true }],
            PartNo: [{ value: null, disabled: true }],
            PartName: [{ value: null, disabled: true }],
            Plate: [{ value: null, disabled: true }],
            Creator: [{ value: null, disabled: true }],
            Remark: [{ value: null, disabled: true }]
        });
        return _this;
    }
    SurveyHistoryDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SurveyHistoryDetailPage');
    };
    SurveyHistoryDetailPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var AssetId, CustNo, PartNo, PartName, i, ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        AssetId = '';
                        CustNo = '';
                        PartNo = '';
                        PartName = '';
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        this.surveyModel = this._surveyProvider.surVeyModel;
                        return [4 /*yield*/, this._surveyProvider.getMoldHistoryDetail()];
                    case 2:
                        _a.sent();
                        this.form.controls['Location'].setValue(this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].location);
                        this.form.controls['MoldId'].setValue(this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].MoldId);
                        for (i = 0; i < this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description.length; i++) {
                            if (i == 0) {
                                AssetId = this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].AssetId;
                                CustNo = this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].CustNo;
                                PartNo = this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].PartNo;
                                PartName = this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].PartName;
                            }
                            else {
                                AssetId += '\n' + this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].AssetId;
                                CustNo += '\n' + this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].CustNo;
                                PartNo += '\n' + this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].PartNo;
                                PartName += '\n' + this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Description[i].PartName;
                            }
                        }
                        this.form.controls['AssetId'].setValue(AssetId);
                        this.form.controls['CustNo'].setValue(CustNo);
                        this.form.controls['PartNo'].setValue(PartNo);
                        this.form.controls['PartName'].setValue(PartName);
                        this.form.controls['Plate'].setValue(this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Plate);
                        this.form.controls['Remark'].setValue(this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].Remark);
                        this.form.controls['Creator'].setValue(this.surveyModel.surveyMoldHistoryDetail.historyDetail[0].FullName);
                        return [3 /*break*/, 4];
                    case 3:
                        ex_1 = _a.sent();
                        this._surveyProvider.submitErrorEvent(this._surveyProvider.surVeyModel2);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    SurveyHistoryDetailPage.prototype.viewImages = function (imageUrl, title) {
        if (imageUrl === void 0) { imageUrl = ''; }
        if (title === void 0) { title = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var view;
            return __generator(this, function (_a) {
                view = this.IP + "/survey/showImage?filename='=" + imageUrl;
                __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].viewImages(view, title);
                return [2 /*return*/];
            });
        });
    };
    SurveyHistoryDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* Component */])({
            selector: 'page-survey-history-detail',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/survey-history-detail/survey-history-detail.html"*/'<!--\n  Generated template for the SurveyHistoryDetailPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>History</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]="form">\n    <ion-list>\n      <ion-item>\n        <ion-label fixed>Location</ion-label>\n        <ion-input formControlName="Location" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Mold ID</ion-label>\n        <ion-input formControlName="MoldId" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>AssetID</ion-label>\n        <ion-textarea formControlName="AssetId" placeholder="AssetID" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>CustNo</ion-label>\n        <ion-textarea formControlName="CustNo" placeholder="CustNo" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>PartNo</ion-label>\n        <ion-textarea formControlName="PartNo" placeholder="PartNo" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>PartName</ion-label>\n        <ion-textarea formControlName="PartName" placeholder="PartName" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Plate</ion-label>\n        <ion-input type="text"  formControlName="Plate"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>{{ \'%label.remark%\' | translate }}</ion-label>\n        <ion-textarea formControlName="Remark" placeholder="Remark" rows="1" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>{{ \'%label.creator%\' | translate }}</ion-label>\n        <ion-input formControlName="Creator" type="text"></ion-input>\n      </ion-item>\n    </ion-list>\n\n    <div padding *ngIf="surveyModel?.surveyMoldHistoryDetail?.images?.length > 0">\n      <accordion-list \n        [title]="\'%label.images%\' | translate"\n        textColor="#FFF"\n        hasMargin="false"\n        headerColor="#f4f4f4"\n        [expanded]="true">\n        <ion-grid>\n          <ion-row>\n            <ion-col col-6 *ngFor="let photo of surveyModel?.surveyMoldHistoryDetail?.images; let id = index">\n              <ion-card class="block">\n                <!-- <img style="height: 200px;" [src]="photo.Photo" alt="" (tap)="viewImages(photo.Photo)" /> -->\n                <img style="height: 200px;" src="{{IP}}/survey/showImage?filename={{photo.Photo}}" alt="" (tap)="viewImages(photo.Photo)" />\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </accordion-list>\n    </div>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/survey-history-detail/survey-history-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__providers_survey_provider_survey_provider__["a" /* SurveyProvider */]])
    ], SurveyHistoryDetailPage);
    return SurveyHistoryDetailPage;
}(__WEBPACK_IMPORTED_MODULE_1__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=survey-history-detail.js.map

/***/ })

});
//# sourceMappingURL=3.js.map