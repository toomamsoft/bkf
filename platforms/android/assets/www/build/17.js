webpackJsonp([17],{

/***/ 444:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoldinoutConfirmOutPageModule", function() { return MoldinoutConfirmOutPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__moldinout_confirm_out__ = __webpack_require__(473);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var MoldinoutConfirmOutPageModule = /** @class */ (function () {
    function MoldinoutConfirmOutPageModule() {
    }
    MoldinoutConfirmOutPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_confirm_out__["a" /* MoldinoutConfirmOutPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__moldinout_confirm_out__["a" /* MoldinoutConfirmOutPage */]),
                __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_confirm_out__["a" /* MoldinoutConfirmOutPage */]
            ]
        })
    ], MoldinoutConfirmOutPageModule);
    return MoldinoutConfirmOutPageModule;
}());

//# sourceMappingURL=moldinout-confirm-out.module.js.map

/***/ }),

/***/ 473:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldinoutConfirmOutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_scan_scan__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_location_accuracy__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_moldinout_provider_moldinout_provider__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};













/**
 * Generated class for the MoldinoutConfirmOutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var gpsOptions = {
    enableHighAccuracy: true
};
var MoldinoutConfirmOutPage = /** @class */ (function (_super) {
    __extends(MoldinoutConfirmOutPage, _super);
    function MoldinoutConfirmOutPage(navCtrl, events, locationAccuracy, geolocation, alertCtrl, formBuilder, _moldinoutProvider, _storageProvider, _scanProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this.locationAccuracy = locationAccuracy;
        _this.geolocation = geolocation;
        _this.alertCtrl = alertCtrl;
        _this.formBuilder = formBuilder;
        _this._moldinoutProvider = _moldinoutProvider;
        _this._storageProvider = _storageProvider;
        _this._scanProvider = _scanProvider;
        _this.FullName = '';
        _this.CreateDate = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].getCurrentDateTimeMySql() || '';
        _this.Latitude = 0.000000000000000;
        _this.Longitude = 0.000000000000000;
        _this.tmpFile1 = [];
        _this.tmpFile2 = [];
        _this.tmpFile3 = [];
        _this.tmpFile4 = [];
        _this.form = _this.formBuilder.group({
            Remark: ['']
        });
        return _this;
    }
    MoldinoutConfirmOutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MoldinoutConfirmOutPage');
    };
    MoldinoutConfirmOutPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].USER_NAME)];
                    case 1:
                        _a.FullName = _b.sent();
                        this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
                        this.locationAccuracy.canRequest().then(function (canRequest) {
                            if (canRequest) {
                                // the accuracy option will be ignored by iOS
                                _this.locationAccuracy.request(_this.locationAccuracy.REQUEST_PRIORITY_LOW_POWER).then(function () {
                                    console.log('requesting location permissions success');
                                }, function (error) { return console.log('Error requesting location permissions', error); });
                            }
                        }).catch(function (error) {
                            var alert = _this.alertCtrl.create({
                                title: 'Attention!',
                                subTitle: error,
                                buttons: ['Close']
                            });
                            alert.present();
                        });
                        this.geolocation.getCurrentPosition(gpsOptions).then(function (resp) {
                            console.log('requesting geolocation:::', resp);
                            _this.Latitude = resp.coords.latitude;
                            _this.Longitude = resp.coords.longitude;
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutConfirmOutPage.prototype.openCamera = function (type) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._scanProvider.getImageFromCamere().then(function (val) {
                            if (val && val != 'No Image Selected' && val != 'cordova_not_available') {
                                if (type == '1') {
                                    _this.tmpFile1.push({ photo: val.toString(), latitude: _this.Latitude, longitude: _this.Longitude });
                                }
                                else if (type == '2') {
                                    _this.tmpFile2.push({ photo: val.toString(), latitude: _this.Latitude, longitude: _this.Longitude });
                                }
                                else if (type == '3') {
                                    _this.tmpFile3.push({ photo: val.toString(), latitude: _this.Latitude, longitude: _this.Longitude });
                                }
                                else if (type == '4') {
                                    _this.tmpFile4.push({ photo: val.toString(), latitude: _this.Latitude, longitude: _this.Longitude });
                                }
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutConfirmOutPage.prototype.deletePhoto = function (type) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var confirm;
            return __generator(this, function (_a) {
                confirm = this.alertCtrl.create({
                    title: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%message.title%"],
                    subTitle: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%message.delete.image%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%message.delete.image%"],
                    buttons: [
                        {
                            text: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%btn.logout.no%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%btn.logout.no%"],
                            handler: function () {
                                console.log('Disagree clicked');
                            }
                        }, {
                            text: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%btn.logout.yes%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%btn.logout.yes%"],
                            handler: function () {
                                if (type == '1') {
                                    _this.tmpFile1 = [];
                                }
                                else if (type == '2') {
                                    _this.tmpFile2 = [];
                                }
                                else if (type == '3') {
                                    _this.tmpFile3 = [];
                                }
                                else if (type == '4') {
                                    _this.tmpFile4 = [];
                                }
                            }
                        }
                    ]
                });
                confirm.present();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutConfirmOutPage.prototype.saveData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.moldinoutModel.ConfirmRemark = this.form.controls['Remark'].value;
                        this.moldinoutModel.ConfirmStatus = 'Y';
                        return [4 /*yield*/, this._moldinoutProvider.ConfirmOrRejectOut()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].viewFilePDF(this.moldinoutModel.statment.pdf)];
                    case 2:
                        _a.sent();
                        this.alertCtrl.create({
                            enableBackdropDismiss: false,
                            title: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%message.title%"],
                            subTitle: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%message.save.success%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%message.save.success%"],
                            buttons: [{
                                    text: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%btn.close%"],
                                    handler: function () {
                                        _this.popToPage('MoldinoutMenuPage', _this.navCtrl);
                                    }
                                }]
                        }).present();
                        return [3 /*break*/, 4];
                    case 3:
                        ex_1 = _a.sent();
                        this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutConfirmOutPage.prototype.ConfirmSaveData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.moldinoutModel.Images = [];
                this.tmpFile4.length > 0 ? this.moldinoutModel.Images.push({ photo: this.tmpFile1[0].photo, latitude: this.tmpFile1[0].latitude, longitude: this.tmpFile1[0].longitude }) : '';
                this.tmpFile2.length > 0 ? this.moldinoutModel.Images.push({ photo: this.tmpFile2[0].photo, latitude: this.tmpFile2[0].latitude, longitude: this.tmpFile2[0].longitude }) : '';
                this.tmpFile3.length > 0 ? this.moldinoutModel.Images.push({ photo: this.tmpFile3[0].photo, latitude: this.tmpFile3[0].latitude, longitude: this.tmpFile3[0].longitude }) : '';
                this.tmpFile4.length > 0 ? this.moldinoutModel.Images.push({ photo: this.tmpFile4[0].photo, latitude: this.tmpFile4[0].latitude, longitude: this.tmpFile4[0].longitude }) : '';
                if (this.moldinoutModel.Images.length < 4) {
                    this.alertCtrl.create({
                        title: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%message.title%"],
                        subTitle: 'จำนวนรูปภาพไม่ครบ กรุณาตรวจสอบอีกครั้ง',
                        buttons: ['OK']
                    }).present();
                    return [2 /*return*/];
                }
                this.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%title.confirm%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%title.confirm%"],
                    subTitle: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%message.save.success%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%message.save.success%"],
                    buttons: [{
                            text: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%btn.cancel%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%btn.cancel%"],
                            handler: function () {
                                console.log('Click Cancel');
                            }
                        },
                        {
                            text: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%btn.confirm%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%btn.confirm%"],
                            handler: function () {
                                _this.saveData();
                            }
                        }]
                }).present();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutConfirmOutPage.prototype.backPage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: __WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_12__assets_i18n_en_json__["%message.title%"],
                    subTitle: 'คุณต้องการยกเลิกรายการนี้ใช่ไหม',
                    buttons: [{
                            text: 'ยกลเลิก',
                            handler: function () {
                                console.log('cancel click');
                            }
                        }, {
                            text: 'ยืนยัน',
                            handler: function () {
                                _this.popToPage('MoldinoutMenuPage', _this.navCtrl);
                            }
                        }]
                }).present();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutConfirmOutPage.prototype.tapViewPhoto = function (imageBase64) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].viewImages(imageBase64);
                return [2 /*return*/];
            });
        });
    };
    MoldinoutConfirmOutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* Component */])({
            selector: 'page-moldinout-confirm-out',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-confirm-out/moldinout-confirm-out.html"*/'<!--\n  Generated template for the MoldinoutConfirmOutPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>ConfirmOut</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-5 align-self-center>\n        <p style="font-family:sans-serif; font-size: 1.1em; text-align: right;">Confirm Out:</p>\n      </ion-col>\n      <ion-col col-7 align-self-center>\n        <p style="font-family:sans-serif; font-size: 1em; text-align: left;">{{ CreateDate | datetime: \'dateAndTime\' }}</p>\n      </ion-col>\n      <ion-col col-5 align-self-center>\n        <p style="font-family:sans-serif; font-size: 1.1em; text-align: right;">By:</p>\n      </ion-col>\n      <ion-col col-7 align-self-center>\n        <p style="font-family:sans-serif; font-size: 1em; text-align: left;">{{ FullName }}</p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-list>\n    <ion-item-divider>\n      <ion-label>\n        Picture Trasport:\n      </ion-label>\n    </ion-item-divider>\n    <ion-grid class="card-background-page">\n      <ion-row>\n        <ion-col col-6 *ngIf="tmpFile1?.length < 1">\n          <pre style="text-align: center;">รูปทะเบียนรถ</pre>\n          <ion-card class="block">\n            <ion-icon name="add" class="addIcon" (click)="openCamera(1)"></ion-icon>\n            <img style="height: 150px;" src="../../assets/imgs/images.png" alt=""/>\n          </ion-card>\n        </ion-col>\n        \n        <ion-col col-6 *ngFor="let photo of tmpFile1;">\n          <pre style="text-align: center;">รูปทะเบียนรถ</pre>\n          <ion-card class="block">\n            <ion-icon name="trash" class="deleteIcon" (click)="deletePhoto(1)"></ion-icon>\n            <img style="height: 150px;" [src]="photo?.photo" (tap)="tapViewPhoto(photo?.photo)" alt="" />\n            <ion-card-content>\n              <p class="gps">{{ photo?.latitude }}, {{ photo?.longitude }}</p>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n\n        <ion-col col-6 *ngIf="tmpFile2?.length < 1">\n          <pre style="text-align: center;">รูปใบขับขี่</pre>\n          <ion-card class="block">\n            <ion-icon name="add" class="addIcon" (click)="openCamera(2)"></ion-icon>\n            <img style="height: 150px;" src="../../assets/imgs/images.png" alt=""/>\n          </ion-card>\n        </ion-col>\n        \n        <ion-col col-6 *ngFor="let photo of tmpFile2;">\n          <pre style="text-align: center;">รูปใบขับขี่</pre>\n          <ion-card class="block">\n            <ion-icon name="trash" class="deleteIcon" (click)="deletePhoto(2)"></ion-icon>\n            <img style="height: 150px;" [src]="photo?.photo" (tap)="tapViewPhoto(photo?.photo)" alt=""/>\n            <ion-card-content>\n              <p class="gps">{{ photo?.latitude }}, {{ photo?.longitude }}</p>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n\n        <ion-col col-6 *ngIf="tmpFile3?.length < 1">\n          <pre style="text-align: center;">รูปหลังรถ</pre>\n          <ion-card class="block">\n            <ion-icon name="add" class="addIcon" (click)="openCamera(3)"></ion-icon>\n            <img style="height: 150px;" src="../../assets/imgs/images.png" alt=""/>\n          </ion-card>\n        </ion-col>\n        \n        <ion-col col-6 *ngFor="let photo of tmpFile3;">\n          <pre style="text-align: center;">รูปหลังรถ</pre>\n          <ion-card class="block">\n            <ion-icon name="trash" class="deleteIcon" (click)="deletePhoto(3)"></ion-icon>\n            <img style="height: 150px;" [src]="photo?.photo" (tap)="tapViewPhoto(photo?.photo)" alt=""/>\n            <ion-card-content>\n              <p class="gps">{{ photo?.latitude }}, {{ photo?.longitude }}</p>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n\n        <ion-col col-6 *ngIf="tmpFile4?.length < 1">\n          <pre style="text-align: center;">อื่นๆ</pre>\n          <ion-card class="block">\n            <ion-icon name="add" class="addIcon" (click)="openCamera(4)"></ion-icon>\n            <img style="height: 150px;" src="../../assets/imgs/images.png" alt=""/>\n          </ion-card>\n        </ion-col>\n\n        <ion-col col-6 *ngFor="let photo of tmpFile4;">\n          <pre style="text-align: center;">อื่นๆ</pre>\n          <ion-card class="block">\n            <ion-icon name="trash" class="deleteIcon" (click)="deletePhoto(4)"></ion-icon>\n            <img style="height: 150px;" [src]="photo?.photo" (tap)="tapViewPhoto(photo?.photo)" alt=""/>\n            <ion-card-content>\n              <p class="gps">{{ photo?.latitude }}, {{ photo?.longitude }}</p>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-list>\n  <form [formGroup]="form">\n    <ion-item>\n      <ion-label floating>Remark</ion-label>\n      <ion-textarea formControlName="Remark" rows="6" autosize></ion-textarea>\n    </ion-item>\n  </form>\n</ion-content>\n\n<ion-footer>\n  <ion-item no-padding class="btn-setting-fav">\n    <ion-row class="two-col-button">\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 class="cancel-btn" (click)="backPage()">{{ \'%btn.back%\' | translate }}</button>\n      </ion-col>\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 [disabled]="!form.valid" (click)="ConfirmSaveData()">{{ \'%btn.survey.save%\' | translate }}</button>\n      </ion-col>\n    </ion-row>\n  </ion-item>\n</ion-footer>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-confirm-out/moldinout-confirm-out.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_10__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_8__providers_moldinout_provider_moldinout_provider__["a" /* MoldinoutProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_storage_provider_storage_provider__["a" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_0__providers_scan_scan__["a" /* ScanProvider */]])
    ], MoldinoutConfirmOutPage);
    return MoldinoutConfirmOutPage;
}(__WEBPACK_IMPORTED_MODULE_6__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=moldinout-confirm-out.js.map

/***/ })

});
//# sourceMappingURL=17.js.map