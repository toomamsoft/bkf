webpackJsonp([4],{

/***/ 446:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SurveyCreatePageModule", function() { return SurveyCreatePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__survey_create__ = __webpack_require__(475);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var SurveyCreatePageModule = /** @class */ (function () {
    function SurveyCreatePageModule() {
    }
    SurveyCreatePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__survey_create__["a" /* SurveyCreatePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__survey_create__["a" /* SurveyCreatePage */]),
                __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__survey_create__["a" /* SurveyCreatePage */],
            ]
        })
    ], SurveyCreatePageModule);
    return SurveyCreatePageModule;
}());

//# sourceMappingURL=survey-create.module.js.map

/***/ }),

/***/ 475:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyCreatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_SurveyModel__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_survey_provider_survey_provider__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_scan_scan__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_storage_provider_storage_provider__ = __webpack_require__(17);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












/**
 * Generated class for the SurveyCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SurveyCreatePage = /** @class */ (function (_super) {
    __extends(SurveyCreatePage, _super);
    function SurveyCreatePage(navCtrl, navParams, events, formBuilder, alertCtrl, _surveyProvider, _scanProvider, _storageProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.events = events;
        _this.formBuilder = formBuilder;
        _this.alertCtrl = alertCtrl;
        _this._surveyProvider = _surveyProvider;
        _this._scanProvider = _scanProvider;
        _this._storageProvider = _storageProvider;
        _this.Mode = 'Transaction';
        _this.limitImage = 5;
        _this.formMold = _this.formBuilder.group({
            Location: ['', [__WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].minLength(1)]],
            MoldId: ['', [__WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].minLength(8)]]
        });
        _this.setFormTransaction();
        return _this;
    }
    SurveyCreatePage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, Promise.all([this.surveyModel = this._surveyProvider.surVeyModel])];
                    case 1:
                        _c.sent();
                        this.getLocation();
                        _a = this;
                        _b = Number;
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].IMAGE_MOLDSERVEY)];
                    case 2:
                        _a.limitImage = _b.apply(void 0, [_c.sent()]);
                        return [2 /*return*/];
                }
            });
        });
    };
    SurveyCreatePage.prototype.ngOnDestroy = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this._surveyProvider.surVeyModel = new __WEBPACK_IMPORTED_MODULE_4__models_SurveyModel__["a" /* SurveyModel */]();
                return [2 /*return*/];
            });
        });
    };
    SurveyCreatePage.prototype.ionViewDidLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log('ionViewDidLoad SurveyCreatePage');
                return [2 /*return*/];
            });
        });
    };
    SurveyCreatePage.prototype.getLocation = function () {
        return __awaiter(this, void 0, void 0, function () {
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Promise.all([this._surveyProvider.getLocationSurvey()])];
                    case 1:
                        _a.sent();
                        this.dataLocation = this.surveyModel.locationDescription;
                        this.surveyModel.Images = [];
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _a.sent();
                        this._surveyProvider.submitErrorEvent(this._surveyProvider.surVeyModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SurveyCreatePage.prototype.getMoldMaster = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert_1, alert_2, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        if (!(this.formMold.controls['MoldId'].value.length >= 8)) return [3 /*break*/, 2];
                        this.surveyModel.MoldId = this.formMold.controls['MoldId'].value;
                        return [4 /*yield*/, Promise.all([this._surveyProvider.getMoldHistory()])];
                    case 1:
                        _a.sent();
                        if (this.surveyModel.surveyMoldHistory.master.length > 0) {
                            this.formTransaction.controls['AssetId'].setValue(this.surveyModel.surveyMoldHistory.master[0].AssetId || this.surveyModel.AssetId);
                            this.formTransaction.controls['CustNo'].setValue(this.surveyModel.surveyMoldHistory.master[0].CustNo || this.surveyModel.CustNo);
                            this.formTransaction.controls['PartNo'].setValue(this.surveyModel.surveyMoldHistory.master[0].PartNo || this.surveyModel.PartNo);
                            this.formTransaction.controls['PartName'].setValue(this.surveyModel.surveyMoldHistory.master[0].PartName || this.surveyModel.PartName);
                        }
                        else if (this.surveyModel.surveyMoldHistory.master.length <= 0) {
                            this.setFormTransaction();
                            alert_1 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%error.moldmaster%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%error.moldmaster%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_1.present();
                        }
                        if (this.surveyModel.surveyMoldHistory.history.length > 0) {
                            alert_2 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.moldmaster%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.moldmaster%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_2.present();
                        }
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        this._surveyProvider.submitErrorEvent(this._surveyProvider.surVeyModel2);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    SurveyCreatePage.prototype.preview = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert_3;
            return __generator(this, function (_a) {
                if (this.formMold.controls['Location'].value === '') {
                    alert_3 = this.alertCtrl.create({
                        enableBackdropDismiss: false,
                        title: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.title%"],
                        subTitle: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%error.location%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%error.location%"],
                        buttons: [{
                                text: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.close%"]
                            }]
                    });
                    alert_3.present();
                    return [2 /*return*/];
                }
                this.surveyModel.Location = this.formMold.controls['Location'].value;
                this.surveyModel.AssetId = this.formTransaction.controls['AssetId'].value;
                this.surveyModel.CustNo = this.formTransaction.controls['CustNo'].value;
                this.surveyModel.PartNo = this.formTransaction.controls['PartNo'].value;
                this.surveyModel.PartName = this.formTransaction.controls['PartName'].value;
                this.surveyModel.Remark = this.formTransaction.controls['Remark'].value;
                this.surveyModel.Plate = this.formTransaction.controls['NoPlate'].value === true ? 'No' : 'Yes';
                this.navCtrl.push('SurveyPreviewPage');
                return [2 /*return*/];
            });
        });
    };
    SurveyCreatePage.prototype.reset = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.title%"],
                    subTitle: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.reset%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.reset%"],
                    buttons: [{
                            text: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.logout.no%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.logout.no%"],
                            handler: function () {
                                console.log('Close Reset');
                            }
                        }, {
                            text: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.logout.yes%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.logout.yes%"],
                            handler: function () {
                                _this._surveyProvider.surVeyModel = new __WEBPACK_IMPORTED_MODULE_4__models_SurveyModel__["a" /* SurveyModel */]();
                                _this.surveyModel = _this._surveyProvider.surVeyModel;
                                _this.formMold = _this.formBuilder.group({
                                    Location: ['', [__WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required]],
                                    MoldId: ['', [__WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].minLength(8)]]
                                });
                                _this.formTransaction = _this.formBuilder.group({
                                    AssetId: [null],
                                    CustNo: ['', [__WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required]],
                                    PartNo: ['', [__WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required]],
                                    PartName: [null],
                                    Plate: [false],
                                    NoPlate: [true],
                                    Remark: [null]
                                });
                                _this.getLocation();
                            }
                        }]
                }).present();
                return [2 /*return*/];
            });
        });
    };
    SurveyCreatePage.prototype.viewHistory = function (Items) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.surveyModel.MoldId = Items.MoldId;
                this.surveyModel.RunNo = Items.RunNo;
                this.navCtrl.push('SurveyHistoryDetailPage');
                return [2 /*return*/];
            });
        });
    };
    SurveyCreatePage.prototype.onChangePlate = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (this.formTransaction.controls['NoPlate'].value) {
                    this.formTransaction.controls['NoPlate'].setValue(false);
                    this.formTransaction.controls['Plate'].setValue(true);
                }
                else if (!this.formTransaction.controls['Plate'].value && !this.formTransaction.controls['NoPlate'].value) {
                    this.formTransaction.controls['Plate'].setValue(true);
                }
                return [2 /*return*/];
            });
        });
    };
    SurveyCreatePage.prototype.onChangeNoPlate = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (this.formTransaction.controls['Plate'].value) {
                    this.formTransaction.controls['NoPlate'].setValue(true);
                    this.formTransaction.controls['Plate'].setValue(false);
                }
                else if (!this.formTransaction.controls['Plate'].value && !this.formTransaction.controls['NoPlate'].value) {
                    this.formTransaction.controls['NoPlate'].setValue(true);
                }
                return [2 /*return*/];
            });
        });
    };
    SurveyCreatePage.prototype.scanQr = function () {
        return __awaiter(this, void 0, void 0, function () {
            var txtScanQR_1, ex_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._scanProvider.scan().then(function (val) {
                                txtScanQR_1 = val.toString();
                            })];
                    case 1:
                        _a.sent();
                        if (txtScanQR_1) {
                            this.surveyModel.MoldId = txtScanQR_1.split(/\|/)[0] || '';
                            this.surveyModel.AssetId = txtScanQR_1.split(/\|/)[1] || '';
                            this.surveyModel.CustNo = txtScanQR_1.split(/\|/)[2] || '';
                            this.surveyModel.PartNo = txtScanQR_1.split(/\|/)[3] || '';
                            this.surveyModel.PartName = txtScanQR_1.split(/\|/)[4] || '';
                            this.formMold.controls['MoldId'].setValue(txtScanQR_1.split(/\|/)[0] || '');
                            this.getMoldMaster();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        ex_2 = _a.sent();
                        console.log(ex_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SurveyCreatePage.prototype.getImageFromCamera = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var alert_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.surveyModel.Images.length >= this.limitImage) {
                            alert_4 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.photo%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.photo%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_4.present();
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this._scanProvider.getImageFromCamere().then(function (val) {
                                if (val && val != 'No Image Selected' && val != 'cordova_not_available') {
                                    _this.surveyModel.Images.push(val);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SurveyCreatePage.prototype.getImageFromAlbum = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var images, alert_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.surveyModel.Images.length >= this.limitImage) {
                            alert_5 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.photo%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.photo%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_5.present();
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this._scanProvider.getImageFormAlbum(this.limitImage - this.surveyModel.Images.length).then(function (val) {
                                if (val && val != 'cordova_not_available') {
                                    images = val;
                                    for (var i = 0; i < images.length; i++) {
                                        _this.surveyModel.Images.push(images[i]);
                                    }
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SurveyCreatePage.prototype.deletePhoto = function (index) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var confirm;
            return __generator(this, function (_a) {
                confirm = this.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.title%"],
                    subTitle: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.delete.image%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.delete.image%"],
                    buttons: [
                        {
                            text: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.logout.no%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.logout.no%"],
                            handler: function () {
                                console.log('Disagree clicked');
                            }
                        }, {
                            text: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.logout.yes%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.logout.yes%"],
                            handler: function () {
                                _this.surveyModel.Images.splice(index, 1);
                            }
                        }
                    ]
                });
                confirm.present();
                return [2 /*return*/];
            });
        });
    };
    SurveyCreatePage.prototype.viewImages = function (imageUrl, title) {
        if (imageUrl === void 0) { imageUrl = ''; }
        if (title === void 0) { title = ''; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                __WEBPACK_IMPORTED_MODULE_10__shared_utilities__["a" /* Utilities */].viewImages(imageUrl, title);
                return [2 /*return*/];
            });
        });
    };
    SurveyCreatePage.prototype.setFormTransaction = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.formTransaction = this.formBuilder.group({
                    AssetId: [null],
                    CustNo: ['', [__WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required]],
                    PartNo: ['', [__WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required]],
                    PartName: [null],
                    Plate: [false],
                    NoPlate: [true],
                    Remark: [null]
                });
                return [2 /*return*/];
            });
        });
    };
    SurveyCreatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-survey-create',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/survey-create/survey-create.html"*/'<!--\n  Generated template for the SurveyCreatePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>Survey</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-fab right bottom *ngIf="surveyModel?.surveyMoldHistory?.master?.length > 0 && Mode == \'Transaction\'">\n    <button ion-fab mini #fab><ion-icon name="arrow-dropup"></ion-icon></button>\n    <ion-fab-list side="top">\n      <button ion-fab (click)="getImageFromCamera()"><ion-icon name="camera"></ion-icon></button>\n      <button ion-fab (click)="getImageFromAlbum()"><ion-icon name="albums"></ion-icon></button>\n    </ion-fab-list>\n  </ion-fab>\n  <form [formGroup]="formMold">\n    <ion-list>\n      <ion-item>\n        <ion-label>Location</ion-label>\n        <ion-select interface="action-sheet" formControlName="Location">\n          <ion-option value="">กรุณาเลือกตำแหน่ง</ion-option>\n          <ion-option *ngFor="let items of dataLocation" [value]="items?.LocationID">{{ items?.LocationDescription }}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label color="primary" floating>Mold ID</ion-label>\n        <ion-input formControlName="MoldId" type="text" (ionBlur)="getMoldMaster()"></ion-input>\n        <button ion-button icon-only clear item-end color="green" (click)="scanQr()">\n          <ion-icon name="qr-scanner"></ion-icon>\n        </button>\n      </ion-item>\n    </ion-list>\n  </form>\n\n  <div *ngIf="surveyModel?.surveyMoldHistory?.master?.length > 0" >\n    <ion-segment [(ngModel)]="Mode">\n      <ion-segment-button value="Transaction">\n        <h6>MOLD</h6>\n      </ion-segment-button>\n      <ion-segment-button value="History">\n        <h6>HISTORY</h6>\n      </ion-segment-button>\n    </ion-segment>\n\n    <div [ngSwitch]="Mode">\n      <ion-list *ngSwitchCase="\'Transaction\'">\n        <form [formGroup]="formTransaction">\n          <ion-item>\n            <ion-label>Plate</ion-label>\n            <ion-checkbox fromControlName="Plate" [checked]="formTransaction.controls[\'Plate\'].value"\n              value="formTransaction.controls[\'NoPlate\'].value" (click)="onChangePlate()"></ion-checkbox>\n          </ion-item>\n          <ion-item>\n            <ion-label>No Plate</ion-label>\n            <ion-checkbox fromControlName="NoPlate" [checked]="formTransaction.controls[\'NoPlate\'].value" \n              value="formTransaction.controls[\'Plate\'].value" (click)="onChangeNoPlate()"></ion-checkbox>\n          </ion-item>\n          <ion-item>\n            <ion-textarea formControlName="Remark" placeholder="Remark" rows="6" autosize></ion-textarea>\n          </ion-item>\n        </form>\n\n        <div padding *ngIf="surveyModel?.Images?.length > 0">\n          <accordion-list \n            [title]="\'%label.images%\' | translate"\n            textColor="#FFF"\n            hasMargin="false"\n            headerColor="#f4f4f4"\n            [expanded]="true">\n            <ion-grid>\n              <ion-row>\n                <ion-col col-6 *ngFor="let photo of surveyModel?.Images; let id = index">\n                  <ion-card class="block">\n                    <ion-icon name="trash" class="deleteIcon" (click)="deletePhoto(id)"></ion-icon>\n                    <img style="height: 180px;" [src]="photo" alt="" (tap)="viewImages(photo)" />\n                  </ion-card>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </accordion-list>\n        </div>\n      </ion-list>\n  \n      <ion-list *ngSwitchCase="\'History\'">\n        <ion-item *ngFor="let items of surveyModel?.surveyMoldHistory?.history" (click)="viewHistory(items)">\n          <h3>MoldID: {{ items.MoldId }}</h3>\n          <p>CustNo: {{ items.CustNo }}</p>\n          <p>PartNo: {{ items.PartNo }}</p>\n          <p>PartName: {{ items.PartName }}</p>\n          <p>CreateDate: {{ items.CreateDate | datetime: \'dateAndTime\' }}</p>\n          <ion-icon name="arrow-forward" item-end></ion-icon>\n        </ion-item>\n      </ion-list>\n    </div>\n\n  </div>\n</ion-content>\n\n<ion-footer>\n  <ion-item no-padding class="btn-setting-fav">\n    <ion-row class="two-col-button">\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 class="cancel-btn" (click)="reset()">Reset</button>\n      </ion-col>\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 [disabled]="!formTransaction.valid && !formMold.valid" (click)="preview()">Preview</button>\n      </ion-col>\n    </ion-row>\n  </ion-item>\n</ion-footer>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/survey-create/survey-create.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_survey_provider_survey_provider__["a" /* SurveyProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_scan_scan__["a" /* ScanProvider */],
            __WEBPACK_IMPORTED_MODULE_11__providers_storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], SurveyCreatePage);
    return SurveyCreatePage;
}(__WEBPACK_IMPORTED_MODULE_3__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=survey-create.js.map

/***/ })

});
//# sourceMappingURL=4.js.map