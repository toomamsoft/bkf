webpackJsonp([8],{

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoldmakerHistoryDetailPageModule", function() { return MoldmakerHistoryDetailPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__moldmaker_history_detail__ = __webpack_require__(465);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MoldmakerHistoryDetailPageModule = /** @class */ (function () {
    function MoldmakerHistoryDetailPageModule() {
    }
    MoldmakerHistoryDetailPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__moldmaker_history_detail__["a" /* MoldmakerHistoryDetailPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__moldmaker_history_detail__["a" /* MoldmakerHistoryDetailPage */]),
            ],
        })
    ], MoldmakerHistoryDetailPageModule);
    return MoldmakerHistoryDetailPageModule;
}());

//# sourceMappingURL=moldmaker-history-detail.module.js.map

/***/ }),

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldmakerHistoryDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_moldmaker_provider_moldmaker_provider__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_utilities__ = __webpack_require__(16);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the MoldmakerHistoryDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MoldmakerHistoryDetailPage = /** @class */ (function (_super) {
    __extends(MoldmakerHistoryDetailPage, _super);
    function MoldmakerHistoryDetailPage(navCtrl, events, _moldmakerProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this._moldmakerProvider = _moldmakerProvider;
        return _this;
    }
    MoldmakerHistoryDetailPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.moldmakerModel = this._moldmakerProvider.moldmakerModel;
                return [2 /*return*/];
            });
        });
    };
    MoldmakerHistoryDetailPage.prototype.ionViewDidLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log('ionViewDidLoad MoldmakerHistoryDetailPage');
                return [2 /*return*/];
            });
        });
    };
    MoldmakerHistoryDetailPage.prototype.viewImages = function (photo) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].viewImages(photo);
                return [2 /*return*/];
            });
        });
    };
    MoldmakerHistoryDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-moldmaker-history-detail',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldmaker-history-detail/moldmaker-history-detail.html"*/'<!--\n  Generated template for the MoldmakerHistoryDetailPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>moldmaker-history-detail</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-grid>\n      <ion-row>\n        <ion-col col-4>\n          <h3>Stauts</h3>\n        </ion-col>\n        <ion-col col-8>\n          <p>{{ moldmakerModel?.getHistoryDetail?.historyDetail[0]?.status }}</p>\n        </ion-col>\n        <ion-col col-4>\n          <h3>Remark</h3>\n        </ion-col>\n        <ion-col col-8>\n          <p>{{ moldmakerModel?.getHistoryDetail?.historyDetail[0]?.status }}</p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-grid *ngIf="(moldmakerModel?.getHistoryDetail?.historyImage?.length > 0)">\n      <ion-row>\n        <ion-col col-6 *ngFor="let photo of moldmakerModel?.getHistoryDetail?.historyImage; let id = index">\n          <ion-card class="card-background-page">\n            <img style="height: 150px" [src]="photo?.photo" (tap)="viewImages(photo?.photo)" alt=""/>\n            <ion-card-content>\n              <p class="gps">{{ photo.latitude }}, {{ photo.longitude }}</p>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldmaker-history-detail/moldmaker-history-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__providers_moldmaker_provider_moldmaker_provider__["a" /* MoldmakerProvider */]])
    ], MoldmakerHistoryDetailPage);
    return MoldmakerHistoryDetailPage;
}(__WEBPACK_IMPORTED_MODULE_2__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=moldmaker-history-detail.js.map

/***/ })

});
//# sourceMappingURL=8.js.map