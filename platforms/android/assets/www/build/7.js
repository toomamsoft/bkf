webpackJsonp([7],{

/***/ 437:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoldmakerHistoryPageModule", function() { return MoldmakerHistoryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__moldmaker_history__ = __webpack_require__(466);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var MoldmakerHistoryPageModule = /** @class */ (function () {
    function MoldmakerHistoryPageModule() {
    }
    MoldmakerHistoryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__moldmaker_history__["a" /* MoldmakerHistoryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__moldmaker_history__["a" /* MoldmakerHistoryPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__moldmaker_history__["a" /* MoldmakerHistoryPage */]
            ]
        })
    ], MoldmakerHistoryPageModule);
    return MoldmakerHistoryPageModule;
}());

//# sourceMappingURL=moldmaker-history.module.js.map

/***/ }),

/***/ 466:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldmakerHistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_MoldMakerModel__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_moldmaker_provider_moldmaker_provider__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_scan_scan__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_location_accuracy__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_geolocation__ = __webpack_require__(267);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};













/**
 * Generated class for the MoldmakerHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var gpsOptions = {
    enableHighAccuracy: true
};
var MoldmakerHistoryPage = /** @class */ (function (_super) {
    __extends(MoldmakerHistoryPage, _super);
    function MoldmakerHistoryPage(navCtrl, events, locationAccuracy, geolocation, formBuilder, alertCtrl, _moldmakerProvider, _scanProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this.locationAccuracy = locationAccuracy;
        _this.geolocation = geolocation;
        _this.formBuilder = formBuilder;
        _this.alertCtrl = alertCtrl;
        _this._moldmakerProvider = _moldmakerProvider;
        _this._scanProvider = _scanProvider;
        _this.limit = 5;
        _this.Latitude = 0.0000000;
        _this.Longitude = 0.0000000;
        _this.Mode = 'Current';
        _this.form = _this.formBuilder.group({
            Remark: ['']
        });
        return _this;
    }
    MoldmakerHistoryPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad MoldmakerHistoryPage');
        this.locationAccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
                // the accuracy option will be ignored by iOS
                _this.locationAccuracy.request(_this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
                    _this.geolocation.getCurrentPosition(gpsOptions).then(function (resp) {
                        console.log('requesting geolocation:::', resp);
                        _this.Latitude = resp.coords.latitude;
                        _this.Longitude = resp.coords.longitude;
                    });
                }, function (error) { return console.log('Error requesting location permissions', error); });
            }
        }).catch(function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Attention!',
                subTitle: error,
                buttons: ['Close']
            });
            alert.present();
        });
    };
    MoldmakerHistoryPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.moldmakerModel = this._moldmakerProvider.moldmakerModel;
                return [2 /*return*/];
            });
        });
    };
    MoldmakerHistoryPage.prototype.ngOnDestroy = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this._moldmakerProvider.moldmakerModel = new __WEBPACK_IMPORTED_MODULE_4__models_MoldMakerModel__["a" /* MoldMakerModel */]();
                this.moldmakerModel = this._moldmakerProvider.moldmakerModel;
                return [2 /*return*/];
            });
        });
    };
    MoldmakerHistoryPage.prototype.deletePhoto = function (index) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var confirm;
            return __generator(this, function (_a) {
                confirm = this.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.title%"],
                    subTitle: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.delete.image%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.delete.image%"],
                    buttons: [
                        {
                            text: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.logout.no%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.logout.no%"],
                            handler: function () {
                                console.log('Disagree clicked');
                            }
                        }, {
                            text: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.logout.yes%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.logout.yes%"],
                            handler: function () {
                                _this.moldmakerModel.Images.splice(index, 1);
                            }
                        }
                    ]
                });
                confirm.present();
                return [2 /*return*/];
            });
        });
    };
    MoldmakerHistoryPage.prototype.tapViewPhoto = function (photo) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].viewImages(photo)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerHistoryPage.prototype.getImageFromCamera = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var alert_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.moldmakerModel.Images.length >= this.limit) {
                            alert_1 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.photo%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.photo%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_1.present();
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this._scanProvider.getImageFromCamere().then(function (val) {
                                if (val && val != 'No Image Selected' && val != 'cordova_not_available') {
                                    _this.moldmakerModel.Images.push({ photo: val.toString(), latitude: _this.Latitude, longitude: _this.Longitude });
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerHistoryPage.prototype.getImageFromAlbum = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var images, alert_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.moldmakerModel.Images.length >= this.limit) {
                            alert_2 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.photo%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.photo%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_2.present();
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this._scanProvider.getImageFormAlbum(this.limit - this.moldmakerModel.Images.length).then(function (val) {
                                if (val && val != 'cordova_not_available') {
                                    images = val;
                                    for (var i = 0; i < images.length; i++) {
                                        _this.moldmakerModel.Images.push({ photo: images[i], latitude: _this.Latitude, longitude: _this.Longitude });
                                    }
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerHistoryPage.prototype.onCompleted = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.moldmakerModel.Remark = this.form.controls['Remark'].value;
                        this.moldmakerModel.Status = 'Completed';
                        return [4 /*yield*/, this._moldmakerProvider.getMoldMakerTransaction()];
                    case 1:
                        _a.sent();
                        if (this.moldmakerModel.saveData.Status) {
                            this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.save.success%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.save.success%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.close%"],
                                        handler: function () {
                                            _this.popToPage('MakerMenuPage', _this.navCtrl);
                                        }
                                    }]
                            }).present();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerHistoryPage.prototype.onInprogress = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.moldmakerModel.Remark = this.form.controls['Remark'].value;
                        this.moldmakerModel.Status = 'Inprogress';
                        return [4 /*yield*/, this._moldmakerProvider.getMoldMakerTransaction()];
                    case 1:
                        _a.sent();
                        if (this.moldmakerModel.saveData.Status) {
                            this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%message.save.success%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%message.save.success%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_8__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_en_json__["%btn.close%"],
                                        handler: function () {
                                            _this.popToPage('MakerMenuPage', _this.navCtrl);
                                        }
                                    }]
                            }).present();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerHistoryPage.prototype.openLaunchNavigator = function (latitude, longitude) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    MoldmakerHistoryPage.prototype.viewHistory = function (itemSelected) {
        return __awaiter(this, void 0, void 0, function () {
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.moldmakerModel.JobId = itemSelected.jobId;
                        this.moldmakerModel.RunNo = itemSelected.id;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this._moldmakerProvider.getMoldMakerHistoryDetail()];
                    case 2:
                        _a.sent();
                        this.navCtrl.push('MoldmakerHistoryDetailPage');
                        return [3 /*break*/, 4];
                    case 3:
                        ex_1 = _a.sent();
                        this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerHistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* Component */])({
            selector: 'page-moldmaker-history',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldmaker-history/moldmaker-history.html"*/'<!--\n  Generated template for the MoldmakerHistoryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>moldmaker-history</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-fab right bottom *ngIf="Mode == \'Current\'">\n    <button ion-fab mini #fab><ion-icon name="arrow-dropup"></ion-icon></button>\n    <ion-fab-list side="top">\n      <button ion-fab (click)="getImageFromCamera()"><ion-icon name="camera"></ion-icon></button>\n      <button ion-fab (click)="getImageFromAlbum()"><ion-icon name="albums"></ion-icon></button>\n    </ion-fab-list>\n  </ion-fab>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col col-4>\n        <h3>Detail :</h3>\n      </ion-col>\n      <ion-col col-8>\n        <p>Mold ID :  {{ moldmakerModel?.getMoldBKF?.bkfview[0]?.moldId }} </p>\n        <p>Actual Out :  {{ moldmakerModel?.getMoldBKF.bkfview[0]?.confirmDate }} </p>\n        <p>Plan In :  {{ moldmakerModel?.getMoldBKF?.bkfview[0]?.planIn }} </p>\n      </ion-col>\n      <ion-col col-4>\n        <h3>Reason :</h3>\n      </ion-col>\n      <ion-col col-8>\n        <p> {{ moldmakerModel?.getMoldBKF?.bkfview[0]?.reason }} </p>\n      </ion-col>\n      <ion-col col-4>\n        <h3>Remark :</h3>\n      </ion-col>\n      <ion-col col-8 >\n        <p> {{ moldmakerModel?.getMoldBKF?.bkfview[0]?.remark }} </p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <div padding *ngIf="moldmakerModel?.getMoldBKF?.bkfview[0]?.photo?.length > 0">\n    <accordion-list \n      [title]="\'Picture From BKF\'"\n      textColor="#FFF"\n      hasMargin="false"\n      headerColor="#f4f4f4"\n      [expanded]="false">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-6 *ngFor="let items of moldmakerModel?.getMoldBKF?.bkfview[0]?.photo; let id = index">\n            <ion-card class="block">\n              <img style="height: 150px;" [src]="items?.photo" alt="" (tap)="tapViewPhoto(items?.photo)" />\n            </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </accordion-list>\n  </div>\n\n  <ion-segment [(ngModel)]="Mode">\n    <ion-segment-button value="Current">\n      <h6>Current</h6>\n    </ion-segment-button>\n    <ion-segment-button value="History">\n      <h6>History</h6>\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]="Mode">\n    <ion-list *ngSwitchCase="\'Current\'">\n      <form [formGroup]="form">\n        <ion-list>\n          <ion-item>\n            <ion-textarea formControlName="Remark" placeholder="Remark" rows="6" autosize></ion-textarea>\n          </ion-item>\n        </ion-list>\n      </form>\n    \n      <div padding *ngIf="moldmakerModel?.Images?.length > 0">\n        <accordion-list \n          [title]="\'%label.images%\' | translate"\n          textColor="#FFF"\n          hasMargin="false"\n          headerColor="#f4f4f4"\n          [expanded]="true">\n          <ion-grid>\n            <ion-row>\n              <ion-col col-6 *ngFor="let items of moldmakerModel?.Images; let id = index">\n                  <ion-card class="card-background-page">\n                  <ion-icon name="trash" class="deleteIcon" (click)="deletePhoto(id)"></ion-icon>\n                  <img style="height: 150px" [src]="items?.photo" (tap)="tapViewPhoto(items?.photo)" alt="" />\n                  <ion-card-content>\n                    <p class="gps" (click)="openLaunchNavigator(items?.latitude, items?.longitude)">{{ items?.latitude }}, {{ items?.longitude }}</p>\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </accordion-list>\n      </div>\n\n      <ion-item no-padding class="btn-setting-fav">\n        <ion-row class="two-col-button">\n          <ion-col col-6>\n            <button no-margin ion-button full large col-12 class="cancel-btn" (click)="onInprogress()">Inprogress</button>\n          </ion-col>\n          <ion-col col-6>\n            <button no-margin ion-button full large col-12 (click)="onCompleted()">Completed</button>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n    </ion-list>\n\n    <ion-list *ngSwitchCase="\'History\'">\n      <ion-item *ngFor="let items of moldmakerModel?.getMoldBKF?.history">\n        <h3>Mold : {{ items?.moldid }}</h3>\n        <p>Date : {{ items?.createdate | datetime: \'dateAndTime\' }}</p>\n        <button ion-button clear item-end (click)="viewHistory(items)">View</button>\n      </ion-item>\n    </ion-list>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldmaker-history/moldmaker-history.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_0__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_moldmaker_provider_moldmaker_provider__["a" /* MoldmakerProvider */],
            __WEBPACK_IMPORTED_MODULE_10__providers_scan_scan__["a" /* ScanProvider */]])
    ], MoldmakerHistoryPage);
    return MoldmakerHistoryPage;
}(__WEBPACK_IMPORTED_MODULE_1__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=moldmaker-history.js.map

/***/ })

});
//# sourceMappingURL=7.js.map