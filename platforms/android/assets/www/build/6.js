webpackJsonp([6],{

/***/ 443:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoldmakerReceivePageModule", function() { return MoldmakerReceivePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__moldmaker_receive__ = __webpack_require__(472);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var MoldmakerReceivePageModule = /** @class */ (function () {
    function MoldmakerReceivePageModule() {
    }
    MoldmakerReceivePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__moldmaker_receive__["a" /* MoldmakerReceivePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__moldmaker_receive__["a" /* MoldmakerReceivePage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__moldmaker_receive__["a" /* MoldmakerReceivePage */]
            ]
        })
    ], MoldmakerReceivePageModule);
    return MoldmakerReceivePageModule;
}());

//# sourceMappingURL=moldmaker-receive.module.js.map

/***/ }),

/***/ 472:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldmakerReceivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_moldmaker_provider_moldmaker_provider__ = __webpack_require__(262);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the MoldmakerReceivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MoldmakerReceivePage = /** @class */ (function (_super) {
    __extends(MoldmakerReceivePage, _super);
    function MoldmakerReceivePage(navCtrl, events, alertCtrl, _moldmakerProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this.alertCtrl = alertCtrl;
        _this._moldmakerProvider = _moldmakerProvider;
        return _this;
    }
    MoldmakerReceivePage.prototype.ionViewDidLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log('ionViewDidLoad MoldmakerReceivePage');
                return [2 /*return*/];
            });
        });
    };
    MoldmakerReceivePage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.moldmakerModel = this._moldmakerProvider.moldmakerModel;
                return [2 /*return*/];
            });
        });
    };
    MoldmakerReceivePage.prototype.onReceive = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldmakerProvider.getReceiveMold()];
                    case 1:
                        _a.sent();
                        if (this.moldmakerModel.getMoldBKF.Status) {
                            this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: '',
                                subTitle: '',
                                buttons: [
                                    {
                                        text: 'Close',
                                        handler: function () {
                                            _this.popToPage('MakerMenuPage', _this.navCtrl);
                                        }
                                    }
                                ]
                            }).present();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _a.sent();
                        this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerReceivePage.prototype.backToPage = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.navCtrl.pop();
                return [2 /*return*/];
            });
        });
    };
    MoldmakerReceivePage.prototype.viewImages = function (photo) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                __WEBPACK_IMPORTED_MODULE_0__shared_utilities__["a" /* Utilities */].viewImages(photo);
                return [2 /*return*/];
            });
        });
    };
    MoldmakerReceivePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* Component */])({
            selector: 'page-moldmaker-receive',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldmaker-receive/moldmaker-receive.html"*/'<!--\n  Generated template for the MoldmakerReceivePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>moldmaker-receive</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-4>\n        <h3>Detail</h3>\n      </ion-col>\n      <ion-col col-8>\n        <p>Mold ID :  {{ moldmakerModel?.getMoldBKF?.bkfview[0]?.moldId }} </p>\n        <p>Actual Out :  {{ moldmakerModel?.getMoldBKF?.bkfview[0]?.confirmDate }} </p>\n        <p>Plan In :  {{ moldmakerModel?.getMoldBKF?.bkfview[0]?.planIn }} </p>\n      </ion-col>\n      <ion-col col-4>\n        <h3>Reason :</h3>\n      </ion-col>\n      <ion-col col-8>\n        <p> {{ moldmakerModel?.getMoldBKF?.bkfview[0]?.reason }} </p>\n      </ion-col>\n      <ion-col col-4>\n        <h3>Remark :</h3>\n      </ion-col>\n      <ion-col col-8 >\n        <p> {{ moldmakerModel?.getMoldBKF?.bkfview[0]?.remark }} </p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <div padding *ngIf="moldmakerModel?.getMoldBKF?.bkfview[0]?.photo?.length > 0">\n    <accordion-list \n      [title]="\'%label.images%\' | translate"\n      textColor="#FFF"\n      hasMargin="false"\n      headerColor="#f4f4f4"\n      [expanded]="true">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-6 *ngFor="let items of moldmakerModel?.getMoldBKF?.bkfview[0]?.photo; let id = index">\n            <ion-card class="block">\n              <img style="height: 200px;" [src]="items?.photo" alt="" (tap)="viewImages(items?.photo)" />\n            </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </accordion-list>\n  </div>\n</ion-content>\n\n<ion-footer>\n  <ion-item no-padding class="btn-setting-fav">\n    <ion-row class="two-col-button">\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 class="cancel-btn" (click)="backToPage()">{{ \'%btn.survey.back%\' | translate }}</button>\n      </ion-col>\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 (click)="onReceive()">Receive</button>\n      </ion-col>\n    </ion-row>\n  </ion-item>\n</ion-footer>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldmaker-receive/moldmaker-receive.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_moldmaker_provider_moldmaker_provider__["a" /* MoldmakerProvider */]])
    ], MoldmakerReceivePage);
    return MoldmakerReceivePage;
}(__WEBPACK_IMPORTED_MODULE_1__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=moldmaker-receive.js.map

/***/ })

});
//# sourceMappingURL=6.js.map