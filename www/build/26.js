webpackJsonp([26],{

/***/ 420:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminEditUserPageModule", function() { return AdminEditUserPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_edit_user__ = __webpack_require__(450);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AdminEditUserPageModule = /** @class */ (function () {
    function AdminEditUserPageModule() {
    }
    AdminEditUserPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__admin_edit_user__["a" /* AdminEditUserPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__admin_edit_user__["a" /* AdminEditUserPage */]),
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__admin_edit_user__["a" /* AdminEditUserPage */]
            ]
        })
    ], AdminEditUserPageModule);
    return AdminEditUserPageModule;
}());

//# sourceMappingURL=admin-edit-user.module.js.map

/***/ }),

/***/ 450:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminEditUserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_admin_provider_admin_provider__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_constant__ = __webpack_require__(3);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the AdminEditUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AdminEditUserPage = /** @class */ (function (_super) {
    __extends(AdminEditUserPage, _super);
    function AdminEditUserPage(navCtrl, events, formBuilder, alertCtrl, _adminProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this.formBuilder = formBuilder;
        _this.alertCtrl = alertCtrl;
        _this._adminProvider = _adminProvider;
        _this.position = [{ id: 1, value: 'Staff' }, { id: 2, value: 'Manager' }, { id: 3, value: 'Department' }, { id: 4, value: 'Division' }];
        _this.form = _this.formBuilder.group({
            EmpID: [{ value: null, disabled: true }],
            FirstName: [null, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]],
            LastName: [null, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]],
            UserType: [null, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]],
            UserGroup: [null,],
            Username: [{ value: null, disabled: true }],
            Password: [null, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]]
        });
        return _this;
    }
    AdminEditUserPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminEditUserPage');
    };
    AdminEditUserPage.prototype.ngOnInit = function () {
        this.adminModel = this._adminProvider.adminModel;
        this.form.controls['FirstName'].setValue(this.adminModel.bkfSelected.firstName);
        this.form.controls['LastName'].setValue(this.adminModel.bkfSelected.lastName);
        this.form.controls['Username'].setValue(this.adminModel.bkfSelected.username);
        this.form.controls['Password'].setValue(this.adminModel.bkfSelected.password);
        this.form.controls['EmpID'].setValue(this.adminModel.bkfSelected.empId);
        this.form.controls['UserType'].setValue(this.adminModel.bkfSelected.userType);
        this.form.controls['UserGroup'].setValue(this.adminModel.bkfSelected.userGroup);
    };
    AdminEditUserPage.prototype.reset = function () {
    };
    AdminEditUserPage.prototype.editAccount = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.adminModel.empid = this.form.controls['EmpID'].value;
                        this.adminModel.firstname = this.form.controls['FirstName'].value;
                        this.adminModel.lastname = this.form.controls['LastName'].value;
                        this.adminModel.position = this.form.controls['UserType'].value;
                        this.adminModel.usergroup = this.form.controls['UserGroup'].value;
                        this.adminModel.username = this.form.controls['Username'].value;
                        this.adminModel.password = this.form.controls['Password'].value;
                        this.adminModel.flag = 'U';
                        return [4 /*yield*/, this._adminProvider.getUpdateUserBKF()];
                    case 1:
                        _a.sent();
                        this.alertCtrl.create({
                            enableBackdropDismiss: false,
                            title: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__["%message.title%"],
                            subTitle: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__["%message.save.success%"] : __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__["%message.save.success%"],
                            buttons: [{
                                    text: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__["%btn.close%"],
                                    handler: function () {
                                        // this.popToPage('SurveyMenuPage', this.navCtrl);
                                        _this.navCtrl.pop();
                                    }
                                }]
                        }).present();
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _a.sent();
                        this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminEditUserPage.prototype.deleteAccount = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    this.adminModel.empid = this.form.controls['EmpID'].value;
                    this.adminModel.flag = 'D';
                    this.alertCtrl.create({
                        enableBackdropDismiss: false,
                        title: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__["%message.title%"],
                        subTitle: 'คุณต้องการลบผู้ใช้งานนี้ใช่ไหม',
                        buttons: [{
                                text: 'ไม่',
                            }, {
                                text: 'ใช่',
                                handler: function () {
                                    _this.delete();
                                }
                            }]
                    }).present();
                }
                catch (ex) {
                    this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
                }
                return [2 /*return*/];
            });
        });
    };
    AdminEditUserPage.prototype.delete = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ex_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._adminProvider.getUpdateUserBKF()];
                    case 1:
                        _a.sent();
                        this.alertCtrl.create({
                            enableBackdropDismiss: false,
                            title: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__["%message.title%"],
                            subTitle: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__["%message.delete.success%"] : __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__["%message.delete.success%"],
                            buttons: [{
                                    text: __WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__["%btn.close%"],
                                    handler: function () {
                                        _this.navCtrl.pop();
                                    }
                                }]
                        }).present();
                        return [3 /*break*/, 3];
                    case 2:
                        ex_2 = _a.sent();
                        this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminEditUserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-admin-edit-user',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/admin-edit-user/admin-edit-user.html"*/'<!--\n  Generated template for the AdminEditUserPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>admin-edit-user</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]="form">\n    <ion-list>\n      <ion-item>\n        <ion-label>รหัสพนักงาน</ion-label>\n        <ion-input type="text" formControlName="EmpID"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>ชื่อ</ion-label>\n        <ion-input type="text" formControlName="FirstName"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>นามสกุล</ion-label>\n        <ion-input type="text" formControlName="LastName"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>ตำแหน่ง</ion-label>\n        <ion-select interface="action-sheet" formControlName="UserType">\n          <ion-option value="">กรุณาเลือกตำแหน่ง</ion-option>\n          <ion-option *ngFor="let items of position" [value]="items?.value">{{ items?.value }}</ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-list>\n\n    <ion-list>\n      <ion-item>\n        <ion-label>ชื่อผุ้ใช้งาน</ion-label>\n        <ion-input type="text" formControlName="Username"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>รหัสผ่าน</ion-label>\n        <ion-input type="text" formControlName="Password"></ion-input>\n      </ion-item>\n    </ion-list>\n  </form>\n  <div style="text-align: center">\n    <button ion-button margin color="danger" (click)="deleteAccount()">ลบผู้ใช้งานนี้</button>\n  </div>\n</ion-content>\n\n<ion-footer>\n  <ion-item no-padding class="btn-setting-fav">\n    <ion-row class="two-col-button">\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 class="cancel-btn" (click)="reset()">Reset</button>\n      </ion-col>\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 [disabled]="!form.valid" (click)="editAccount()">Save</button>\n      </ion-col>\n    </ion-row>\n  </ion-item>\n</ion-footer>'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/admin-edit-user/admin-edit-user.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_admin_provider_admin_provider__["a" /* AdminProvider */]])
    ], AdminEditUserPage);
    return AdminEditUserPage;
}(__WEBPACK_IMPORTED_MODULE_0__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=admin-edit-user.js.map

/***/ })

});
//# sourceMappingURL=26.js.map