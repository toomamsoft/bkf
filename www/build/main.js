webpackJsonp([30],[
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CONSTANT; });
var CONSTANT = {
    SERVER: "NODE",
    APP_LANGUAGE: 'en',
    // IPAddress: 'http://mobileapp.bkf.co.th:3000',
    IPAddress: 'http://172.20.10.7:3000',
    // IPAddress: 'http://report.bkf.co.th:3111',
    // IPAddress: 'http://192.168.2.150:3000',
    FCM_TOKEN: 'DEVICE_TOKEN',
    USER_TYPE: 'USER_TYPE',
    IMAGE_MOLDINOUT: 'IMAGE_MOLDINOUT',
    USER_ID: 'USER_ID',
    IMAGE_MOLDSERVEY: 'IMAGE_MOLDSERVEY',
    ACTIVE_MOBILE: 'ACTIVE_MOBILE',
    USER_NAME: 'USER_NAME',
    LANGURAGE: 'language',
    TIMEOUT: 120000,
    WIDTH: 600,
    HEIGHT: 800,
    AdminAdapterModel: {
        GET_USER_BKF: 'getListuserBKF',
        GET_USER_MOLD: 'getListuserMold',
        GET_CONFIG: 'getConfig'
    },
    AuthenticateUserModel: {
        USER_ID: 'EmpID',
        USER_TYPE: 'TypeLogin',
        FULL_NAME: 'NameEN',
        IMEI: 'IMEI',
        STATUS: 'Status',
        IMAGES_SURVEY: 'Images',
        IMAGES_MOLDINOUT: 'ImagesInOut',
        USER_LOGIN: 'userLogin',
        DEVICE_ALLOW: 'DeviceAllow'
    },
    SettingModel: {
        CONFIG: 'ConfigApplication'
    },
    LocationModel: {
        LOCATION_ID: "LocationID",
        LOCATION_DESCRIPTION: "LocationDescription"
    },
    HistorySurveyModel: {
        MOLDLOCATION: 'MoldLocation',
        MOLDMASTER: 'MoldMaster',
        MOLDHISTORY: 'MoldHistory',
        MOLDSEARCH: 'MoldSearch'
    },
    HistorySurveyDetailModel: {
        HISTORY: 'MoldHistoryDetail',
        IMAGES: 'MoldHistoryImages'
    },
    SurveySubMasterModel: {
        MOLDID: 'MoldID',
        ASSETID: 'AssetID',
        CUSTNO: 'CustNo',
        PARTNO: 'PartNo',
        PARTNAME: 'PartName'
    },
    SurveySubSearchModel: {
        MOLDID: 'MoldID',
        ASSETID: 'AssetID',
        PLATE: 'Plage',
        FULLNAME: 'Name',
        CREATEDATE: 'CreateDate',
        RUNNO: 'RunNo'
    },
    SurveySubHistoryModel: {
        MOLDID: 'MoldID',
        ASSETID: 'AssetID',
        CUSTNO: 'CustNo',
        PARTNO: 'PartNo',
        PARTNAME: 'PartName',
        LOCATION: 'LocationDescription',
        PLATE: 'Plate',
        REMARK: 'Remark',
        FULLNAME: 'Names',
        CREATEDATE: 'CreateDate',
        RUNNO: 'RunNo'
    },
    SurveySubHistoryDatailModel: {
        MOLDID: 'MoldID',
        LOCATION: 'LocationDescription',
        PLATE: 'Plate',
        REMARK: 'Remark',
        FULLNAME: 'Names',
        CREATEDATE: 'CreateDate',
        RUNNO: 'RunNo',
        DESCRIPTION: 'Discription',
        SUBDESCRIPTION: {
            ASSETID: 'AssetID',
            CUSTNO: 'CustNo',
            PARTNO: 'PartNo',
            PARTNAME: 'PartName'
        }
    },
    SurveySaveMoldModel: {
        STATUS: 'Status',
        MESSAGE: 'Message',
        PDF: 'Pdf'
    },
    SurveySubHistoryDetailImageModel: {
        PHOTO: 'ImagePath'
    },
    MoldInoutAdapterModel: {
        NEWMOLDINOUT: 'NewMoldInOut',
        SETTING_MOLDMAKER: 'MoldMaker',
        SETTING_REASON: 'Reason',
        MOLDINOUT_WAITNG: 'GetApproveByWaiting',
        MOLDINOUT_IMAGEOUT: 'ImagesOut',
        MOLDINOUT_IMAGEIN: 'ImagesIn',
        MOLDINOUT_IMAGETRANSPORT: 'ImagesTransport',
        MOLDINOUT_SEARCH: 'MoldInOutSearch',
        MOLDINOUT_STATUS: 'MoldInOutStatus',
        MOLDINOUT_APPROVE_IN: 'approveIn',
        MOLDINOUT_APPROVE_OUT: 'approveOut'
    },
    SubNewMoldInOut: {
        MOLDID: 'MoldId',
        DESCRIPTION: 'Discription',
        SUBDESCRIPTION: {
            ASSETID: 'AssetID',
            CUSTNO: 'CustNo',
            PARTNO: 'PartNo',
            PARTNAME: 'PartName'
        },
        JOBID: 'JobId',
        MAKERMOLD: 'MakerMoldID',
        MAKERID: 'MakerId',
        PLANOUT: 'PlanOut',
        PLANIN: 'PlanIn',
        REASON: 'ReasonId',
        NAMECREATE: 'NameTHCreate',
        REMARKNEW: 'RemarkNew',
        CONFIRMDATE: 'ConfirmOutDate',
        NAMECONFIRMOUT: 'NameConfirmOut',
        REMARKOUT: 'RemarkOut',
        STATUS: 'CurrentStatus',
        PRINT: 'Printed',
        PDF: 'PDF',
        FLAG: 'Flag',
        MAKERSTATUS: 'MoldMakerStatus',
        REMARKIN: 'RemarkIn'
    },
    SubMoldInoutWatingModel: {
        JOBID: 'JobId',
        MOLDID: 'MoldId',
        CUSNO: 'CustNo',
        PARTNO: 'PartNo',
        PARTNAME: 'PartName',
        STATUS: 'CurrentStatus',
        CREATEDATE: 'CreateDate'
    },
    SubMoldInoutConfig: {
        ID: 'ID',
        DESCIRPTION: 'Description'
    },
    SubMoldInoutImages: {
        IMAGES_TYPE: 'ImageType',
        LATITUDE: 'Latitude',
        LONGITUDE: 'Longitude',
        IMAGES_NAME: 'PhotoName'
    },
    SubMoldInOutSearch: {
        JOBID: 'JobId',
        MOLDID: 'MoldId',
        CUSTNO: 'CustNo',
        PARTNO: 'PartNo',
        PARTNAME: 'PartName',
        MAKERNAME: 'MakerName',
        STATUS: 'SearchStatus',
        CONFIRMOUT_DATE: 'ConfirmOutDate'
    },
    SubMoldInOutStatus: {
        JOBID: 'JobId',
        MOLDID: 'MoldID',
        PLANIN: 'PlanIn',
        PLANOUT: 'PlanOut',
        FULLNAME: 'FullName',
        MAKERNAME: 'MakerName',
        REASON: 'Reason',
        STATUS: 'DelayStatus'
    },
    SubMoldInOutApprove: {
        JOBID: "JobId",
        EMPID: "EmpID",
        TYPE: "ApproveType",
        STATUS: "Status",
        DATE: "ApproveDate",
        REMARK: "Remark",
        NAME: "NameTH",
        USER_TYPE: "UserType"
    },
    MoldMakerAdapterModel: {
        LIST_MOLD: 'getListMold',
        VIEW_BY_BKF: 'ViewHistoryWithBKF',
        HISTORY: 'history',
        HISTORY_BKF: 'bkfview',
        HISTORY_DETAIL: 'historyDetail',
        HISTORY_IMAGE: 'historyImages'
    },
    SubGetListMoldModel: {
        JOBID: "JobId",
        MOLDID: "MoldID",
        MAKERMOLDID: "MakerMoldId",
        PLANOUT: "PlanOut",
        ACTUAL: "Actual",
        PLANIN: "PlanIn",
        REMARK: "RemarkNew",
        PARTNO: "PartNo",
        CUSTNO: "CustNo",
        PARTNAME: "PartName",
        STATUS: "MoldMakerStaus"
    },
    SubMoldMakerHistory: {
        ID: 'ID',
        MOLDID: 'MoldId',
        JOBID: 'JobID',
        REMARK: 'Remark',
        CREATEDATE: 'CreateDate',
        STATUS: 'Status'
    },
    SubMoldMakerHistoryDetail: {
        MOLDID: '',
        JOBID: 'JobID',
        REMARK: 'Remark',
        PHOTO: 'Photo',
        CREATEDATE: '',
        STATUS: 'Status',
        SubPhoto: {
            PHOTONAME: 'PhotoName',
            LATITUDE: 'Latitude',
            LONGITUDE: 'Longitude'
        }
    },
    SubMoldMakerBKFVIEW: {
        JOBID: 'JobId',
        MOLDID: 'MoldId',
        PLANOUT: 'PlanOut',
        PLANINT: 'PlanIn',
        REMARK: 'RemarkNew',
        REASON: 'Reason',
        CONFIRMDATE: 'ConfirmOutDate',
        PHOTO: 'Photo',
        SubPhoto: {
            PHOTONAME: 'PhotoName',
            LATITUDE: 'Latitude',
            LONGITUDE: 'Longitude'
        }
    },
    SubMoldBKFVIEWHistoryModel: {
        JOBID: 'JobId',
        MOLDID: 'MoldId',
        REMARK: 'Remark',
        CREATEDATE: 'CreateDate',
        PHOTO: 'Photo',
        STAUTS: 'Status',
        SubPhoto: {
            PHOTONAME: 'PhotoName',
            LATITUDE: 'Latitude',
            LONGITUDE: 'Longitude'
        }
    },
    MessageAdapterModel: {
        MESSAGE: 'notification'
    },
    SubMessageModel: {
        MESSAGEID: 'MessageID',
        SUBJECT: 'MessageSubject',
        DESCRIPTION: 'MessageDescription',
        FLAG: 'ReadStatus',
        MESSAGE_DATE: 'MessageDate',
        JOBID: 'JobId',
        MOLDID: 'MoldId',
        STATUS: 'CurrentStatus'
    },
    SubAdminGetListBKF: {
        EMPID: "empId",
        FIRSTNAME: "firstName",
        LASTNAME: "lastName",
        USERGROUP: "userGroup",
        USERNAME: "username",
        PASSWORD: "password",
        USERTYPE: "userType"
    },
    SubAdminGetListMold: {
        FIRSTNAME: "MakerName",
        EMPID: "EmpID",
        USERNAME: "username",
        PASSWORD: "password",
        USERTYPE: "userType"
    },
    SubAdminGetConfig: {
        SURVEY: "Survey",
        MOLDINOUT: "MoldInOut",
        MOLDMAKER: "MoldMaker",
        ISONUMBER: "ISO"
    },
    AUTHENTICATION_AUTHENTICATE_ADAPTER: '/login/authentication',
    AUTHENTICATION_DEVICE_ALLOW_ADAPTER: '/login/deviceAllow',
    AUTHENTICATION_ACTIVE_ADAPTER: '/login/activeDevice',
    AUTHENtICATION_CONFIG: '/login/config',
    SURVEY_LOCATION_ADAPTER: '/survey/getLocation',
    SURVEY_HISTORY_ADAPTER: '/survey/getMoldHistory',
    SURVEY_HISOTRY_DETAIL_ADAPTER: '/survey/getMoldHistoryDetail',
    SURVEY_SARCH_ADAPTER: '/survey/getMoldSearch',
    SURVEY_SAVE_ADAPTER: '/survey/saveMold',
    MOLDINOUT_SETTING_ADAPTER: '/inout/setting',
    MOLDINOUT_MASTER_ADAPTER: '/inout/newmoldinout',
    MOLDINOUT_HISTORY_ADAPTER: '/inout/getHistory',
    MOLDINOUT_APPROVE_ADAPTER: '/inout/getApprove',
    MOLDINOUT_SAVE_NEW_ADAPTER: '/inout/saveMoldOut',
    MOLDINOUT_APPROVE_REJECT_OUT: '/inout/approveOrReject',
    MOLDINOUT_APPROVE_CONFIRMOUT: '/inout/confirmOut',
    MOLDINOUT_APPROVE_CONFIRMIN: '/inout/confirmIn',
    MOLDINOUT_APPROVE_REJECT_IN: '/inout/approveInOrReject',
    MOLDINOUT_SEARCH_ADAPTER: '/inout/moldSearch',
    MOLDINOUT_STATUS_ADAPTER: '/inout/moldStatus',
    MOLDINOUT_GET_WAITING: '/inout/getApproveByWaiting',
    MOLDMAKER_GETLISTMOLD_ADAPTER: '/maker/getListMold',
    MOLDMAKER_BKFVIEW_ADAPTER: '/maker/bkfview',
    MOLDMAKER_HISTORY_ADAPTER: '/maker/getMoldHistory',
    MOLDMAKER_HISTORY_DETAIL_ADAPTER: '/maker/getMoldHistoryDetail',
    MOLDMAKER_RECEIVE_ADAPTER: '/maker/receive',
    MOLDMAKER_SAVE_TRANSACTION_ADAPTER: '/maker/transaction',
    MESSAGE_GETMESSGE_ADAPTER: '/inbox/notification',
    MESSAGE_READ_MESSGE_ADAPTER: '/inbox/readNotification',
    ADMIN_GET_USER_BKF_ADAPTER: '/admin/getListUserBKF',
    ADMIN_GET_CONFIG_ADAPTER: '/admin/getConfig',
    ADMIN_GET_USER_MOLD_ADAPTER: '/admin/getListUserMold',
    ADMIN_GET_UPDATE_USER_BKF_ADAPTER: '/admin/getUpdateUserBKF',
    ADMIN_GET_UPDATE_USER_MAKER_ADAPTER: '/admin/getUpdateUserMaker',
    ADMIN_GET_UPDATE_CONFIG_ADAPTER: '/admin/getUpdateConfig'
};
//# sourceMappingURL=constant.js.map

/***/ }),
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Utilities; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_photo_viewer__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_app_version__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_device__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_uid__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_opener__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_file_transfer__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_launch_navigator__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_location_accuracy__ = __webpack_require__(124);
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












var Utilities = /** @class */ (function () {
    function Utilities() {
    }
    Utilities.IMEI = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        if (_this.uid != null || undefined) {
                            resolve(_this.uid.IMEI || _this.device.serial || '000000000000000');
                        }
                        else {
                            resolve(_this.device.serial || '000000000000000');
                        }
                    })];
            });
        });
    };
    Utilities.setItem = function (key, value) {
        localStorage.setItem(key, value);
    };
    Utilities.getItem = function (key) {
        var returnItms;
        returnItms = localStorage.getItem(key);
        return returnItms;
    };
    Utilities.getAppVersion = function () {
        var _this = this;
        return new Promise(function (resove) {
            _this.appVersion.getVersionNumber().then(function (res) {
                resove(res);
            }).catch(function (e) {
                resove("APP NOT ON DEVICE.");
            });
        });
    };
    Utilities.getDeviceUUID = function () {
        return (this.device.platform) ? this.device.uuid : "b4493d5e-8070-3c12-af42-f052d67dbc11";
    };
    Utilities.padDigits = function (number, digits) {
        return Array(Math.max(digits - String(number).length + 1, 0)).join('0') + number;
    };
    Utilities.formatDateDDMMYYYY = function (date, separator) {
        var yyyy = date.getUTCFullYear();
        var mm = Utilities.padDigits((date.getMonth() + 1), 2);
        var dd = Utilities.padDigits(date.getDate(), 2);
        separator = separator ? separator : '';
        return dd + separator + mm + separator + yyyy;
    };
    Utilities.getCurrentDateTimeMySql = function () {
        var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
        var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0, 19);
        var mySqlDT = localISOTime;
        return mySqlDT;
    };
    Utilities.mapJsontoModel = function (json, classReference) {
        var jsonConvert = new __WEBPACK_IMPORTED_MODULE_2_json2typescript__["JsonConvert"]();
        jsonConvert.operationMode = __WEBPACK_IMPORTED_MODULE_2_json2typescript__["OperationMode"].ENABLE; // print some debug data
        jsonConvert.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
        jsonConvert.valueCheckingMode = __WEBPACK_IMPORTED_MODULE_2_json2typescript__["ValueCheckingMode"].ALLOW_NULL; // never allow null
        // Map to the country class
        var obj;
        try {
            obj = jsonConvert.deserialize(json, classReference);
            return obj;
            //console.log(obj);
            //obj.cities[0].printInfo(); // prints: Basel was founded in -200 and is really beautiful!
        }
        catch (e) {
            console.log('error mapJsontoModel:::', e);
            throw e;
        }
    };
    Utilities.getBase64Image = function (fileImages) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var canvas, ctx, img;
            return __generator(this, function (_a) {
                canvas = document.createElement("canvas");
                ctx = canvas.getContext("2d");
                img = new Image();
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var _a;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    img.onload = function () {
                                        canvas.width = __WEBPACK_IMPORTED_MODULE_0__constant__["a" /* CONSTANT */].WIDTH;
                                        canvas.height = __WEBPACK_IMPORTED_MODULE_0__constant__["a" /* CONSTANT */].HEIGHT;
                                        ctx.fillStyle = "white";
                                        ctx.fillRect(0, 0, canvas.width, canvas.height);
                                        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
                                        var imageBase64Data = canvas.toDataURL("image/jpeg", 0.98);
                                        resolve(imageBase64Data);
                                    };
                                    _a = img;
                                    return [4 /*yield*/, fileImages.replace('file:///', '/')];
                                case 1:
                                    _a.src = _b.sent();
                                    return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    Utilities.resizeImage = function (imageBage64) {
        return __awaiter(this, void 0, void 0, function () {
            var canvas, ctx;
            return __generator(this, function (_a) {
                canvas = document.createElement("canvas");
                ctx = canvas.getContext("2d");
                ctx.fillStyle = "white";
                ctx.fillRect(0, 0, canvas.width, canvas.height);
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        var img = new Image();
                        img.onload = function () {
                            canvas.width = __WEBPACK_IMPORTED_MODULE_0__constant__["a" /* CONSTANT */].WIDTH;
                            canvas.height = __WEBPACK_IMPORTED_MODULE_0__constant__["a" /* CONSTANT */].HEIGHT;
                            ctx.fillStyle = "white";
                            ctx.fillRect(0, 0, canvas.width, canvas.height);
                            ctx.drawImage(img, 0, 0, __WEBPACK_IMPORTED_MODULE_0__constant__["a" /* CONSTANT */].WIDTH, __WEBPACK_IMPORTED_MODULE_0__constant__["a" /* CONSTANT */].HEIGHT);
                            var imageBase64Data = canvas.toDataURL("image/jpeg", 0.98);
                            resolve(imageBase64Data);
                        };
                        img.src = imageBage64;
                    })];
            });
        });
    };
    Utilities.viewImages = function (imageUrl, title) {
        if (imageUrl === void 0) { imageUrl = ''; }
        if (title === void 0) { title = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var options;
            return __generator(this, function (_a) {
                try {
                    options = {
                        share: true,
                        closeButton: true,
                        copyToReference: true,
                        headers: '',
                        piccasoOptions: {} // If this is not provided, an exception will be triggered
                    };
                    this.photoViewer.show(imageUrl, title, options);
                }
                catch (ex) {
                    console.log('Error viewImages::: ', ex);
                }
                return [2 /*return*/];
            });
        });
    };
    Utilities.viewFilePDF = function (filename, type) {
        if (type === void 0) { type = 'application/pdf'; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var download, path, transfer;
            return __generator(this, function (_a) {
                // const options: DocumentViewerOptions = {
                //     title: `${filename}`
                // }
                try {
                    download = __WEBPACK_IMPORTED_MODULE_0__constant__["a" /* CONSTANT */].IPAddress + "/inout/download?filename=" + filename;
                    path = this.file.dataDirectory;
                    transfer = this.fileTranfer.create();
                    transfer.download(download, "" + path + filename).then(function (entry) {
                        var url = entry.toURL();
                        if (_this.platform.is('ios')) {
                            // this.documentViewer.viewDocument(url, `${type}`, options)
                        }
                        else {
                            _this.fileOpener.open(url, "" + type).then(function () { });
                        }
                    }).catch(function (error) {
                        return error;
                    });
                }
                catch (ex) {
                    return [2 /*return*/, ex];
                }
                return [2 /*return*/];
            });
        });
    };
    Utilities.openGoogleMapApplication = function (latitude, longitude) {
        if (latitude === void 0) { latitude = 0.00; }
        if (longitude === void 0) { longitude = 0.00; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.launchNavigator.isAppAvailable(this.launchNavigator.APP.GOOGLE_MAPS).then(function (isAvailable) {
                    var app;
                    if (isAvailable) {
                        app = _this.launchNavigator.APP.GOOGLE_MAPS;
                    }
                    else {
                        console.warn("Google Maps not available - falling back to user selection");
                        app = _this.launchNavigator.APP.USER_SELECT;
                    }
                    _this.launchNavigator.navigate([latitude, longitude], {
                        app: app
                    });
                }).catch(function (error) {
                    console.log('Error launchNavigator::: ', error);
                });
                return [2 /*return*/];
            });
        });
    };
    Utilities.accessGPS = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.locationAccuracy.canRequest().then(function (canRequest) {
                    if (canRequest) {
                        _this.locationAccuracy.request(_this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
                            console.log('requesting location permissions success');
                        }, function (error) {
                            console.log('Error requesting location permissions', error);
                        });
                    }
                }).catch(function (error) {
                    console.log('Error locationAccuracy::: ', error);
                });
                return [2 /*return*/];
            });
        });
    };
    Utilities.device = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_device__["a" /* Device */]();
    Utilities.appVersion = new __WEBPACK_IMPORTED_MODULE_3__ionic_native_app_version__["a" /* AppVersion */]();
    Utilities.photoViewer = new __WEBPACK_IMPORTED_MODULE_1__ionic_native_photo_viewer__["a" /* PhotoViewer */]();
    Utilities.uid = new __WEBPACK_IMPORTED_MODULE_5__ionic_native_uid__["a" /* Uid */]();
    Utilities.fileOpener = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_opener__["a" /* FileOpener */]();
    Utilities.platform = new __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["n" /* Platform */]();
    Utilities.file = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */]();
    Utilities.fileTranfer = new __WEBPACK_IMPORTED_MODULE_9__ionic_native_file_transfer__["a" /* FileTransfer */]();
    Utilities.launchNavigator = new __WEBPACK_IMPORTED_MODULE_10__ionic_native_launch_navigator__["a" /* LaunchNavigator */]();
    Utilities.locationAccuracy = new __WEBPACK_IMPORTED_MODULE_11__ionic_native_location_accuracy__["a" /* LocationAccuracy */]();
    return Utilities;
}());

//# sourceMappingURL=utilities.js.map

/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


/*
  Generated class for the StorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var StorageProvider = /** @class */ (function () {
    function StorageProvider() {
        console.log('Hello StorageProvider Provider');
    }
    StorageProvider.prototype.getKey = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var value;
                        return __generator(this, function (_a) {
                            try {
                                value = null;
                                value = localStorage.getItem(key);
                                return [2 /*return*/, resolve(value)];
                            }
                            catch (e) {
                                console.log('getKey error', e);
                                return [2 /*return*/, resolve(null)];
                            }
                            return [2 /*return*/];
                        });
                    }); })];
            });
        });
    };
    StorageProvider.prototype.setKey = function (key, value) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var result;
                        return __generator(this, function (_a) {
                            try {
                                result = true;
                                localStorage.setItem(key, value);
                                return [2 /*return*/, resolve(result)];
                            }
                            catch (e) {
                                return [2 /*return*/, reject(e)];
                            }
                            return [2 /*return*/];
                        });
                    }); })];
            });
        });
    };
    StorageProvider.prototype.removeKey = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var result;
                        return __generator(this, function (_a) {
                            try {
                                result = true;
                                localStorage.removeItem(key);
                                return [2 /*return*/, resolve(result)];
                            }
                            catch (e) {
                                return [2 /*return*/, reject(e)];
                            }
                            return [2 /*return*/];
                        });
                    }); })];
            });
        });
    };
    StorageProvider.prototype.clear = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ignore, _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        ignore = {};
                        _a = ignore;
                        _b = __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].ACTIVE_MOBILE;
                        return [4 /*yield*/, this.getKey(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].ACTIVE_MOBILE)];
                    case 1:
                        _a[_b] = _c.sent();
                        return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                                var result;
                                return __generator(this, function (_a) {
                                    try {
                                        result = null;
                                        localStorage.clear();
                                        localStorage.setItem(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].ACTIVE_MOBILE, ignore[__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].ACTIVE_MOBILE]);
                                        return [2 /*return*/, resolve(result)];
                                    }
                                    catch (e) {
                                        return [2 /*return*/, reject(e)];
                                    }
                                    return [2 /*return*/];
                                });
                            }); })];
                }
            });
        });
    };
    StorageProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], StorageProvider);
    return StorageProvider;
}());

//# sourceMappingURL=storage-provider.js.map

/***/ }),
/* 18 */,
/* 19 */,
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OperationBaseModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OperationBaseModel = /** @class */ (function () {
    function OperationBaseModel() {
        this.errorStatus = undefined;
        this.errornNumber = undefined;
        this.errorCode = undefined;
        this.errorName = undefined;
        this.errorMsg = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])("status", __WEBPACK_IMPORTED_MODULE_0_json2typescript__["Any"], true),
        __metadata("design:type", Object)
    ], OperationBaseModel.prototype, "errorStatus", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])("number", __WEBPACK_IMPORTED_MODULE_0_json2typescript__["Any"], true),
        __metadata("design:type", Object)
    ], OperationBaseModel.prototype, "errornNumber", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])("code", __WEBPACK_IMPORTED_MODULE_0_json2typescript__["Any"], true),
        __metadata("design:type", Object)
    ], OperationBaseModel.prototype, "errorCode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])("name", __WEBPACK_IMPORTED_MODULE_0_json2typescript__["Any"], true),
        __metadata("design:type", Object)
    ], OperationBaseModel.prototype, "errorName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])("message", __WEBPACK_IMPORTED_MODULE_0_json2typescript__["Any"], true),
        __metadata("design:type", Object)
    ], OperationBaseModel.prototype, "errorMsg", void 0);
    OperationBaseModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("OperationBaseModel")
    ], OperationBaseModel);
    return OperationBaseModel;
}());

//# sourceMappingURL=OperationBaseModel.js.map

/***/ }),
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */
/***/ (function(module, exports) {

module.exports = {"%signin%":"เข้าสู่ระบบ","%label.username%":"ชื่อผู้ใช้งาน","%label.password%":"รหัสผ่าน","%error.login%":"ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง","%message.logout%":"คุณต้องการออกจากระบบใช่ไหม","%message.title%":"แจ้งเตือน","%btn.logout%":"ออกจากระบบ","%btn.logout.yes%":"ใช่","%btn.logout.no%":"ไม่ใช่","%language.change%":"เปลี่ยนภาษา","%language%":"ไทย","%loading%":"กรุณารอสักครู่","%btn.close%":"ปิด","%message.delete.image%":"คุณต้องการลบรูปภาพนี้?","%message.reset%":"คุณต้องการล้างข้อมูลใช่ไหม","%message.photo%":"จำนวนรูปภาพ เกินกว่าที่กำหนด","%error.location%":"กรุณาเลือกตำแหน่ง","%btn.survey.save%":"บันทึก","%btn.survey.back%":"กลับ","%label.creator%":"ชื่อผู้ตรวจสอบ","%label.remark%":"หมายเหตุ","%label.images%":"รูปภาพ","%error.moldmaster%":"ไม่พบข้อมูล","%message.moldmaster%":"มีประวัติการทำรายการ","%session.timeout%":"คุณทำรายการเกินระยะเวลาที่กำหนด","%message.notfound%":"ไม่พบข้อมูล","%message.serach.mold.error%":"กรุณาระบุ Mold","%0%":"การเชื่อมต่อผิดพลาด","%404%":"404 Not Found","%message.save.success%":"บันทึกข้อมูลเรียบร้อยแล้ว","%message.delete.success%":"ลบข้อมูลเรียบร้อยแล้ว","%message.active.mobile%":"โทรศัพท์เครื่องนี้ไม่ได้รับสิทธิ์ในการใช้งาน \nกรุณาติดต่อเจ้าหน้าที่ IT Support","%message.active.password%":"กรุณาระบุรหัสผ่าน","%message.active.error%":"รหัสผ่านผิดพลาด<br>กรุณาลองใหม่อีกครั้ง","%btn.back%":"กลับสู่หน้าหลัก","%btn.confirm%":"ยืนยัน","%btn.cancel%":"ยกเลิก","%title.confirm%":"ยืนยันทำรายการ"}

/***/ }),
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_LoginModel__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_provider_base_provider__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__adapter_authentication_adapter_authentication_adapter__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__models_package_AuthenticateUserAdapterModel__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__models_package_SettingAdapterModel__ = __webpack_require__(188);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};













/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LoginProvider = /** @class */ (function (_super) {
    __extends(LoginProvider, _super);
    function LoginProvider(app, menuCtrl, alertCtrl, loadingCtrl, _authenticateAdapterProvider, _storageProvider) {
        var _this = _super.call(this, alertCtrl, loadingCtrl) || this;
        _this.app = app;
        _this.menuCtrl = menuCtrl;
        _this.alertCtrl = alertCtrl;
        _this.loadingCtrl = loadingCtrl;
        _this._authenticateAdapterProvider = _authenticateAdapterProvider;
        _this._storageProvider = _storageProvider;
        _this.loginModel = new __WEBPACK_IMPORTED_MODULE_1__models_LoginModel__["a" /* LoginModel */]();
        _this.loginModel2 = new __WEBPACK_IMPORTED_MODULE_1__models_LoginModel__["a" /* LoginModel */]();
        console.log('Hello LoginProvider Provider');
        return _this;
    }
    LoginProvider.prototype.authenticateUser = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._authenticateAdapterProvider.authenticateUser(this.loginModel)];
                    case 1:
                        response = _a.sent();
                        console.log("authenticateUser ::: ", response);
                        this.loginModel.authenticateUser = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_8__models_package_AuthenticateUserAdapterModel__["a" /* AuthenticateUserAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        this.loginModel2 = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_1, __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_1;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginProvider.prototype.activeMobile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._authenticateAdapterProvider.activeMobile(this.loginModel)];
                    case 1:
                        response = _a.sent();
                        console.log("activeMobile ::: ", response);
                        this.loginModel.authenticateUser = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_8__models_package_AuthenticateUserAdapterModel__["a" /* AuthenticateUserAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_2 = _a.sent();
                        this.loginModel2 = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_2, __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_2;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginProvider.prototype.deviceAllow = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._authenticateAdapterProvider.deviceAllow()];
                    case 1:
                        response = _a.sent();
                        console.log("deviceAllow ::: ", response);
                        this.loginModel.authenticateUser = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_8__models_package_AuthenticateUserAdapterModel__["a" /* AuthenticateUserAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_3 = _a.sent();
                        this.loginModel2 = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_3, __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginProvider.prototype.settingConfig = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._authenticateAdapterProvider.settingConfig()];
                    case 1:
                        response = _a.sent();
                        console.log("settingConfig ::: ", response);
                        this.loginModel.setting = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_12__models_package_SettingAdapterModel__["a" /* SettingAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_4 = _a.sent();
                        this.loginModel2 = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_4, __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_4;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginProvider.prototype.authenticateLogout = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json__["%message.title%"],
                    message: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%message.logout%"] : __WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json__["%message.logout%"],
                    buttons: [{
                            text: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%btn.logout.no%"] : __WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json__["%btn.logout.no%"],
                            handler: function () {
                                console.log('cancel');
                            }
                        }, {
                            text: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_11__assets_i18n_th_json__["%btn.logout.yes%"] : __WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json__["%btn.logout.yes%"],
                            handler: function () {
                                _this._storageProvider.clear();
                                _this.menuCtrl.enable(false, 'authenticateduser');
                                _this.menuCtrl.enable(false, 'authenticatedadmin');
                                _this.menuCtrl.enable(false, 'authenticatedmaker');
                                _this.app.getActiveNav().setRoot("LoginPage");
                            }
                        }]
                }).present();
                return [2 /*return*/];
            });
        });
    };
    LoginProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["k" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__adapter_authentication_adapter_authentication_adapter__["a" /* AuthenticationAdapterProvider */],
            __WEBPACK_IMPORTED_MODULE_9__storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], LoginProvider);
    return LoginProvider;
}(__WEBPACK_IMPORTED_MODULE_3__base_provider_base_provider__["a" /* BaseProvider */]));

//# sourceMappingURL=login-provider.js.map

/***/ }),
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BaseProvider = /** @class */ (function () {
    function BaseProvider(alertCtrl, loadingCtrl) {
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        console.log('Hello BaseProvider Provider');
    }
    BaseProvider.prototype.mappingErrorCodeToErrorMessage = function (err) {
        var errMapping = Object.create(err);
        if (__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th') {
            if (!!errMapping.errorCode) {
                errMapping.errorMsg = __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__["%" + errMapping.errorCode + "%"] || errMapping.errorMsg;
            }
            else {
                errMapping.errorMsg = __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__[errMapping.errorMsg] || errMapping.errorMsg;
            }
        }
        else {
            if (!!errMapping.errorCode) {
                errMapping.errorMsg = __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__["%" + errMapping.errorCode + "%"] || errMapping.errorMsg;
            }
            else {
                errMapping.errorMsg = __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__[errMapping.errorMsg] || errMapping.errorMsg;
            }
        }
        return errMapping;
    };
    BaseProvider.prototype.submitErrorEvent = function (operationBaseModel) {
        console.log('submitErrorEvent operationBaseModel', operationBaseModel);
        if (operationBaseModel) {
            var errorMapping = this.mappingErrorCodeToErrorMessage(operationBaseModel);
            var alert_1 = this.alertCtrl.create({
                enableBackdropDismiss: false,
                title: __WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__["%message.title%"],
                subTitle: __WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__['%' + errorMapping.errorStatus + '%'] : __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__['%' + errorMapping.errorStatus + '%'] || errorMapping.errorMsg || errorMapping.errorName,
                buttons: [{
                        text: __WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__["%btn.close%"]
                    }]
            });
            alert_1.present();
        }
    };
    BaseProvider.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: __WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__["%loading%"] : __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__["%loading%"]
        });
        this.loading.present();
    };
    BaseProvider.prototype.dismissLoading = function () {
        this.loading.dismiss();
    };
    BaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */]])
    ], BaseProvider);
    return BaseProvider;
}());

//# sourceMappingURL=base-provider.js.map

/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnectionUtilProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__BKFConnection_connection_Connection__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_LoginModel__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var ConnectionUtilProvider = /** @class */ (function () {
    function ConnectionUtilProvider(_connection, _http, _loadingCtrl) {
        this._connection = _connection;
        this._http = _http;
        this._loadingCtrl = _loadingCtrl;
        this.userModel = new __WEBPACK_IMPORTED_MODULE_4__models_LoginModel__["a" /* LoginModel */]();
        console.log('Hello ConnectionUtilProvider Provider');
    }
    ConnectionUtilProvider.prototype.getConnection = function () {
        return this._connection.getConnection(__WEBPACK_IMPORTED_MODULE_3__shared_constant__["a" /* CONSTANT */].SERVER, this._http);
    };
    ConnectionUtilProvider.prototype.invokeAdapter = function (endpoint, param) {
        return __awaiter(this, void 0, void 0, function () {
            var loading, response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("[invokeAdapter][" + endpoint + "] Param ::: ", param);
                        loading = this._loadingCtrl.create({
                            content: __WEBPACK_IMPORTED_MODULE_3__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__["%loading%"] : __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__["%loading%"]
                        });
                        loading.present();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        return [4 /*yield*/, this.getConnection().getResourceRequest(endpoint, param, this.userModel)];
                    case 2:
                        response = _a.sent();
                        console.log("[RESPONSE][" + endpoint + "] Response ::: ", response);
                        if (response.Status && response.Status == "SuccessWithWarning") {
                            console.log("[SUCCESS WITH WARNING][" + endpoint + "] ::: ", response);
                            return [2 /*return*/, response];
                        }
                        else if (response.Status && response.Status != "Failure") {
                            console.log("[SUCCESS][" + endpoint + "] ::: ", response);
                            return [2 /*return*/, response];
                        }
                        else {
                            console.log('invokeAdapter else');
                            throw response;
                        }
                        return [3 /*break*/, 5];
                    case 3:
                        err_1 = _a.sent();
                        console.log("[ERROR] catch [ " + endpoint + "] ::: ", err_1);
                        throw err_1;
                    case 4:
                        loading.dismiss();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    ConnectionUtilProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__BKFConnection_connection_Connection__["a" /* Connection */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* LoadingController */]])
    ], ConnectionUtilProvider);
    return ConnectionUtilProvider;
}());

//# sourceMappingURL=connection-util.js.map

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_izitoast__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_izitoast___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_izitoast__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constant__ = __webpack_require__(3);


var BasePage = /** @class */ (function () {
    function BasePage(event) {
        this.event = event;
        this.iziToastInstance = __WEBPACK_IMPORTED_MODULE_0_izitoast___default.a;
        this._lang = __WEBPACK_IMPORTED_MODULE_1__constant__["a" /* CONSTANT */].APP_LANGUAGE;
    }
    BasePage.prototype.intitialLanguage = function (translate, language) {
        if (language === void 0) { language = this._lang; }
        translate.setDefaultLang(language);
        BasePage.translate = translate;
    };
    BasePage.prototype.getLanguage = function () {
        return BasePage.translate;
    };
    BasePage.prototype.setLanguage = function (lang) {
        var _this = this;
        console.log('setLanguage', lang);
        __WEBPACK_IMPORTED_MODULE_1__constant__["a" /* CONSTANT */].APP_LANGUAGE = lang;
        return new Promise((function (reslove) {
            _this._lang = lang;
            return reslove();
        }));
    };
    BasePage.prototype.popToPage = function (page, navCtrl) {
        var views = navCtrl.getViews();
        var position = views.findIndex(function (val) {
            return val.name == page;
        });
        console.log('view:::', views);
        console.log('position:::', position);
        if (position != -1) {
            var viewIndex = views[position].index;
            navCtrl.popTo(navCtrl.getByIndex(viewIndex));
        }
        else {
            navCtrl.setRoot('MainPage');
        }
    };
    BasePage.prototype.getIZIToastInstance = function () {
        return this.iziToastInstance;
    };
    BasePage.prototype.getToastOptions = function (title, message, ms, backgroundColor) {
        if (title === void 0) { title = ''; }
        if (message === void 0) { message = ''; }
        if (ms === void 0) { ms = 2300; }
        if (backgroundColor === void 0) { backgroundColor = 'dark'; }
        var toastOptions = {
            // id: null,
            //title: title,
            icon: '',
            message: message,
            timeout: ms,
            close: false,
            progressBar: false,
            position: "topCenter",
            // prepare for use
            onOpening: function () { },
            onOpened: function () { },
            onClosing: function () { },
            onClosed: function () { },
            transitionIn: "fadeInDown",
            transitionInMobile: "fadeInDown",
            transitionOut: "fadeOutUp",
            transitionOutMobile: "fadeOutUp",
            animateInside: false,
            backgroundColor: backgroundColor
        };
        return toastOptions;
    };
    BasePage.prototype.destructuringSection = function (options) {
        console.log(options);
        var arrayOption = Object.keys(options).map(function (key) { return options[key]; });
        console.log(arrayOption);
        return arrayOption;
    };
    BasePage.prototype.showSuccessToast = function (options) {
        try {
            this.getIZIToastInstance().success(this.getToastOptions.apply(this, this.destructuringSection(options)));
        }
        catch (err) {
            console.error(err);
        }
    };
    return BasePage;
}());

//# sourceMappingURL=basepage.js.map

/***/ }),
/* 43 */
/***/ (function(module, exports) {

module.exports = {"%signin%":"Sign In","%label.username%":"Username","%label.password%":"Password","%error.login%":"username or password is incorrect","%message.logout%":"Are you sure you want logout?","%message.title%":"Alert","%btn.logout%":"Logout","%btn.logout.yes%":"Yes","%btn.logout.no%":"No","%language.change%":"Switch to","%language%":"English","%loading%":"Please wait","%btn.close%":"Close","%message.delete.image%":"Are you sure you want to delete?","%message.reset%":"Are you sure you want to reset?","%message.photo%":"The number of images exceeded","%error.location%":"Please select the location","%btn.survey.save%":"Save","%btn.survey.back%":"Back","%label.creator%":"Creator","%label.remark%":"Remark","%label.images%":"Photo","%error.moldmaster%":"Mold not found","%message.moldmaster%":"This Mold has transaction history","%session.timeout%":"The session has expired","%message.notfound%":"Data not found","%message.serach.mold.error%":"Please inside Mold","%0%":"Connection error","%404%":"404 Not Found","%message.save.success%":"Data saved successfully","%message.delete.success%":"Data delete successfully","%message.active.mobile%":"โทรศัพท์เครื่องนี้ไม่ได้รับสิทธิ์ในการใช้งาน \nกรุณาติดต่อเจ้าหน้าที่ IT Support","%message.active.password%":"Please inside password","%message.active.error%":"Password is incorrect,<br>Please try again","%btn.back%":"Back Home","%btn.confirm%":"Confirm","%btn.cancel%":"Cancel","%title.confirm%":"Confirm Save Data"}

/***/ }),
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldinoutProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_MoldInOutModel__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__base_provider_base_provider__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_package_OperationBaseModel__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__adapter_moldinout_adapter_moldinout_adapter__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models_package_MoldInOutAdapterModel__ = __webpack_require__(202);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/*
  Generated class for the MoldinoutProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var MoldinoutProvider = /** @class */ (function (_super) {
    __extends(MoldinoutProvider, _super);
    function MoldinoutProvider(alertCtrl, loadingCtrl, _moldInoutAdapterProvider) {
        var _this = _super.call(this, alertCtrl, loadingCtrl) || this;
        _this.alertCtrl = alertCtrl;
        _this.loadingCtrl = loadingCtrl;
        _this._moldInoutAdapterProvider = _moldInoutAdapterProvider;
        _this.moldInOutModel = new __WEBPACK_IMPORTED_MODULE_0__models_MoldInOutModel__["a" /* MoldInOutModel */]();
        _this.moldInOutModel2 = new __WEBPACK_IMPORTED_MODULE_0__models_MoldInOutModel__["a" /* MoldInOutModel */]();
        console.log('Hello MoldinoutProvider Provider');
        return _this;
    }
    MoldinoutProvider.prototype.getSetting = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldInoutAdapterProvider.getSetting()];
                    case 1:
                        response = _a.sent();
                        console.log("getSetting ::: ", response);
                        this.moldInOutModel.getSetting = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        this.moldInOutModel2 = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_1, __WEBPACK_IMPORTED_MODULE_5__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutProvider.prototype.getNewMoldOut = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldInoutAdapterProvider.getNewMoldOut(this.moldInOutModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getNewMoldOut ::: ", response);
                        this.moldInOutModel.getNewMoldInOut = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_2 = _a.sent();
                        this.moldInOutModel2 = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_2, __WEBPACK_IMPORTED_MODULE_5__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutProvider.prototype.saveDataNewMold = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldInoutAdapterProvider.saveDataNewMold(this.moldInOutModel)];
                    case 1:
                        response = _a.sent();
                        console.log("saveDataNewMold ::: ", response);
                        this.moldInOutModel.getNewMoldInOut = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_3 = _a.sent();
                        this.moldInOutModel2 = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_3, __WEBPACK_IMPORTED_MODULE_5__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutProvider.prototype.getApprove = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldInoutAdapterProvider.getApprove(this.moldInOutModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getApprove ::: ", response);
                        this.moldInOutModel.getApprove = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_4 = _a.sent();
                        this.moldInOutModel2 = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_4, __WEBPACK_IMPORTED_MODULE_5__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutProvider.prototype.ApproveOrRejectOut = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldInoutAdapterProvider.ApproveOrRejectOut(this.moldInOutModel)];
                    case 1:
                        response = _a.sent();
                        console.log("ApproveOrRejectOut ::: ", response);
                        this.moldInOutModel.statment = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_5 = _a.sent();
                        this.moldInOutModel2 = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_5, __WEBPACK_IMPORTED_MODULE_5__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutProvider.prototype.ConfirmOrRejectOut = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldInoutAdapterProvider.ConfirmOrRejectOut(this.moldInOutModel)];
                    case 1:
                        response = _a.sent();
                        console.log("ConfirmOrRejectOut ::: ", response);
                        this.moldInOutModel.statment = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_6 = _a.sent();
                        this.moldInOutModel2 = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_6, __WEBPACK_IMPORTED_MODULE_5__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutProvider.prototype.moldSearch = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldInoutAdapterProvider.moldSearch(this.moldInOutModel)];
                    case 1:
                        response = _a.sent();
                        console.log("moldSearch ::: ", response);
                        this.moldInOutModel.getSearch = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_7 = _a.sent();
                        this.moldInOutModel2 = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_7, __WEBPACK_IMPORTED_MODULE_5__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutProvider.prototype.moldStatus = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldInoutAdapterProvider.moldStatus(this.moldInOutModel)];
                    case 1:
                        response = _a.sent();
                        console.log("moldStatus ::: ", response);
                        this.moldInOutModel.getStatus = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_8 = _a.sent();
                        this.moldInOutModel2 = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_8, __WEBPACK_IMPORTED_MODULE_5__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutProvider.prototype.getWaitingApprove = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldInoutAdapterProvider.getWaitingApprove()];
                    case 1:
                        response = _a.sent();
                        console.log("getWaitingApprove ::: ", response);
                        this.moldInOutModel.getWaiting = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_9 = _a.sent();
                        this.moldInOutModel2 = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_9, __WEBPACK_IMPORTED_MODULE_5__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    }; //MOLDINOUT_APPROVE_CONFIRMIN
    MoldinoutProvider.prototype.ConfirmIn = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldInoutAdapterProvider.ConfirmIn(this.moldInOutModel)];
                    case 1:
                        response = _a.sent();
                        console.log("ConfirmIn ::: ", response);
                        this.moldInOutModel.statment = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_10 = _a.sent();
                        this.moldInOutModel2 = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_10, __WEBPACK_IMPORTED_MODULE_5__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutProvider.prototype.ApproveOrRejectIn = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_11;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldInoutAdapterProvider.ApproveOrRejectIn(this.moldInOutModel)];
                    case 1:
                        response = _a.sent();
                        console.log("ApproveOrRejectIn ::: ", response);
                        this.moldInOutModel.statment = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_11 = _a.sent();
                        this.moldInOutModel2 = __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_11, __WEBPACK_IMPORTED_MODULE_5__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__adapter_moldinout_adapter_moldinout_adapter__["a" /* MoldInoutAdapterProvider */]])
    ], MoldinoutProvider);
    return MoldinoutProvider;
}(__WEBPACK_IMPORTED_MODULE_2__base_provider_base_provider__["a" /* BaseProvider */]));

//# sourceMappingURL=moldinout-provider.js.map

/***/ }),
/* 55 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_image_picker__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_utilities__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






// import { File } from '@ionic-native/file';

/*
  Generated class for the ScanProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ScanProvider = /** @class */ (function () {
    function ScanProvider(barcodeScanner, camera, imagePicker, 
        // private file: File,
        alertCtrl, loadingCtrl) {
        this.barcodeScanner = barcodeScanner;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        console.log('Hello ScanProvider Provider');
    }
    ScanProvider.prototype.scan = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var options;
            return __generator(this, function (_a) {
                options = {
                    preferFrontCamera: false,
                    showFlipCameraButton: true,
                    showTorchButton: true,
                    torchOn: false,
                    prompt: 'Place a barcode inside the scan area',
                    // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
                    resultDisplayDuration: 500,
                    //formats: 'QR_CODE,PDF_417', // default: all but PDF_417 and RSS_EXPANDED
                    // Android only (portrait|landscape), default unset so it rotates with the device
                    orientation: 'portrait',
                    disableAnimations: true,
                    disableSuccessBeep: false // iOS
                };
                return [2 /*return*/, new Promise(function (resolve) {
                        _this.barcodeScanner.scan(options)
                            .then(function (data) {
                            resolve(data.text);
                        }).catch(function (error) {
                            _this.showMessageError(error);
                            resolve('');
                        });
                    })];
            });
        });
    };
    ScanProvider.prototype.getImageFromCamere = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var loading, options;
            return __generator(this, function (_a) {
                loading = this.loadingCtrl.create({});
                loading.present();
                options = {
                    quality: 100,
                    allowEdit: false,
                    destinationType: this.camera.DestinationType.DATA_URL,
                    sourceType: this.camera.PictureSourceType.CAMERA,
                    encodingType: this.camera.EncodingType.JPEG,
                    saveToPhotoAlbum: true,
                    correctOrientation: true
                };
                return [2 /*return*/, new Promise(function (resolve) {
                        _this.camera.getPicture(options).then(function (b64str) { return __awaiter(_this, void 0, void 0, function () {
                            var b64resize;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_6__shared_utilities__["a" /* Utilities */].resizeImage('data:image/jpeg;base64,' + b64str)];
                                    case 1:
                                        b64resize = _a.sent();
                                        loading.dismiss();
                                        resolve(b64resize);
                                        return [2 /*return*/];
                                }
                            });
                        }); }).catch(function (error) {
                            if (error != 'No Image Selected') {
                                _this.showMessageError(error);
                            }
                            loading.dismiss();
                            resolve(error);
                        });
                    })
                    // return new Promise(resolve => {
                    //   this.camera.getPicture(options).then(async fileUri => {
                    //     var imagePath = fileUri.substr(0, fileUri.lastIndexOf('/') + 1);
                    //     var imageName = fileUri.substr(fileUri.lastIndexOf('/') + 1);
                    //     let fileImages = await this.file.readAsDataURL(imagePath, imageName).then((b64str) => {
                    //       return b64str
                    //     })
                    //     resolve(await Utilities.getBase64Image(fileImages))
                    //   }).catch((error) => {
                    //     if(error != 'No Image Selected'){
                    //       this.showMessageError(error);
                    //     }
                    //     resolve(error)
                    //   })
                    // })
                ];
            });
        });
    };
    ScanProvider.prototype.getImageFormAlbum = function (limit) {
        if (limit === void 0) { limit = 5; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var base64Images, options;
            return __generator(this, function (_a) {
                base64Images = [];
                options = {
                    quality: 100,
                    maximumImagesCount: limit,
                    height: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].HEIGHT,
                    width: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].WIDTH,
                    outputType: 1
                };
                return [2 /*return*/, new Promise(function (resolve) {
                        _this.imagePicker.getPictures(options)
                            .then(function (results) { return __awaiter(_this, void 0, void 0, function () {
                            var i;
                            return __generator(this, function (_a) {
                                for (i = 0; i < results.length; i++) {
                                    base64Images.push('data:image/jpeg;base64,' + results[i]);
                                }
                                resolve(base64Images);
                                return [2 /*return*/];
                            });
                        }); }).catch(function (error) {
                            _this.showMessageError(error);
                            resolve(error);
                        });
                    })];
            });
        });
    };
    ScanProvider.prototype.showMessageError = function (msg) {
        var alert = this.alertCtrl.create({
            enableBackdropDismiss: false,
            title: 'Attention!',
            subTitle: msg,
            buttons: ['Close']
        });
        alert.present();
    };
    ScanProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_image_picker__["a" /* ImagePicker */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */]])
    ], ScanProvider);
    return ScanProvider;
}());

//# sourceMappingURL=scan.js.map

/***/ }),
/* 56 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_package_MessageAdapterModel__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__base_provider_base_provider__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__adapter_message_adapter_message_adapter__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models_MessageModel__ = __webpack_require__(369);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/*
  Generated class for the MessageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var MessageProvider = /** @class */ (function (_super) {
    __extends(MessageProvider, _super);
    function MessageProvider(alertCtrl, loadingCtrl, _messageAdapterProvider) {
        var _this = _super.call(this, alertCtrl, loadingCtrl) || this;
        _this.alertCtrl = alertCtrl;
        _this.loadingCtrl = loadingCtrl;
        _this._messageAdapterProvider = _messageAdapterProvider;
        _this.messageModel = new __WEBPACK_IMPORTED_MODULE_7__models_MessageModel__["a" /* MessageModel */]();
        _this.messageModel2 = new __WEBPACK_IMPORTED_MODULE_7__models_MessageModel__["a" /* MessageModel */]();
        console.log('Hello MessageProvider Provider');
        return _this;
    }
    MessageProvider.prototype.getMessageDetail = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._messageAdapterProvider.getMessageDetail()];
                    case 1:
                        response = _a.sent();
                        console.log("getMessageDetail ::: ", response);
                        this.messageModel.getMessageDetail = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_0__models_package_MessageAdapterModel__["a" /* MessageAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        this.messageModel2 = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_1, __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MessageProvider.prototype.getReadMessage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._messageAdapterProvider.readMessage(this.messageModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getReadMessage ::: ", response);
                        this.messageModel.readMessage = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_0__models_package_MessageAdapterModel__["a" /* MessageAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_2 = _a.sent();
                        this.messageModel2 = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_2, __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MessageProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__adapter_message_adapter_message_adapter__["a" /* MessageAdapterProvider */]])
    ], MessageProvider);
    return MessageProvider;
}(__WEBPACK_IMPORTED_MODULE_2__base_provider_base_provider__["a" /* BaseProvider */]));

//# sourceMappingURL=message-provider.js.map

/***/ }),
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */,
/* 94 */,
/* 95 */,
/* 96 */,
/* 97 */,
/* 98 */,
/* 99 */,
/* 100 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubPhotoModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubPhotoModel = /** @class */ (function () {
    function SubPhotoModel() {
        this.photo = undefined;
        this.latitude = undefined;
        this.longitude = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerBKFVIEW.SubPhoto.PHOTONAME, String, true),
        __metadata("design:type", String)
    ], SubPhotoModel.prototype, "photo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerBKFVIEW.SubPhoto.LATITUDE, String, true),
        __metadata("design:type", String)
    ], SubPhotoModel.prototype, "latitude", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerBKFVIEW.SubPhoto.LONGITUDE, String, true),
        __metadata("design:type", String)
    ], SubPhotoModel.prototype, "longitude", void 0);
    SubPhotoModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubPhotoModel")
    ], SubPhotoModel);
    return SubPhotoModel;
}());

//# sourceMappingURL=SubPhotoModel.js.map

/***/ }),
/* 101 */,
/* 102 */,
/* 103 */,
/* 104 */,
/* 105 */,
/* 106 */,
/* 107 */,
/* 108 */,
/* 109 */,
/* 110 */,
/* 111 */,
/* 112 */,
/* 113 */,
/* 114 */,
/* 115 */,
/* 116 */,
/* 117 */,
/* 118 */,
/* 119 */,
/* 120 */,
/* 121 */,
/* 122 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__datetime_datetime__ = __webpack_require__(378);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__datetime_datetime__["a" /* DatetimePipe */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__datetime_datetime__["a" /* DatetimePipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),
/* 123 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__accordion_menu_accordion_menu__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__accordion_list_accordion_list__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__mold_in_out_approve_mold_in_out_approve__ = __webpack_require__(381);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__accordion_menu_accordion_menu__["a" /* AccordionMenuComponent */],
                __WEBPACK_IMPORTED_MODULE_3__accordion_list_accordion_list__["a" /* AccordionListComponent */],
                __WEBPACK_IMPORTED_MODULE_4__mold_in_out_approve_mold_in_out_approve__["a" /* MoldInOutApproveComponent */]
            ],
            imports: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* IonicPageModule */].forChild('')],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__accordion_menu_accordion_menu__["a" /* AccordionMenuComponent */],
                __WEBPACK_IMPORTED_MODULE_3__accordion_list_accordion_list__["a" /* AccordionListComponent */],
                __WEBPACK_IMPORTED_MODULE_4__mold_in_out_approve_mold_in_out_approve__["a" /* MoldInOutApproveComponent */]
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),
/* 124 */,
/* 125 */,
/* 126 */,
/* 127 */,
/* 128 */,
/* 129 */,
/* 130 */,
/* 131 */,
/* 132 */,
/* 133 */,
/* 134 */,
/* 135 */,
/* 136 */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 136;

/***/ }),
/* 137 */,
/* 138 */,
/* 139 */,
/* 140 */,
/* 141 */,
/* 142 */,
/* 143 */,
/* 144 */,
/* 145 */,
/* 146 */,
/* 147 */,
/* 148 */,
/* 149 */,
/* 150 */,
/* 151 */,
/* 152 */,
/* 153 */,
/* 154 */,
/* 155 */,
/* 156 */,
/* 157 */,
/* 158 */,
/* 159 */,
/* 160 */,
/* 161 */,
/* 162 */,
/* 163 */,
/* 164 */,
/* 165 */,
/* 166 */,
/* 167 */,
/* 168 */,
/* 169 */,
/* 170 */,
/* 171 */,
/* 172 */,
/* 173 */,
/* 174 */,
/* 175 */,
/* 176 */,
/* 177 */,
/* 178 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/active-mobile/active-mobile.module": [
		179
	],
	"../pages/admin-config/admin-config.module": [
		418,
		28
	],
	"../pages/admin-create-user/admin-create-user.module": [
		419,
		27
	],
	"../pages/admin-edit-user/admin-edit-user.module": [
		420,
		26
	],
	"../pages/admin-menu/admin-menu.module": [
		421,
		25
	],
	"../pages/admin-user-bkf/admin-user-bkf.module": [
		422,
		24
	],
	"../pages/admin-user-list/admin-user-list.module": [
		423,
		23
	],
	"../pages/admin-user-mold/admin-user-mold.module": [
		424,
		22
	],
	"../pages/bkf-main/bkf-main.module": [
		198
	],
	"../pages/drawing-image/drawing-image.module": [
		201
	],
	"../pages/edit-images/edit-images.module": [
		425,
		21
	],
	"../pages/login/login.module": [
		205
	],
	"../pages/main/main.module": [
		210
	],
	"../pages/maker-menu/maker-menu.module": [
		426,
		20
	],
	"../pages/moldinout-approve/moldinout-approve.module": [
		427,
		19
	],
	"../pages/moldinout-confirm-in/moldinout-confirm-in.module": [
		428,
		18
	],
	"../pages/moldinout-confirm-out/moldinout-confirm-out.module": [
		444,
		17
	],
	"../pages/moldinout-create/moldinout-create.module": [
		445,
		16
	],
	"../pages/moldinout-history/moldinout-history.module": [
		447,
		15
	],
	"../pages/moldinout-menu/moldinout-menu.module": [
		434,
		29
	],
	"../pages/moldinout-preview/moldinout-preview.module": [
		429,
		14
	],
	"../pages/moldinout-reject-out/moldinout-reject-out.module": [
		430,
		13
	],
	"../pages/moldinout-search/moldinout-search.module": [
		431,
		12
	],
	"../pages/moldinout-status/moldinout-status.module": [
		432,
		11
	],
	"../pages/moldinout-view-moldmaker/moldinout-view-moldmaker.module": [
		433,
		10
	],
	"../pages/moldinout-waiting-for-approve/moldinout-waiting-for-approve.module": [
		435,
		9
	],
	"../pages/moldmaker-history-detail/moldmaker-history-detail.module": [
		436,
		8
	],
	"../pages/moldmaker-history/moldmaker-history.module": [
		437,
		7
	],
	"../pages/moldmaker-receive/moldmaker-receive.module": [
		438,
		6
	],
	"../pages/notification/notification.module": [
		439,
		5
	],
	"../pages/survey-create/survey-create.module": [
		446,
		4
	],
	"../pages/survey-history-detail/survey-history-detail.module": [
		440,
		3
	],
	"../pages/survey-history/survey-history.module": [
		441,
		2
	],
	"../pages/survey-menu/survey-menu.module": [
		442,
		1
	],
	"../pages/survey-preview/survey-preview.module": [
		443,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 178;
module.exports = webpackAsyncContext;

/***/ }),
/* 179 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActiveMobilePageModule", function() { return ActiveMobilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__active_mobile__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ActiveMobilePageModule = /** @class */ (function () {
    function ActiveMobilePageModule() {
    }
    ActiveMobilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__active_mobile__["a" /* ActiveMobilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__active_mobile__["a" /* ActiveMobilePage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__active_mobile__["a" /* ActiveMobilePage */]
            ]
        })
    ], ActiveMobilePageModule);
    return ActiveMobilePageModule;
}());

//# sourceMappingURL=active-mobile.module.js.map

/***/ }),
/* 180 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActiveMobilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_login_provider_login_provider__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__ = __webpack_require__(33);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










/**
 * Generated class for the ActiveMobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ActiveMobilePage = /** @class */ (function (_super) {
    __extends(ActiveMobilePage, _super);
    function ActiveMobilePage(navCtrl, navParams, events, translate, alertCtrl, storageProvider, _loginProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.events = events;
        _this.translate = translate;
        _this.alertCtrl = alertCtrl;
        _this.storageProvider = storageProvider;
        _this._loginProvider = _loginProvider;
        _this.intitialLanguage(_this.translate);
        return _this;
    }
    ActiveMobilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ActiveMobilePage');
    };
    ActiveMobilePage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        // this.setLanguage(CONSTANT.APP_LANGUAGE);
                        _a = this;
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].IMEI()];
                    case 1:
                        // this.setLanguage(CONSTANT.APP_LANGUAGE);
                        _a.IMEI = _b.sent();
                        this.loginModel = this._loginProvider.loginModel;
                        return [2 /*return*/];
                }
            });
        });
    };
    ActiveMobilePage.prototype.ActiveMobile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._loginProvider.deviceAllow()];
                    case 1:
                        _a.sent();
                        if (this.loginModel.authenticateUser.deviceAllow.length > 0) {
                            this.navCtrl.setRoot('LoginPage');
                        }
                        else {
                            this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%message.title%"],
                                subTitle: '',
                                inputs: [{
                                        type: 'password',
                                        name: 'keyActive',
                                    }],
                                buttons: [{
                                        text: 'Cancel',
                                        handler: function () {
                                            console.log('Cancel');
                                        }
                                    }, {
                                        text: 'Active',
                                        handler: function (data) {
                                            _this.loginModel.keyActive = data.keyActive;
                                            _this.SendActiveMobile();
                                        }
                                    }]
                            }).present();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _a.sent();
                        this._loginProvider.submitErrorEvent(this._loginProvider.loginModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ActiveMobilePage.prototype.SendActiveMobile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var ex_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        if (this.loginModel.keyActive == '') {
                            this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%message.active.password%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%message.active.password%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            }).present();
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this._loginProvider.activeMobile()];
                    case 1:
                        _a.sent();
                        if (this.loginModel.authenticateUser.status == 'Active') {
                            this.storageProvider.setKey(__WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].ACTIVE_MOBILE, 'ACTIVE');
                            this.navCtrl.push('LoginPage');
                        }
                        else {
                            this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%message.active.error%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%message.active.error%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            }).present();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        ex_2 = _a.sent();
                        this._loginProvider.submitErrorEvent(this._loginProvider.loginModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ActiveMobilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-active-mobile',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/active-mobile/active-mobile.html"*/'<ion-header no-border>\n    <ion-navbar transparent>\n    </ion-navbar>\n  </ion-header>\n\n<ion-content>\n  <div class="login-container">\n    <img class="logo" src="../../assets/imgs/Logo_BKF.jpg" alt="" />\n  </div>\n\n  <div padding>\n    <div class="text-message">{{ \'%message.active.mobile%\' | translate }}</div>\n    <div class="text-imei ">{{ IMEI }}</div>\n\n    <button ion-button full (click)="ActiveMobile()">Active</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/active-mobile/active-mobile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_storage_provider_storage_provider__["a" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_login_provider_login_provider__["a" /* LoginProvider */]])
    ], ActiveMobilePage);
    return ActiveMobilePage;
}(__WEBPACK_IMPORTED_MODULE_0__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=active-mobile.js.map

/***/ }),
/* 181 */,
/* 182 */,
/* 183 */,
/* 184 */,
/* 185 */,
/* 186 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__package_OperationBaseModel__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__package_AuthenticateUserAdapterModel__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__package_SettingAdapterModel__ = __webpack_require__(188);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// import { Utilities } from './../shared/utilities';
// import { CONSTATT } from './../shared/constant';




// import { MOCK } from "../mock/mockdata";
var LoginModel = /** @class */ (function (_super) {
    __extends(LoginModel, _super);
    function LoginModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.authenticateUser = new __WEBPACK_IMPORTED_MODULE_2__package_AuthenticateUserAdapterModel__["a" /* AuthenticateUserAdapterModel */]();
        _this.setting = new __WEBPACK_IMPORTED_MODULE_3__package_SettingAdapterModel__["a" /* SettingAdapterModel */]();
        return _this;
    }
    LoginModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("LoginModel")
    ], LoginModel);
    return LoginModel;
}(__WEBPACK_IMPORTED_MODULE_1__package_OperationBaseModel__["a" /* OperationBaseModel */]));

//# sourceMappingURL=LoginModel.js.map

/***/ }),
/* 187 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticateUserAdapterModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SubLoginModel__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__SubDeviceAllowMoldel__ = __webpack_require__(330);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthenticateUserAdapterModel = /** @class */ (function () {
    function AuthenticateUserAdapterModel() {
        this.UserId = undefined;
        this.UserType = undefined;
        this.FullName = undefined;
        this.IMEI = undefined;
        this.ImageSurvey = undefined;
        this.ImageMoldInOut = undefined;
        this.userLogin = undefined;
        this.deviceAllow = undefined;
        this.status = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.USER_ID, String, true),
        __metadata("design:type", String)
    ], AuthenticateUserAdapterModel.prototype, "UserId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.USER_TYPE, String, true),
        __metadata("design:type", String)
    ], AuthenticateUserAdapterModel.prototype, "UserType", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.FULL_NAME, String, true),
        __metadata("design:type", String)
    ], AuthenticateUserAdapterModel.prototype, "FullName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.IMEI, String, true),
        __metadata("design:type", String)
    ], AuthenticateUserAdapterModel.prototype, "IMEI", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.IMAGES_SURVEY, String, true),
        __metadata("design:type", String)
    ], AuthenticateUserAdapterModel.prototype, "ImageSurvey", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.IMAGES_MOLDINOUT, String, true),
        __metadata("design:type", String)
    ], AuthenticateUserAdapterModel.prototype, "ImageMoldInOut", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.USER_LOGIN, [__WEBPACK_IMPORTED_MODULE_2__SubLoginModel__["a" /* SubLoginModel */]], true),
        __metadata("design:type", Array)
    ], AuthenticateUserAdapterModel.prototype, "userLogin", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.DEVICE_ALLOW, [__WEBPACK_IMPORTED_MODULE_3__SubDeviceAllowMoldel__["a" /* SubDeviceAllowMoldel */]], true),
        __metadata("design:type", Array)
    ], AuthenticateUserAdapterModel.prototype, "deviceAllow", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.STATUS, String, true),
        __metadata("design:type", String)
    ], AuthenticateUserAdapterModel.prototype, "status", void 0);
    AuthenticateUserAdapterModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("AuthenticateUserAdapterModel")
    ], AuthenticateUserAdapterModel);
    return AuthenticateUserAdapterModel;
}());

//# sourceMappingURL=AuthenticateUserAdapterModel.js.map

/***/ }),
/* 188 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingAdapterModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SettingConfigModel__ = __webpack_require__(331);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SettingAdapterModel = /** @class */ (function () {
    function SettingAdapterModel() {
        this.config = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SettingModel.CONFIG, [__WEBPACK_IMPORTED_MODULE_2__SettingConfigModel__["a" /* SettingConfigModel */]], true),
        __metadata("design:type", Array)
    ], SettingAdapterModel.prototype, "config", void 0);
    SettingAdapterModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SettingAdapterModel")
    ], SettingAdapterModel);
    return SettingAdapterModel;
}());

//# sourceMappingURL=SettingAdapterModel.js.map

/***/ }),
/* 189 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationAdapterProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__connection_util_connection_util__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__storage_provider_storage_provider__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AuthenticationAdapterProvider = /** @class */ (function () {
    function AuthenticationAdapterProvider(_connectionUtil, _storageProvider) {
        this._connectionUtil = _connectionUtil;
        this._storageProvider = _storageProvider;
        console.log('Hello AuthenticationAdapterProvider Provider');
    }
    AuthenticationAdapterProvider.prototype.authenticateUser = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param, _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        param = null;
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {
                            username: inputObj.username,
                            password: inputObj.password
                        };
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].FCM_TOKEN)];
                    case 1:
                        _e.token = _f.sent();
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].IMEI()];
                    case 2:
                        param = [(_a[_b] = (_c[_d] = (_e.imei = _f.sent(),
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].AUTHENTICATION_AUTHENTICATE_ADAPTER, param)];
                }
            });
        });
    };
    AuthenticationAdapterProvider.prototype.deviceAllow = function () {
        return __awaiter(this, void 0, void 0, function () {
            var param, _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        param = null;
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {};
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].IMEI()];
                    case 1:
                        param = [(_a[_b] = (_c[_d] = (_e.imei = _f.sent(),
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].AUTHENTICATION_DEVICE_ALLOW_ADAPTER, param)];
                }
            });
        });
    };
    AuthenticationAdapterProvider.prototype.activeMobile = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param, _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        param = null;
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {
                            password: inputObj.keyActive
                        };
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].IMEI()];
                    case 1:
                        param = [(_a[_b] = (_c[_d] = (_e.imei = _f.sent(),
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].AUTHENTICATION_ACTIVE_ADAPTER, param)];
                }
            });
        });
    };
    AuthenticationAdapterProvider.prototype.settingConfig = function () {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {}
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].AUTHENtICATION_CONFIG, param)];
            });
        });
    };
    AuthenticationAdapterProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__connection_util_connection_util__["a" /* ConnectionUtilProvider */],
            __WEBPACK_IMPORTED_MODULE_4__storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], AuthenticationAdapterProvider);
    return AuthenticationAdapterProvider;
}());

//# sourceMappingURL=authentication-adapter.js.map

/***/ }),
/* 190 */,
/* 191 */,
/* 192 */,
/* 193 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Connection; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__MockConnection__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__NodeConnection__ = __webpack_require__(337);


var Connection = /** @class */ (function () {
    function Connection() {
    }
    Connection.prototype.getConnection = function (connection, http) {
        if (connection == null) {
            return null;
        }
        if (!this.connectionMobile) {
            if ("MOCK" === connection) {
                this.connectionMobile = new __WEBPACK_IMPORTED_MODULE_0__MockConnection__["a" /* MockConnection */]();
            }
            else if ("NODE" === connection) {
                this.connectionMobile = new __WEBPACK_IMPORTED_MODULE_1__NodeConnection__["a" /* NodeConnection */](http);
            }
        }
        return this.connectionMobile;
    };
    return Connection;
}());

//# sourceMappingURL=Connection.js.map

/***/ }),
/* 194 */,
/* 195 */,
/* 196 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminAdapterModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SubAdminGetListUserBKF__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__SubAdminGetListUserMold__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__SubAdminGetConfig__ = __webpack_require__(366);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminAdapterModel = /** @class */ (function () {
    function AdminAdapterModel() {
        this.Status = undefined;
        this.Message = undefined;
        this.getListBKF = undefined;
        this.getListMold = undefined;
        this.getConfig = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySaveMoldModel.STATUS, Number, true),
        __metadata("design:type", String)
    ], AdminAdapterModel.prototype, "Status", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySaveMoldModel.MESSAGE, String, true),
        __metadata("design:type", String)
    ], AdminAdapterModel.prototype, "Message", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AdminAdapterModel.GET_USER_BKF, [__WEBPACK_IMPORTED_MODULE_2__SubAdminGetListUserBKF__["a" /* SubAdminGetListUserBKF */]], true),
        __metadata("design:type", Array)
    ], AdminAdapterModel.prototype, "getListBKF", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AdminAdapterModel.GET_USER_MOLD, [__WEBPACK_IMPORTED_MODULE_3__SubAdminGetListUserMold__["a" /* SubAdminGetListUserMold */]], true),
        __metadata("design:type", Array)
    ], AdminAdapterModel.prototype, "getListMold", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AdminAdapterModel.GET_CONFIG, [__WEBPACK_IMPORTED_MODULE_4__SubAdminGetConfig__["a" /* SubAdminGetConfig */]], true),
        __metadata("design:type", Array)
    ], AdminAdapterModel.prototype, "getConfig", void 0);
    AdminAdapterModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("AdminAdapterModel")
    ], AdminAdapterModel);
    return AdminAdapterModel;
}());

//# sourceMappingURL=AdminAdapterModel.js.map

/***/ }),
/* 197 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminAdapterProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__connection_util_connection_util__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



/*
  Generated class for the AdminAdapterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AdminAdapterProvider = /** @class */ (function () {
    function AdminAdapterProvider(_connectionUtil) {
        this._connectionUtil = _connectionUtil;
        console.log('Hello AdminAdapterProvider Provider');
    }
    AdminAdapterProvider.prototype.getListUserBKF = function () {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {}
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].ADMIN_GET_USER_BKF_ADAPTER, param)];
            });
        });
    };
    AdminAdapterProvider.prototype.getListUserMold = function () {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {}
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].ADMIN_GET_USER_MOLD_ADAPTER, param)];
            });
        });
    };
    AdminAdapterProvider.prototype.getConfig = function () {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {}
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].ADMIN_GET_CONFIG_ADAPTER, param)];
            });
        });
    };
    AdminAdapterProvider.prototype.getCreateUserBKF = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                userid: '',
                                firstname: '',
                                lastname: '',
                                position: ''
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].ADMIN_GET_CONFIG_ADAPTER, param)];
            });
        });
    };
    AdminAdapterProvider.prototype.getUpdateUserBKF = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                empid: inputObj.empid,
                                username: inputObj.username || '',
                                password: inputObj.password || '',
                                firstname: inputObj.firstname || '',
                                lastname: inputObj.lastname || '',
                                position: inputObj.position || '',
                                usergroup: inputObj.usergroup || '',
                                usertype: inputObj.usertype || '',
                                flag: inputObj.flag
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].ADMIN_GET_UPDATE_USER_BKF_ADAPTER, param)];
            });
        });
    };
    AdminAdapterProvider.prototype.getUpdateUserMaker = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                empid: inputObj.empid,
                                username: inputObj.username || '',
                                password: inputObj.password || '',
                                firstname: inputObj.firstname || '',
                                lastname: inputObj.lastname || '',
                                position: inputObj.position || '',
                                usergroup: inputObj.usergroup || '',
                                usertype: inputObj.usertype || '',
                                flag: inputObj.flag
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].ADMIN_GET_UPDATE_USER_MAKER_ADAPTER, param)];
            });
        });
    };
    AdminAdapterProvider.prototype.getUpdateConfig = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                survey: inputObj.imgSurvey,
                                moldinout: inputObj.imgInOut,
                                moldmaker: inputObj.imgMaker,
                                iso: inputObj.isoNumber
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].ADMIN_GET_UPDATE_CONFIG_ADAPTER, param)];
            });
        });
    };
    AdminAdapterProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__connection_util_connection_util__["a" /* ConnectionUtilProvider */]])
    ], AdminAdapterProvider);
    return AdminAdapterProvider;
}());

//# sourceMappingURL=admin-adapter.js.map

/***/ }),
/* 198 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BkfMainPageModule", function() { return BkfMainPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__bkf_main__ = __webpack_require__(367);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BkfMainPageModule = /** @class */ (function () {
    function BkfMainPageModule() {
    }
    BkfMainPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__bkf_main__["a" /* BkfMainPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__bkf_main__["a" /* BkfMainPage */]),
            ],
        })
    ], BkfMainPageModule);
    return BkfMainPageModule;
}());

//# sourceMappingURL=bkf-main.module.js.map

/***/ }),
/* 199 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageAdapterModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SubMessageModel__ = __webpack_require__(368);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MessageAdapterModel = /** @class */ (function () {
    function MessageAdapterModel() {
        this.Status = undefined;
        this.Message = undefined;
        this.detail = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySaveMoldModel.STATUS, Number, true),
        __metadata("design:type", String)
    ], MessageAdapterModel.prototype, "Status", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySaveMoldModel.MESSAGE, String, true),
        __metadata("design:type", String)
    ], MessageAdapterModel.prototype, "Message", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].MessageAdapterModel.MESSAGE, [__WEBPACK_IMPORTED_MODULE_2__SubMessageModel__["a" /* SubMessageModel */]], true),
        __metadata("design:type", Array)
    ], MessageAdapterModel.prototype, "detail", void 0);
    MessageAdapterModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("MessageAdapterModel")
    ], MessageAdapterModel);
    return MessageAdapterModel;
}());

//# sourceMappingURL=MessageAdapterModel.js.map

/***/ }),
/* 200 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageAdapterProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__connection_util_connection_util__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/*
  Generated class for the NotificationAdapterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var MessageAdapterProvider = /** @class */ (function () {
    function MessageAdapterProvider(_connectionUtil, _storageProvider) {
        this._connectionUtil = _connectionUtil;
        this._storageProvider = _storageProvider;
        console.log('Hello NotificationAdapterProvider Provider');
    }
    MessageAdapterProvider.prototype.getMessageDetail = function () {
        return __awaiter(this, void 0, void 0, function () {
            var param, _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        param = null;
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {};
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_3__shared_constant__["a" /* CONSTANT */].USER_ID)];
                    case 1:
                        param = [(_a[_b] = (_c[_d] = (_e.empid = _f.sent(),
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_3__shared_constant__["a" /* CONSTANT */].MESSAGE_GETMESSGE_ADAPTER, param)];
                }
            });
        });
    };
    MessageAdapterProvider.prototype.readMessage = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param, _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        param = null;
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {};
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_3__shared_constant__["a" /* CONSTANT */].USER_ID)];
                    case 1:
                        param = [(_a[_b] = (_c[_d] = (_e.makerid = _f.sent(),
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_3__shared_constant__["a" /* CONSTANT */].MESSAGE_READ_MESSGE_ADAPTER, param)];
                }
            });
        });
    };
    MessageAdapterProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__connection_util_connection_util__["a" /* ConnectionUtilProvider */],
            __WEBPACK_IMPORTED_MODULE_2__storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], MessageAdapterProvider);
    return MessageAdapterProvider;
}());

//# sourceMappingURL=message-adapter.js.map

/***/ }),
/* 201 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DrawingImagePageModule", function() { return DrawingImagePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__drawing_image__ = __webpack_require__(370);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DrawingImagePageModule = /** @class */ (function () {
    function DrawingImagePageModule() {
    }
    DrawingImagePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__drawing_image__["a" /* DrawingImagePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__drawing_image__["a" /* DrawingImagePage */]),
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__drawing_image__["a" /* DrawingImagePage */]
            ]
        })
    ], DrawingImagePageModule);
    return DrawingImagePageModule;
}());

//# sourceMappingURL=drawing-image.module.js.map

/***/ }),
/* 202 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldInOutAdapterModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__SubMoldInOutApproveModel__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__SubNewMoldInOut__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__SubMoldInOutSettingModel__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__SubMoldInOutImageMoldel__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__SubMoldInOutSearchModel__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__SubMoldInOutStatusModel__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__SubMoldInOutWatingModel__ = __webpack_require__(377);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MoldInOutAdapterModel = /** @class */ (function () {
    function MoldInOutAdapterModel() {
        this.Status = undefined;
        this.Message = undefined;
        this.pdf = undefined;
        this.makerId = undefined;
        this.moldSearch = undefined;
        this.moldStatus = undefined;
        this.reason = undefined;
        this.inoutMold = undefined;
        this.imagesOut = undefined;
        this.imagesIn = undefined;
        this.imagesTransport = undefined;
        this.approveIn = undefined;
        this.approveOut = undefined;
        this.getWaiting = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SurveySaveMoldModel.STATUS, Number, true),
        __metadata("design:type", String)
    ], MoldInOutAdapterModel.prototype, "Status", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SurveySaveMoldModel.MESSAGE, String, true),
        __metadata("design:type", String)
    ], MoldInOutAdapterModel.prototype, "Message", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SurveySaveMoldModel.PDF, String, true),
        __metadata("design:type", String)
    ], MoldInOutAdapterModel.prototype, "pdf", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].MoldInoutAdapterModel.SETTING_MOLDMAKER, [__WEBPACK_IMPORTED_MODULE_4__SubMoldInOutSettingModel__["a" /* SubMoldInOutSetting */]], true),
        __metadata("design:type", Array)
    ], MoldInOutAdapterModel.prototype, "makerId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].MoldInoutAdapterModel.MOLDINOUT_SEARCH, [__WEBPACK_IMPORTED_MODULE_6__SubMoldInOutSearchModel__["a" /* SubMoldInOutSearchModel */]], true),
        __metadata("design:type", Array)
    ], MoldInOutAdapterModel.prototype, "moldSearch", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].MoldInoutAdapterModel.MOLDINOUT_STATUS, [__WEBPACK_IMPORTED_MODULE_7__SubMoldInOutStatusModel__["a" /* SubMoldInOutStatusModel */]], true),
        __metadata("design:type", Array)
    ], MoldInOutAdapterModel.prototype, "moldStatus", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].MoldInoutAdapterModel.SETTING_REASON, [__WEBPACK_IMPORTED_MODULE_4__SubMoldInOutSettingModel__["a" /* SubMoldInOutSetting */]], true),
        __metadata("design:type", Array)
    ], MoldInOutAdapterModel.prototype, "reason", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].MoldInoutAdapterModel.NEWMOLDINOUT, [__WEBPACK_IMPORTED_MODULE_3__SubNewMoldInOut__["a" /* SubNewMoldInOut */]], true),
        __metadata("design:type", Array)
    ], MoldInOutAdapterModel.prototype, "inoutMold", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].MoldInoutAdapterModel.MOLDINOUT_IMAGEOUT, [__WEBPACK_IMPORTED_MODULE_5__SubMoldInOutImageMoldel__["a" /* SubMoldInOutImageMoldel */]], true),
        __metadata("design:type", Array)
    ], MoldInOutAdapterModel.prototype, "imagesOut", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].MoldInoutAdapterModel.MOLDINOUT_IMAGEIN, [__WEBPACK_IMPORTED_MODULE_5__SubMoldInOutImageMoldel__["a" /* SubMoldInOutImageMoldel */]], true),
        __metadata("design:type", Array)
    ], MoldInOutAdapterModel.prototype, "imagesIn", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].MoldInoutAdapterModel.MOLDINOUT_IMAGETRANSPORT, [__WEBPACK_IMPORTED_MODULE_5__SubMoldInOutImageMoldel__["a" /* SubMoldInOutImageMoldel */]], true),
        __metadata("design:type", Array)
    ], MoldInOutAdapterModel.prototype, "imagesTransport", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].MoldInoutAdapterModel.MOLDINOUT_APPROVE_IN, [__WEBPACK_IMPORTED_MODULE_0__SubMoldInOutApproveModel__["a" /* SubMoldInOutApproveModel */]], true),
        __metadata("design:type", Array)
    ], MoldInOutAdapterModel.prototype, "approveIn", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].MoldInoutAdapterModel.MOLDINOUT_APPROVE_OUT, [__WEBPACK_IMPORTED_MODULE_0__SubMoldInOutApproveModel__["a" /* SubMoldInOutApproveModel */]], true),
        __metadata("design:type", Array)
    ], MoldInOutAdapterModel.prototype, "approveOut", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].MoldInoutAdapterModel.MOLDINOUT_WAITNG, [__WEBPACK_IMPORTED_MODULE_8__SubMoldInOutWatingModel__["a" /* SubMoldInOutWatingModel */]], true),
        __metadata("design:type", Array)
    ], MoldInOutAdapterModel.prototype, "getWaiting", void 0);
    MoldInOutAdapterModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonObject"])("MoldInOutAdapterModel")
    ], MoldInOutAdapterModel);
    return MoldInOutAdapterModel;
}());

//# sourceMappingURL=MoldInOutAdapterModel.js.map

/***/ }),
/* 203 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DescriptionMoldel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DescriptionMoldel = /** @class */ (function () {
    function DescriptionMoldel() {
        this.AssetId = undefined;
        this.CustNo = undefined;
        this.PartNo = undefined;
        this.PartName = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDatailModel.SUBDESCRIPTION.ASSETID, String, true),
        __metadata("design:type", String)
    ], DescriptionMoldel.prototype, "AssetId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDatailModel.SUBDESCRIPTION.CUSTNO, String, true),
        __metadata("design:type", String)
    ], DescriptionMoldel.prototype, "CustNo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDatailModel.SUBDESCRIPTION.PARTNO, String, true),
        __metadata("design:type", String)
    ], DescriptionMoldel.prototype, "PartNo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDatailModel.SUBDESCRIPTION.PARTNAME, String, true),
        __metadata("design:type", String)
    ], DescriptionMoldel.prototype, "PartName", void 0);
    DescriptionMoldel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("DescriptionMoldel")
    ], DescriptionMoldel);
    return DescriptionMoldel;
}());

//# sourceMappingURL=DescriptionMoldel.js.map

/***/ }),
/* 204 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldInoutAdapterProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__connection_util_connection_util__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__storage_provider_storage_provider__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var MoldInoutAdapterProvider = /** @class */ (function () {
    function MoldInoutAdapterProvider(_connectionUtil, _storageProvider) {
        this._connectionUtil = _connectionUtil;
        this._storageProvider = _storageProvider;
        console.log('Hello MoldInoutAdapterProvider Provider');
    }
    MoldInoutAdapterProvider.prototype.getSetting = function () {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {}
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDINOUT_SETTING_ADAPTER, param)];
            });
        });
    };
    MoldInoutAdapterProvider.prototype.getNewMoldOut = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                "moldid": inputObj.MoldId,
                                "jobid": inputObj.JobId
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDINOUT_MASTER_ADAPTER, param)];
            });
        });
    };
    MoldInoutAdapterProvider.prototype.getMoldOutHistory = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                "moldid": "",
                                "jobid": ""
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDINOUT_HISTORY_ADAPTER, param)];
            });
        });
    };
    MoldInoutAdapterProvider.prototype.getApprove = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                "jobid": inputObj.JobId,
                                "approvetype": ''
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDINOUT_APPROVE_ADAPTER, param)];
            });
        });
    };
    MoldInoutAdapterProvider.prototype.saveDataNewMold = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param, _a, _b, _c, _d, _e, _f, _g;
            return __generator(this, function (_h) {
                switch (_h.label) {
                    case 0:
                        param = null;
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {
                            'JobId': '',
                            'Mold_ID': inputObj.MoldId,
                            'MakerId': inputObj.MakerId,
                            'PlanOut': inputObj.PlanOut,
                            'PlanIn': inputObj.PlanIn,
                            'ReasonId': inputObj.ReasonId,
                            'Remark_new': inputObj.Remark || '',
                            'ConfirmOrCancel': null,
                            'Remark_out': '',
                            'ConfirmOutDate': null,
                            'Remark_out_Transport': '',
                            'CurrentStaus': 'new',
                            'CurrentStausRemark': ''
                        };
                        _f = 'EmpID';
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].USER_ID)];
                    case 1:
                        _e[_f] = _h.sent(),
                            _e['CreateDate'] = '';
                        _g = 'IMEI';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].IMEI()];
                    case 2:
                        param = [(_a[_b] = (_c[_d] = (_e[_g] = _h.sent(),
                                _e['Printed'] = '',
                                _e['flag'] = 'inprogress',
                                _e['MoldMakerStaus'] = '',
                                _e['Custno'] = inputObj.CustNo,
                                _e['PartNo'] = inputObj.PartNo,
                                _e['PartName'] = inputObj.PartName,
                                _e['Photo'] = inputObj.Images,
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDINOUT_SAVE_NEW_ADAPTER, param)];
                }
            });
        });
    };
    MoldInoutAdapterProvider.prototype.ApproveOrRejectOut = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param, _a, _b, _c, _d, _e, _f;
            return __generator(this, function (_g) {
                switch (_g.label) {
                    case 0:
                        param = null;
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {
                            'JobId': inputObj.JobId
                        };
                        _f = 'EmpID';
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].USER_ID)];
                    case 1:
                        param = [(_a[_b] = (_c[_d] = (_e[_f] = _g.sent(),
                                _e['ApproveType'] = inputObj.ApproveType,
                                _e['Status'] = inputObj.ApproveStatus,
                                _e['Remark'] = inputObj.ApproveRemark,
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDINOUT_APPROVE_REJECT_OUT, param)];
                }
            });
        });
    };
    MoldInoutAdapterProvider.prototype.ConfirmOrRejectOut = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param, _a, _b, _c, _d, _e, _f, _g;
            return __generator(this, function (_h) {
                switch (_h.label) {
                    case 0:
                        param = null;
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {
                            'JobId': inputObj.JobId,
                            'ConfirmOrCancel': inputObj.ConfirmStatus,
                            'Remark_out': inputObj.ConfirmStatus == 'N' ? inputObj.ConfirmRemark : '',
                            'Remark_out_Transport': inputObj.ConfirmStatus == 'Y' ? inputObj.ConfirmRemark : '',
                            'CurrentStaus': inputObj.ConfirmStatus == 'Y' ? 'ConfirmOut' : 'Completed',
                            'MoldMakerStaus': ''
                        };
                        _f = 'EmpID';
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].USER_ID)];
                    case 1:
                        _e[_f] = _h.sent();
                        _g = 'IMEI';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].IMEI()];
                    case 2:
                        param = [(_a[_b] = (_c[_d] = (_e[_g] = _h.sent(),
                                _e['Photo'] = inputObj.Images,
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDINOUT_APPROVE_CONFIRMOUT, param)];
                }
            });
        });
    };
    MoldInoutAdapterProvider.prototype.moldSearch = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                moldid: inputObj.MoldId
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDINOUT_SEARCH_ADAPTER, param)];
            });
        });
    };
    MoldInoutAdapterProvider.prototype.moldStatus = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                id: inputObj.statusId
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDINOUT_STATUS_ADAPTER, param)];
            });
        });
    };
    MoldInoutAdapterProvider.prototype.getWaitingApprove = function () {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {}
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDINOUT_GET_WAITING, param)];
            });
        });
    };
    MoldInoutAdapterProvider.prototype.ConfirmIn = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param, _a, _b, _c, _d, _e, _f;
            return __generator(this, function (_g) {
                switch (_g.label) {
                    case 0:
                        param = null;
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {
                            'JobId': inputObj.JobId,
                            'confirmorreject': inputObj.ConfirmStatus,
                            'remark_in': inputObj.ConfirmRemark
                        };
                        _f = 'IMEI';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].IMEI()];
                    case 1:
                        param = [(_a[_b] = (_c[_d] = (_e[_f] = _g.sent(),
                                _e['Photo'] = inputObj.Images,
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDINOUT_APPROVE_CONFIRMIN, param)];
                }
            });
        });
    };
    MoldInoutAdapterProvider.prototype.ApproveOrRejectIn = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param, _a, _b, _c, _d, _e, _f;
            return __generator(this, function (_g) {
                switch (_g.label) {
                    case 0:
                        param = null;
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {
                            'JobId': inputObj.JobId
                        };
                        _f = 'EmpID';
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].USER_ID)];
                    case 1:
                        param = [(_a[_b] = (_c[_d] = (_e[_f] = _g.sent(),
                                _e['Status'] = inputObj.ApproveStatus,
                                _e['Remark'] = inputObj.ApproveRemark,
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDINOUT_APPROVE_REJECT_IN, param)];
                }
            });
        });
    };
    MoldInoutAdapterProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__connection_util_connection_util__["a" /* ConnectionUtilProvider */],
            __WEBPACK_IMPORTED_MODULE_4__storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], MoldInoutAdapterProvider);
    return MoldInoutAdapterProvider;
}());

//# sourceMappingURL=moldinout-adapter.js.map

/***/ }),
/* 205 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pipes_pipes_module__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_4__pipes_pipes_module__["a" /* PipesModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),
/* 206 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_login_provider_login_provider__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_scan_scan__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











var LoginPage = /** @class */ (function (_super) {
    __extends(LoginPage, _super);
    function LoginPage(navCtrl, navParams, events, formBuilder, translate, alertCtrl, _loginProvider, _scanProvider, _storageProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.events = events;
        _this.formBuilder = formBuilder;
        _this.translate = translate;
        _this.alertCtrl = alertCtrl;
        _this._loginProvider = _loginProvider;
        _this._scanProvider = _scanProvider;
        _this._storageProvider = _storageProvider;
        _this.intitialLanguage(_this.translate);
        _this.formLogin = _this.formBuilder.group({
            username: [null, [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].maxLength(16)]],
            password: [null, [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].maxLength(16)]]
        });
        return _this;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.loginModel = this._loginProvider.loginModel;
                return [2 /*return*/];
            });
        });
    };
    LoginPage.prototype.onLogin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert_1, ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.loginModel.username = this.formLogin.controls['username'].value;
                        this.loginModel.password = this.formLogin.controls['password'].value;
                        return [4 /*yield*/, Promise.all([this._loginProvider.authenticateUser(), this._loginProvider.settingConfig()])];
                    case 1:
                        _a.sent();
                        if (this.loginModel.authenticateUser.userLogin.length > 0) {
                            this._storageProvider.setKey(__WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].USER_ID, this.loginModel.authenticateUser.userLogin[0].userId);
                            this._storageProvider.setKey(__WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].USER_NAME, this.loginModel.authenticateUser.userLogin[0].fullName);
                            this._storageProvider.setKey(__WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].USER_TYPE, this.loginModel.authenticateUser.userLogin[0].userType);
                            this._storageProvider.setKey(__WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].IMAGE_MOLDINOUT, this.loginModel.setting.config[0].imageInOut);
                            this._storageProvider.setKey(__WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].IMAGE_MOLDSERVEY, this.loginModel.setting.config[0].imageInOut);
                            this.navCtrl.setRoot("MainPage");
                        }
                        else {
                            alert_1 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__["%error.login%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__["%error.login%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__["%btn.close%"]
                                    }]
                            });
                            alert_1.present();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _a.sent();
                        this._loginProvider.submitErrorEvent(this._loginProvider.loginModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.scan = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = this.formLogin.controls['username']).setValue;
                        return [4 /*yield*/, this._scanProvider.scan()];
                    case 1:
                        _b.apply(_a, [_c.sent()]);
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/login/login.html"*/'<ion-header no-border>\n  <ion-navbar transparent>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div class="login-container">\n    <img class="logo" src="../../assets/imgs/Logo_BKF.jpg" alt="" />\n    <form [formGroup]="formLogin">\n      <ion-item>\n        <label>{{ \'%label.username%\' | translate }}</label>\n        <ion-input type="text" placeholder="{{ \'%label.username%\' | translate }}" formControlName="username" ></ion-input>\n      </ion-item>\n      <ion-item>\n        <label>{{ \'%label.password%\' | translate }}</label>\n        <ion-input type="password" placeholder="{{ \'%label.password%\' | translate }}" formControlName="password"></ion-input>\n      </ion-item>\n      <button ion-button margin [disabled]="!formLogin.valid" (click)="onLogin()">{{"%signin%"|translate}}</button>\n      <p (click)="scan()"><strong>Sign in using QR codes</strong></p>\n    </form>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_login_provider_login_provider__["a" /* LoginProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_scan_scan__["a" /* ScanProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], LoginPage);
    return LoginPage;
}(__WEBPACK_IMPORTED_MODULE_2__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=login.js.map

/***/ }),
/* 207 */,
/* 208 */,
/* 209 */,
/* 210 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainPageModule", function() { return MainPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__main__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var MainPageModule = /** @class */ (function () {
    function MainPageModule() {
    }
    MainPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__main__["a" /* MainPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__main__["a" /* MainPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__main__["a" /* MainPage */]
            ]
        })
    ], MainPageModule);
    return MainPageModule;
}());

//# sourceMappingURL=main.module.js.map

/***/ }),
/* 211 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__moldinout_menu_moldinout_menu__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_login_provider_login_provider__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_scan_scan__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_moldinout_provider_moldinout_provider__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_message_provider_message_provider__ = __webpack_require__(56);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MainPage = /** @class */ (function (_super) {
    __extends(MainPage, _super);
    function MainPage(navCtrl, alertCtrl, events, menuCtrl, translate, _loginProvider, _storageProvider, _moldinoutProvider, _scanProvider, _messageProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.alertCtrl = alertCtrl;
        _this.events = events;
        _this.menuCtrl = menuCtrl;
        _this.translate = translate;
        _this._loginProvider = _loginProvider;
        _this._storageProvider = _storageProvider;
        _this._moldinoutProvider = _moldinoutProvider;
        _this._scanProvider = _scanProvider;
        _this._messageProvider = _messageProvider;
        _this.USER_NAME = '';
        _this.USER_TYPE = '';
        _this.itemMenu = [
            {
                name: 'Fix Asset Survey',
                sub: [{
                        title: 'Survey',
                        icon: 'analytics',
                        component: 'SurveyMenuPage/SurveyCreatePage'
                    }, {
                        title: 'History',
                        icon: 'time',
                        component: 'SurveyMenuPage/SurveyHistoryPage'
                    }]
            },
            {
                name: 'Mold In/Out',
                sub: [{
                        title: 'Request Mold Out',
                        icon: 'git-pull-request',
                        component: 'MoldinoutMenuPage/MoldinoutCreatePage'
                    }, {
                        title: 'Search',
                        icon: 'search',
                        component: 'MoldinoutMenuPage/MoldinoutSearchPage'
                    }, {
                        title: 'Status',
                        icon: 'pulse',
                        component: 'MoldinoutMenuPage/MoldinoutStatusPage'
                    }]
            }
        ];
        _this.intitialLanguage(_this.translate);
        return _this;
    }
    MainPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    MainPage.prototype.ionViewDidLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        console.log('ionViewDidLoad MainPage');
                        _a = this;
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_4__shared_constant__["a" /* CONSTANT */].USER_TYPE)];
                    case 1:
                        _a.USER_TYPE = _c.sent();
                        _b = this;
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_4__shared_constant__["a" /* CONSTANT */].USER_NAME)];
                    case 2:
                        _b.USER_NAME = _c.sent();
                        this.menuCtrl.swipeEnable(false);
                        if (this.USER_TYPE == 'Admin') {
                            this.menuCtrl.enable(false, 'authenticateduser');
                            this.menuCtrl.enable(true, 'authenticatedadmin');
                            this.menuCtrl.enable(false, 'authenticatedmaker');
                            this.rootPage = 'AdminMenuPage';
                        }
                        else if (this.USER_TYPE == 'MoldMaker') {
                            this.menuCtrl.enable(false, 'authenticateduser');
                            this.menuCtrl.enable(false, 'authenticatedadmin');
                            this.menuCtrl.enable(true, 'authenticatedmaker');
                            this.rootPage = 'MakerMenuPage';
                        }
                        else {
                            this.menuCtrl.enable(true, 'authenticateduser');
                            this.menuCtrl.enable(false, 'authenticatedadmin');
                            this.menuCtrl.enable(false, 'authenticatedmaker');
                            this.rootPage = 'BkfMainPage';
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MainPage.prototype.setLang = function () {
        return __awaiter(this, void 0, void 0, function () {
            var lang;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        lang = __WEBPACK_IMPORTED_MODULE_4__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? 'en' : 'th';
                        return [4 /*yield*/, this.setLanguage(lang)];
                    case 1:
                        _a.sent();
                        this.intitialLanguage(this.translate);
                        return [2 /*return*/];
                }
            });
        });
    };
    MainPage.prototype.openPage = function (page) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var pages;
            return __generator(this, function (_a) {
                pages = page.split(/\//g);
                console.log(pages);
                this.navCtrl.push(pages[0]).then(function () {
                    if (pages[1] !== 'MoldinoutCreatePage') {
                        _this.navCtrl.push(pages[1]);
                    }
                    else {
                        new __WEBPACK_IMPORTED_MODULE_0__moldinout_menu_moldinout_menu__["a" /* MoldinoutMenuPage */](_this.navCtrl, _this.events, _this.alertCtrl, _this._loginProvider, _this._moldinoutProvider, _this._scanProvider, _this._messageProvider, _this._storageProvider).scanQr();
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    MainPage.prototype.onLogout = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this._loginProvider.authenticateLogout();
                return [2 /*return*/];
            });
        });
    };
    MainPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* Component */])({
            selector: 'page-main',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/main/main.html"*/'<ion-menu id="authenticateduser" [content]="mycontent">\n  <ion-header>\n    <ion-toolbar class="user-profile">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-4>\n            <div class="user-avatar">\n              <img src="../../assets/imgs/defaul-avatar.jpg" alt="" />\n            </div>\n          </ion-col>\n          <ion-col padding-top col-8>\n            <h6 ion-text class="no-margin bold text-white">\n              {{ USER_NAME }}\n            </h6>\n            <span ion-text>{{ USER_TYPE }}</span>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-toolbar>\n  </ion-header>\n  <ion-content>\n    <accordion-menu *ngFor="let item of itemMenu; let index = index"\n      [title]="item.name"\n      textColor="#FFF"\n      hasMargin="false"\n      headerColor="#A9ADAA"\n      [expanded]="true">\n      <ion-list>\n        <button ion-item menuClose *ngFor="let menu of item.sub" (click)="openPage(menu.component)">\n          <ion-icon item-left [name]="menu.icon"></ion-icon>\n          <span ion-text color="green">{{ menu.title }}</span>\n        </button>\n      </ion-list>\n    </accordion-menu>\n  </ion-content>\n  <ion-footer>\n    <ion-toolbar>\n      <ion-row>\n        <p>{{ \'%language.change%\' | translate }}: </p><pre (click)="setLang()">{{ \'%language%\' | translate }}</pre>\n      </ion-row>\n      <ion-buttons end>\n        <button ion-button icon-end menuClose round color="danger" (click)="onLogout()">\n          {{ \'%btn.logout%\' | translate }}\n        </button>\n      </ion-buttons>\n    </ion-toolbar>\n  </ion-footer>\n</ion-menu>\n\n<ion-menu id="authenticatedadmin" [content]="mycontent">\n  <ion-header>\n    <ion-toolbar class="user-profile">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-4>\n            <div class="user-avatar">\n              <img src="../../assets/imgs/defaul-avatar.jpg" alt="" />\n            </div>\n          </ion-col>\n          <ion-col padding-top col-8>\n            <h6 ion-text class="no-margin bold text-white">\n              {{ USER_NAME }}\n            </h6>\n            <span ion-text>{{ USER_TYPE }}</span>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-toolbar>\n  </ion-header>\n  <ion-content></ion-content>\n  <ion-footer>\n    <ion-toolbar>\n      <ion-row>\n        <p>{{ \'%language.change%\' | translate }}: </p><pre (click)="setLang()">{{ \'%language%\' | translate }}</pre>\n      </ion-row>\n      <ion-buttons end>\n        <button ion-button icon-end menuClose round color="danger" (click)="onLogout()">\n          {{ \'%btn.logout%\' | translate }}\n        </button>\n      </ion-buttons>\n    </ion-toolbar>\n  </ion-footer>\n</ion-menu>\n\n<ion-menu id="authenticatedmaker" [content]="mycontent">\n  <ion-header>\n    <ion-toolbar class="user-profile">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-4>\n            <div class="user-avatar">\n              <img src="../../assets/imgs/defaul-avatar.jpg" alt="" />\n            </div>\n          </ion-col>\n          <ion-col padding-top col-8>\n            <h6 ion-text class="no-margin bold text-white">\n              {{ USER_NAME }}\n            </h6>\n            <span ion-text>{{ USER_TYPE }}</span>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-toolbar>\n  </ion-header>\n  <ion-content></ion-content>\n  <ion-footer>\n    <ion-toolbar>\n      <ion-row>\n        <p>{{ \'%language.change%\' | translate }}: </p><pre (click)="setLang()">{{ \'%language%\' | translate }}</pre>\n      </ion-row>\n      <ion-buttons end>\n        <button ion-button icon-end menuClose round color="danger" (click)="onLogout()">\n          {{ \'%btn.logout%\' | translate }}\n        </button>\n      </ion-buttons>\n    </ion-toolbar>\n  </ion-footer>\n</ion-menu>\n\n<ion-nav #mycontent [root]="rootPage"></ion-nav>'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/main/main.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_6__providers_login_provider_login_provider__["a" /* LoginProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_storage_provider_storage_provider__["a" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_moldinout_provider_moldinout_provider__["a" /* MoldinoutProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_scan_scan__["a" /* ScanProvider */],
            __WEBPACK_IMPORTED_MODULE_10__providers_message_provider_message_provider__["a" /* MessageProvider */]])
    ], MainPage);
    return MainPage;
}(__WEBPACK_IMPORTED_MODULE_1__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=main.js.map

/***/ }),
/* 212 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldMakerAdapterModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SubGetListMoldModel__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__SubMoldMakerFormBKFModel__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__SubMoldMakerHistoryModel__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__SubMoldBKFVIEWHistoryModel__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__SubMoldMakerHistoryDetailModel__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__SubPhotoModel__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MoldMakerAdapterModel = /** @class */ (function () {
    function MoldMakerAdapterModel() {
        this.Status = undefined;
        this.Message = undefined;
        this.getListMold = undefined;
        this.history = undefined;
        this.historyDetail = undefined;
        this.historyImage = undefined;
        this.bkfview = undefined;
        this.bkfviewHistory = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySaveMoldModel.STATUS, Number, true),
        __metadata("design:type", String)
    ], MoldMakerAdapterModel.prototype, "Status", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySaveMoldModel.MESSAGE, String, true),
        __metadata("design:type", String)
    ], MoldMakerAdapterModel.prototype, "Message", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].MoldMakerAdapterModel.LIST_MOLD, [__WEBPACK_IMPORTED_MODULE_2__SubGetListMoldModel__["a" /* SubGetListMoldModel */]], true),
        __metadata("design:type", Array)
    ], MoldMakerAdapterModel.prototype, "getListMold", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].MoldMakerAdapterModel.HISTORY, [__WEBPACK_IMPORTED_MODULE_4__SubMoldMakerHistoryModel__["a" /* SubMoldMakerHistoryModel */]], true),
        __metadata("design:type", Array)
    ], MoldMakerAdapterModel.prototype, "history", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].MoldMakerAdapterModel.HISTORY_DETAIL, [__WEBPACK_IMPORTED_MODULE_6__SubMoldMakerHistoryDetailModel__["a" /* SubMoldMakerHistoryDetailModel */]], true),
        __metadata("design:type", Array)
    ], MoldMakerAdapterModel.prototype, "historyDetail", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].MoldMakerAdapterModel.HISTORY_IMAGE, [__WEBPACK_IMPORTED_MODULE_7__SubPhotoModel__["a" /* SubPhotoModel */]], true),
        __metadata("design:type", Array)
    ], MoldMakerAdapterModel.prototype, "historyImage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].MoldMakerAdapterModel.HISTORY_BKF, [__WEBPACK_IMPORTED_MODULE_3__SubMoldMakerFormBKFModel__["a" /* SubMoldMakerFormBKFModel */]], true),
        __metadata("design:type", Array)
    ], MoldMakerAdapterModel.prototype, "bkfview", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].MoldMakerAdapterModel.VIEW_BY_BKF, [__WEBPACK_IMPORTED_MODULE_5__SubMoldBKFVIEWHistoryModel__["a" /* SubMoldBKFVIEWHistoryModel */]], true),
        __metadata("design:type", Array)
    ], MoldMakerAdapterModel.prototype, "bkfviewHistory", void 0);
    MoldMakerAdapterModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("MoldMakerAdapterModel")
    ], MoldMakerAdapterModel);
    return MoldMakerAdapterModel;
}());

//# sourceMappingURL=MoldMakerAdapterModel.js.map

/***/ }),
/* 213 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldmakerAdapterProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__connection_util_connection_util__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_utilities__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



// import { Utilities } from '../../../shared/utilities';


/*
  Generated class for the MoldmakerAdapterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var MoldmakerAdapterProvider = /** @class */ (function () {
    function MoldmakerAdapterProvider(_connectionUtil, _storageProvider) {
        this._connectionUtil = _connectionUtil;
        this._storageProvider = _storageProvider;
        console.log('Hello MoldmakerAdapterProvider Provider');
    }
    MoldmakerAdapterProvider.prototype.getListMold = function () {
        return __awaiter(this, void 0, void 0, function () {
            var param, _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        param = null;
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {};
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].USER_ID)];
                    case 1:
                        param = [(_a[_b] = (_c[_d] = (_e.makerid = _f.sent(),
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDMAKER_GETLISTMOLD_ADAPTER, param)];
                }
            });
        });
    };
    MoldmakerAdapterProvider.prototype.getViewMoldFromBKF = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                jobid: inputObj.JobId
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDMAKER_HISTORY_ADAPTER, param)];
            });
        });
    };
    MoldmakerAdapterProvider.prototype.getReceiveMold = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                jobid: inputObj.JobId
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDMAKER_RECEIVE_ADAPTER, param)];
            });
        });
    };
    MoldmakerAdapterProvider.prototype.getMoldMakerTransaction = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param, _a, _b, _c, _d, _e, _f, _g;
            return __generator(this, function (_h) {
                switch (_h.label) {
                    case 0:
                        param = null;
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {};
                        _f = 'imei';
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].IMEI()];
                    case 1:
                        _e[_f] = _h.sent(),
                            _e['jobid'] = inputObj.JobId;
                        _g = 'makerid';
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].USER_ID)];
                    case 2:
                        param = [(_a[_b] = (_c[_d] = (_e[_g] = _h.sent(),
                                _e['remark'] = inputObj.Remark,
                                _e['status'] = inputObj.Status,
                                _e['photo'] = inputObj.Images,
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDMAKER_SAVE_TRANSACTION_ADAPTER, param)];
                }
            });
        });
    };
    MoldmakerAdapterProvider.prototype.getMoldMakerHistoryDetail = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                'jobid': inputObj.JobId,
                                'runno': inputObj.RunNo,
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDMAKER_HISTORY_DETAIL_ADAPTER, param)];
            });
        });
    };
    MoldmakerAdapterProvider.prototype.getBKFViewHistory = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var param;
            return __generator(this, function (_a) {
                param = null;
                param = [{
                        "parameter": {
                            "inboxParam": {
                                jobid: inputObj.JobId
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].MOLDMAKER_BKFVIEW_ADAPTER, param)];
            });
        });
    };
    MoldmakerAdapterProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__connection_util_connection_util__["a" /* ConnectionUtilProvider */],
            __WEBPACK_IMPORTED_MODULE_3__storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], MoldmakerAdapterProvider);
    return MoldmakerAdapterProvider;
}());

//# sourceMappingURL=moldmaker-adapter.js.map

/***/ }),
/* 214 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveySaveMoldModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SurveySaveMoldModel = /** @class */ (function () {
    function SurveySaveMoldModel() {
        this.Status = undefined;
        this.Message = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySaveMoldModel.STATUS, Number, true),
        __metadata("design:type", String)
    ], SurveySaveMoldModel.prototype, "Status", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySaveMoldModel.MESSAGE, String, true),
        __metadata("design:type", String)
    ], SurveySaveMoldModel.prototype, "Message", void 0);
    SurveySaveMoldModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SurveySaveMoldModel")
    ], SurveySaveMoldModel);
    return SurveySaveMoldModel;
}());

//# sourceMappingURL=SurveySaveMoldModel.js.map

/***/ }),
/* 215 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationSurveyAdapterModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LocationSurveyAdapterModel = /** @class */ (function () {
    function LocationSurveyAdapterModel() {
        this.LocationID = undefined;
        this.LocationDescription = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].LocationModel.LOCATION_ID, String, true),
        __metadata("design:type", String)
    ], LocationSurveyAdapterModel.prototype, "LocationID", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].LocationModel.LOCATION_DESCRIPTION, String, true),
        __metadata("design:type", String)
    ], LocationSurveyAdapterModel.prototype, "LocationDescription", void 0);
    LocationSurveyAdapterModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("LocationSurveyAdapterModel")
    ], LocationSurveyAdapterModel);
    return LocationSurveyAdapterModel;
}());

//# sourceMappingURL=LocationSurveyAdapterModel.js.map

/***/ }),
/* 216 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistorySurveyAdapterModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SubMoldHistoryModel__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__SubMoldMasterModel__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__SubMoldSearchModel__ = __webpack_require__(390);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HistorySurveyAdapterModel = /** @class */ (function () {
    function HistorySurveyAdapterModel() {
        this.master = undefined;
        this.history = undefined;
        this.search = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].HistorySurveyModel.MOLDMASTER, [__WEBPACK_IMPORTED_MODULE_3__SubMoldMasterModel__["a" /* SubMoldMasterModel */]], true),
        __metadata("design:type", Array)
    ], HistorySurveyAdapterModel.prototype, "master", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].HistorySurveyModel.MOLDHISTORY, [__WEBPACK_IMPORTED_MODULE_2__SubMoldHistoryModel__["a" /* SubMoldHistoryModel */]], true),
        __metadata("design:type", Array)
    ], HistorySurveyAdapterModel.prototype, "history", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].HistorySurveyModel.MOLDSEARCH, [__WEBPACK_IMPORTED_MODULE_4__SubMoldSearchModel__["a" /* SubMoldSearchModel */]], true),
        __metadata("design:type", Array)
    ], HistorySurveyAdapterModel.prototype, "search", void 0);
    HistorySurveyAdapterModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("HistorySurveyAdapterModel")
    ], HistorySurveyAdapterModel);
    return HistorySurveyAdapterModel;
}());

//# sourceMappingURL=HistorySurveyAdapterModel.js.map

/***/ }),
/* 217 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryDetailSurveyAdapderMoldel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SubMoldMasterImageModel__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__SubMoldHistoryDetailModel__ = __webpack_require__(392);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HistoryDetailSurveyAdapderMoldel = /** @class */ (function () {
    function HistoryDetailSurveyAdapderMoldel() {
        this.historyDetail = undefined;
        this.images = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].HistorySurveyDetailModel.HISTORY, [__WEBPACK_IMPORTED_MODULE_3__SubMoldHistoryDetailModel__["a" /* SubMoldHistoryDetailModel */]], true),
        __metadata("design:type", Array)
    ], HistoryDetailSurveyAdapderMoldel.prototype, "historyDetail", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].HistorySurveyDetailModel.IMAGES, [__WEBPACK_IMPORTED_MODULE_2__SubMoldMasterImageModel__["a" /* SubMoldMasterImageModel */]], true),
        __metadata("design:type", Array)
    ], HistoryDetailSurveyAdapderMoldel.prototype, "images", void 0);
    HistoryDetailSurveyAdapderMoldel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("HistoryDetailSurveyAdapderMoldel")
    ], HistoryDetailSurveyAdapderMoldel);
    return HistoryDetailSurveyAdapderMoldel;
}());

//# sourceMappingURL=HistoryDetailSurveyAdapderMoldel.js.map

/***/ }),
/* 218 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyAdapterProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__connection_util_connection_util__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__storage_provider_storage_provider__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var SurveyAdapterProvider = /** @class */ (function () {
    function SurveyAdapterProvider(_connectionUtil, _storageProvider) {
        this._connectionUtil = _connectionUtil;
        this._storageProvider = _storageProvider;
        console.log('Hello SurveyAdapterProvider Provider');
    }
    SurveyAdapterProvider.prototype.getLocationSurvey = function () {
        return __awaiter(this, void 0, void 0, function () {
            var params;
            return __generator(this, function (_a) {
                params = [{
                        "parameter": {
                            "inboxParam": {}
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SURVEY_LOCATION_ADAPTER, params)];
            });
        });
    };
    SurveyAdapterProvider.prototype.getHistorySurvey = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var params;
            return __generator(this, function (_a) {
                params = [{
                        "parameter": {
                            "inboxParam": {
                                "MoldID": inputObj.MoldId
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SURVEY_HISTORY_ADAPTER, params)];
            });
        });
    };
    SurveyAdapterProvider.prototype.getHistoryDetail = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var params;
            return __generator(this, function (_a) {
                params = [{
                        "parameter": {
                            "inboxParam": {
                                "MoldID": inputObj.MoldId,
                                "RunNo": inputObj.RunNo
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SURVEY_HISOTRY_DETAIL_ADAPTER, params)];
            });
        });
    };
    SurveyAdapterProvider.prototype.getSearchSurvey = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var params;
            return __generator(this, function (_a) {
                params = [{
                        "parameter": {
                            "inboxParam": {
                                "startDate": inputObj.From,
                                "endDate": inputObj.To,
                                "MoldId": inputObj.MoldId
                            }
                        }
                    }];
                return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SURVEY_SARCH_ADAPTER, params)];
            });
        });
    };
    SurveyAdapterProvider.prototype.saveDataMold = function (inputObj) {
        return __awaiter(this, void 0, void 0, function () {
            var params, _a, _b, _c, _d, _e, _f, _g;
            return __generator(this, function (_h) {
                switch (_h.label) {
                    case 0:
                        _a = {};
                        _b = "parameter";
                        _c = {};
                        _d = "inboxParam";
                        _e = {
                            "moldid": inputObj.MoldId,
                            "assetid": inputObj.AssetId,
                            "custno": inputObj.CustNo,
                            "partno": inputObj.PartNo,
                            "partname": inputObj.PartName,
                            "plate": inputObj.Plate,
                            "location": inputObj.Location,
                            "remark": inputObj.Remark
                        };
                        _f = "empid";
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].USER_ID)];
                    case 1:
                        _e[_f] = _h.sent();
                        _g = "imei";
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].IMEI()];
                    case 2:
                        params = [(_a[_b] = (_c[_d] = (_e[_g] = _h.sent(),
                                _e["photo"] = inputObj.Images,
                                _e),
                                _c),
                                _a)];
                        return [2 /*return*/, this._connectionUtil.invokeAdapter(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SURVEY_SAVE_ADAPTER, params)];
                }
            });
        });
    };
    SurveyAdapterProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__connection_util_connection_util__["a" /* ConnectionUtilProvider */], __WEBPACK_IMPORTED_MODULE_4__storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], SurveyAdapterProvider);
    return SurveyAdapterProvider;
}());

//# sourceMappingURL=survey-adapter.js.map

/***/ }),
/* 219 */,
/* 220 */,
/* 221 */,
/* 222 */,
/* 223 */,
/* 224 */,
/* 225 */,
/* 226 */,
/* 227 */,
/* 228 */,
/* 229 */,
/* 230 */,
/* 231 */,
/* 232 */,
/* 233 */,
/* 234 */,
/* 235 */,
/* 236 */,
/* 237 */,
/* 238 */,
/* 239 */,
/* 240 */,
/* 241 */,
/* 242 */,
/* 243 */,
/* 244 */,
/* 245 */,
/* 246 */,
/* 247 */,
/* 248 */,
/* 249 */,
/* 250 */,
/* 251 */,
/* 252 */,
/* 253 */,
/* 254 */,
/* 255 */,
/* 256 */,
/* 257 */,
/* 258 */,
/* 259 */,
/* 260 */,
/* 261 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DirectivesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__autosize_autosize__ = __webpack_require__(387);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var DirectivesModule = /** @class */ (function () {
    function DirectivesModule() {
    }
    DirectivesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__autosize_autosize__["a" /* Autosize */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__autosize_autosize__["a" /* Autosize */]]
        })
    ], DirectivesModule);
    return DirectivesModule;
}());

//# sourceMappingURL=directives.module.js.map

/***/ }),
/* 262 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldmakerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__base_provider_base_provider__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__adapter_moldmaker_adapter_moldmaker_adapter__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_MoldMakerModel__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_package_MoldMakerAdapterModel__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models_package_OperationBaseModel__ = __webpack_require__(20);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/*
  Generated class for the MoldmakerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var MoldmakerProvider = /** @class */ (function (_super) {
    __extends(MoldmakerProvider, _super);
    function MoldmakerProvider(alertCtrl, loadingCtrl, _moldmakerAdapterProvider) {
        var _this = _super.call(this, alertCtrl, loadingCtrl) || this;
        _this.alertCtrl = alertCtrl;
        _this.loadingCtrl = loadingCtrl;
        _this._moldmakerAdapterProvider = _moldmakerAdapterProvider;
        _this.moldmakerModel = new __WEBPACK_IMPORTED_MODULE_4__models_MoldMakerModel__["a" /* MoldMakerModel */]();
        _this.moldmakerModel2 = new __WEBPACK_IMPORTED_MODULE_4__models_MoldMakerModel__["a" /* MoldMakerModel */]();
        console.log('Hello MoldmakerProvider Provider');
        return _this;
    }
    MoldmakerProvider.prototype.getListMold = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldmakerAdapterProvider.getListMold()];
                    case 1:
                        response = _a.sent();
                        console.log("getListMold ::: ", response);
                        this.moldmakerModel.getListMode = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_6__models_package_MoldMakerAdapterModel__["a" /* MoldMakerAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        this.moldmakerModel2 = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_1, __WEBPACK_IMPORTED_MODULE_7__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_1;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerProvider.prototype.getViewMoldFromBKF = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldmakerAdapterProvider.getViewMoldFromBKF(this.moldmakerModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getViewMoldFromBKF ::: ", response);
                        this.moldmakerModel.getMoldBKF = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_6__models_package_MoldMakerAdapterModel__["a" /* MoldMakerAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_2 = _a.sent();
                        this.moldmakerModel2 = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_2, __WEBPACK_IMPORTED_MODULE_7__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_2;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerProvider.prototype.getReceiveMold = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldmakerAdapterProvider.getReceiveMold(this.moldmakerModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getReceiveMold ::: ", response);
                        this.moldmakerModel.getMoldBKF = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_6__models_package_MoldMakerAdapterModel__["a" /* MoldMakerAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_3 = _a.sent();
                        this.moldmakerModel2 = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_3, __WEBPACK_IMPORTED_MODULE_7__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerProvider.prototype.getMoldMakerTransaction = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldmakerAdapterProvider.getMoldMakerTransaction(this.moldmakerModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getMoldMakerTransaction ::: ", response);
                        this.moldmakerModel.saveData = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_6__models_package_MoldMakerAdapterModel__["a" /* MoldMakerAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_4 = _a.sent();
                        this.moldmakerModel2 = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_4, __WEBPACK_IMPORTED_MODULE_7__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_4;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerProvider.prototype.getMoldMakerHistoryDetail = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldmakerAdapterProvider.getMoldMakerHistoryDetail(this.moldmakerModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getMoldMakerHistoryDetail ::: ", response);
                        this.moldmakerModel.getHistoryDetail = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_6__models_package_MoldMakerAdapterModel__["a" /* MoldMakerAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_5 = _a.sent();
                        this.moldmakerModel2 = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_5, __WEBPACK_IMPORTED_MODULE_7__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_5;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerProvider.prototype.getBKFViewHistory = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldmakerAdapterProvider.getBKFViewHistory(this.moldmakerModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getBKFViewHistory ::: ", response);
                        this.moldmakerModel.bkfview = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_6__models_package_MoldMakerAdapterModel__["a" /* MoldMakerAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_6 = _a.sent();
                        this.moldmakerModel2 = __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_6, __WEBPACK_IMPORTED_MODULE_7__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_6;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldmakerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__adapter_moldmaker_adapter_moldmaker_adapter__["a" /* MoldmakerAdapterProvider */]])
    ], MoldmakerProvider);
    return MoldmakerProvider;
}(__WEBPACK_IMPORTED_MODULE_0__base_provider_base_provider__["a" /* BaseProvider */]));

//# sourceMappingURL=moldmaker-provider.js.map

/***/ }),
/* 263 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_package_AdminAdapterModel__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__adapter_admin_adapter_admin_adapter__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_AdminModel__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_provider_base_provider__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_utilities__ = __webpack_require__(16);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/*
  Generated class for the AdminProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AdminProvider = /** @class */ (function (_super) {
    __extends(AdminProvider, _super);
    function AdminProvider(alertCtrl, loadingCtrl, _adminAdapterProvider) {
        var _this = _super.call(this, alertCtrl, loadingCtrl) || this;
        _this.alertCtrl = alertCtrl;
        _this.loadingCtrl = loadingCtrl;
        _this._adminAdapterProvider = _adminAdapterProvider;
        _this.adminModel = new __WEBPACK_IMPORTED_MODULE_2__models_AdminModel__["a" /* AdminModel */]();
        _this.adminModel2 = new __WEBPACK_IMPORTED_MODULE_2__models_AdminModel__["a" /* AdminModel */]();
        console.log('Hello AdminProvider Provider');
        return _this;
    }
    AdminProvider.prototype.getListUserBKF = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._adminAdapterProvider.getListUserBKF()];
                    case 1:
                        response = _a.sent();
                        console.log("getListUserBKF ::: ", response);
                        this.adminModel.getListBKF = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_0__models_package_AdminAdapterModel__["a" /* AdminAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        this.adminModel2 = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_1, __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_1;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminProvider.prototype.getListUserMold = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._adminAdapterProvider.getListUserMold()];
                    case 1:
                        response = _a.sent();
                        console.log("getLisUserMold ::: ", response);
                        this.adminModel.getListMold = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_0__models_package_AdminAdapterModel__["a" /* AdminAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_2 = _a.sent();
                        this.adminModel2 = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_2, __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_2;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminProvider.prototype.getConfig = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._adminAdapterProvider.getConfig()];
                    case 1:
                        response = _a.sent();
                        console.log("getConfig ::: ", response);
                        this.adminModel.getConfig = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_0__models_package_AdminAdapterModel__["a" /* AdminAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_3 = _a.sent();
                        this.adminModel2 = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_3, __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminProvider.prototype.getUpdateUserBKF = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._adminAdapterProvider.getUpdateUserBKF(this.adminModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getUpdateUserBKF ::: ", response);
                        this.adminModel.statement = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_0__models_package_AdminAdapterModel__["a" /* AdminAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_4 = _a.sent();
                        this.adminModel2 = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_4, __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_4;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminProvider.prototype.getUpdateUserMaker = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._adminAdapterProvider.getUpdateUserMaker(this.adminModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getUpdateUserMaker ::: ", response);
                        this.adminModel.statement = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_0__models_package_AdminAdapterModel__["a" /* AdminAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_5 = _a.sent();
                        this.adminModel2 = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_5, __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_5;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminProvider.prototype.getUpdateConfig = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._adminAdapterProvider.getUpdateConfig(this.adminModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getUpdateConfig ::: ", response);
                        this.adminModel.statement = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_0__models_package_AdminAdapterModel__["a" /* AdminAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_6 = _a.sent();
                        this.adminModel2 = __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_6, __WEBPACK_IMPORTED_MODULE_6__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_6;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1__adapter_admin_adapter_admin_adapter__["a" /* AdminAdapterProvider */]])
    ], AdminProvider);
    return AdminProvider;
}(__WEBPACK_IMPORTED_MODULE_3__base_provider_base_provider__["a" /* BaseProvider */]));

//# sourceMappingURL=admin-provider.js.map

/***/ }),
/* 264 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldInOutModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__package_MoldInOutAdapterModel__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__package_OperationBaseModel__ = __webpack_require__(20);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MoldInOutModel = /** @class */ (function (_super) {
    __extends(MoldInOutModel, _super);
    function MoldInOutModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.getNewMoldInOut = new __WEBPACK_IMPORTED_MODULE_0__package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]();
        _this.getSetting = new __WEBPACK_IMPORTED_MODULE_0__package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]();
        _this.getWaiting = new __WEBPACK_IMPORTED_MODULE_0__package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]();
        _this.getSearch = new __WEBPACK_IMPORTED_MODULE_0__package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]();
        _this.getStatus = new __WEBPACK_IMPORTED_MODULE_0__package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]();
        _this.getApprove = new __WEBPACK_IMPORTED_MODULE_0__package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]();
        _this.statment = new __WEBPACK_IMPORTED_MODULE_0__package_MoldInOutAdapterModel__["a" /* MoldInOutAdapterModel */]();
        _this.Images = [];
        return _this;
    }
    MoldInOutModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonObject"])("MoldInOutModel")
    ], MoldInOutModel);
    return MoldInOutModel;
}(__WEBPACK_IMPORTED_MODULE_2__package_OperationBaseModel__["a" /* OperationBaseModel */]));

//# sourceMappingURL=MoldInOutModel.js.map

/***/ }),
/* 265 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__base_provider_base_provider__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_package_OperationBaseModel__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_SurveyModel__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__adapter_survey_adapter_survey_adapter__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models_package_HistorySurveyAdapterModel__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__models_package_HistoryDetailSurveyAdapderMoldel__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__models_package_LocationSurveyAdapterModel__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__models_package_SurveySaveMoldModel__ = __webpack_require__(214);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











var SurveyProvider = /** @class */ (function (_super) {
    __extends(SurveyProvider, _super);
    function SurveyProvider(alertCtrl, loadingCtrl, _survey) {
        var _this = _super.call(this, alertCtrl, loadingCtrl) || this;
        _this.alertCtrl = alertCtrl;
        _this.loadingCtrl = loadingCtrl;
        _this._survey = _survey;
        _this.surVeyModel = new __WEBPACK_IMPORTED_MODULE_5__models_SurveyModel__["a" /* SurveyModel */]();
        _this.surVeyModel2 = new __WEBPACK_IMPORTED_MODULE_5__models_SurveyModel__["a" /* SurveyModel */]();
        console.log('Hello SurveyProvider Provider');
        return _this;
    }
    SurveyProvider.prototype.getLocationSurvey = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._survey.getLocationSurvey()];
                    case 1:
                        response = _a.sent();
                        console.log("getLocationSurvey ::: ", response);
                        this.surVeyModel.locationDescription = __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData.MoldLocation, __WEBPACK_IMPORTED_MODULE_9__models_package_LocationSurveyAdapterModel__["a" /* LocationSurveyAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        this.surVeyModel2 = __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_1, __WEBPACK_IMPORTED_MODULE_4__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_1;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SurveyProvider.prototype.getMoldHistory = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._survey.getHistorySurvey(this.surVeyModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getHistorySurvey ::: ", response);
                        this.surVeyModel.surveyMoldHistory = __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_HistorySurveyAdapterModel__["a" /* HistorySurveyAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_2 = _a.sent();
                        this.surVeyModel2 = __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_2, __WEBPACK_IMPORTED_MODULE_4__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_2;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SurveyProvider.prototype.getSearchSurvey = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._survey.getSearchSurvey(this.surVeyModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getSearchSurvey ::: ", response);
                        this.surVeyModel.surveyMoldHistory = __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_7__models_package_HistorySurveyAdapterModel__["a" /* HistorySurveyAdapterModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_3 = _a.sent();
                        this.surVeyModel2 = __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_3, __WEBPACK_IMPORTED_MODULE_4__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SurveyProvider.prototype.getMoldHistoryDetail = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._survey.getHistoryDetail(this.surVeyModel)];
                    case 1:
                        response = _a.sent();
                        console.log("getHistoryDetailSurvey ::: ", response);
                        this.surVeyModel.surveyMoldHistoryDetail = __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_8__models_package_HistoryDetailSurveyAdapderMoldel__["a" /* HistoryDetailSurveyAdapderMoldel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_4 = _a.sent();
                        this.surVeyModel2 = __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_4, __WEBPACK_IMPORTED_MODULE_4__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_4;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SurveyProvider.prototype.saveDataMold = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, err_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._survey.saveDataMold(this.surVeyModel)];
                    case 1:
                        response = _a.sent();
                        console.log("saveDataMold ::: ", response);
                        this.surVeyModel.surveyMoldSave = __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].mapJsontoModel(response.ResponseData, __WEBPACK_IMPORTED_MODULE_10__models_package_SurveySaveMoldModel__["a" /* SurveySaveMoldModel */]);
                        return [3 /*break*/, 3];
                    case 2:
                        err_5 = _a.sent();
                        this.surVeyModel2 = __WEBPACK_IMPORTED_MODULE_3__shared_utilities__["a" /* Utilities */].mapJsontoModel(err_5, __WEBPACK_IMPORTED_MODULE_4__models_package_OperationBaseModel__["a" /* OperationBaseModel */]);
                        throw err_5;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SurveyProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__adapter_survey_adapter_survey_adapter__["a" /* SurveyAdapterProvider */]])
    ], SurveyProvider);
    return SurveyProvider;
}(__WEBPACK_IMPORTED_MODULE_1__base_provider_base_provider__["a" /* BaseProvider */]));

//# sourceMappingURL=survey-provider.js.map

/***/ }),
/* 266 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldMakerModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__package_MoldMakerAdapterModel__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__package_OperationBaseModel__ = __webpack_require__(20);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MoldMakerModel = /** @class */ (function (_super) {
    __extends(MoldMakerModel, _super);
    function MoldMakerModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.getListMode = new __WEBPACK_IMPORTED_MODULE_0__package_MoldMakerAdapterModel__["a" /* MoldMakerAdapterModel */]();
        _this.getMoldBKF = new __WEBPACK_IMPORTED_MODULE_0__package_MoldMakerAdapterModel__["a" /* MoldMakerAdapterModel */]();
        _this.getHistoryDetail = new __WEBPACK_IMPORTED_MODULE_0__package_MoldMakerAdapterModel__["a" /* MoldMakerAdapterModel */]();
        _this.bkfview = new __WEBPACK_IMPORTED_MODULE_0__package_MoldMakerAdapterModel__["a" /* MoldMakerAdapterModel */]();
        _this.saveData = new __WEBPACK_IMPORTED_MODULE_0__package_MoldMakerAdapterModel__["a" /* MoldMakerAdapterModel */]();
        _this.Images = [];
        return _this;
    }
    MoldMakerModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonObject"])("MoldMakerModel")
    ], MoldMakerModel);
    return MoldMakerModel;
}(__WEBPACK_IMPORTED_MODULE_2__package_OperationBaseModel__["a" /* OperationBaseModel */]));

//# sourceMappingURL=MoldMakerModel.js.map

/***/ }),
/* 267 */,
/* 268 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__package_AdminAdapterModel__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__package_OperationBaseModel__ = __webpack_require__(20);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AdminModel = /** @class */ (function (_super) {
    __extends(AdminModel, _super);
    function AdminModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.getListBKF = new __WEBPACK_IMPORTED_MODULE_0__package_AdminAdapterModel__["a" /* AdminAdapterModel */]();
        _this.getListMold = new __WEBPACK_IMPORTED_MODULE_0__package_AdminAdapterModel__["a" /* AdminAdapterModel */]();
        _this.getConfig = new __WEBPACK_IMPORTED_MODULE_0__package_AdminAdapterModel__["a" /* AdminAdapterModel */]();
        _this.statement = new __WEBPACK_IMPORTED_MODULE_0__package_AdminAdapterModel__["a" /* AdminAdapterModel */]();
        return _this;
    }
    AdminModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonObject"])("AdminModel")
    ], AdminModel);
    return AdminModel;
}(__WEBPACK_IMPORTED_MODULE_2__package_OperationBaseModel__["a" /* OperationBaseModel */]));

//# sourceMappingURL=AdminModel.js.map

/***/ }),
/* 269 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__package_SurveySaveMoldModel__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__package_OperationBaseModel__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__package_LocationSurveyAdapterModel__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__package_HistorySurveyAdapterModel__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__package_HistoryDetailSurveyAdapderMoldel__ = __webpack_require__(217);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// import { SearchSurveyAdapterModel } from "./package/SearchSurveyAdapterModel";
var SurveyModel = /** @class */ (function (_super) {
    __extends(SurveyModel, _super);
    function SurveyModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // from api
        _this.locationDescription = new __WEBPACK_IMPORTED_MODULE_3__package_LocationSurveyAdapterModel__["a" /* LocationSurveyAdapterModel */]();
        _this.surveyMoldHistory = new __WEBPACK_IMPORTED_MODULE_4__package_HistorySurveyAdapterModel__["a" /* HistorySurveyAdapterModel */]();
        _this.surveyMoldHistoryDetail = new __WEBPACK_IMPORTED_MODULE_5__package_HistoryDetailSurveyAdapderMoldel__["a" /* HistoryDetailSurveyAdapderMoldel */]();
        _this.surveyMoldSave = new __WEBPACK_IMPORTED_MODULE_0__package_SurveySaveMoldModel__["a" /* SurveySaveMoldModel */]();
        return _this;
    }
    SurveyModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonObject"])("SurveyModel")
    ], SurveyModel);
    return SurveyModel;
}(__WEBPACK_IMPORTED_MODULE_2__package_OperationBaseModel__["a" /* OperationBaseModel */]));

//# sourceMappingURL=SurveyModel.js.map

/***/ }),
/* 270 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldinoutMenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_login_provider_login_provider__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_moldinout_provider_moldinout_provider__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_scan_scan__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_message_provider_message_provider__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__shared_utilities__ = __webpack_require__(16);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












/**
 * Generated class for the MoldinoutMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MoldinoutMenuPage = /** @class */ (function (_super) {
    __extends(MoldinoutMenuPage, _super);
    function MoldinoutMenuPage(navCtrl, events, alertCtrl, _loginProvider, _moldinoutProvider, _scanProvider, _messageProvider, _storageProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this.alertCtrl = alertCtrl;
        _this._loginProvider = _loginProvider;
        _this._moldinoutProvider = _moldinoutProvider;
        _this._scanProvider = _scanProvider;
        _this._messageProvider = _messageProvider;
        _this._storageProvider = _storageProvider;
        _this.countMessage = 0;
        _this.countApprove = 0;
        _this.fullName = '';
        _this.platformVersion = '';
        return _this;
    }
    MoldinoutMenuPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MoldinoutMenuPage');
    };
    MoldinoutMenuPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.messageModel = this._messageProvider.messageModel;
                this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
                return [2 /*return*/];
            });
        });
    };
    MoldinoutMenuPage.prototype.ionViewDidEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, ex_1;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 4, , 5]);
                        _a = this;
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].USER_NAME)];
                    case 1:
                        _a.fullName = _c.sent();
                        _b = this;
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_11__shared_utilities__["a" /* Utilities */].getAppVersion()];
                    case 2:
                        _b.platformVersion = _c.sent();
                        this.messageModel = this._messageProvider.messageModel;
                        return [4 /*yield*/, Promise.all([this._messageProvider.getMessageDetail(), this._moldinoutProvider.getWaitingApprove()])];
                    case 3:
                        _c.sent();
                        this.countMessage = this.messageModel.getMessageDetail.detail.length;
                        this.countApprove = this.moldinoutModel.getWaiting.getWaiting.length;
                        return [3 /*break*/, 5];
                    case 4:
                        ex_1 = _c.sent();
                        console.log(ex_1);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutMenuPage.prototype.onNotification = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.navCtrl.push('NotificationPage');
                return [2 /*return*/];
            });
        });
    };
    MoldinoutMenuPage.prototype.onLogout = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this._loginProvider.authenticateLogout();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutMenuPage.prototype.scanQr = function () {
        return __awaiter(this, void 0, void 0, function () {
            var txtScanQR;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
                        return [4 /*yield*/, this._scanProvider.scan().then(function (val) {
                                txtScanQR = val.toString();
                            }).catch(function (error) {
                                console.log(error);
                            })];
                    case 1:
                        _a.sent();
                        if (txtScanQR) {
                            this.moldinoutModel.JobId = '';
                            this.moldinoutModel.MoldId = txtScanQR.split(/\|/)[0] || '';
                            this.moldinoutModel.AssetId = txtScanQR.split(/\|/)[1] || '';
                            this.moldinoutModel.CustNo = txtScanQR.split(/\|/)[2] || '';
                            this.moldinoutModel.PartNo = txtScanQR.split(/\|/)[3] || '';
                            this.moldinoutModel.PartName = txtScanQR.split(/\|/)[4] || '';
                            this.LoadDataMoldInOut();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutMenuPage.prototype.LoadDataMoldInOut = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var alert_1, ex_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldinoutProvider.getNewMoldOut()];
                    case 1:
                        _a.sent();
                        if (this.moldinoutModel.getNewMoldInOut.inoutMold.length > 0) {
                            if (this.moldinoutModel.getNewMoldInOut.inoutMold[0].jobId !== '') {
                                this.alertCtrl.create({
                                    title: 'Alert',
                                    subTitle: 'This mold has Transaction',
                                    buttons: [{
                                            text: 'Cancel',
                                            handler: function () {
                                                console.log('Cancel Click');
                                            }
                                        }, {
                                            text: 'Continue',
                                            handler: function () {
                                                _this.moldinoutModel.MoldId = _this.moldinoutModel.getNewMoldInOut.inoutMold[0].moldId;
                                                _this.moldinoutModel.JobId = _this.moldinoutModel.getNewMoldInOut.inoutMold[0].jobId;
                                                _this.navCtrl.push('MoldinoutHistoryPage');
                                            }
                                        }]
                                }).present();
                            }
                            else {
                                this.navCtrl.push('MoldinoutCreatePage');
                            }
                        }
                        else {
                            alert_1 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__["%message.notfound%"] : __WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json__["%message.notfound%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_9__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_10__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_1.present();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        ex_2 = _a.sent();
                        this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutMenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-moldinout-menu',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-menu/moldinout-menu.html"*/'<!--\n  Generated template for the MoldinoutMenuPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>Mold In/Out Menu</ion-title>\n    <ion-buttons end  class="cart-btn">\n      <button ion-button (click)="onNotification()">\n        <ion-icon name="notifications"></ion-icon>\n        <ion-badge class="cart-badge" *ngIf="countMessage > 0" >{{ countMessage }}</ion-badge>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <h4 style="font-size: 1.2em; text-align: center;">Welcome, {{ fullName }}</h4>\n  <div style="position: relative;">\n    <div class="content-menu">\n      <h4></h4>\n    </div>\n  </div>\n  <div padding>\n    <button ion-button full (click)="scanQr()">Request Mold Out</button>\n    <button ion-button full [navPush]="\'MoldinoutWaitingForApprovePage\'" *ngIf="countApprove > 0">Waiting For Approve</button>\n    <button ion-button full [navPush]="\'MoldinoutSearchPage\'">Search</button>\n    <button ion-button full [navPush]="\'MoldinoutStatusPage\'">Status</button>\n    <button ion-button full (click)="onLogout()">Logout</button>\n  </div>\n  <div class="version">\n    <pre>Version {{ platformVersion }}</pre>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-menu/moldinout-menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_login_provider_login_provider__["a" /* LoginProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_moldinout_provider_moldinout_provider__["a" /* MoldinoutProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_scan_scan__["a" /* ScanProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_message_provider_message_provider__["a" /* MessageProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], MoldinoutMenuPage);
    return MoldinoutMenuPage;
}(__WEBPACK_IMPORTED_MODULE_0__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=moldinout-menu.js.map

/***/ }),
/* 271 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(289);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),
/* 272 */,
/* 273 */,
/* 274 */,
/* 275 */,
/* 276 */,
/* 277 */,
/* 278 */,
/* 279 */,
/* 280 */,
/* 281 */,
/* 282 */,
/* 283 */,
/* 284 */,
/* 285 */,
/* 286 */,
/* 287 */,
/* 288 */,
/* 289 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export HttpLoaderFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_android_permissions__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_barcode_scanner__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_image_picker__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_local_notifications__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_location_accuracy__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_network__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_photo_viewer__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_status_bar__ = __webpack_require__(260);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__app_component__ = __webpack_require__(412);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_home_home__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_active_mobile_active_mobile_module__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_bkf_main_bkf_main_module__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_login_login_module__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_main_main_module__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_drawing_image_drawing_image_module__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pipes_pipes_module__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_components_module__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__directives_directives_module__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ngx_translate_http_loader__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__angular_common_http__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_base_provider_base_provider__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__BKFConnection_connection_Connection__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_connection_util_connection_util__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__providers_adapter_survey_adapter_survey_adapter__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_survey_provider_survey_provider__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__providers_notification_provider_notification_provider__ = __webpack_require__(416);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__providers_adapter_authentication_adapter_authentication_adapter__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__providers_login_provider_login_provider__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__providers_scan_scan__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__providers_adapter_moldinout_adapter_moldinout_adapter__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__providers_moldinout_provider_moldinout_provider__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__providers_security_provider_security_provider__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__providers_storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__providers_adapter_moldmaker_adapter_moldmaker_adapter__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__providers_moldmaker_provider_moldmaker_provider__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__providers_adapter_message_adapter_message_adapter__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__providers_message_provider_message_provider__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__providers_adapter_admin_adapter_admin_adapter__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__providers_admin_provider_admin_provider__ = __webpack_require__(263);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















































function HttpLoaderFactory(http) {
    return new __WEBPACK_IMPORTED_MODULE_26__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_16__pages_home_home__["a" /* HomePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_27__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* MyApp */], {
                    backButtonIcon: 'ios-arrow-back',
                    menuType: 'push',
                    swipeBackEnabled: false,
                    iconMode: 'ios'
                }, {
                    links: [
                        { loadChildren: '../pages/active-mobile/active-mobile.module#ActiveMobilePageModule', name: 'ActiveMobilePage', segment: 'active-mobile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-config/admin-config.module#AdminConfigPageModule', name: 'AdminConfigPage', segment: 'admin-config', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-create-user/admin-create-user.module#AdminCreateUserPageModule', name: 'AdminCreateUserPage', segment: 'admin-create-user', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-edit-user/admin-edit-user.module#AdminEditUserPageModule', name: 'AdminEditUserPage', segment: 'admin-edit-user', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-menu/admin-menu.module#AdminMenuPageModule', name: 'AdminMenuPage', segment: 'admin-menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-user-bkf/admin-user-bkf.module#AdminUserBkfPageModule', name: 'AdminUserBkfPage', segment: 'admin-user-bkf', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-user-list/admin-user-list.module#AdminUserListPageModule', name: 'AdminUserListPage', segment: 'admin-user-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin-user-mold/admin-user-mold.module#AdminUserMoldPageModule', name: 'AdminUserMoldPage', segment: 'admin-user-mold', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/bkf-main/bkf-main.module#BkfMainPageModule', name: 'BkfMainPage', segment: 'bkf-main', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/drawing-image/drawing-image.module#DrawingImagePageModule', name: 'DrawingImagePage', segment: 'drawing-image', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/edit-images/edit-images.module#EditImagesPageModule', name: 'EditImagesPage', segment: 'edit-images', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/main.module#MainPageModule', name: 'MainPage', segment: 'main', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/maker-menu/maker-menu.module#MakerMenuPageModule', name: 'MakerMenuPage', segment: 'maker-menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldinout-approve/moldinout-approve.module#MoldinoutApprovePageModule', name: 'MoldinoutApprovePage', segment: 'moldinout-approve', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldinout-confirm-in/moldinout-confirm-in.module#MoldinoutConfirmInPageModule', name: 'MoldinoutConfirmInPage', segment: 'moldinout-confirm-in', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldinout-preview/moldinout-preview.module#MoldinoutPreviewPageModule', name: 'MoldinoutPreviewPage', segment: 'moldinout-preview', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldinout-reject-out/moldinout-reject-out.module#MoldinoutRejectOutPageModule', name: 'MoldinoutRejectOutPage', segment: 'moldinout-reject-out', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldinout-search/moldinout-search.module#MoldinoutSearchPageModule', name: 'MoldinoutSearchPage', segment: 'moldinout-search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldinout-status/moldinout-status.module#MoldinoutStatusPageModule', name: 'MoldinoutStatusPage', segment: 'moldinout-status', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldinout-view-moldmaker/moldinout-view-moldmaker.module#MoldinoutViewMoldmakerPageModule', name: 'MoldinoutViewMoldmakerPage', segment: 'moldinout-view-moldmaker', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldinout-menu/moldinout-menu.module#MoldinoutMenuPageModule', name: 'MoldinoutMenuPage', segment: 'moldinout-menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldinout-waiting-for-approve/moldinout-waiting-for-approve.module#MoldinoutWaitingForApprovePageModule', name: 'MoldinoutWaitingForApprovePage', segment: 'moldinout-waiting-for-approve', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldmaker-history-detail/moldmaker-history-detail.module#MoldmakerHistoryDetailPageModule', name: 'MoldmakerHistoryDetailPage', segment: 'moldmaker-history-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldmaker-history/moldmaker-history.module#MoldmakerHistoryPageModule', name: 'MoldmakerHistoryPage', segment: 'moldmaker-history', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldmaker-receive/moldmaker-receive.module#MoldmakerReceivePageModule', name: 'MoldmakerReceivePage', segment: 'moldmaker-receive', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notification/notification.module#NotificationPageModule', name: 'NotificationPage', segment: 'notification', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/survey-history-detail/survey-history-detail.module#SurveyHistoryDetailPageModule', name: 'SurveyHistoryDetailPage', segment: 'survey-history-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/survey-history/survey-history.module#SurveyHistoryPageModule', name: 'SurveyHistoryPage', segment: 'survey-history', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/survey-menu/survey-menu.module#SurveyMenuPageModule', name: 'SurveyMenuPage', segment: 'survey-menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/survey-preview/survey-preview.module#SurveyPreviewPageModule', name: 'SurveyPreviewPage', segment: 'survey-preview', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldinout-confirm-out/moldinout-confirm-out.module#MoldinoutConfirmOutPageModule', name: 'MoldinoutConfirmOutPage', segment: 'moldinout-confirm-out', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldinout-create/moldinout-create.module#MoldinoutCreatePageModule', name: 'MoldinoutCreatePage', segment: 'moldinout-create', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/survey-create/survey-create.module#SurveyCreatePageModule', name: 'SurveyCreatePage', segment: 'survey-create', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/moldinout-history/moldinout-history.module#MoldinoutHistoryPageModule', name: 'MoldinoutHistoryPage', segment: 'moldinout-history', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_25__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_25__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: HttpLoaderFactory,
                        deps: [__WEBPACK_IMPORTED_MODULE_27__angular_common_http__["a" /* HttpClient */]]
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_17__pages_active_mobile_active_mobile_module__["ActiveMobilePageModule"],
                __WEBPACK_IMPORTED_MODULE_18__pages_bkf_main_bkf_main_module__["BkfMainPageModule"],
                __WEBPACK_IMPORTED_MODULE_21__pages_drawing_image_drawing_image_module__["DrawingImagePageModule"],
                __WEBPACK_IMPORTED_MODULE_19__pages_login_login_module__["LoginPageModule"],
                __WEBPACK_IMPORTED_MODULE_20__pages_main_main_module__["MainPageModule"],
                __WEBPACK_IMPORTED_MODULE_23__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_24__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_22__pipes_pipes_module__["a" /* PipesModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_15__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_16__pages_home_home__["a" /* HomePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_android_permissions__["a" /* AndroidPermissions */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_image_picker__["a" /* ImagePicker */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_local_notifications__["a" /* LocalNotifications */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_photo_viewer__["a" /* PhotoViewer */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_28__providers_base_provider_base_provider__["a" /* BaseProvider */],
                __WEBPACK_IMPORTED_MODULE_29__BKFConnection_connection_Connection__["a" /* Connection */],
                __WEBPACK_IMPORTED_MODULE_30__providers_connection_util_connection_util__["a" /* ConnectionUtilProvider */],
                __WEBPACK_IMPORTED_MODULE_31__providers_adapter_survey_adapter_survey_adapter__["a" /* SurveyAdapterProvider */],
                __WEBPACK_IMPORTED_MODULE_32__providers_survey_provider_survey_provider__["a" /* SurveyProvider */],
                __WEBPACK_IMPORTED_MODULE_33__providers_notification_provider_notification_provider__["a" /* NotificationProvider */],
                __WEBPACK_IMPORTED_MODULE_34__providers_adapter_authentication_adapter_authentication_adapter__["a" /* AuthenticationAdapterProvider */],
                __WEBPACK_IMPORTED_MODULE_35__providers_login_provider_login_provider__["a" /* LoginProvider */],
                __WEBPACK_IMPORTED_MODULE_36__providers_scan_scan__["a" /* ScanProvider */],
                __WEBPACK_IMPORTED_MODULE_37__providers_adapter_moldinout_adapter_moldinout_adapter__["a" /* MoldInoutAdapterProvider */],
                __WEBPACK_IMPORTED_MODULE_38__providers_moldinout_provider_moldinout_provider__["a" /* MoldinoutProvider */],
                { provide: __WEBPACK_IMPORTED_MODULE_27__angular_common_http__["b" /* HttpClientModule */], useClass: __WEBPACK_IMPORTED_MODULE_39__providers_security_provider_security_provider__["a" /* SecurityProvider */], useValue: false },
                __WEBPACK_IMPORTED_MODULE_40__providers_storage_provider_storage_provider__["a" /* StorageProvider */],
                __WEBPACK_IMPORTED_MODULE_41__providers_adapter_moldmaker_adapter_moldmaker_adapter__["a" /* MoldmakerAdapterProvider */],
                __WEBPACK_IMPORTED_MODULE_42__providers_moldmaker_provider_moldmaker_provider__["a" /* MoldmakerProvider */],
                __WEBPACK_IMPORTED_MODULE_43__providers_adapter_message_adapter_message_adapter__["a" /* MessageAdapterProvider */],
                __WEBPACK_IMPORTED_MODULE_44__providers_message_provider_message_provider__["a" /* MessageProvider */],
                __WEBPACK_IMPORTED_MODULE_45__providers_adapter_admin_adapter_admin_adapter__["a" /* AdminAdapterProvider */],
                __WEBPACK_IMPORTED_MODULE_46__providers_admin_provider_admin_provider__["a" /* AdminProvider */],
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_1__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),
/* 290 */,
/* 291 */,
/* 292 */,
/* 293 */,
/* 294 */,
/* 295 */,
/* 296 */,
/* 297 */,
/* 298 */,
/* 299 */,
/* 300 */,
/* 301 */,
/* 302 */,
/* 303 */,
/* 304 */,
/* 305 */,
/* 306 */,
/* 307 */,
/* 308 */,
/* 309 */,
/* 310 */,
/* 311 */,
/* 312 */,
/* 313 */,
/* 314 */,
/* 315 */,
/* 316 */,
/* 317 */,
/* 318 */,
/* 319 */,
/* 320 */,
/* 321 */,
/* 322 */,
/* 323 */,
/* 324 */,
/* 325 */,
/* 326 */,
/* 327 */,
/* 328 */,
/* 329 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubLoginModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubLoginModel = /** @class */ (function () {
    function SubLoginModel() {
        this.fullName = undefined;
        this.userId = undefined;
        this.imageSurvey = undefined;
        this.imageInOut = undefined;
        this.userType = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.FULL_NAME, String, true),
        __metadata("design:type", String)
    ], SubLoginModel.prototype, "fullName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.USER_ID, String, true),
        __metadata("design:type", String)
    ], SubLoginModel.prototype, "userId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.IMAGES_SURVEY, String, true),
        __metadata("design:type", String)
    ], SubLoginModel.prototype, "imageSurvey", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.IMAGES_MOLDINOUT, String, true),
        __metadata("design:type", String)
    ], SubLoginModel.prototype, "imageInOut", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.USER_TYPE, String, true),
        __metadata("design:type", String)
    ], SubLoginModel.prototype, "userType", void 0);
    SubLoginModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubLoginModel")
    ], SubLoginModel);
    return SubLoginModel;
}());

//# sourceMappingURL=SubLoginModel.js.map

/***/ }),
/* 330 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubDeviceAllowMoldel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubDeviceAllowMoldel = /** @class */ (function () {
    function SubDeviceAllowMoldel() {
        this.IMEI = undefined;
        this.status = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.IMEI, String, true),
        __metadata("design:type", String)
    ], SubDeviceAllowMoldel.prototype, "IMEI", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.STATUS, String, true),
        __metadata("design:type", String)
    ], SubDeviceAllowMoldel.prototype, "status", void 0);
    SubDeviceAllowMoldel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubDeviceAllowMoldel")
    ], SubDeviceAllowMoldel);
    return SubDeviceAllowMoldel;
}());

//# sourceMappingURL=SubDeviceAllowMoldel.js.map

/***/ }),
/* 331 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingConfigModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SettingConfigModel = /** @class */ (function () {
    function SettingConfigModel() {
        this.imageSurvey = undefined;
        this.imageInOut = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.IMAGES_SURVEY, String, true),
        __metadata("design:type", String)
    ], SettingConfigModel.prototype, "imageSurvey", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].AuthenticateUserModel.IMAGES_MOLDINOUT, String, true),
        __metadata("design:type", String)
    ], SettingConfigModel.prototype, "imageInOut", void 0);
    SettingConfigModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SettingConfigModel")
    ], SettingConfigModel);
    return SettingConfigModel;
}());

//# sourceMappingURL=SettingConfigModel.js.map

/***/ }),
/* 332 */,
/* 333 */,
/* 334 */,
/* 335 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MockConnection; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__mock_mockdata__ = __webpack_require__(336);

var MockConnection = /** @class */ (function () {
    function MockConnection() {
    }
    MockConnection.prototype.getResourceRequest = function (endpoint, parameters) {
        console.log('%c ::[Mock Connection!!]:: ', 'background: #222; color: #fff; font-size:20px;');
        console.log("Mock Endpoint::: ", endpoint, parameters);
        var key, keyAdapterParam;
        for (key in parameters[0].parameter) {
            if (key.match(/Param/)) {
                keyAdapterParam = key;
            }
        }
        // prepare endpoint API match to tomcat mock server
        var NodeEndpoint; //, funcNames = endpoint.split("/");
        NodeEndpoint = endpoint;
        console.log("NodeEndpoint ::: ", NodeEndpoint);
        var param = parameters[0].parameter[keyAdapterParam];
        // prepare query string
        console.log("MockParameters ::: ", param);
        return new Promise(function (resolve, reject) {
            if (!endpoint.includes('_')) {
                if (__WEBPACK_IMPORTED_MODULE_0__mock_mockdata__["a" /* MOCK */][endpoint]) {
                    resolve(__WEBPACK_IMPORTED_MODULE_0__mock_mockdata__["a" /* MOCK */][endpoint]);
                }
                else {
                    reject(__WEBPACK_IMPORTED_MODULE_0__mock_mockdata__["a" /* MOCK */]["mockError"]);
                }
            }
            else {
                reject(__WEBPACK_IMPORTED_MODULE_0__mock_mockdata__["a" /* MOCK */]["mockError"]);
            }
        });
    };
    return MockConnection;
}());

//# sourceMappingURL=MockConnection.js.map

/***/ }),
/* 336 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MOCK; });
var MOCK = {
    "mockError": {
        status: -1,
        name: "",
        number: "Mockup error message",
        code: "mock001"
    },
};
//# sourceMappingURL=mockdata.js.map

/***/ }),
/* 337 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NodeConnection; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ConnectionUtil__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_retry__ = __webpack_require__(339);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_retry___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_retry__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_delay__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_delay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_delay__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_filter__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_filter__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};

// import { HttpParams } from '@angular/common/http';






var NodeConnection = /** @class */ (function (_super) {
    __extends(NodeConnection, _super);
    function NodeConnection() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NodeConnection.prototype.getResourceRequest = function (endpoint, parameters, userModel) {
        // console.log('Node Connection!!', userModel);
        // console.log(`Node Endpoint::: `, endpoint, parameters);
        var _this = this;
        var key, keyAdapterParam;
        for (key in parameters[0].parameter) {
            if (key.match(/Param/)) {
                keyAdapterParam = key;
            }
        }
        // prepare endpoint API match to tomcat mock server
        var NodeEndpoint; //, funcNames = endpoint.split("/");
        NodeEndpoint = endpoint;
        // console.log(`NodeEndpoint ::: `, CONSTANT.IPAddress + NodeEndpoint);
        var param = parameters[0].parameter[keyAdapterParam];
        // prepare query string
        // const nodeParameters = new HttpParams(param);
        // console.log(`NodeParameters ::: `, nodeParameters.toString());
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    this.http.post(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].IPAddress + NodeEndpoint, param).timeout(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].TIMEOUT).subscribe(function (response) {
                        resolve(response);
                    }, function (err) {
                        resolve(err);
                    });
                }
                catch (err) {
                    reject(err);
                }
                return [2 /*return*/];
            });
        }); });
    };
    return NodeConnection;
}(__WEBPACK_IMPORTED_MODULE_0__ConnectionUtil__["a" /* NodeConnectionUtils */]));

//# sourceMappingURL=NodeConnection.js.map

/***/ }),
/* 338 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NodeConnectionUtils; });
var NodeConnectionUtils = /** @class */ (function () {
    function NodeConnectionUtils(http) {
        this.http = http;
    }
    return NodeConnectionUtils;
}());

//# sourceMappingURL=ConnectionUtil.js.map

/***/ }),
/* 339 */,
/* 340 */,
/* 341 */,
/* 342 */,
/* 343 */,
/* 344 */,
/* 345 */,
/* 346 */,
/* 347 */,
/* 348 */,
/* 349 */,
/* 350 */,
/* 351 */,
/* 352 */,
/* 353 */,
/* 354 */,
/* 355 */,
/* 356 */,
/* 357 */,
/* 358 */,
/* 359 */,
/* 360 */,
/* 361 */,
/* 362 */,
/* 363 */,
/* 364 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubAdminGetListUserBKF; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubAdminGetListUserBKF = /** @class */ (function () {
    function SubAdminGetListUserBKF() {
        this.empId = undefined;
        this.firstName = undefined;
        this.lastName = undefined;
        this.userGroup = undefined;
        this.userType = undefined;
        this.username = undefined;
        this.password = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetListBKF.EMPID, String, true),
        __metadata("design:type", String)
    ], SubAdminGetListUserBKF.prototype, "empId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetListBKF.FIRSTNAME, String, true),
        __metadata("design:type", String)
    ], SubAdminGetListUserBKF.prototype, "firstName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetListBKF.LASTNAME, String, true),
        __metadata("design:type", String)
    ], SubAdminGetListUserBKF.prototype, "lastName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetListBKF.USERGROUP, String, true),
        __metadata("design:type", String)
    ], SubAdminGetListUserBKF.prototype, "userGroup", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetListBKF.USERTYPE, String, true),
        __metadata("design:type", String)
    ], SubAdminGetListUserBKF.prototype, "userType", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetListBKF.USERNAME, String, true),
        __metadata("design:type", String)
    ], SubAdminGetListUserBKF.prototype, "username", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetListBKF.PASSWORD, String, true),
        __metadata("design:type", String)
    ], SubAdminGetListUserBKF.prototype, "password", void 0);
    SubAdminGetListUserBKF = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubAdminGetListUserBKF")
    ], SubAdminGetListUserBKF);
    return SubAdminGetListUserBKF;
}());

//# sourceMappingURL=SubAdminGetListUserBKF.js.map

/***/ }),
/* 365 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubAdminGetListUserMold; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubAdminGetListUserMold = /** @class */ (function () {
    function SubAdminGetListUserMold() {
        this.empId = undefined;
        this.fullName = undefined;
        this.userType = undefined;
        this.username = undefined;
        this.password = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetListMold.EMPID, String, true),
        __metadata("design:type", String)
    ], SubAdminGetListUserMold.prototype, "empId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetListMold.FIRSTNAME, String, true),
        __metadata("design:type", String)
    ], SubAdminGetListUserMold.prototype, "fullName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetListMold.USERTYPE, String, true),
        __metadata("design:type", String)
    ], SubAdminGetListUserMold.prototype, "userType", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetListMold.USERNAME, String, true),
        __metadata("design:type", String)
    ], SubAdminGetListUserMold.prototype, "username", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetListMold.PASSWORD, String, true),
        __metadata("design:type", String)
    ], SubAdminGetListUserMold.prototype, "password", void 0);
    SubAdminGetListUserMold = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubAdminGetListUserMold")
    ], SubAdminGetListUserMold);
    return SubAdminGetListUserMold;
}());

//# sourceMappingURL=SubAdminGetListUserMold.js.map

/***/ }),
/* 366 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubAdminGetConfig; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubAdminGetConfig = /** @class */ (function () {
    function SubAdminGetConfig() {
        this.survey = undefined;
        this.inout = undefined;
        this.mold = undefined;
        this.iso = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetConfig.SURVEY, String, true),
        __metadata("design:type", String)
    ], SubAdminGetConfig.prototype, "survey", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetConfig.MOLDINOUT, String, true),
        __metadata("design:type", String)
    ], SubAdminGetConfig.prototype, "inout", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetConfig.MOLDMAKER, String, true),
        __metadata("design:type", String)
    ], SubAdminGetConfig.prototype, "mold", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubAdminGetConfig.ISONUMBER, String, true),
        __metadata("design:type", String)
    ], SubAdminGetConfig.prototype, "iso", void 0);
    SubAdminGetConfig = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubAdminGetConfig")
    ], SubAdminGetConfig);
    return SubAdminGetConfig;
}());

//# sourceMappingURL=SubAdminGetConfig.js.map

/***/ }),
/* 367 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BkfMainPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_message_provider_message_provider__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_login_provider_login_provider__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_utilities__ = __webpack_require__(16);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the BkfMainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BkfMainPage = /** @class */ (function (_super) {
    __extends(BkfMainPage, _super);
    function BkfMainPage(navCtrl, navParams, events, _messageProvider, _loginProvider, _storageProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.events = events;
        _this._messageProvider = _messageProvider;
        _this._loginProvider = _loginProvider;
        _this._storageProvider = _storageProvider;
        _this.countMessage = 0;
        _this.fullName = '';
        _this.platformVersion = '';
        return _this;
    }
    BkfMainPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BkfMainPage');
    };
    BkfMainPage.prototype.ionViewDidEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, ex_1;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 4, , 5]);
                        _a = this;
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].USER_NAME)];
                    case 1:
                        _a.fullName = _c.sent();
                        _b = this;
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].getAppVersion()];
                    case 2:
                        _b.platformVersion = _c.sent();
                        this.messageModel = this._messageProvider.messageModel;
                        return [4 /*yield*/, this._messageProvider.getMessageDetail()];
                    case 3:
                        _c.sent();
                        this.countMessage = this.messageModel.getMessageDetail.detail.length;
                        return [3 /*break*/, 5];
                    case 4:
                        ex_1 = _c.sent();
                        console.log(ex_1);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    BkfMainPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.messageModel = this._messageProvider.messageModel;
                return [2 /*return*/];
            });
        });
    };
    BkfMainPage.prototype.onNotification = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.navCtrl.push('NotificationPage');
                return [2 /*return*/];
            });
        });
    };
    BkfMainPage.prototype.onLogout = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this._loginProvider.authenticateLogout();
                return [2 /*return*/];
            });
        });
    };
    BkfMainPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* Component */])({
            selector: 'page-bkf-main',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/bkf-main/bkf-main.html"*/'<!--\n  Generated template for the BkfMainPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Mold Main Menu</ion-title>\n    <ion-buttons end class="cart-btn">\n      <button ion-button (click)="onNotification()">\n        <ion-icon name="notifications"></ion-icon>\n        <ion-badge class="cart-badge" *ngIf="countMessage > 0" >{{ countMessage }}</ion-badge>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <h4 style="font-size: 1.2em; text-align: center;">Welcome, {{ fullName }}</h4>\n  <div style="position: relative;">\n    <div class="content-menu">\n      <h4 style="font-size: 1.2em"> <br>\n       \n      </h4>\n    </div>\n    <ion-grid style="position: absolute; top: 75%; margin: 0px;">\n      <ion-row>\n        <ion-col col-6 col-xl-4 col-lg-4 col-md-4>\n          <ion-card text-center padding style="border:solid 1px #3d974c; border-radius: 10px 10px 10px 10px;" [navPush]="\'SurveyMenuPage\'">\n            <img src="../../assets/imgs/survey.png" alt="" />\n            <h2 style="padding: 10px 0px 0px 0px; font-size: 0.8rem; font-weight: bold;">Fixed Asset Survey</h2>\n          </ion-card>\n        </ion-col>\n        <ion-col col-6 col-xl-4 col-lg-4 col-md-4>\n          <ion-card text-center padding style="border:solid 1px #3d974c; border-radius: 10px 10px 10px 10px;" [navPush]="\'MoldinoutMenuPage\'">\n            <img src="../../assets/imgs/repare.png" alt="" />\n            <h2 style="padding: 10px 0px 0px 0px; font-size: 1rem; font-weight: bold;">Mold In/Out</h2>\n          </ion-card>\n        </ion-col>\n        <ion-col col-6 col-xl-4 col-lg-4 col-md-4>\n          <ion-card text-center padding style="border:solid 1px #3d974c; border-radius: 10px 10px 10px 10px;" (tap)="onLogout()">\n            <img src="../../assets/imgs/open.png" alt="" />\n            <h2 style="padding: 10px 0px 0px 0px; font-size: 1rem; font-weight: bold;">Log Out</h2>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <div class="version">\n      <pre>Version {{ platformVersion }}</pre>\n    </div>\n  </div>\n \n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/bkf-main/bkf-main.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__providers_message_provider_message_provider__["a" /* MessageProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_login_provider_login_provider__["a" /* LoginProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], BkfMainPage);
    return BkfMainPage;
}(__WEBPACK_IMPORTED_MODULE_1__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=bkf-main.js.map

/***/ }),
/* 368 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMessageModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMessageModel = /** @class */ (function () {
    function SubMessageModel() {
        this.messageId = undefined;
        this.messageSubject = undefined;
        this.messageDescription = undefined;
        this.readStatus = undefined;
        this.messageDate = undefined;
        this.jobId = undefined;
        this.moldId = undefined;
        this.status = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMessageModel.MESSAGEID, String, true),
        __metadata("design:type", String)
    ], SubMessageModel.prototype, "messageId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMessageModel.SUBJECT, String, true),
        __metadata("design:type", String)
    ], SubMessageModel.prototype, "messageSubject", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMessageModel.DESCRIPTION, String, true),
        __metadata("design:type", String)
    ], SubMessageModel.prototype, "messageDescription", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMessageModel.FLAG, String, true),
        __metadata("design:type", String)
    ], SubMessageModel.prototype, "readStatus", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMessageModel.MESSAGE_DATE, String, true),
        __metadata("design:type", String)
    ], SubMessageModel.prototype, "messageDate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMessageModel.JOBID, String, true),
        __metadata("design:type", String)
    ], SubMessageModel.prototype, "jobId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMessageModel.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubMessageModel.prototype, "moldId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMessageModel.STATUS, String, true),
        __metadata("design:type", String)
    ], SubMessageModel.prototype, "status", void 0);
    SubMessageModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMessageModel")
    ], SubMessageModel);
    return SubMessageModel;
}());

//# sourceMappingURL=SubMessageModel.js.map

/***/ }),
/* 369 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__package_MessageAdapterModel__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__package_OperationBaseModel__ = __webpack_require__(20);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MessageModel = /** @class */ (function (_super) {
    __extends(MessageModel, _super);
    function MessageModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.getMessageDetail = new __WEBPACK_IMPORTED_MODULE_0__package_MessageAdapterModel__["a" /* MessageAdapterModel */]();
        _this.readMessage = new __WEBPACK_IMPORTED_MODULE_0__package_MessageAdapterModel__["a" /* MessageAdapterModel */]();
        return _this;
    }
    MessageModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonObject"])("MessageModel")
    ], MessageModel);
    return MessageModel;
}(__WEBPACK_IMPORTED_MODULE_2__package_OperationBaseModel__["a" /* OperationBaseModel */]));

//# sourceMappingURL=MessageModel.js.map

/***/ }),
/* 370 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DrawingImagePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_moldinout_provider_moldinout_provider__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



// import { CONSTANT } from '../../shared/constant';
/**
 * Generated class for the DrawingImagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DrawingImagePage = /** @class */ (function () {
    function DrawingImagePage(navCtrl, renderer, platform, _moldinoutProvider) {
        this.navCtrl = navCtrl;
        this.renderer = renderer;
        this.platform = platform;
        this._moldinoutProvider = _moldinoutProvider;
        this.storedImages = [];
        // Color Stuff
        this.selectedColor = '#9e2956';
        this.colors = ['#9e2956', '#c2281d', '#de722f', '#edbf4c', '#5db37e', '#459cde', '#4250ad', '#802fa3'];
    }
    DrawingImagePage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
                this.storedImages.push(this.moldinoutModel.ImageSelectedName);
                return [2 /*return*/];
            });
        });
    };
    DrawingImagePage.prototype.ionViewDidEnter = function () {
        // https://github.com/ionic-team/ionic/issues/9071#issuecomment-362920591
        // Get the height of the fixed item
        var itemHeight = this.fixedContainer.nativeElement.offsetHeight;
        var scroll = this.content.getScrollElement();
        // Add preexisting scroll margin to fixed container size
        itemHeight = Number.parseFloat(scroll.style.marginTop.replace("px", "")) + itemHeight;
        scroll.style.marginTop = itemHeight + 'px';
    };
    DrawingImagePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // Set the Canvas Element and its size
        this.canvasElement = this.canvas.nativeElement;
        this.canvasElement.width = this.platform.width() + '';
        this.canvasElement.height = 380;
        // var canvas = ctx.canvas ;
        var context = this.canvasElement.getContext('2d');
        var img = new Image();
        img.onload = function () {
            context.fillStyle = "white";
            context.fillRect(0, 0, _this.platform.width(), 350);
            context.drawImage(img, 0, 0, _this.platform.width(), 350);
        };
        img.src = this.moldinoutModel.ImageSelectedName;
    };
    DrawingImagePage.prototype.selectColor = function (color) {
        this.selectedColor = color;
    };
    DrawingImagePage.prototype.startDrawing = function (ev) {
        var canvasPosition = this.canvasElement.getBoundingClientRect();
        this.saveX = ev.touches[0].pageX - canvasPosition.x;
        this.saveY = ev.touches[0].pageY - canvasPosition.y;
    };
    DrawingImagePage.prototype.moved = function (ev) {
        var canvasPosition = this.canvasElement.getBoundingClientRect();
        var ctx = this.canvasElement.getContext('2d');
        var currentX = ev.touches[0].pageX - canvasPosition.x;
        var currentY = ev.touches[0].pageY - canvasPosition.y;
        ctx.lineJoin = 'round';
        ctx.strokeStyle = this.selectedColor;
        ctx.lineWidth = 2;
        ctx.beginPath();
        ctx.moveTo(this.saveX, this.saveY);
        ctx.lineTo(currentX, currentY);
        ctx.closePath();
        ctx.stroke();
        this.saveX = currentX;
        this.saveY = currentY;
    };
    DrawingImagePage.prototype.saveCanvasImage = function () {
        var dataUrl = this.canvasElement.toDataURL("image/jpeg", 0.98);
        var ctx = this.canvasElement.getContext('2d');
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        var element = this.moldinoutModel.Images[this.moldinoutModel.ImageSelectedIndex];
        element.photo = dataUrl;
        this.navCtrl.pop();
        // let ctx = this.canvasElement.getContext('2d');
        // ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
        // let name = new Date().getTime() + '.png';
        // let path = this.file.dataDirectory;
        // let options: IWriteOptions = { replace: true };
        // var data = dataUrl.split(',')[1];
        // let blob = this.b64toBlob(data, 'image/png');
        // this.file.writeFile(path, name, blob, options).then(res => {
        // }, err => {
        //   console.log('error: ', err);
        // });
    };
    // https://forum.ionicframework.com/t/save-base64-encoded-image-to-specific-filepath/96180/3
    DrawingImagePage.prototype.b64toBlob = function (b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('imageCanvas'),
        __metadata("design:type", Object)
    ], DrawingImagePage.prototype, "canvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */])
    ], DrawingImagePage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('fixedContainer'),
        __metadata("design:type", Object)
    ], DrawingImagePage.prototype, "fixedContainer", void 0);
    DrawingImagePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-drawing-image',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/drawing-image/drawing-image.html"*/'<!--\n  Generated template for the DrawingImagePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>Drawing-image</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div #fixedContainer ion-fixed>\n    <ion-row>\n      <ion-col *ngFor="let color of colors" [style.background]="color" class="color-block" tappable (click)="selectColor(color)"></ion-col>\n    </ion-row>\n  \n    <ion-row radio-group [(ngModel)]="selectedColor">\n      <ion-col *ngFor="let color of colors" text-center>\n        <ion-radio [value]="color"></ion-radio>\n      </ion-col>\n    </ion-row>\n    \n    <canvas #imageCanvas (touchstart)="startDrawing($event)" (touchmove)="moved($event)"></canvas>\n  \n    <div padding> \n      <button ion-button full (click)="saveCanvasImage()">Save Image</button>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/drawing-image/drawing-image.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__providers_moldinout_provider_moldinout_provider__["a" /* MoldinoutProvider */]])
    ], DrawingImagePage);
    return DrawingImagePage;
}());

//# sourceMappingURL=drawing-image.js.map

/***/ }),
/* 371 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldInOutApproveModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMoldInOutApproveModel = /** @class */ (function () {
    function SubMoldInOutApproveModel() {
        this.jobId = undefined;
        this.empId = undefined;
        this.approveType = undefined;
        this.status = undefined;
        this.date = undefined;
        this.remark = undefined;
        this.fullName = undefined;
        this.userType = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutApprove.JOBID, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutApproveModel.prototype, "jobId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutApprove.EMPID, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutApproveModel.prototype, "empId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutApprove.TYPE, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutApproveModel.prototype, "approveType", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutApprove.STATUS, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutApproveModel.prototype, "status", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutApprove.DATE, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutApproveModel.prototype, "date", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutApprove.REMARK, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutApproveModel.prototype, "remark", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutApprove.NAME, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutApproveModel.prototype, "fullName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutApprove.USER_TYPE, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutApproveModel.prototype, "userType", void 0);
    SubMoldInOutApproveModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldInOutApproveModel")
    ], SubMoldInOutApproveModel);
    return SubMoldInOutApproveModel;
}());

//# sourceMappingURL=SubMoldInOutApproveModel.js.map

/***/ }),
/* 372 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubNewMoldInOut; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__DescriptionMoldel__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SubNewMoldInOut = /** @class */ (function () {
    function SubNewMoldInOut() {
        this.description = undefined;
        this.moldId = undefined;
        this.jobId = undefined;
        this.makerMold = undefined;
        this.makerId = undefined;
        this.planOut = undefined;
        this.planIn = undefined;
        this.reason = undefined;
        this.nameCreate = undefined;
        this.remarkNew = undefined;
        this.confirmDate = undefined;
        this.nameConfirmout = undefined;
        this.remarkOut = undefined;
        this.status = undefined;
        this.print = undefined;
        this.pdf = undefined;
        this.flag = undefined;
        this.makerStatus = undefined;
        this.remarkIn = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.DESCRIPTION, [__WEBPACK_IMPORTED_MODULE_2__DescriptionMoldel__["a" /* DescriptionMoldel */]], true),
        __metadata("design:type", Array)
    ], SubNewMoldInOut.prototype, "description", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "moldId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.JOBID, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "jobId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.MAKERMOLD, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "makerMold", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.MAKERID, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "makerId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.PLANOUT, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "planOut", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.PLANIN, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "planIn", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.REASON, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "reason", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.NAMECREATE, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "nameCreate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.REMARKNEW, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "remarkNew", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.CONFIRMDATE, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "confirmDate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.NAMECONFIRMOUT, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "nameConfirmout", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.REMARKOUT, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "remarkOut", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.STATUS, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "status", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.PRINT, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "print", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.PDF, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "pdf", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.FLAG, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "flag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.MAKERSTATUS, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "makerStatus", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubNewMoldInOut.REMARKIN, String, true),
        __metadata("design:type", String)
    ], SubNewMoldInOut.prototype, "remarkIn", void 0);
    SubNewMoldInOut = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubNewMoldInOut")
    ], SubNewMoldInOut);
    return SubNewMoldInOut;
}());

//# sourceMappingURL=SubNewMoldInOut.js.map

/***/ }),
/* 373 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldInOutSetting; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMoldInOutSetting = /** @class */ (function () {
    function SubMoldInOutSetting() {
        this.id = undefined;
        this.description = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutConfig.ID, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutSetting.prototype, "id", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutConfig.DESCIRPTION, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutSetting.prototype, "description", void 0);
    SubMoldInOutSetting = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldInOutSetting")
    ], SubMoldInOutSetting);
    return SubMoldInOutSetting;
}());

//# sourceMappingURL=SubMoldInOutSettingModel.js.map

/***/ }),
/* 374 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldInOutImageMoldel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMoldInOutImageMoldel = /** @class */ (function () {
    function SubMoldInOutImageMoldel() {
        this.ImageType = undefined;
        this.Latiture = undefined;
        this.Longiture = undefined;
        this.PhotoName = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutImages.IMAGES_TYPE, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutImageMoldel.prototype, "ImageType", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutImages.LATITUDE, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutImageMoldel.prototype, "Latiture", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutImages.LONGITUDE, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutImageMoldel.prototype, "Longiture", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutImages.IMAGES_NAME, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutImageMoldel.prototype, "PhotoName", void 0);
    SubMoldInOutImageMoldel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldInOutImageMoldel")
    ], SubMoldInOutImageMoldel);
    return SubMoldInOutImageMoldel;
}());

//# sourceMappingURL=SubMoldInOutImageMoldel.js.map

/***/ }),
/* 375 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldInOutSearchModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMoldInOutSearchModel = /** @class */ (function () {
    function SubMoldInOutSearchModel() {
        this.jobId = undefined;
        this.moldId = undefined;
        this.custNo = undefined;
        this.partNo = undefined;
        this.partName = undefined;
        this.makerName = undefined;
        this.status = undefined;
        this.confirmDate = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutSearch.JOBID, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutSearchModel.prototype, "jobId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutSearch.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutSearchModel.prototype, "moldId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutSearch.CUSTNO, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutSearchModel.prototype, "custNo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutSearch.PARTNO, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutSearchModel.prototype, "partNo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutSearch.PARTNAME, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutSearchModel.prototype, "partName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutSearch.MAKERNAME, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutSearchModel.prototype, "makerName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutSearch.STATUS, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutSearchModel.prototype, "status", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutSearch.CONFIRMOUT_DATE, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutSearchModel.prototype, "confirmDate", void 0);
    SubMoldInOutSearchModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldInOutSearchModel")
    ], SubMoldInOutSearchModel);
    return SubMoldInOutSearchModel;
}());

//# sourceMappingURL=SubMoldInOutSearchModel.js.map

/***/ }),
/* 376 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldInOutStatusModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMoldInOutStatusModel = /** @class */ (function () {
    function SubMoldInOutStatusModel() {
        this.jobId = undefined;
        this.moldId = undefined;
        this.planIn = undefined;
        this.planOut = undefined;
        this.name = undefined;
        this.makerName = undefined;
        this.reson = undefined;
        this.status = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutStatus.JOBID, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutStatusModel.prototype, "jobId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutStatus.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutStatusModel.prototype, "moldId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutStatus.PLANIN, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutStatusModel.prototype, "planIn", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutStatus.PLANOUT, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutStatusModel.prototype, "planOut", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutStatus.FULLNAME, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutStatusModel.prototype, "name", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutStatus.MAKERNAME, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutStatusModel.prototype, "makerName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutStatus.REASON, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutStatusModel.prototype, "reson", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInOutStatus.STATUS, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutStatusModel.prototype, "status", void 0);
    SubMoldInOutStatusModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldInOutStatusModel")
    ], SubMoldInOutStatusModel);
    return SubMoldInOutStatusModel;
}());

//# sourceMappingURL=SubMoldInOutStatusModel.js.map

/***/ }),
/* 377 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldInOutWatingModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMoldInOutWatingModel = /** @class */ (function () {
    function SubMoldInOutWatingModel() {
        this.jobid = undefined;
        this.moldid = undefined;
        this.custno = undefined;
        this.partno = undefined;
        this.partname = undefined;
        this.createdate = undefined;
        this.status = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutWatingModel.JOBID, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutWatingModel.prototype, "jobid", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutWatingModel.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutWatingModel.prototype, "moldid", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutWatingModel.CUSNO, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutWatingModel.prototype, "custno", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutWatingModel.PARTNO, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutWatingModel.prototype, "partno", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutWatingModel.PARTNAME, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutWatingModel.prototype, "partname", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutWatingModel.CREATEDATE, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutWatingModel.prototype, "createdate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldInoutWatingModel.STATUS, String, true),
        __metadata("design:type", String)
    ], SubMoldInOutWatingModel.prototype, "status", void 0);
    SubMoldInOutWatingModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldInOutWatingModel")
    ], SubMoldInOutWatingModel);
    return SubMoldInOutWatingModel;
}());

//# sourceMappingURL=SubMoldInOutWatingModel.js.map

/***/ }),
/* 378 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatetimePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * Generated class for the DatetimePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var DatetimePipe = /** @class */ (function () {
    function DatetimePipe() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    DatetimePipe.prototype.transform = function (date, format) {
        var inputDate = new Date((date.replace(/T,Z/g, ' ').trim()));
        var ThaiDay = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'];
        // let shortThaiMonth = [
        //     'ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
        //     'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'
        // ];
        // let longThaiMonth = [
        //     'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
        //     'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
        // ];
        var longEngMonth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var shortEngMonth = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var longMonth = [];
        var shortMonth = [];
        var yearNumber = 0;
        longMonth = longEngMonth;
        shortMonth = shortEngMonth;
        yearNumber = 0;
        var dataDate = [
            inputDate.getDay(), inputDate.getDate(), inputDate.getMonth(), inputDate.getFullYear(), inputDate.getHours(), inputDate.getMinutes(), inputDate.getSeconds()
        ];
        var outputDateFull = [
            'วัน ' + ThaiDay[dataDate[0]],
            'ที่ ' + dataDate[1],
            'เดือน ' + longMonth[dataDate[2]],
            'พ.ศ. ' + (dataDate[3] + yearNumber)
        ];
        var outputDateShort = [
            dataDate[1],
            shortMonth[dataDate[2]],
            dataDate[3] + yearNumber
        ];
        var outputDateMedium = [
            dataDate[1],
            longMonth[dataDate[2]],
            dataDate[3] + yearNumber
        ];
        var outputTime = [
            this.checkTwoDigit(dataDate[4]),
            this.checkTwoDigit(dataDate[5]),
            this.checkTwoDigit(dataDate[6])
        ];
        var outputTimeNoSec = [
            this.checkTwoDigit(dataDate[4]),
            this.checkTwoDigit(dataDate[5])
        ];
        var returnDate;
        returnDate = outputDateMedium.join(" ");
        if (format == 'full') {
            returnDate = outputDateFull.join(" ");
        }
        if (format == 'medium') {
            returnDate = outputDateMedium.join(" ");
        }
        if (format == 'short') {
            returnDate = outputDateShort.join(" ");
        }
        if (format == "dateAndTime") {
            returnDate = outputDateShort.join(" ") + " - " + outputTime.join(":");
        }
        if (format == "dateAndTimeNoSec") {
            returnDate = outputDateShort.join(" ") + " : " + outputTimeNoSec.join(".");
        }
        if (format == 'dateTimeComma') {
            returnDate = outputDateShort.join(" ") + ", " + outputTime.join(":");
        }
        if (format == 'time') {
            returnDate = outputTimeNoSec.join(":");
        }
        if (format == 'dateNow') {
            var dteNow = new Date();
            var date_1 = dteNow.getDate() - inputDate.getDate();
            var month = dteNow.getMonth() - inputDate.getMonth();
            var year = dteNow.getFullYear() - inputDate.getFullYear();
            if ((date_1 + month + year) === 0) {
                returnDate = 'Today';
            }
            else if ((date_1 + month + year) == 1) {
                returnDate = 'Yesterday';
            }
            else {
                returnDate = outputDateMedium.join(" ");
            }
        }
        return returnDate;
    };
    DatetimePipe.prototype.checkTwoDigit = function (date) {
        var dataFormat = '';
        if (date.toString().length < 2) {
            dataFormat = '0' + date.toString();
        }
        else {
            dataFormat = date;
        }
        return dataFormat;
    };
    DatetimePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'datetime',
        })
    ], DatetimePipe);
    return DatetimePipe;
}());

//# sourceMappingURL=datetime.js.map

/***/ }),
/* 379 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccordionMenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the AccordionMenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var AccordionMenuComponent = /** @class */ (function () {
    function AccordionMenuComponent(renderer) {
        this.renderer = renderer;
        this.headerColor = '#F53D3D';
        this.textColor = '#FFF';
        this.contentColor = '#F9F9F9';
        this.hasMargin = true;
    }
    AccordionMenuComponent.prototype.ngOnInit = function () {
    };
    AccordionMenuComponent.prototype.ngAfterViewInit = function () {
        this.viewHeight = this.elementView.nativeElement.offsetHeight;
        if (!this.expanded) {
            this.renderer.setElementStyle(this.elementView.nativeElement, 'height', 0 + 'px');
        }
    };
    AccordionMenuComponent.prototype.toggleAccordion = function () {
        this.expanded = !this.expanded;
        var newHeight = this.expanded ? '100%' : '0px';
        this.renderer.setElementStyle(this.elementView.nativeElement, 'height', newHeight);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], AccordionMenuComponent.prototype, "headerColor", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], AccordionMenuComponent.prototype, "textColor", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], AccordionMenuComponent.prototype, "contentColor", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], AccordionMenuComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Boolean)
    ], AccordionMenuComponent.prototype, "hasMargin", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Boolean)
    ], AccordionMenuComponent.prototype, "expanded", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('accordionContent'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], AccordionMenuComponent.prototype, "elementView", void 0);
    AccordionMenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'accordion-menu',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/components/accordion-menu/accordion-menu.html"*/'<ion-list class="accordion-menu">\n    <ion-list-header no-lines no-padding>\n        <button ion-item detail-none\n                [style.background]="headerColor"\n                (click)="toggleAccordion()"\n                class="accordion-header"\n                [class.active]="expanded">\n            <ion-icon\n            item-left\n            name="ios-arrow-forward">\n            </ion-icon>\n            {{ title }}\n        </button>\n        <section #accordionContent\n                [style.background]="contentColor"\n                [class.active]="expanded"\n                class="accordion-content">\n        <ng-content></ng-content>\n        </section>\n    </ion-list-header>\n</ion-list>'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/components/accordion-menu/accordion-menu.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer */]])
    ], AccordionMenuComponent);
    return AccordionMenuComponent;
}());

//# sourceMappingURL=accordion-menu.js.map

/***/ }),
/* 380 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccordionListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the AccordionListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var AccordionListComponent = /** @class */ (function () {
    function AccordionListComponent(renderer) {
        this.renderer = renderer;
        this.headerColor = '#F53D3D';
        this.textColor = '#FFF';
        this.contentColor = '#F9F9F9';
        this.title = '';
        this.hasMargin = true;
    }
    AccordionListComponent.prototype.ngOnInit = function () {
    };
    AccordionListComponent.prototype.ngAfterViewInit = function () {
        this.viewHeight = this.elementView.nativeElement.offsetHeight;
        if (!this.expanded) {
            this.renderer.setElementStyle(this.elementView.nativeElement, 'height', 0 + 'px');
        }
    };
    AccordionListComponent.prototype.toggleAccordion = function () {
        this.expanded = !this.expanded;
        var newHeight = this.expanded ? '100%' : '0px';
        this.renderer.setElementStyle(this.elementView.nativeElement, 'height', newHeight);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], AccordionListComponent.prototype, "headerColor", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], AccordionListComponent.prototype, "textColor", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], AccordionListComponent.prototype, "contentColor", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], AccordionListComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Boolean)
    ], AccordionListComponent.prototype, "hasMargin", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Boolean)
    ], AccordionListComponent.prototype, "expanded", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('accordionContent'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], AccordionListComponent.prototype, "elementView", void 0);
    AccordionListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'accordion-list',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/components/accordion-list/accordion-list.html"*/'<ion-list class="accordion-menu">\n  <ion-list-header no-lines no-padding>\n    <button ion-item detail-none\n            [style.background]="headerColor"\n            (click)="toggleAccordion()"\n            class="accordion-header"\n            [class.active]="expanded">\n        <ion-icon\n        item-left\n        name="ios-arrow-forward">\n        </ion-icon>\n        {{ title }}\n    </button>\n    <section #accordionContent\n            [style.background]="contentColor"\n            [class.active]="expanded"\n            class="accordion-content">\n      <ng-content></ng-content>\n    </section>\n  </ion-list-header>\n</ion-list>'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/components/accordion-list/accordion-list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer */]])
    ], AccordionListComponent);
    return AccordionListComponent;
}());

//# sourceMappingURL=accordion-list.js.map

/***/ }),
/* 381 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldInOutApproveComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_basepage__ = __webpack_require__(42);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



/**
 * Generated class for the MoldInOutApproveComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var MoldInOutApproveComponent = /** @class */ (function (_super) {
    __extends(MoldInOutApproveComponent, _super);
    function MoldInOutApproveComponent(alertCtrl, events) {
        var _this = _super.call(this, events) || this;
        _this.alertCtrl = alertCtrl;
        _this.events = events;
        console.log('Hello MoldInOutApproveComponent Component');
        return _this;
    }
    MoldInOutApproveComponent.prototype.viewRemark = function (remark) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.alertCtrl.create({
                    title: 'Remark',
                    subTitle: remark,
                    buttons: ['close']
                }).present();
                return [2 /*return*/];
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Array)
    ], MoldInOutApproveComponent.prototype, "taggedApprove", void 0);
    MoldInOutApproveComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'mold-in-out-approve',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/components/mold-in-out-approve/mold-in-out-approve.html"*/'<ion-grid style="display: block; margin: 0px;">\n  <pre style="text-align: left; font-family: sans-serif;padding: 0px 0px 0px 15px;">รายการอนุมัติ</pre>\n    <ion-row *ngFor="let items of taggedApprove">\n      <ion-col col-4 align-self-center>\n        <p style="text-align: right; font-family: sans-serif; font-size: 0.8em;">{{ items.fullName }}</p>\n      </ion-col>\n      <ion-col col-3 align-self-center>\n        <div *ngIf="items.status == \'Waiting\'">\n          <p style="color: blue; text-align: left; font-family: sans-serif; font-size: 0.8em;">{{ items.status }}</p>\n        </div>\n        <div *ngIf="items.status == \'Reject\'">\n          <p style="color:red; text-align: left; font-family: sans-serif; font-size: 0.8em;">{{ items.status }}</p>\n        </div>\n        <div *ngIf="items.status == \'ApproveOut\' || items.status == \'ApproveIn\'">\n          <p style="color:#65a506; text-align: left; font-family: sans-serif; font-size: 0.8em;">Approve</p>\n        </div>\n      </ion-col>\n      <ion-col col-3 align-self-center>\n        <p style="text-align: left; font-family: sans-serif; font-size: 0.8em;">{{ items.date }}</p>\n      </ion-col>\n      <ion-col col-2 align-self-center *ngIf="(items.remark) && (items.remark != \'\')">\n        <button id="notification-button" ion-button clear (click)="viewRemark(items.remark)">\n          <ion-icon name="alert">\n          </ion-icon>  \n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/components/mold-in-out-approve/mold-in-out-approve.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */]])
    ], MoldInOutApproveComponent);
    return MoldInOutApproveComponent;
}(__WEBPACK_IMPORTED_MODULE_2__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=mold-in-out-approve.js.map

/***/ }),
/* 382 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubGetListMoldModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubGetListMoldModel = /** @class */ (function () {
    function SubGetListMoldModel() {
        this.actual = undefined;
        this.custno = undefined;
        this.jobid = undefined;
        this.makerid = undefined;
        this.moldid = undefined;
        this.partname = undefined;
        this.partno = undefined;
        this.planin = undefined;
        this.planout = undefined;
        this.remark = undefined;
        this.status = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubGetListMoldModel.ACTUAL, String, true),
        __metadata("design:type", String)
    ], SubGetListMoldModel.prototype, "actual", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubGetListMoldModel.CUSTNO, String, true),
        __metadata("design:type", String)
    ], SubGetListMoldModel.prototype, "custno", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubGetListMoldModel.JOBID, String, true),
        __metadata("design:type", String)
    ], SubGetListMoldModel.prototype, "jobid", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubGetListMoldModel.MAKERMOLDID, String, true),
        __metadata("design:type", String)
    ], SubGetListMoldModel.prototype, "makerid", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubGetListMoldModel.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubGetListMoldModel.prototype, "moldid", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubGetListMoldModel.PARTNAME, String, true),
        __metadata("design:type", String)
    ], SubGetListMoldModel.prototype, "partname", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubGetListMoldModel.PARTNO, String, true),
        __metadata("design:type", String)
    ], SubGetListMoldModel.prototype, "partno", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubGetListMoldModel.PLANIN, String, true),
        __metadata("design:type", String)
    ], SubGetListMoldModel.prototype, "planin", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubGetListMoldModel.PLANOUT, String, true),
        __metadata("design:type", String)
    ], SubGetListMoldModel.prototype, "planout", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubGetListMoldModel.REMARK, String, true),
        __metadata("design:type", String)
    ], SubGetListMoldModel.prototype, "remark", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubGetListMoldModel.STATUS, String, true),
        __metadata("design:type", String)
    ], SubGetListMoldModel.prototype, "status", void 0);
    SubGetListMoldModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubGetListMoldModel")
    ], SubGetListMoldModel);
    return SubGetListMoldModel;
}());

//# sourceMappingURL=SubGetListMoldModel.js.map

/***/ }),
/* 383 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldMakerFormBKFModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__SubPhotoModel__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SubMoldMakerFormBKFModel = /** @class */ (function () {
    function SubMoldMakerFormBKFModel() {
        this.jobId = undefined;
        this.moldId = undefined;
        this.confirmDate = undefined;
        this.planIn = undefined;
        this.planOut = undefined;
        this.remark = undefined;
        this.reason = undefined;
        this.photo = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerBKFVIEW.JOBID, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerFormBKFModel.prototype, "jobId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerBKFVIEW.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerFormBKFModel.prototype, "moldId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerBKFVIEW.CONFIRMDATE, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerFormBKFModel.prototype, "confirmDate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerBKFVIEW.PLANINT, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerFormBKFModel.prototype, "planIn", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerBKFVIEW.PLANOUT, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerFormBKFModel.prototype, "planOut", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerBKFVIEW.REMARK, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerFormBKFModel.prototype, "remark", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerBKFVIEW.REASON, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerFormBKFModel.prototype, "reason", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerBKFVIEW.PHOTO, [__WEBPACK_IMPORTED_MODULE_2__SubPhotoModel__["a" /* SubPhotoModel */]], true),
        __metadata("design:type", Array)
    ], SubMoldMakerFormBKFModel.prototype, "photo", void 0);
    SubMoldMakerFormBKFModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldMakerFormBKFModel")
    ], SubMoldMakerFormBKFModel);
    return SubMoldMakerFormBKFModel;
}());

//# sourceMappingURL=SubMoldMakerFormBKFModel.js.map

/***/ }),
/* 384 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldMakerHistoryModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMoldMakerHistoryModel = /** @class */ (function () {
    function SubMoldMakerHistoryModel() {
        this.jobId = undefined;
        this.moldid = undefined;
        this.id = undefined;
        this.createdate = undefined;
        this.remark = undefined;
        this.status = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerHistory.JOBID, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerHistoryModel.prototype, "jobId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerHistory.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerHistoryModel.prototype, "moldid", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerHistory.ID, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerHistoryModel.prototype, "id", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerHistory.CREATEDATE, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerHistoryModel.prototype, "createdate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerHistory.REMARK, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerHistoryModel.prototype, "remark", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerHistory.STATUS, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerHistoryModel.prototype, "status", void 0);
    SubMoldMakerHistoryModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldMakerHistoryModel")
    ], SubMoldMakerHistoryModel);
    return SubMoldMakerHistoryModel;
}());

//# sourceMappingURL=SubMoldMakerHistoryModel.js.map

/***/ }),
/* 385 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldBKFVIEWHistoryModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__SubPhotoModel__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SubMoldBKFVIEWHistoryModel = /** @class */ (function () {
    function SubMoldBKFVIEWHistoryModel() {
        this.jobId = undefined;
        this.moldId = undefined;
        this.createDate = undefined;
        this.remark = undefined;
        this.status = undefined;
        this.photo = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SubMoldBKFVIEWHistoryModel.JOBID, String, true),
        __metadata("design:type", String)
    ], SubMoldBKFVIEWHistoryModel.prototype, "jobId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SubMoldBKFVIEWHistoryModel.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubMoldBKFVIEWHistoryModel.prototype, "moldId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SubMoldBKFVIEWHistoryModel.CREATEDATE, String, true),
        __metadata("design:type", String)
    ], SubMoldBKFVIEWHistoryModel.prototype, "createDate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SubMoldBKFVIEWHistoryModel.REMARK, String, true),
        __metadata("design:type", String)
    ], SubMoldBKFVIEWHistoryModel.prototype, "remark", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SubMoldBKFVIEWHistoryModel.STAUTS, String, true),
        __metadata("design:type", String)
    ], SubMoldBKFVIEWHistoryModel.prototype, "status", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_2__shared_constant__["a" /* CONSTANT */].SubMoldBKFVIEWHistoryModel.PHOTO, [__WEBPACK_IMPORTED_MODULE_0__SubPhotoModel__["a" /* SubPhotoModel */]], true),
        __metadata("design:type", Array)
    ], SubMoldBKFVIEWHistoryModel.prototype, "photo", void 0);
    SubMoldBKFVIEWHistoryModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1_json2typescript__["JsonObject"])("SubMoldBKFVIEWHistoryModel")
    ], SubMoldBKFVIEWHistoryModel);
    return SubMoldBKFVIEWHistoryModel;
}());

//# sourceMappingURL=SubMoldBKFVIEWHistoryModel.js.map

/***/ }),
/* 386 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldMakerHistoryDetailModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMoldMakerHistoryDetailModel = /** @class */ (function () {
    function SubMoldMakerHistoryDetailModel() {
        this.jobId = undefined;
        this.remark = undefined;
        this.status = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerHistoryDetail.JOBID, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerHistoryDetailModel.prototype, "jobId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerHistoryDetail.REMARK, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerHistoryDetailModel.prototype, "remark", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SubMoldMakerHistoryDetail.STATUS, String, true),
        __metadata("design:type", String)
    ], SubMoldMakerHistoryDetailModel.prototype, "status", void 0);
    SubMoldMakerHistoryDetailModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldMakerHistoryDetailModel")
    ], SubMoldMakerHistoryDetailModel);
    return SubMoldMakerHistoryDetailModel;
}());

//# sourceMappingURL=SubMoldMakerHistoryDetailModel.js.map

/***/ }),
/* 387 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Autosize; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Autosize = /** @class */ (function () {
    function Autosize(element) {
        this.element = element;
        // console.log('Hello AutosizeDirective Directive');
    }
    Autosize.prototype.onInput = function (textArea) {
        this.adjust();
    };
    Autosize.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () { return _this.adjust(); }, 0);
    };
    Autosize.prototype.adjust = function () {
        var textArea = this.element.nativeElement.getElementsByTagName('textarea')[0];
        textArea.style.overflow = 'hidden';
        textArea.style.height = 'auto';
        textArea.style.height = textArea.scrollHeight + 'px';
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* HostListener */])('input', ['$event.target']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [HTMLTextAreaElement]),
        __metadata("design:returntype", void 0)
    ], Autosize.prototype, "onInput", null);
    Autosize = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Directive */])({
            selector: 'ion-textarea[autosize]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]])
    ], Autosize);
    return Autosize;
}());

//# sourceMappingURL=autosize.js.map

/***/ }),
/* 388 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldHistoryModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMoldHistoryModel = /** @class */ (function () {
    function SubMoldHistoryModel() {
        this.MoldId = undefined;
        this.AssetId = undefined;
        this.CustNo = undefined;
        this.PartNo = undefined;
        this.PartName = undefined;
        this.Location = undefined;
        this.Plate = undefined;
        this.Remark = undefined;
        this.FullName = undefined;
        this.CreateDate = undefined;
        this.RunNo = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryModel.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryModel.prototype, "MoldId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryModel.ASSETID, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryModel.prototype, "AssetId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryModel.CUSTNO, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryModel.prototype, "CustNo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryModel.PARTNO, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryModel.prototype, "PartNo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryModel.PARTNAME, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryModel.prototype, "PartName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryModel.LOCATION, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryModel.prototype, "Location", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryModel.PLATE, Number, true),
        __metadata("design:type", String)
    ], SubMoldHistoryModel.prototype, "Plate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryModel.REMARK, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryModel.prototype, "Remark", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryModel.FULLNAME, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryModel.prototype, "FullName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryModel.CREATEDATE, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryModel.prototype, "CreateDate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryModel.RUNNO, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryModel.prototype, "RunNo", void 0);
    SubMoldHistoryModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldHistoryModel")
    ], SubMoldHistoryModel);
    return SubMoldHistoryModel;
}());

//# sourceMappingURL=SubMoldHistoryModel.js.map

/***/ }),
/* 389 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldMasterModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMoldMasterModel = /** @class */ (function () {
    function SubMoldMasterModel() {
        this.MoldId = undefined;
        this.AssetId = undefined;
        this.CustNo = undefined;
        this.PartNo = undefined;
        this.PartName = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubMasterModel.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubMoldMasterModel.prototype, "MoldId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubMasterModel.ASSETID, String, true),
        __metadata("design:type", String)
    ], SubMoldMasterModel.prototype, "AssetId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubMasterModel.CUSTNO, String, true),
        __metadata("design:type", String)
    ], SubMoldMasterModel.prototype, "CustNo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubMasterModel.PARTNO, String, true),
        __metadata("design:type", String)
    ], SubMoldMasterModel.prototype, "PartNo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubMasterModel.PARTNAME, String, true),
        __metadata("design:type", String)
    ], SubMoldMasterModel.prototype, "PartName", void 0);
    SubMoldMasterModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldMasterModel")
    ], SubMoldMasterModel);
    return SubMoldMasterModel;
}());

//# sourceMappingURL=SubMoldMasterModel.js.map

/***/ }),
/* 390 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldSearchModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMoldSearchModel = /** @class */ (function () {
    function SubMoldSearchModel() {
        this.MoldId = undefined;
        this.AssetId = undefined;
        this.Plate = undefined;
        this.FullName = undefined;
        this.CreateDate = undefined;
        this.RunNo = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubSearchModel.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubMoldSearchModel.prototype, "MoldId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubSearchModel.ASSETID, String, true),
        __metadata("design:type", String)
    ], SubMoldSearchModel.prototype, "AssetId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubSearchModel.PLATE, String, true),
        __metadata("design:type", String)
    ], SubMoldSearchModel.prototype, "Plate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubSearchModel.FULLNAME, String, true),
        __metadata("design:type", String)
    ], SubMoldSearchModel.prototype, "FullName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubSearchModel.CREATEDATE, String, true),
        __metadata("design:type", String)
    ], SubMoldSearchModel.prototype, "CreateDate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubSearchModel.RUNNO, String, true),
        __metadata("design:type", String)
    ], SubMoldSearchModel.prototype, "RunNo", void 0);
    SubMoldSearchModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldSearchModel")
    ], SubMoldSearchModel);
    return SubMoldSearchModel;
}());

//# sourceMappingURL=SubMoldSearchModel.js.map

/***/ }),
/* 391 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldMasterImageModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMoldMasterImageModel = /** @class */ (function () {
    function SubMoldMasterImageModel() {
        this.Photo = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDetailImageModel.PHOTO, String, true),
        __metadata("design:type", String)
    ], SubMoldMasterImageModel.prototype, "Photo", void 0);
    SubMoldMasterImageModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldMasterImageModel")
    ], SubMoldMasterImageModel);
    return SubMoldMasterImageModel;
}());

//# sourceMappingURL=SubMoldMasterImageModel.js.map

/***/ }),
/* 392 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMoldHistoryDetailModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_json2typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_json2typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__DescriptionMoldel__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SubMoldHistoryDetailModel = /** @class */ (function () {
    function SubMoldHistoryDetailModel() {
        this.MoldId = undefined;
        this.location = undefined;
        this.Plate = undefined;
        this.Remark = undefined;
        this.FullName = undefined;
        this.CreateDate = undefined;
        this.RunNo = undefined;
        this.Description = undefined;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDatailModel.MOLDID, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryDetailModel.prototype, "MoldId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDatailModel.LOCATION, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryDetailModel.prototype, "location", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDatailModel.PLATE, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryDetailModel.prototype, "Plate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDatailModel.REMARK, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryDetailModel.prototype, "Remark", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDatailModel.FULLNAME, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryDetailModel.prototype, "FullName", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDatailModel.CREATEDATE, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryDetailModel.prototype, "CreateDate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDatailModel.RUNNO, String, true),
        __metadata("design:type", String)
    ], SubMoldHistoryDetailModel.prototype, "RunNo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonProperty"])(__WEBPACK_IMPORTED_MODULE_1__shared_constant__["a" /* CONSTANT */].SurveySubHistoryDatailModel.DESCRIPTION, [__WEBPACK_IMPORTED_MODULE_2__DescriptionMoldel__["a" /* DescriptionMoldel */]], true),
        __metadata("design:type", Array)
    ], SubMoldHistoryDetailModel.prototype, "Description", void 0);
    SubMoldHistoryDetailModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0_json2typescript__["JsonObject"])("SubMoldHistoryDetailModel")
    ], SubMoldHistoryDetailModel);
    return SubMoldHistoryDetailModel;
}());

//# sourceMappingURL=SubMoldHistoryDetailModel.js.map

/***/ }),
/* 393 */,
/* 394 */,
/* 395 */,
/* 396 */,
/* 397 */,
/* 398 */,
/* 399 */,
/* 400 */,
/* 401 */,
/* 402 */,
/* 403 */,
/* 404 */,
/* 405 */,
/* 406 */,
/* 407 */,
/* 408 */,
/* 409 */,
/* 410 */,
/* 411 */,
/* 412 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(260);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_active_mobile_active_mobile__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_main_main__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_login_provider_login_provider__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__shared_constant__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, app, 
        // private ionicApp: IonicApp,
        toastCtrl, _loginProvider, _storageProvider) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.app = app;
        this.toastCtrl = toastCtrl;
        this._loginProvider = _loginProvider;
        this._storageProvider = _storageProvider;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_active_mobile_active_mobile__["a" /* ActiveMobilePage */];
        this.deviceAllow();
        platform.ready().then(function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                // Okay, so the platform is ready and our plugins are available.
                // Here you can do any higher level native things you might need.
                // CONSTANT.APP_LANGUAGE = (await this._storageProvider.getKey(CONSTANT.LANGURAGE)).toString() == null ? 'en' : (await this._storageProvider.getKey(CONSTANT.LANGURAGE)).toString();
                statusBar.styleDefault();
                splashScreen.hide();
                this.registerBackButton();
                return [2 /*return*/];
            });
        }); });
    }
    MyApp.prototype.registerBackButton = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var lastTimeBackPress, timePeriodToExit;
            return __generator(this, function (_a) {
                lastTimeBackPress = 0;
                timePeriodToExit = 2000;
                this.platform.registerBackButtonAction(function (fn, priority) {
                    // priority=100;
                    console.log('getActiveNavs::::', _this.app.getActiveNavs());
                    var nav = _this.app.getActiveNavs()[0];
                    // let activeModal = this.ionicApp._modalPortal.getActive() ||
                    //   this.ionicApp._toastPortal.getActive() ||
                    //   this.ionicApp._overlayPortal.getActive();
                    // console.log('activeModal:::',activeModal)
                    // if (activeModal) {
                    //   activeModal.dismiss();
                    //   return;
                    // }
                    var activeView = nav.getActive();
                    console.log('navControl:::', activeView);
                    if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
                        _this.platform.exitApp();
                    }
                    else {
                        var toast = _this.toastCtrl.create({
                            message: 'Press back again to exit App',
                            duration: 2000,
                            position: 'bottom'
                        });
                        toast.present();
                        lastTimeBackPress = new Date().getTime();
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    // async deviceAllow() {
    //   this.loginModel = this._loginProvider.loginModel;
    //   this._storageProvider.getKey(CONSTANT.ACTIVE_MOBILE).then(async val => {
    //     if(val) {
    //       if(await this._storageProvider.getKey(CONSTANT.USER_TYPE)) {
    //         this.rootPage = MainPage;
    //       }else{
    //         this.rootPage = LoginPage;
    //       }
    //     } else {
    //       await this._loginProvider.deviceAllow().catch( () => { this._loginProvider.submitErrorEvent(this._loginProvider.loginModel2); });
    //       if(this.loginModel.authenticateUser.deviceAllow.length > 0){
    //         this._storageProvider.setKey(CONSTANT.ACTIVE_MOBILE, 'true');
    //         this.rootPage = LoginPage;
    //       }
    //     }
    //   }).catch(() => {
    //     this.rootPage = ActiveMobilePage;
    //   })  
    // }
    MyApp.prototype.deviceAllow = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.loginModel = this._loginProvider.loginModel;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this._loginProvider.deviceAllow()];
                    case 2:
                        _a.sent();
                        if (this.loginModel.authenticateUser.deviceAllow.length > 0) {
                            this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].ACTIVE_MOBILE).then(function (val) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (!val) return [3 /*break*/, 2];
                                            return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].USER_TYPE)];
                                        case 1:
                                            if (_a.sent()) {
                                                this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_main_main__["a" /* MainPage */];
                                            }
                                            else {
                                                this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
                                            }
                                            return [3 /*break*/, 4];
                                        case 2: return [4 /*yield*/, this._loginProvider.deviceAllow().catch(function () { _this._loginProvider.submitErrorEvent(_this._loginProvider.loginModel2); })];
                                        case 3:
                                            _a.sent();
                                            if (this.loginModel.authenticateUser.deviceAllow.length > 0) {
                                                this._storageProvider.setKey(__WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].ACTIVE_MOBILE, 'true');
                                                this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
                                            }
                                            _a.label = 4;
                                        case 4: return [2 /*return*/];
                                    }
                                });
                            }); }).catch(function () {
                                _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_active_mobile_active_mobile__["a" /* ActiveMobilePage */];
                            });
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this._loginProvider.submitErrorEvent(this._loginProvider.loginModel2);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_8__providers_login_provider_login_provider__["a" /* LoginProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),
/* 413 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Ionic Blank\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  The world is your oyster.\n  <p>\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will be your guide.\n  </p>\n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),
/* 414 */,
/* 415 */,
/* 416 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_local_notifications__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var NotificationProvider = /** @class */ (function () {
    function NotificationProvider(localNotification, platform) {
        this.localNotification = localNotification;
        this.platform = platform;
        console.log('Hello NotificationProvider Provider');
    }
    NotificationProvider.prototype.showNotification = function (text) {
        if (text === void 0) { text = ''; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.localNotification.schedule({
                    text: text,
                    sound: this.platform.is('ios') ? "file://beep.caf" : "file://song.mp3"
                });
                return [2 /*return*/];
            });
        });
    };
    NotificationProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_local_notifications__["a" /* LocalNotifications */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */]])
    ], NotificationProvider);
    return NotificationProvider;
}());

//# sourceMappingURL=notification-provider.js.map

/***/ }),
/* 417 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SecurityProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__storage_provider_storage_provider__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var MINUTES_UNITL_AUTO_LOGOUT = 60; // in mins
var CHECK_INTERVAL = 10000; // in ms
var STORE_KEY = 'lastAction';
/*
  Generated class for the SecurityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var SecurityProvider = /** @class */ (function () {
    function SecurityProvider(app, alertCtrl, platform, menuCtrl, _storageProvider) {
        var _this = this;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.menuCtrl = menuCtrl;
        this._storageProvider = _storageProvider;
        console.log('Hello SecurityProvider Provider');
        this.platform.ready().then(function () {
            _this.check();
            _this.initListener();
            _this.initInterval();
            localStorage.setItem(STORE_KEY, Date.now().toString());
        });
    }
    SecurityProvider.prototype.getLastAction = function () {
        return parseInt(localStorage.getItem(STORE_KEY));
    };
    SecurityProvider.prototype.setLastAction = function (lastAction) {
        localStorage.setItem(STORE_KEY, lastAction.toString());
    };
    SecurityProvider.prototype.initListener = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                document.body.addEventListener('click', function () { return _this.reset(); });
                document.body.addEventListener('keydown', function () { return _this.reset(); });
                document.body.addEventListener('keyup', function () { return _this.reset(); });
                document.body.addEventListener('keypress', function () { return _this.reset(); });
                document.body.addEventListener('touchstart', function () { return _this.reset(); });
                return [2 /*return*/];
            });
        });
    };
    SecurityProvider.prototype.reset = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.setLastAction(Date.now());
                return [2 /*return*/];
            });
        });
    };
    SecurityProvider.prototype.initInterval = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                setInterval(function () {
                    _this.check();
                }, CHECK_INTERVAL);
                return [2 /*return*/];
            });
        });
    };
    SecurityProvider.prototype.check = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var nav, activeView, now, timeleft, diff, isTimeout;
            return __generator(this, function (_a) {
                nav = this.app.getActiveNavs()[0];
                activeView = nav.getActive();
                if ((activeView.name !== 'LoginPage') && (activeView.name !== 'ActiveMobilePage')) {
                    now = Date.now();
                    timeleft = this.getLastAction() + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
                    diff = timeleft - now;
                    isTimeout = diff < 0;
                    if (isTimeout) {
                        this._storageProvider.removeKey(STORE_KEY);
                        console.log('activeView:::', activeView);
                        this.alertCtrl.create({
                            enableBackdropDismiss: false,
                            title: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__["%message.title%"],
                            subTitle: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__["%session.timeout%"] : __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__["%session.timeout%"],
                            buttons: [{
                                    text: __WEBPACK_IMPORTED_MODULE_0__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_3__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_4__assets_i18n_en_json__["%btn.close%"],
                                    handler: function () {
                                        // activeView.getNav().popAll();
                                        // const views: any[] = this.app.getActiveNavs();
                                        // console.log('authen view:::',views)
                                        _this._storageProvider.clear();
                                        _this.menuCtrl.enable(false, 'authenticateduser');
                                        _this.menuCtrl.enable(false, 'authenticatedadmin');
                                        _this.menuCtrl.enable(false, 'authenticatedmaker');
                                        _this.app.getActiveNav().setRoot('LoginPage');
                                    }
                                }
                            ]
                        }).present();
                    }
                }
                return [2 /*return*/];
            });
        });
    };
    SecurityProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_5__storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], SecurityProvider);
    return SecurityProvider;
}());

//# sourceMappingURL=security-provider.js.map

/***/ })
],[271]);
//# sourceMappingURL=main.js.map