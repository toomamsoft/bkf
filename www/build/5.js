webpackJsonp([5],{

/***/ 439:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationPageModule", function() { return NotificationPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notification__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var NotificationPageModule = /** @class */ (function () {
    function NotificationPageModule() {
    }
    NotificationPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notification__["a" /* NotificationPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notification__["a" /* NotificationPage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__notification__["a" /* NotificationPage */]
            ]
        })
    ], NotificationPageModule);
    return NotificationPageModule;
}());

//# sourceMappingURL=notification.module.js.map

/***/ }),

/***/ 468:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_message_provider_message_provider__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_moldinout_provider_moldinout_provider__ = __webpack_require__(54);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NotificationPage = /** @class */ (function (_super) {
    __extends(NotificationPage, _super);
    function NotificationPage(navCtrl, events, _messageProvider, _moldinoutProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this._messageProvider = _messageProvider;
        _this._moldinoutProvider = _moldinoutProvider;
        _this.TransactionsMessage = [];
        return _this;
    }
    NotificationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotificationPage');
    };
    NotificationPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.messageModel = this._messageProvider.messageModel;
                this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
                return [2 /*return*/];
            });
        });
    };
    NotificationPage.prototype.ionViewDidEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.LoadDate()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    NotificationPage.prototype.LoadDate = function () {
        return __awaiter(this, void 0, void 0, function () {
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.messageModel = this._messageProvider.messageModel;
                        return [4 /*yield*/, this._messageProvider.getMessageDetail()];
                    case 1:
                        _a.sent();
                        this.TransactionsMessage = this.messageModel.getMessageDetail.detail.reduce(function (filtered, option) {
                            var position = filtered.findIndex(function (val) {
                                var firstDate = new Date(Date.parse(val.messageDate.replace('Z', '')));
                                var secondDate = new Date(Date.parse(option.messageDate.replace('Z', '')));
                                return __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].formatDateDDMMYYYY(firstDate) === __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].formatDateDDMMYYYY(secondDate);
                            });
                            if (position == -1) {
                                var obj = {
                                    messageDate: option.messageDate,
                                    transaction: [{
                                            MessageTime: option.messageDate,
                                            JobId: option.jobId,
                                            MoldId: option.moldId,
                                            messageID: option.messageId,
                                            messageSubject: option.messageSubject,
                                            messageDescription: option.messageDescription,
                                            readStatus: option.readStatus
                                        }]
                                };
                                filtered.push(obj);
                            }
                            else {
                                var obj = {
                                    MessageTime: option.messageDate,
                                    JobId: option.jobId,
                                    MoldId: option.moldId,
                                    messageID: option.messageId,
                                    messageSubject: option.messageSubject,
                                    messageDescription: option.messageDescription,
                                    readStatus: option.readStatus
                                };
                                filtered[position].transaction.push(obj);
                            }
                            return filtered;
                        }, []);
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _a.sent();
                        this._messageProvider.submitErrorEvent(this._messageProvider.messageModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    NotificationPage.prototype.readMessage = function (itemSelected) {
        return __awaiter(this, void 0, void 0, function () {
            var ex_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.moldinoutModel.JobId = itemSelected.JobId;
                        this.moldinoutModel.MoldId = itemSelected.MoldId;
                        return [4 /*yield*/, this._moldinoutProvider.getNewMoldOut()];
                    case 1:
                        _a.sent();
                        this.navCtrl.push('MoldinoutHistoryPage');
                        return [3 /*break*/, 3];
                    case 2:
                        ex_2 = _a.sent();
                        this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    NotificationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-notification',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/notification/notification.html"*/'<!--\n  Generated template for the NotificationPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>Notification</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list *ngFor="let items of TransactionsMessage;">\n    <ion-item-divider>\n      <ion-label>\n        {{ items.messageDate | datetime : \'dateNow\' }}\n      </ion-label>\n    </ion-item-divider>\n    <div *ngFor="let itemSub of items.transaction">\n      <ion-item (click)="readMessage(itemSub)">\n        <ion-icon item-left><p>{{ itemSub.MessageTime | datetime: \'time\'}}</p></ion-icon>\n        <h5>{{ itemSub.messageSubject}}</h5>\n        <p>{{ itemSub.messageDescription}}</p>\n      </ion-item>\n    </div>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/notification/notification.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__providers_message_provider_message_provider__["a" /* MessageProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_moldinout_provider_moldinout_provider__["a" /* MoldinoutProvider */]])
    ], NotificationPage);
    return NotificationPage;
}(__WEBPACK_IMPORTED_MODULE_0__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=notification.js.map

/***/ })

});
//# sourceMappingURL=5.js.map