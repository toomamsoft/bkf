webpackJsonp([20],{

/***/ 426:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MakerMenuPageModule", function() { return MakerMenuPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maker_menu__ = __webpack_require__(456);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MakerMenuPageModule = /** @class */ (function () {
    function MakerMenuPageModule() {
    }
    MakerMenuPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__maker_menu__["a" /* MakerMenuPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__maker_menu__["a" /* MakerMenuPage */]),
            ],
        })
    ], MakerMenuPageModule);
    return MakerMenuPageModule;
}());

//# sourceMappingURL=maker-menu.module.js.map

/***/ }),

/***/ 456:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MakerMenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_MoldMakerModel__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_moldmaker_provider_moldmaker_provider__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_scan_scan__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_constant__ = __webpack_require__(3);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the MakerMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MakerMenuPage = /** @class */ (function (_super) {
    __extends(MakerMenuPage, _super);
    function MakerMenuPage(navCtrl, navParams, events, alertCtrl, _moldmakerProvider, _scanProvider, _storageProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.events = events;
        _this.alertCtrl = alertCtrl;
        _this._moldmakerProvider = _moldmakerProvider;
        _this._scanProvider = _scanProvider;
        _this._storageProvider = _storageProvider;
        _this.fullName = '';
        return _this;
    }
    MakerMenuPage.prototype.ionViewWillEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('ionViewDidLoad MakerMenuPage');
                        this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this._moldmakerProvider.getListMold()];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        ex_1 = _a.sent();
                        this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MakerMenuPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_7__shared_constant__["a" /* CONSTANT */].USER_NAME)];
                    case 1:
                        _a.fullName = _b.sent();
                        this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
                        return [2 /*return*/];
                }
            });
        });
    };
    MakerMenuPage.prototype.doRefresh = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var ex_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, 3, 4]);
                        this._moldmakerProvider.moldmakerModel = new __WEBPACK_IMPORTED_MODULE_3__models_MoldMakerModel__["a" /* MoldMakerModel */]();
                        this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
                        return [4 /*yield*/, this._moldmakerProvider.getListMold()];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 2:
                        ex_2 = _a.sent();
                        this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
                        return [3 /*break*/, 4];
                    case 3:
                        event.complete();
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MakerMenuPage.prototype.viewHistory = function (jobid) {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
                        this.moldMakerModel.JobId = jobid;
                        return [4 /*yield*/, this._moldmakerProvider.getViewMoldFromBKF()];
                    case 1:
                        _a.sent();
                        this.navCtrl.push('MoldmakerHistoryPage');
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MakerMenuPage.prototype.ReceiveMold = function (jobid, moldid) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var txtScanQR_1, ex_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        return [4 /*yield*/, this._scanProvider.scan().then(function (val) {
                                txtScanQR_1 = val.toString();
                            })];
                    case 1:
                        _a.sent();
                        if (!txtScanQR_1) return [3 /*break*/, 4];
                        this.moldMakerModel.MoldId = txtScanQR_1.split(/\|/)[0] || '';
                        this.moldMakerModel.AssetId = txtScanQR_1.split(/\|/)[1] || '';
                        this.moldMakerModel.CustNo = txtScanQR_1.split(/\|/)[2] || '';
                        this.moldMakerModel.PartNo = txtScanQR_1.split(/\|/)[3] || '';
                        this.moldMakerModel.PartName = txtScanQR_1.split(/\|/)[4] || '';
                        if (!(moldid != this.moldMakerModel.MoldId)) return [3 /*break*/, 2];
                        this.alertCtrl.create({
                            title: 'Alert',
                            message: 'Mold ID: ' + this.moldMakerModel.MoldId + '  Not match',
                            buttons: ['OK']
                        }).present();
                        return [2 /*return*/];
                    case 2:
                        this.moldMakerModel.JobId = jobid;
                        return [4 /*yield*/, this._moldmakerProvider.getViewMoldFromBKF().catch(function () {
                                _this._moldmakerProvider.submitErrorEvent(_this._moldmakerProvider.moldmakerModel2);
                            })];
                    case 3:
                        _a.sent();
                        this.navCtrl.push('MoldmakerReceivePage');
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        ex_3 = _a.sent();
                        console.log(ex_3);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    MakerMenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-maker-menu',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/maker-menu/maker-menu.html"*/'<!--\n  Generated template for the MakerMenuPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Maker Main Menu</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <p style="text-align: center; vertical-align: middle; font-family: sans-serif"><b>Mold Maker Name: {{ fullName }}</b></p>\n  \n    <ion-refresher (ionRefresh)="doRefresh($event)">\n      <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n    \n    <div>\n      <ion-row>\n        <ion-col col-4 style="text-align:center; border:1px solid; color: #f39033; background-color: #f39033;vertical-align: middle; height: 30px;">\n          <a style="font-family: sans-serif;font-size: 0.8em;color: #ffffff">Waiting Receive</a>\n        </ion-col>\n        <ion-col col-4 style="text-align:center; border:1px solid; color: #e8eb50; background-color: #e8eb50;vertical-align: middle; height: 30px;">\n          <a style="font-family: sans-serif; font-size: 0.8em;color: #ffffff">Inprogress</a>\n        </ion-col>\n        <ion-col col-4 style="text-align:center; border:1px solid; color: #68bb81; background-color: #68bb81;vertical-align: middle; height: 30px;">\n          <a style="font-family: sans-serif; font-size: 0.8em; color: #ffffff">Completed</a>\n        </ion-col>\n      </ion-row>\n    </div>\n    \n    <ion-list >\n      <div *ngFor="let items of moldMakerModel?.getListMode?.getListMold">\n        <ion-item *ngIf="items?.status == \'\'" color="receive">\n          <h3 style="font-family: sans-serif">Maker MoldID: {{ items?.makerid }}</h3>\n          <h3 style="font-family: sans-serif">MoldID: {{ items?.moldid }}</h3>\n          <h3 style="font-family: sans-serif">Cust Part: {{ items?.custno }}</h3>\n          <h3 style="font-family: sans-serif">BKF Part: {{ items?.partno }}</h3>\n          <h3 style="font-family: sans-serif">Actual Out: {{ items?.actual }}</h3>\n          <h3 style="font-family: sans-serif">Plan In: {{ items?.planin }}</h3>\n          <button ion-button clear item-end (click)="viewHistory(items?.jobid)" *ngIf="(items?.status != \'Receive\') && (items?.status != \'\')">View</button>\n          <button ion-button clear item-end (click)="ReceiveMold(items?.jobid, items?.moldid)" *ngIf="(items?.status == \'\')">Scan Qr</button>\n        </ion-item>\n        <ion-item *ngIf="items.status == \'Inprogress\' || items.status == \'New\'" color="yellow">\n          <h3 style="font-family: sans-serif">Maker MoldID: {{ items?.makerid }}</h3>\n          <h3 style="font-family: sans-serif">MoldID: {{ items?.moldid }}</h3>\n          <h3 style="font-family: sans-serif">Cust Part: {{ items?.custno }}</h3>\n          <h3 style="font-family: sans-serif">BKF Part: {{ items?.partno }}</h3>\n          <h3 style="font-family: sans-serif">Actual Out: {{ items?.actual }}</h3>\n          <h3 style="font-family: sans-serif">Plan In: {{ items?.planin }}</h3>\n          <button ion-button clear item-end (click)="viewHistory(items?.jobid)" *ngIf="(items?.status != \'Receive\') && (items?.status != \'\')">View</button>\n          <button ion-button clear item-end (click)="ReceiveMold(items?.jobid, items?.moldid)" *ngIf="(items?.status == \'\')">Scan Qr</button>\n        </ion-item>\n        <ion-item *ngIf="items.status == \'Completed\'" color="completed">\n          <h3 style="font-family: sans-serif">Maker MoldID: {{ items?.makerid }}</h3>\n          <h3 style="font-family: sans-serif">MoldID: {{ items?.moldid }}</h3>\n          <h3 style="font-family: sans-serif">Cust Part: {{ items?.custno }}</h3>\n          <h3 style="font-family: sans-serif">BKF Part: {{ items?.partno }}</h3>\n          <h3 style="font-family: sans-serif">Actual Out: {{ items?.actual }}</h3>\n          <h3 style="font-family: sans-serif">Plan In: {{ items?.planin }}</h3>\n          <button ion-button clear item-end (click)="viewHistory(items?.jobid)" *ngIf="(items?.status != \'Receive\') && (items?.status != \'\')">View</button>\n          <button ion-button clear item-end (click)="ReceiveMold(items?.jobid, items?.moldid)" *ngIf="(items?.status == \'\')">Scan Qr</button>\n        </ion-item>\n      </div>\n    </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/maker-menu/maker-menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_moldmaker_provider_moldmaker_provider__["a" /* MoldmakerProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_scan_scan__["a" /* ScanProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_storage_provider_storage_provider__["a" /* StorageProvider */]])
    ], MakerMenuPage);
    return MakerMenuPage;
}(__WEBPACK_IMPORTED_MODULE_0__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=maker-menu.js.map

/***/ })

});
//# sourceMappingURL=20.js.map