webpackJsonp([15],{

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoldinoutHistoryPageModule", function() { return MoldinoutHistoryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__moldinout_history__ = __webpack_require__(476);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var MoldinoutHistoryPageModule = /** @class */ (function () {
    function MoldinoutHistoryPageModule() {
    }
    MoldinoutHistoryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_history__["a" /* MoldinoutHistoryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__moldinout_history__["a" /* MoldinoutHistoryPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_history__["a" /* MoldinoutHistoryPage */]
            ]
        })
    ], MoldinoutHistoryPageModule);
    return MoldinoutHistoryPageModule;
}());

//# sourceMappingURL=moldinout-history.module.js.map

/***/ }),

/***/ 476:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldinoutHistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_moldinout_provider_moldinout_provider__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_MoldInOutModel__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_moldmaker_provider_moldmaker_provider__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_storage_provider_storage_provider__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_scan_scan__ = __webpack_require__(55);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};













/**
 * Generated class for the MoldinoutHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MoldinoutHistoryPage = /** @class */ (function (_super) {
    __extends(MoldinoutHistoryPage, _super);
    function MoldinoutHistoryPage(navCtrl, events, alertCtrl, 
        // private modalCtrl: ModalController,
        formBuilder, actionsheetCtrl, _moldinoutProvider, _moldmakerProvider, _storageProvider, _scanProvider, platform) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this.alertCtrl = alertCtrl;
        _this.formBuilder = formBuilder;
        _this.actionsheetCtrl = actionsheetCtrl;
        _this._moldinoutProvider = _moldinoutProvider;
        _this._moldmakerProvider = _moldmakerProvider;
        _this._storageProvider = _storageProvider;
        _this._scanProvider = _scanProvider;
        _this.platform = platform;
        _this.Mode = 'Out';
        _this.limit = 5;
        _this.Latitude = 0.0000000000000000;
        _this.Longitude = 0.0000000000000000;
        _this.approveOut = false;
        _this.approveIn = false;
        _this.confirmOut = false;
        _this.confirmIn = false;
        _this.IP = __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].IPAddress;
        _this.form = _this.formBuilder.group({
            MoldId: [{ value: null, disabled: true }],
            MoldMaker: [{ value: null, disabled: true }],
            AssetId: [{ value: null, disabled: true }],
            CustNo: [{ value: null, disabled: true }],
            PartNo: [{ value: null, disabled: true }],
            PartName: [{ value: null, disabled: true }],
            Remark: [{ value: null, disabled: true }],
            MakerId: [{ value: null, disabled: true }],
            ReasonId: [{ value: null, disabled: true }],
            PlanIn: [{ value: null, disabled: true }],
            PlanOut: [{ value: null, disabled: true }]
        });
        return _this;
    }
    MoldinoutHistoryPage.prototype.ionViewDidLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var AssetId, CustNo, PartNo, PartName, i, ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('ionViewDidLoad MoldinoutHistoryPage');
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, Promise.all([this._moldinoutProvider.getSetting(), this._moldinoutProvider.getApprove()])];
                    case 2:
                        _a.sent();
                        AssetId = '';
                        CustNo = '';
                        PartNo = '';
                        PartName = '';
                        this.form.controls['MoldId'].setValue(this.moldinoutModel.MoldId);
                        this.form.controls['MoldMaker'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].makerMold);
                        for (i = 0; i < this.moldinoutModel.getNewMoldInOut.inoutMold[0].description.length; i++) {
                            if (i == 0) {
                                AssetId = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].AssetId;
                                CustNo = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].CustNo;
                                PartNo = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartNo;
                                PartName = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartName;
                            }
                            else {
                                AssetId += '\n' + this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].AssetId;
                                CustNo += '\n' + this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].CustNo;
                                PartNo += '\n' + this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartNo;
                                PartName += '\n' + this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartName;
                            }
                        }
                        this.form.controls['AssetId'].setValue(AssetId);
                        this.form.controls['CustNo'].setValue(CustNo);
                        this.form.controls['PartNo'].setValue(PartNo);
                        this.form.controls['PartName'].setValue(PartName);
                        this.form.controls['Remark'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].remarkNew);
                        this.form.controls['MakerId'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].makerId);
                        this.form.controls['ReasonId'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].reason);
                        this.form.controls['PlanIn'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].planIn);
                        this.form.controls['PlanOut'].setValue(this.moldinoutModel.getNewMoldInOut.inoutMold[0].planIn);
                        if (this.moldinoutModel.getNewMoldInOut.inoutMold[0].makerStatus == 'Completed') {
                            this.Mode = 'In';
                        }
                        if (this.moldinoutModel.getApprove.approveIn.filter(function (val) { return val.status === 'Waiting'; }).length > 0 || this.moldinoutModel.getNewMoldInOut.inoutMold[0].status == 'Completed') {
                            this.approveIn = true;
                        }
                        if (this.moldinoutModel.getApprove.approveOut.filter(function (val) { return val.status === 'Waiting'; }).length > 0 &&
                            this.moldinoutModel.getApprove.approveOut.filter(function (val) { return val.status === 'Reject'; }).length <= 0 &&
                            this.moldinoutModel.getApprove.approveOut.filter(function (val) { return val.status === 'ApproveOut'; }).length <= this.moldinoutModel.getApprove.approveOut.length) {
                            this.approveOut = true;
                        }
                        if (this.moldinoutModel.getNewMoldInOut.inoutMold[0].status === 'ApproveOut' &&
                            this.moldinoutModel.getApprove.approveOut.filter(function (val) { return val.status === 'ApproveOut'; }).length <= this.moldinoutModel.getApprove.approveOut.length) {
                            this.confirmOut = true;
                        }
                        console.log(this.approveIn, this.approveOut, this.confirmOut);
                        return [3 /*break*/, 4];
                    case 3:
                        ex_1 = _a.sent();
                        this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutHistoryPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
                this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
                return [2 /*return*/];
            });
        });
    };
    MoldinoutHistoryPage.prototype.ngOnDestroy = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this._moldinoutProvider.moldInOutModel = new __WEBPACK_IMPORTED_MODULE_4__models_MoldInOutModel__["a" /* MoldInOutModel */]();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutHistoryPage.prototype.tapViewPhoto = function (photo) {
        return __awaiter(this, void 0, void 0, function () {
            var view;
            return __generator(this, function (_a) {
                view = this.IP + "/inout/showImage?filename='=" + photo;
                console.log(photo);
                __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].viewImages(view);
                return [2 /*return*/];
            });
        });
    };
    MoldinoutHistoryPage.prototype.viewPdf = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].viewFilePDF(this.moldinoutModel.getNewMoldInOut.inoutMold[0].pdf)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutHistoryPage.prototype.openLaunchNavigator = function (latitude, longitude) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_7__shared_utilities__["a" /* Utilities */].openGoogleMapApplication(latitude, longitude)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutHistoryPage.prototype.viewHistory = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
                this.moldMakerModel.JobId = this.moldinoutModel.getNewMoldInOut.inoutMold[0].jobId;
                this.navCtrl.push('MoldinoutViewMoldmakerPage');
                return [2 /*return*/];
            });
        });
    };
    MoldinoutHistoryPage.prototype.presentActionSheet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var actionSheet;
            return __generator(this, function (_a) {
                actionSheet = this.actionsheetCtrl.create({
                    title: 'Albums',
                    cssClass: 'action-sheets-basic-page',
                    buttons: [
                        {
                            text: 'Camera',
                            role: 'destructive',
                            icon: !this.platform.is('ios') ? 'camera' : null,
                            handler: function () {
                                _this.openCamera();
                            }
                        },
                        {
                            text: 'Album',
                            icon: !this.platform.is('ios') ? 'albums' : null,
                            handler: function () {
                                _this.getImageFromGallery();
                            }
                        },
                        {
                            text: 'Cancel',
                            role: 'cancel',
                            icon: !this.platform.is('ios') ? 'close' : null,
                            handler: function () {
                                console.log('Cancel clicked');
                            }
                        }
                    ]
                });
                actionSheet.present();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutHistoryPage.prototype.openCamera = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var alert_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.moldinoutModel.Images.length >= this.limit) {
                            alert_1 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.photo%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.photo%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_1.present();
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this._scanProvider.getImageFromCamere().then(function (val) {
                                if (val && val != 'No Image Selected' && val != 'cordova_not_available') {
                                    _this.moldinoutModel.Images.push({ photo: val.toString(), latitude: _this.Latitude, longitude: _this.Longitude });
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutHistoryPage.prototype.getImageFromGallery = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var images, alert_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.moldinoutModel.Images.length >= this.limit) {
                            alert_2 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.photo%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.photo%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_2.present();
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this._scanProvider.getImageFormAlbum(this.limit - this.moldinoutModel.Images.length).then(function (val) {
                                if (val && val != 'cordova_not_available') {
                                    images = val;
                                    for (var i = 0; i < images.length; i++) {
                                        _this.moldinoutModel.Images.push({ photo: images[i], latitude: _this.Latitude, longitude: _this.Longitude });
                                    }
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutHistoryPage.prototype.deletePhoto = function (index) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var confirm;
            return __generator(this, function (_a) {
                confirm = this.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.title%"],
                    subTitle: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.delete.image%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.delete.image%"],
                    buttons: [
                        {
                            text: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%btn.logout.no%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%btn.logout.no%"],
                            handler: function () {
                                console.log('Disagree clicked');
                            }
                        }, {
                            text: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%btn.logout.yes%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%btn.logout.yes%"],
                            handler: function () {
                                _this.moldinoutModel.Images.splice(index, 1);
                            }
                        }
                    ]
                });
                confirm.present();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutHistoryPage.prototype.approveTypeOut = function () {
        return __awaiter(this, void 0, void 0, function () {
            var CheckApprove, i, _a, tmp, tmp;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        CheckApprove = false;
                        i = 0;
                        _b.label = 1;
                    case 1:
                        if (!(i < this.moldinoutModel.getApprove.approveOut.length)) return [3 /*break*/, 4];
                        _a = this.moldinoutModel.getApprove.approveOut[i].userType;
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].USER_TYPE)];
                    case 2:
                        if (_a == (_b.sent())) {
                            if (i != 0) {
                                tmp = this.moldinoutModel.getApprove.approveOut[i - 1];
                                if (tmp.status == 'ApproveOut') {
                                    if (this.moldinoutModel.getApprove.approveOut[i].status == 'Waiting') {
                                        CheckApprove = true;
                                        this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveOut[i].jobId;
                                        this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
                                        this.moldinoutModel.ApproveType = 'out';
                                        this.moldinoutModel.ApproveStatus = 'ApproveOut';
                                    }
                                    else {
                                        CheckApprove = false;
                                    }
                                }
                            }
                            else {
                                tmp = this.moldinoutModel.getApprove.approveOut[i];
                                if (tmp.status == 'ApproveOut') {
                                    CheckApprove = false;
                                }
                                else if (tmp.status == 'Waiting') {
                                    CheckApprove = true;
                                    this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveOut[i].jobId;
                                    this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
                                    this.moldinoutModel.ApproveType = 'out';
                                    this.moldinoutModel.ApproveStatus = 'ApproveOut';
                                }
                            }
                        }
                        _b.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4:
                        if (!CheckApprove) {
                            this.alertCtrl.create({
                                title: 'แจ้งเตือน',
                                message: 'คุณไม่มีสิทธิในการอนุมัติ',
                                buttons: ["OK"]
                            }).present();
                            return [2 /*return*/];
                        }
                        this.navCtrl.push('MoldinoutApprovePage');
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutHistoryPage.prototype.rejectTypeOut = function () {
        return __awaiter(this, void 0, void 0, function () {
            var CheckApprove, i, _a, tmp, tmp;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        CheckApprove = false;
                        i = 0;
                        _b.label = 1;
                    case 1:
                        if (!(i < this.moldinoutModel.getApprove.approveOut.length)) return [3 /*break*/, 4];
                        _a = this.moldinoutModel.getApprove.approveOut[i].userType;
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].USER_TYPE)];
                    case 2:
                        if (_a == (_b.sent())) {
                            if (i != 0) {
                                tmp = this.moldinoutModel.getApprove.approveOut[i - 1];
                                if (tmp.status == 'ApproveOut' || tmp.status == 'Reject') {
                                    if (this.moldinoutModel.getApprove.approveOut[i].status == 'Waiting') {
                                        CheckApprove = true;
                                        this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveOut[i].jobId;
                                        this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
                                        this.moldinoutModel.ApproveType = 'out';
                                        this.moldinoutModel.ApproveStatus = 'Reject';
                                    }
                                    else {
                                        CheckApprove = false;
                                    }
                                }
                            }
                            else {
                                tmp = this.moldinoutModel.getApprove.approveOut[i];
                                if (tmp.status == 'ApproveOut' || tmp.status == 'Reject') {
                                    CheckApprove = false;
                                }
                                else if (tmp.status == 'Waiting') {
                                    CheckApprove = true;
                                    this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveOut[i].jobId;
                                    this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
                                    this.moldinoutModel.ApproveType = 'out';
                                    this.moldinoutModel.ApproveStatus = 'Reject';
                                }
                            }
                        }
                        _b.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4:
                        if (!CheckApprove) {
                            this.alertCtrl.create({
                                title: 'แจ้งเตือน',
                                message: 'คุณไม่มีสิทธิในการอนุมัติ',
                                buttons: ["OK"]
                            }).present();
                            return [2 /*return*/];
                        }
                        this.navCtrl.push('MoldinoutApprovePage');
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutHistoryPage.prototype.rejectConfirmOut = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.moldinoutModel.ConfirmStatus = 'N';
                this.navCtrl.push('MoldinoutConfirmInPage');
                return [2 /*return*/];
            });
        });
    };
    MoldinoutHistoryPage.prototype.approveTypeIn = function () {
        return __awaiter(this, void 0, void 0, function () {
            var CheckApprove, i, _a, tmp, tmp;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        CheckApprove = false;
                        i = 0;
                        _b.label = 1;
                    case 1:
                        if (!(i < this.moldinoutModel.getApprove.approveIn.length)) return [3 /*break*/, 4];
                        _a = this.moldinoutModel.getApprove.approveIn[i].userType;
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].USER_TYPE)];
                    case 2:
                        if (_a == (_b.sent())) {
                            if (i != 0) {
                                tmp = this.moldinoutModel.getApprove.approveIn[i - 1];
                                if (tmp.status == 'ApproveIn' || tmp.status == 'Reject') {
                                    if (this.moldinoutModel.getApprove.approveIn[i].status == 'Waiting') {
                                        CheckApprove = true;
                                        this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveIn[i].jobId;
                                        this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
                                        this.moldinoutModel.ApproveType = 'in';
                                        this.moldinoutModel.ApproveStatus = 'ApproveIn';
                                    }
                                    else {
                                        CheckApprove = false;
                                    }
                                }
                            }
                            else {
                                tmp = this.moldinoutModel.getApprove.approveIn[i];
                                if (tmp.status == 'ApproveIn' || tmp.status == 'Reject') {
                                    CheckApprove = false;
                                }
                                else if (tmp.status == 'Waiting') {
                                    CheckApprove = true;
                                    this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveIn[i].jobId;
                                    this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
                                    this.moldinoutModel.ApproveType = 'in';
                                    this.moldinoutModel.ApproveStatus = 'ApproveIn';
                                }
                            }
                        }
                        _b.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4:
                        if (!CheckApprove) {
                            this.alertCtrl.create({
                                title: 'แจ้งเตือน',
                                message: 'คุณไม่มีสิทธิในการอนุมัติ',
                                buttons: ["OK"]
                            }).present();
                            return [2 /*return*/];
                        }
                        this.navCtrl.push('MoldinoutApprovePage');
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutHistoryPage.prototype.rejectTypeIn = function () {
        return __awaiter(this, void 0, void 0, function () {
            var CheckApprove, i, _a, tmp, tmp;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        CheckApprove = false;
                        i = 0;
                        _b.label = 1;
                    case 1:
                        if (!(i < this.moldinoutModel.getApprove.approveIn.length)) return [3 /*break*/, 4];
                        _a = this.moldinoutModel.getApprove.approveIn[i].userType;
                        return [4 /*yield*/, this._storageProvider.getKey(__WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].USER_TYPE)];
                    case 2:
                        if (_a == (_b.sent())) {
                            if (i != 0) {
                                tmp = this.moldinoutModel.getApprove.approveIn[i - 1];
                                if (tmp.status == 'ApproveIn' || tmp.status == 'Reject') {
                                    if (this.moldinoutModel.getApprove.approveIn[i].status == 'Waiting') {
                                        CheckApprove = true;
                                        this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveIn[i].jobId;
                                        this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
                                        this.moldinoutModel.ApproveType = 'in';
                                        this.moldinoutModel.ApproveStatus = 'Reject';
                                    }
                                    else {
                                        CheckApprove = false;
                                    }
                                }
                            }
                            else {
                                tmp = this.moldinoutModel.getApprove.approveIn[i];
                                if (tmp.status == 'ApproveIn' || tmp.status == 'Reject') {
                                    CheckApprove = false;
                                }
                                else if (tmp.status == 'Waiting') {
                                    CheckApprove = true;
                                    this.moldinoutModel.JobId = this.moldinoutModel.getApprove.approveIn[i].jobId;
                                    this.moldinoutModel.MoldId = this.form.controls['MoldId'].value;
                                    this.moldinoutModel.ApproveType = 'in';
                                    this.moldinoutModel.ApproveStatus = 'Reject';
                                }
                            }
                        }
                        _b.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4:
                        if (!CheckApprove) {
                            this.alertCtrl.create({
                                title: 'แจ้งเตือน',
                                message: 'คุณไม่มีสิทธิในการอนุมัติ',
                                buttons: ["OK"]
                            }).present();
                            return [2 /*return*/];
                        }
                        this.navCtrl.push('MoldinoutApprovePage');
                        return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutHistoryPage.prototype.ConfirmOut = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.navCtrl.push('MoldinoutConfirmOutPage');
                return [2 /*return*/];
            });
        });
    };
    MoldinoutHistoryPage.prototype.ConfirmIn = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (this.moldinoutModel.Images.length <= 0) {
                    this.alertCtrl.create({
                        title: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_10__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_11__assets_i18n_en_json__["%message.title%"],
                        subTitle: 'จำนวนรูปภาพไม่ครบ กรุณาตรวจสอบอีกครั้ง',
                        buttons: ['OK']
                    }).present();
                    return [2 /*return*/];
                }
                this.navCtrl.push('MoldinoutConfirmInPage');
                return [2 /*return*/];
            });
        });
    };
    MoldinoutHistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-moldinout-history',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-history/moldinout-history.html"*/'<!--\n  Generated template for the MoldinoutHistoryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>Mold In/Out</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]="form">\n    <ion-list>\n      <ion-item>\n        <ion-label fixed>Mold ID</ion-label>\n        <ion-input formControlName="MoldId" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>MoldMakerID</ion-label>\n        <ion-input formControlName="MoldMaker" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>AssetID</ion-label>\n        <ion-textarea formControlName="AssetId" placeholder="AssetID" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>CustNo</ion-label>\n        <ion-textarea formControlName="CustNo" placeholder="CustNo" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>PartNo</ion-label>\n        <ion-textarea formControlName="PartNo" placeholder="PartNo" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>PartName</ion-label>\n        <ion-textarea formControlName="PartName" placeholder="PartName" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label>MoldMaker</ion-label>\n        <ion-select interface="action-sheet" formControlName="MakerId">\n          <ion-option value="">กรุณาเลือก MoldMaker</ion-option>\n          <ion-option *ngFor="let items of moldinoutModel?.getSetting?.makerId" [value]="items?.id">{{ items?.description }}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label>Plan Out</ion-label>\n        <ion-datetime pickerFormat="DD/MM/YYYY HH:mm" displayFormat="DD/MM/YYYY HH:mm" formControlName="PlanOut"></ion-datetime>\n      </ion-item>\n      <ion-item>\n        <ion-label>Plan In</ion-label>\n        <ion-datetime pickerFormat="DD/MM/YYYY HH:mm" displayFormat="DD/MM/YYYY HH:mm" formControlName="PlanIn"></ion-datetime>\n      </ion-item>\n      <ion-item>\n        <ion-label>Reason</ion-label>\n        <ion-select interface="action-sheet" formControlName="ReasonId">\n          <ion-option value="">กรุณาเลือก MoldMaker</ion-option>\n          <ion-option *ngFor="let items of moldinoutModel?.getSetting?.reason" [value]="items?.id">{{ items?.description }}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Remark</ion-label>\n        <ion-textarea formControlName="Remark" placeholder="Remark" rows="5" autosize></ion-textarea>\n      </ion-item>\n    </ion-list>\n  </form>\n\n  <ion-segment [(ngModel)]="Mode">\n    <ion-segment-button value="Out">\n      <h6>OUT</h6>\n    </ion-segment-button>\n    <ion-segment-button value="In" [disabled]="(moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.makerStatus == \'Completed\') ? false : true">\n      <h6>IN</h6>\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]="Mode">\n    <ion-list *ngSwitchCase="\'Out\'">\n      <div padding>\n        <button ion-button full color="primary" (click)="viewPdf()"\n          *ngIf="moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.print == \'A\' && moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.status != \'Completed\' && moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.makerStatus == \'\'">\n          Reprint\n        </button >\n        <button ion-button full color="secondary" (click)="viewHistory()" *ngIf="moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.status != \'Completed\' && moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.makerStatus != \'\'">\n          View MoldMaker Update\n        </button>\n      </div>\n      <div *ngIf="moldinoutModel?.getNewMoldInOut?.imagesOut?.length > 0">\n        <accordion-list\n          [title]="\'Picture Mold Out\'"\n          textColor="#FFF"\n          hasMargin="false"\n          headerColor="#f4f4f4"\n          [expanded]="true">\n          <ion-grid>\n            <ion-row>\n              <ion-col col-6 *ngFor="let photo of moldinoutModel?.getNewMoldInOut?.imagesOut; let id = index">\n                <ion-card class="card-background-page">\n                  <img style="height: 150px" src="{{IP}}/inout/showImage?filename={{photo.PhotoName}}" (tap)="tapViewPhoto(photo?.PhotoName)" alt="" />\n                  <ion-card-content>\n                    <p class="gps" (tap)="openLaunchNavigator(photo?.Latiture ,photo?.Longiture)">{{ photo?.Latiture }}, {{ photo?.Longiture }}</p>\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n            </ion-row>\n          </ion-grid> \n        </accordion-list>\n      </div>\n\n      <div *ngIf="moldinoutModel?.getNewMoldInOut?.imagesTransport?.length > 0">\n        <accordion-list\n          [title]="\'Picture Confirm Out\'"\n          textColor="#FFF"\n          hasMargin="false"\n          headerColor="#f4f4f4"\n          [expanded]="true">\n          <ion-grid style="padding:0px; margin:0px;">\n            <ion-row>\n              <ion-col col-6 *ngFor="let photo of moldinoutModel?.getNewMoldInOut?.imagesTransport; let i = index">\n                <pre style="text-align: center;" *ngIf="i == 0">รูปทะเบียนรถ</pre>\n                <pre style="text-align: center;" *ngIf="i == 1">รูปใบขับขี่</pre>\n                <pre style="text-align: center;" *ngIf="i == 2">รูปหลังรถ</pre>\n                <pre style="text-align: center;" *ngIf="i == 3"> อื่นๆ</pre>\n                <ion-card class="card-background-page">\n                  <img style="height: 150px" src="{{IP}}/inout/showImage?filename={{photo.PhotoName}}" (tap)="tapViewPhoto(photo?.PhotoName)" alt="" />\n                  <ion-card-content>\n                    <p class="gps" (tap)="openLaunchNavigator(photo?.Latiture ,photo?.Longiture)">{{ photo?.Latiture }}, {{ photo?.Longiture }}</p>\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </accordion-list>\n      </div>\n\n      <div *ngIf="moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.nameConfirmout !=\'\'" padding style="padding-top: 0px; padding-bottom: 5px;">\n        <pre style="text-align: left; font-family: sans-serif;">Confirm Out : {{ moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.nameConfirmout }}</pre>\n        <pre style="text-align: left; font-family: sans-serif;">DateTime Out : {{ moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.confirmDate | datetime: \'dateAndTime\' }}</pre>\n        <pre style="text-align: left; font-family: sans-serif;">Remark Out : {{ moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.remarkOut }}</pre>\n      </div>\n\n      <mold-in-out-approve [taggedApprove]="moldinoutModel?.getApprove?.approveOut"></mold-in-out-approve>\n      <div *ngIf="approveOut">\n        <ion-item no-padding class="btn-setting-fav">\n          <ion-row class="two-col-button">\n            <ion-col col-6>\n              <button no-margin ion-button full large col-12 class="cancel-btn" (click)="rejectTypeOut()">Reject</button>\n            </ion-col>\n            <ion-col col-6>\n              <button no-margin ion-button full large col-12 (click)="approveTypeOut()">Approve</button>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n      </div>\n\n      <div *ngIf="confirmOut">\n        <ion-item no-padding class="btn-setting-fav">\n          <ion-row class="two-col-button">\n            <ion-col col-6>\n              <button no-margin ion-button full large col-12 class="cancel-btn" (click)="rejectConfirmOut()">Reject</button>\n            </ion-col>\n            <ion-col col-6>\n              <button no-margin ion-button full large col-12 (click)="ConfirmOut()">ConfirmOut</button>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n      </div>\n      \n    </ion-list>\n\n    <ion-list *ngSwitchCase="\'In\'">\n      <div padding>\n        <button ion-button full color="secondary" (click)="viewHistory()">View MoldMaker Update</button>\n      </div>\n\n      <ion-row>\n        <ion-col col-6>\n          <ion-col col-12>\n            <ion-grid *ngIf="moldinoutModel?.getNewMoldInOut?.imagesOut?.length > 0">\n              <ion-row>\n                <ion-col col-12 *ngFor="let photo of moldinoutModel?.getNewMoldInOut?.imagesOut; let id = index">\n                  <ion-card class="card-background-page">\n                    <img style="height: 150px" src="{{IP}}/inout/showImage?filename={{photo.PhotoName}}" (tap)="tapViewPhoto(photo?.PhotoName)" alt=""/>\n                    <ion-card-content>\n                      <p class="gps" (click)="openLaunchNavigator(photo?.Latiture, photo?.Longiture)">{{ photo?.Latiture }}, {{ photo?.Longiture }}</p>\n                    </ion-card-content>\n                  </ion-card>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-col>\n        </ion-col>\n        \n        <ion-col col-6> \n          <ion-col col-12 *ngIf="moldinoutModel?.getNewMoldInOut?.imagesIn?.length > 0">\n            <ion-grid *ngIf="(moldinoutModel?.getNewMoldInOut?.imagesIn?.length > 0 && moldinoutModel?.getNewMoldInOut?.imagesOut?.length > 0)">\n              <ion-row>\n                <ion-col col-12 *ngFor="let photo of moldinoutModel?.getNewMoldInOut?.imagesIn; let id = index">\n                  <ion-card class="card-background-page">\n                    <img style="height: 150px" src="{{IP}}/inout/showImage?filename={{photo.PhotoName}}" (tap)="tapViewPhoto(photo?.PhotoName)" alt=""/>\n                    <ion-card-content>\n                      <p class="gps" (click)="openLaunchNavigator(photo?.Latiture, photo?.Longiture)">{{ photo?.Latiture }}, {{ photo?.Longiture }}</p>\n                    </ion-card-content>\n                  </ion-card>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-col>\n  \n          <ion-col col-6 *ngIf="moldinoutModel?.getNewMoldInOut?.imagesIn?.length <= 0">\n            <ion-col col-12>\n              <ion-grid>\n                <ion-row>\n                  <ion-col col-12 *ngFor="let photo of moldinoutModel?.Images; let id = index">\n                    <ion-card class="card-background-page">\n                      <ion-icon name="trash" class="deleteIcon" (click)="deletePhoto(id)"></ion-icon>\n                      <img style="height: 150px" [src]="photo?.photo" (tap)="tapViewPhoto(photo?.photo)" alt="" />\n                      <ion-card-content>\n                        <p class="gps" (click)="openLaunchNavigator(photo?.latitude, photo?.longitude)">{{ photo?.latitude }}, {{ photo?.longitude }}</p>\n                      </ion-card-content>\n                    </ion-card>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-col>\n  \n            <ion-col col-6 *ngIf="moldinoutModel?.getNewMoldInOut?.imagesIn?.length <= 0 && moldinoutModel?.Images?.length < limit">\n              <ion-col col-12 *ngIf="moldinoutModel?.Images?.length < limit">\n                <ion-card class="card-background-page">\n                  <ion-icon name="add" class="addIcon" (click)="presentActionSheet()"></ion-icon>\n                  <img style="height: 150px" src="../../assets/imgs/images.png" alt=""/>\n                </ion-card>\n              </ion-col> \n            </ion-col>\n          </ion-col>\n        </ion-col>\n      </ion-row>\n\n      <div padding style="padding-top: 0px; padding-bottom: 5px;" *ngIf="moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.remarkIn != \'\'">\n        <pre style="text-align: left; font-family: sans-serif;">Remark In : {{ moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.remarkIn }}</pre>\n      </div>\n\n      <mold-in-out-approve [taggedApprove]="moldinoutModel?.getApprove?.approveIn" *ngIf="(approveIn) && (moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.status != \'ConfirmOut\')"></mold-in-out-approve>\n      <div *ngIf="(approveIn) && (moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.status == \'ConfirmIn\')">\n        <ion-item no-padding class="btn-setting-fav">\n          <ion-row class="two-col-button">\n            <ion-col col-6>\n              <button no-margin ion-button full large col-12 class="cancel-btn" (click)="rejectTypeIn()">Reject</button>\n            </ion-col>\n            <ion-col col-6>\n              <button no-margin ion-button full large col-12 (click)="approveTypeIn()">Approve</button>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n      </div>\n      <div padding *ngIf="(moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.makerStatus == \'Completed\') && (moldinoutModel?.getNewMoldInOut?.inoutMold[0]?.status == \'ConfirmOut\')">\n        <button ion-button full color="primary" (click)="ConfirmIn()">\n          Confirm In\n        </button>\n      </div>\n    </ion-list>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-history/moldinout-history.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_moldinout_provider_moldinout_provider__["a" /* MoldinoutProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_moldmaker_provider_moldmaker_provider__["a" /* MoldmakerProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_storage_provider_storage_provider__["a" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_12__providers_scan_scan__["a" /* ScanProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */]])
    ], MoldinoutHistoryPage);
    return MoldinoutHistoryPage;
}(__WEBPACK_IMPORTED_MODULE_0__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=moldinout-history.js.map

/***/ })

});
//# sourceMappingURL=15.js.map