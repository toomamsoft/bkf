webpackJsonp([10],{

/***/ 433:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoldinoutViewMoldmakerPageModule", function() { return MoldinoutViewMoldmakerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__moldinout_view_moldmaker__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var MoldinoutViewMoldmakerPageModule = /** @class */ (function () {
    function MoldinoutViewMoldmakerPageModule() {
    }
    MoldinoutViewMoldmakerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_view_moldmaker__["a" /* MoldinoutViewMoldmakerPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__moldinout_view_moldmaker__["a" /* MoldinoutViewMoldmakerPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_5__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_view_moldmaker__["a" /* MoldinoutViewMoldmakerPage */]
            ]
        })
    ], MoldinoutViewMoldmakerPageModule);
    return MoldinoutViewMoldmakerPageModule;
}());

//# sourceMappingURL=moldinout-view-moldmaker.module.js.map

/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldinoutViewMoldmakerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_MoldMakerModel__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_moldmaker_provider_moldmaker_provider__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_constant__ = __webpack_require__(3);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the MoldinoutViewMoldmakerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MoldinoutViewMoldmakerPage = /** @class */ (function (_super) {
    __extends(MoldinoutViewMoldmakerPage, _super);
    function MoldinoutViewMoldmakerPage(navCtrl, events, _moldmakerProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this._moldmakerProvider = _moldmakerProvider;
        _this.IP = __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].IPAddress;
        return _this;
    }
    MoldinoutViewMoldmakerPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MoldinoutViewMoldmakerPage');
        this.LoadData();
    };
    MoldinoutViewMoldmakerPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.moldMakerModel = this._moldmakerProvider.moldmakerModel;
                return [2 /*return*/];
            });
        });
    };
    MoldinoutViewMoldmakerPage.prototype.ngOnDestroy = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this._moldmakerProvider.moldmakerModel = new __WEBPACK_IMPORTED_MODULE_3__models_MoldMakerModel__["a" /* MoldMakerModel */];
                return [2 /*return*/];
            });
        });
    };
    MoldinoutViewMoldmakerPage.prototype.LoadData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldmakerProvider.getBKFViewHistory()];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _a.sent();
                        this._moldmakerProvider.submitErrorEvent(this._moldmakerProvider.moldmakerModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutViewMoldmakerPage.prototype.tapViewPhoto = function (photo) {
        return __awaiter(this, void 0, void 0, function () {
            var view;
            return __generator(this, function (_a) {
                view = this.IP + "/inout/showImage?filename='=" + photo;
                __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].viewImages(view);
                return [2 /*return*/];
            });
        });
    };
    MoldinoutViewMoldmakerPage.prototype.openLaunchNavigator = function (latitude, longitude) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                __WEBPACK_IMPORTED_MODULE_5__shared_utilities__["a" /* Utilities */].openGoogleMapApplication(latitude, longitude);
                return [2 /*return*/];
            });
        });
    };
    MoldinoutViewMoldmakerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-moldinout-view-moldmaker',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-view-moldmaker/moldinout-view-moldmaker.html"*/'<!--\n  Generated template for the MoldinoutViewMoldmakerPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>View MoldMaker</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <accordion-menu *ngFor="let item of moldMakerModel?.bkfview?.bkfviewHistory; let i = index"\n    [title]="item?.createDate"\n    textColor="#0f0f0f"\n    hasMargin="false"\n    headerColor="#f4f4f4"\n    [expanded]="i === 0">\n    <ion-list>\n      <ion-item>\n        <ion-label fixed>MoldID</ion-label>\n        <ion-input type="text" value="{{ item?.moldId }}" [disabled]="true"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Status</ion-label>\n        <ion-input type="text" value="{{ item?.status }}" [disabled]="true"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>CreateDate</ion-label>\n        <ion-input type="text" value="{{ item?.createDate }}" [disabled]="true"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Remark</ion-label>\n        <ion-textarea value="{{ item.remark }}" value="{{ item?.remark }}" [disabled]="true" autosize></ion-textarea>\n      </ion-item>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-6 *ngFor="let photo of item?.photo; ">\n            <ion-card class="card-background-page">\n              <!-- <img style="height: 150px" [src]="photo?.photo" (tap)="tapViewPhoto(photo?.photo)" alt="" /> -->\n              <img style="height: 150px" src="{{IP}}/survey/showImage?filename={{photo.photo}}" (tap)="tapViewPhoto(photo?.photo)" alt="" />\n              <ion-card-content>\n                <p class="gps" (tap)="openLaunchNavigator(photo?.latitude ,photo?.longitude)">{{ photo?.latitude }}, {{ photo?.latitude }}</p>\n              </ion-card-content>\n            </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-list>\n  </accordion-menu>\n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-view-moldmaker/moldinout-view-moldmaker.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__providers_moldmaker_provider_moldmaker_provider__["a" /* MoldmakerProvider */]])
    ], MoldinoutViewMoldmakerPage);
    return MoldinoutViewMoldmakerPage;
}(__WEBPACK_IMPORTED_MODULE_0__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=moldinout-view-moldmaker.js.map

/***/ })

});
//# sourceMappingURL=10.js.map