webpackJsonp([2],{

/***/ 441:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SurveyHistoryPageModule", function() { return SurveyHistoryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__survey_history__ = __webpack_require__(470);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var SurveyHistoryPageModule = /** @class */ (function () {
    function SurveyHistoryPageModule() {
    }
    SurveyHistoryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__survey_history__["a" /* SurveyHistoryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__survey_history__["a" /* SurveyHistoryPage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__survey_history__["a" /* SurveyHistoryPage */]
            ]
        })
    ], SurveyHistoryPageModule);
    return SurveyHistoryPageModule;
}());

//# sourceMappingURL=survey-history.module.js.map

/***/ }),

/***/ 470:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyHistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_survey_provider_survey_provider__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_SurveyModel__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_scan_scan__ = __webpack_require__(55);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











/**
 * Generated class for the SurveyHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SurveyHistoryPage = /** @class */ (function (_super) {
    __extends(SurveyHistoryPage, _super);
    function SurveyHistoryPage(navCtrl, navParams, event, alertCtrl, formBuilder, _surveyProvider, _scanProvider) {
        var _this = _super.call(this, event) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.event = event;
        _this.alertCtrl = alertCtrl;
        _this.formBuilder = formBuilder;
        _this._surveyProvider = _surveyProvider;
        _this._scanProvider = _scanProvider;
        _this.Mode = 'formMold';
        _this.form = _this.formBuilder.group({
            MoldId: [''],
            fromDate: [null, [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]],
            toDate: [null, [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]]
        });
        return _this;
    }
    SurveyHistoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SurveyHistoryPage');
    };
    SurveyHistoryPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Promise.all([this.surveyModel = this._surveyProvider.surVeyModel])];
                    case 1:
                        _a.sent();
                        this.form.controls['fromDate'].setValue(__WEBPACK_IMPORTED_MODULE_6__shared_utilities__["a" /* Utilities */].getCurrentDateTimeMySql());
                        this.form.controls['toDate'].setValue(__WEBPACK_IMPORTED_MODULE_6__shared_utilities__["a" /* Utilities */].getCurrentDateTimeMySql());
                        return [2 /*return*/];
                }
            });
        });
    };
    SurveyHistoryPage.prototype.ngOnDestroy = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this._surveyProvider.surVeyModel = new __WEBPACK_IMPORTED_MODULE_4__models_SurveyModel__["a" /* SurveyModel */]();
                return [2 /*return*/];
            });
        });
    };
    SurveyHistoryPage.prototype.onSearchByMold = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert_1;
            return __generator(this, function (_a) {
                if (this.form.controls['MoldId'].value == '') {
                    alert_1 = this.alertCtrl.create({
                        enableBackdropDismiss: false,
                        title: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%message.title%"],
                        subTitle: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%message.serach.mold.error%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%message.serach.mold.error%"],
                        buttons: [{
                                text: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%btn.close%"]
                            }]
                    });
                    alert_1.present();
                    return [2 /*return*/];
                }
                this.surveyModel.MoldId = this.form.controls['MoldId'].value;
                this.surveyModel.From = '';
                this.surveyModel.To = '';
                this.onSearchHistory();
                return [2 /*return*/];
            });
        });
    };
    SurveyHistoryPage.prototype.onSearchByDate = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.surveyModel.MoldId = '';
                this.surveyModel.From = this.form.controls['fromDate'].value;
                this.surveyModel.To = this.form.controls['toDate'].value;
                this.onSearchHistory();
                return [2 /*return*/];
            });
        });
    };
    SurveyHistoryPage.prototype.scan = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = this.form.controls['MoldId']).setValue;
                        return [4 /*yield*/, this._scanProvider.scan()];
                    case 1:
                        _b.apply(_a, [_c.sent()]);
                        if (this.form.controls['MoldId'].value == '') {
                            return [2 /*return*/];
                        }
                        this.surveyModel.MoldId = this.form.controls['MoldId'].value;
                        this.surveyModel.From = '';
                        this.surveyModel.To = '';
                        this.onSearchHistory();
                        return [2 /*return*/];
                }
            });
        });
    };
    SurveyHistoryPage.prototype.onSearchHistory = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert_2, ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Promise.all([this._surveyProvider.getSearchSurvey()])];
                    case 1:
                        _a.sent();
                        if (this.surveyModel.surveyMoldHistory.search.length <= 0) {
                            alert_2 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%message.notfound%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%message.notfound%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_9__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_2.present();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _a.sent();
                        this._surveyProvider.submitErrorEvent(this._surveyProvider.surVeyModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SurveyHistoryPage.prototype.viewHistory = function (Items) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.surveyModel.MoldId = Items.MoldId;
                this.surveyModel.RunNo = Items.RunNo;
                this.navCtrl.push('SurveyHistoryDetailPage');
                return [2 /*return*/];
            });
        });
    };
    SurveyHistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-survey-history',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/survey-history/survey-history.html"*/'<!--\n  Generated template for the SurveyHistoryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>History</ion-title>\n  </ion-navbar>\n  <ion-toolbar no-border-top>\n    <ion-segment [(ngModel)]="Mode">\n      <ion-segment-button value="formMold">\n        <h6>MOLD</h6>\n      </ion-segment-button>\n      <ion-segment-button value="formDate">\n        <h6>HISTORY</h6>\n      </ion-segment-button>\n    </ion-segment>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div [ngSwitch]="Mode">\n    <form [formGroup]="form">\n      <ion-list *ngSwitchCase="\'formMold\'">\n        <ion-item>\n          <ion-label floating>Mold ID, BKF PartNumber, Customer PartNumber</ion-label>\n          <ion-input type="text" formControlName="MoldId"></ion-input>\n          <button ion-button icon-only clear item-end (click)="scan()">\n            <ion-icon name="qr-scanner"></ion-icon>\n          </button>\n        </ion-item>\n        <div padding>\n          <button ion-button icon-end full (click)="onSearchByMold()">\n            Search\n            <ion-icon name="search"></ion-icon>\n          </button>\n        </div>\n\n        <ion-list>\n          <ion-item *ngFor="let items of surveyModel?.surveyMoldHistory?.search" (click)="viewHistory(items)">\n            <h3>MoldID: {{ items.MoldId }}</h3>\n            <p>CustNo: {{ items.AssetId }}</p>\n            <p>PartNo: {{ items.Plate }}</p>\n            <p>CreateDate: {{ items.CreateDate | datetime: \'dateAndTime\' }}</p>\n            <ion-icon name="arrow-forward" item-end></ion-icon>\n          </ion-item>\n        </ion-list>\n      </ion-list>\n      \n      <ion-list *ngSwitchCase="\'formDate\'">\n        <ion-item>\n          <ion-label>From </ion-label>\n          <ion-datetime pickerFormat="DD/MM/YYYY" pickerFormat="DD/MM/YYYY" formControlName="fromDate"></ion-datetime>\n        </ion-item>\n        <ion-item>\n          <ion-label>To </ion-label>\n          <ion-datetime displayFormat="DD/MM/YYYY" pickerFormat="DD/MM/YYYY" formControlName="toDate"></ion-datetime>\n        </ion-item>\n        <div padding>\n          <button ion-button icon-end full [disabled]="!form.valid" (click)="onSearchByDate()">\n            Search\n            <ion-icon name="search"></ion-icon>\n          </button>\n        </div>\n        <ion-list>\n          <ion-item *ngFor="let items of surveyModel?.surveyMoldHistory?.search" (click)="viewHistory(items)">\n            <h3>MoldID: {{ items.MoldId }}</h3>\n            <p>CustNo: {{ items.AssetId }}</p>\n            <p>PartNo: {{ items.Plate }}</p>\n            <p>CreateDate: {{ items.CreateDate | datetime: \'dateAndTime\' }}</p>\n            <ion-icon name="arrow-forward" item-end></ion-icon>\n          </ion-item>\n        </ion-list>\n      </ion-list>\n    </form>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/survey-history/survey-history.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_survey_provider_survey_provider__["a" /* SurveyProvider */],
            __WEBPACK_IMPORTED_MODULE_10__providers_scan_scan__["a" /* ScanProvider */]])
    ], SurveyHistoryPage);
    return SurveyHistoryPage;
}(__WEBPACK_IMPORTED_MODULE_0__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=survey-history.js.map

/***/ })

});
//# sourceMappingURL=2.js.map