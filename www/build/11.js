webpackJsonp([11],{

/***/ 432:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoldinoutStatusPageModule", function() { return MoldinoutStatusPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__moldinout_status__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pipes_pipes_module__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var MoldinoutStatusPageModule = /** @class */ (function () {
    function MoldinoutStatusPageModule() {
    }
    MoldinoutStatusPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_status__["a" /* MoldinoutStatusPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__moldinout_status__["a" /* MoldinoutStatusPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_status__["a" /* MoldinoutStatusPage */]
            ]
        })
    ], MoldinoutStatusPageModule);
    return MoldinoutStatusPageModule;
}());

//# sourceMappingURL=moldinout-status.module.js.map

/***/ }),

/***/ 462:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldinoutStatusPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_MoldInOutModel__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_moldinout_provider_moldinout_provider__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_constant__ = __webpack_require__(3);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









/**
 * Generated class for the MoldinoutStatusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MoldinoutStatusPage = /** @class */ (function (_super) {
    __extends(MoldinoutStatusPage, _super);
    function MoldinoutStatusPage(navCtrl, events, alertCtrl, formBuilder, _moldinoutProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this.alertCtrl = alertCtrl;
        _this.formBuilder = formBuilder;
        _this._moldinoutProvider = _moldinoutProvider;
        _this.dataStatus = [];
        _this.form = _this.formBuilder.group({
            Status: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]]
        });
        return _this;
    }
    MoldinoutStatusPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MoldinoutStatusPage');
    };
    MoldinoutStatusPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.molInOutModel = this._moldinoutProvider.moldInOutModel;
                // this.dataResult = this.molInOutModel.getStatus;
                this.dataStatus = [{
                        id: '', description: 'Please select Status'
                    }, {
                        id: '1', description: 'Waiting For Approve Out'
                    }, {
                        id: '2', description: 'Waiting For Tooling Pickup'
                    }, {
                        id: '3', description: 'Processing For Transportation'
                    }, {
                        id: '4', description: 'Tooling Receive Completed'
                    }, {
                        id: '5', description: 'Waiting For Approve In'
                    }, {
                        id: '6', description: 'Tooling Approve Completed'
                    }, {
                        id: '7', description: 'Tooling Reject&Cancel'
                    }, {
                        id: '8', description: 'Tooling Completed'
                    }];
                return [2 /*return*/];
            });
        });
    };
    MoldinoutStatusPage.prototype.ngOnDestroy = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this._moldinoutProvider.moldInOutModel = new __WEBPACK_IMPORTED_MODULE_4__models_MoldInOutModel__["a" /* MoldInOutModel */]();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutStatusPage.prototype.onSelectStatus = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert_1, ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        if (this.form.controls['Status'].value == '') {
                            return [2 /*return*/];
                        }
                        this.dataResult = [];
                        this._moldinoutProvider.moldInOutModel = new __WEBPACK_IMPORTED_MODULE_4__models_MoldInOutModel__["a" /* MoldInOutModel */]();
                        this.molInOutModel = this._moldinoutProvider.moldInOutModel;
                        this.molInOutModel.statusId = this.form.controls['Status'].value;
                        return [4 /*yield*/, this._moldinoutProvider.moldStatus()];
                    case 1:
                        _a.sent();
                        if (this.molInOutModel.getStatus.moldStatus.length <= 0) {
                            alert_1 = this.alertCtrl.create({
                                enableBackdropDismiss: false,
                                title: __WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__["%message.title%"],
                                subTitle: __WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__["%message.notfound%"] : __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__["%message.notfound%"],
                                buttons: [{
                                        text: __WEBPACK_IMPORTED_MODULE_8__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__["%btn.close%"]
                                    }]
                            });
                            alert_1.present();
                        }
                        else {
                            this.dataResult = this.molInOutModel.getStatus.moldStatus;
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _a.sent();
                        this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutStatusPage.prototype.viewHistory = function (itemSelected) {
        return __awaiter(this, void 0, void 0, function () {
            var ex_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        this._moldinoutProvider.moldInOutModel = new __WEBPACK_IMPORTED_MODULE_4__models_MoldInOutModel__["a" /* MoldInOutModel */]();
                        this.molInOutModel = this._moldinoutProvider.moldInOutModel;
                        this.molInOutModel.MoldId = itemSelected.moldId;
                        this.molInOutModel.JobId = itemSelected.jobId;
                        return [4 /*yield*/, this._moldinoutProvider.getNewMoldOut()];
                    case 1:
                        _a.sent();
                        this.navCtrl.push('MoldinoutHistoryPage');
                        return [3 /*break*/, 3];
                    case 2:
                        ex_2 = _a.sent();
                        this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutStatusPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-moldinout-status',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-status/moldinout-status.html"*/'<!--\n  Generated template for the MoldinoutStatusPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>Status</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <form [formGroup]="form">\n      <ion-item>\n        <ion-label>Status</ion-label>\n        <ion-select interface="action-sheet" formControlName="Status">\n          <ion-option *ngFor="let items of dataStatus" [value]="items.id" (ionSelect)="onSelectStatus()">{{ items.description }}</ion-option>\n        </ion-select>\n      </ion-item>\n    </form>\n  </ion-list>\n\n  <ion-list>\n    <ion-item *ngFor="let items of dataResult" (click)="viewHistory(items)">\n      <h3>MoldID: {{ items?.moldId }}</h3>\n      <p>Plan Out: {{ items?.planOut }}</p>\n      <p>Plan In: {{ items?.planIn }}</p>\n      <p>Mold Maker: {{ items?.makerName }}</p>\n      <p>Reson: {{ items?.reson }}</p>\n      <p>Creator: {{ items?.name }}</p>\n      <p *ngIf="items?.status != \'\'" style="color: red">{{ items?.status }}</p>\n      <ion-icon name="arrow-forward" item-end></ion-icon>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-status/moldinout-status.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_5__providers_moldinout_provider_moldinout_provider__["a" /* MoldinoutProvider */]])
    ], MoldinoutStatusPage);
    return MoldinoutStatusPage;
}(__WEBPACK_IMPORTED_MODULE_2__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=moldinout-status.js.map

/***/ })

});
//# sourceMappingURL=11.js.map