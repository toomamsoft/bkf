webpackJsonp([27],{

/***/ 419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminCreateUserPageModule", function() { return AdminCreateUserPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_create_user__ = __webpack_require__(449);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AdminCreateUserPageModule = /** @class */ (function () {
    function AdminCreateUserPageModule() {
    }
    AdminCreateUserPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__admin_create_user__["a" /* AdminCreateUserPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__admin_create_user__["a" /* AdminCreateUserPage */]),
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__admin_create_user__["a" /* AdminCreateUserPage */]
            ]
        })
    ], AdminCreateUserPageModule);
    return AdminCreateUserPageModule;
}());

//# sourceMappingURL=admin-create-user.module.js.map

/***/ }),

/***/ 449:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminCreateUserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_admin_provider_admin_provider__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the AdminCreateUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AdminCreateUserPage = /** @class */ (function (_super) {
    __extends(AdminCreateUserPage, _super);
    function AdminCreateUserPage(navCtrl, events, formBuilder, alertCtrl, _adminProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this.formBuilder = formBuilder;
        _this.alertCtrl = alertCtrl;
        _this._adminProvider = _adminProvider;
        _this.position = [{ id: 1, value: 'Staff' }, { id: 2, value: 'Manager' }, { id: 3, value: 'Department' }, { id: 4, value: 'Division' }];
        _this.form = _this.formBuilder.group({
            EmpID: [null, [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]],
            FirstName: [null, [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]],
            LastName: [null, [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]],
            UserType: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]],
            UserGroup: [null],
            Username: [null, [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]],
            Password: [null, [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]]
        });
        return _this;
    }
    AdminCreateUserPage.prototype.ngOnInit = function () {
        this.adminModel = this._adminProvider.adminModel;
    };
    AdminCreateUserPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminCreateUserPage');
    };
    AdminCreateUserPage.prototype.reset = function () {
    };
    AdminCreateUserPage.prototype.save = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    this.adminModel.empid = this.form.controls['EmpID'].value;
                    this.adminModel.firstname = this.form.controls['FirstName'].value;
                    this.adminModel.lastname = this.form.controls['LastName'].value;
                    this.adminModel.position = this.form.controls['UserType'].value;
                    this.adminModel.usergroup = this.form.controls['UserGroup'].value;
                    this.adminModel.username = this.form.controls['Username'].value;
                    this.adminModel.password = this.form.controls['Password'].value;
                    this.adminModel.flag = 'I';
                    if (this.adminModel.getListBKF.getListBKF.filter(function (val) {
                        return val.empId == _this.adminModel.empid;
                    }).length > 0) {
                        this.alertCtrl.create({
                            enableBackdropDismiss: false,
                            title: 'แจ้งเตือน',
                            subTitle: 'รหัสพนักงานนี้มีอยู่ในระบบแล้ว กรุราลองใหม่อีกครั้ง',
                            buttons: [{
                                    text: __WEBPACK_IMPORTED_MODULE_5__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__["%btn.close%"]
                                }]
                        }).present();
                        return [2 /*return*/];
                    }
                    if (this.adminModel.getListBKF.getListBKF.filter(function (val) {
                        return val.username == _this.adminModel.username;
                    }).length > 0) {
                        this.alertCtrl.create({
                            enableBackdropDismiss: false,
                            title: 'แจ้งเตือน',
                            subTitle: 'ชื่อผู้ใช้งานนี้มีอยู่ในระบบแล้ว กรุราลองใหม่อีกครั้ง',
                            buttons: [{
                                    text: __WEBPACK_IMPORTED_MODULE_5__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__["%btn.close%"]
                                }]
                        }).present();
                        return [2 /*return*/];
                    }
                    this.alertCtrl.create({
                        enableBackdropDismiss: false,
                        title: 'แจ้งเตือน',
                        subTitle: 'คุณต้องการเพิ่มพนักงานใช่หรือไม่',
                        buttons: [{
                                text: 'ไม่'
                            }, {
                                text: 'ใช่',
                                handler: function () {
                                    _this.addEmployee();
                                }
                            }]
                    }).present();
                }
                catch (ex) {
                    this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
                }
                return [2 /*return*/];
            });
        });
    };
    AdminCreateUserPage.prototype.addEmployee = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._adminProvider.getUpdateUserBKF()];
                    case 1:
                        _a.sent();
                        this.alertCtrl.create({
                            enableBackdropDismiss: false,
                            title: __WEBPACK_IMPORTED_MODULE_5__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__["%message.title%"],
                            subTitle: __WEBPACK_IMPORTED_MODULE_5__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__["%message.save.success%"] : __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__["%message.save.success%"],
                            buttons: [{
                                    text: __WEBPACK_IMPORTED_MODULE_5__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_6__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_7__assets_i18n_en_json__["%btn.close%"],
                                    handler: function () {
                                        _this.navCtrl.pop();
                                    }
                                }]
                        }).present();
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _a.sent();
                        this._adminProvider.submitErrorEvent(this._adminProvider.adminModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AdminCreateUserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-admin-create-user',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/admin-create-user/admin-create-user.html"*/'<!--\n  Generated template for the AdminEditUserPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>admin-edit-create</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]="form">\n    <ion-list>\n      <ion-item>\n        <ion-label>รหัสพนักงาน</ion-label>\n        <ion-input type="text" formControlName="EmpID"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>ชื่อ</ion-label>\n        <ion-input type="text" formControlName="FirstName"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>นามสกุล</ion-label>\n        <ion-input type="text" formControlName="LastName"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>ตำแหน่ง</ion-label>\n        <ion-select interface="action-sheet" formControlName="UserType">\n          <ion-option value="">กรุณาเลือกตำแหน่ง</ion-option>\n          <ion-option *ngFor="let items of position" [value]="items?.value">{{ items?.value }}</ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-list>\n\n\n    <ion-list>\n      <ion-item>\n        <ion-label>ชื่อผุ้ใช้งาน</ion-label>\n        <ion-input type="text" formControlName="Username"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>รหัสผ่าน</ion-label>\n        <ion-input type="text" formControlName="Password"></ion-input>\n      </ion-item>\n    </ion-list>\n  </form>\n</ion-content>\n<ion-footer>\n  <ion-item no-padding class="btn-setting-fav">\n    <ion-row class="two-col-button">\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 class="cancel-btn" (click)="reset()">Reset</button>\n      </ion-col>\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 (click)="save()" [disabled]="!form.valid">Save</button>\n      </ion-col>\n    </ion-row>\n  </ion-item>\n</ion-footer>'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/admin-create-user/admin-create-user.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_admin_provider_admin_provider__["a" /* AdminProvider */]])
    ], AdminCreateUserPage);
    return AdminCreateUserPage;
}(__WEBPACK_IMPORTED_MODULE_0__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=admin-create-user.js.map

/***/ })

});
//# sourceMappingURL=27.js.map