webpackJsonp([14],{

/***/ 429:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoldinoutPreviewPageModule", function() { return MoldinoutPreviewPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__moldinout_preview__ = __webpack_require__(459);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var MoldinoutPreviewPageModule = /** @class */ (function () {
    function MoldinoutPreviewPageModule() {
    }
    MoldinoutPreviewPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_preview__["a" /* MoldinoutPreviewPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__moldinout_preview__["a" /* MoldinoutPreviewPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_preview__["a" /* MoldinoutPreviewPage */]
            ]
        })
    ], MoldinoutPreviewPageModule);
    return MoldinoutPreviewPageModule;
}());

//# sourceMappingURL=moldinout-preview.module.js.map

/***/ }),

/***/ 459:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldinoutPreviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_utilities__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_moldinout_provider_moldinout_provider__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









/**
 * Generated class for the MoldinoutPreviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MoldinoutPreviewPage = /** @class */ (function (_super) {
    __extends(MoldinoutPreviewPage, _super);
    function MoldinoutPreviewPage(navCtrl, events, alertCtrl, formBuilder, _moldinoutProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this.alertCtrl = alertCtrl;
        _this.formBuilder = formBuilder;
        _this._moldinoutProvider = _moldinoutProvider;
        _this.Mode = 'Out';
        _this.form = _this.formBuilder.group({
            MoldId: [{ value: null, disabled: true }, [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]],
            MoldMaker: [{ value: null, disabled: true }],
            AssetId: [{ value: null, disabled: true }],
            CustNo: [{ value: null, disabled: true }],
            PartNo: [{ value: null, disabled: true }],
            PartName: [{ value: null, disabled: true }],
            Remark: [{ value: null, disabled: true }],
            MakerId: [{ value: null, disabled: true }],
            ReasonId: [{ value: null, disabled: true }],
            PlanIn: [{ value: null, disabled: true }],
            PlanOut: [{ value: null, disabled: true }]
        });
        return _this;
    }
    MoldinoutPreviewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MoldinoutPreviewPage');
    };
    MoldinoutPreviewPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var AssetId, CustNo, PartNo, PartName, i;
            return __generator(this, function (_a) {
                AssetId = '';
                CustNo = '';
                PartNo = '';
                PartName = '';
                this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
                this.form.controls['MoldId'].setValue(this.moldinoutModel.MoldId);
                this.form.controls['MoldMaker'].setValue(this.moldinoutModel.MoldMaker);
                for (i = 0; i < this.moldinoutModel.getNewMoldInOut.inoutMold[0].description.length; i++) {
                    if (i == 0) {
                        AssetId = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].AssetId;
                        CustNo = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].CustNo;
                        PartNo = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartNo;
                        PartName = this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartName;
                    }
                    else {
                        AssetId += '\n' + this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].AssetId;
                        CustNo += '\n' + this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].CustNo;
                        PartNo += '\n' + this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartNo;
                        PartName += '\n' + this.moldinoutModel.getNewMoldInOut.inoutMold[0].description[i].PartName;
                    }
                }
                this.form.controls['AssetId'].setValue(AssetId);
                this.form.controls['CustNo'].setValue(CustNo);
                this.form.controls['PartNo'].setValue(PartNo);
                this.form.controls['PartName'].setValue(PartName);
                this.form.controls['Remark'].setValue(this.moldinoutModel.Remark);
                this.form.controls['MakerId'].setValue(this.moldinoutModel.MakerId);
                this.form.controls['ReasonId'].setValue(this.moldinoutModel.ReasonId);
                this.form.controls['PlanIn'].setValue(this.moldinoutModel.PlanIn);
                this.form.controls['PlanOut'].setValue(this.moldinoutModel.PlanOut);
                return [2 /*return*/];
            });
        });
    };
    MoldinoutPreviewPage.prototype.tapViewPhoto = function (imageBase64) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                __WEBPACK_IMPORTED_MODULE_4__shared_utilities__["a" /* Utilities */].viewImages(imageBase64);
                return [2 /*return*/];
            });
        });
    };
    MoldinoutPreviewPage.prototype.ConfirmSaveData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%title.confirm%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%title.confirm%"],
                    subTitle: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%message.save.success%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%message.save.success%"],
                    buttons: [{
                            text: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%btn.cancel%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%btn.cancel%"],
                            handler: function () {
                                console.log('Click Cancel');
                            }
                        },
                        {
                            text: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%btn.confirm%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%btn.confirm%"],
                            handler: function () {
                                _this.saveData();
                            }
                        }]
                }).present();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutPreviewPage.prototype.saveData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this._moldinoutProvider.saveDataNewMold()];
                    case 1:
                        _a.sent();
                        this.alertCtrl.create({
                            enableBackdropDismiss: false,
                            title: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%message.title%"],
                            subTitle: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%message.save.success%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%message.save.success%"],
                            buttons: [{
                                    text: __WEBPACK_IMPORTED_MODULE_6__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_7__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_8__assets_i18n_en_json__["%btn.close%"],
                                    handler: function () {
                                        _this.popToPage('MoldinoutMenuPage', _this.navCtrl);
                                    }
                                }]
                        }).present();
                        return [3 /*break*/, 3];
                    case 2:
                        ex_1 = _a.sent();
                        this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutPreviewPage.prototype.backToPage = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.navCtrl.pop();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutPreviewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-moldinout-preview',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-preview/moldinout-preview.html"*/'<!--\n  Generated template for the MoldinoutCreatePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>Mold In/Out Preview</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]="form">\n    <ion-list>\n      <ion-item>\n        <ion-label fixed>Mold ID</ion-label>\n        <ion-input formControlName="MoldId" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>MoldMakerID</ion-label>\n        <ion-input formControlName="MoldMaker" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>AssetID</ion-label>\n        <ion-textarea formControlName="AssetId" placeholder="AssetID" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>CustNo</ion-label>\n        <ion-textarea formControlName="CustNo" placeholder="CustNo" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>PartNo</ion-label>\n        <ion-textarea formControlName="PartNo" placeholder="PartNo" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>PartName</ion-label>\n        <ion-textarea formControlName="PartName" placeholder="PartName" rows="5" autosize></ion-textarea>\n      </ion-item>\n      <ion-item>\n        <ion-label>MoldMaker</ion-label>\n        <ion-select interface="action-sheet" formControlName="MakerId">\n          <ion-option value="">กรุณาเลือก MoldMaker</ion-option>\n          <ion-option *ngFor="let items of moldinoutModel?.getSetting?.makerId" [value]="items?.id">{{ items?.description }}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label>Plan Out</ion-label>\n        <ion-datetime pickerFormat="DD/MM/YYYY HH:mm" displayFormat="DD/MM/YYYY HH:mm" formControlName="PlanOut"></ion-datetime>\n      </ion-item>\n      <ion-item>\n        <ion-label>Plan In</ion-label>\n        <ion-datetime pickerFormat="DD/MM/YYYY HH:mm" displayFormat="DD/MM/YYYY HH:mm" formControlName="PlanIn"></ion-datetime>\n      </ion-item>\n      <ion-item>\n        <ion-label>Reason</ion-label>\n        <ion-select interface="action-sheet" formControlName="ReasonId">\n          <ion-option value="">กรุณาเลือก MoldMaker</ion-option>\n          <ion-option *ngFor="let items of moldinoutModel?.getSetting?.reason" [value]="items?.id">{{ items?.description }}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label fixed>Remark</ion-label>\n        <ion-textarea formControlName="Remark" placeholder="Remark" rows="5" autosize></ion-textarea>\n      </ion-item>\n    </ion-list>\n  </form>\n\n  <ion-segment [(ngModel)]="Mode">\n    <ion-segment-button value="Out">\n      <h6>OUT</h6>\n    </ion-segment-button>\n    <ion-segment-button value="In" [disabled]="true">\n      <h6>IN</h6>\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]="Mode">\n    <ion-list *ngSwitchCase="\'Out\'">\n      <accordion-list\n        [title]="\'%label.images%\' | translate"\n        textColor="#FFF"\n        hasMargin="false"\n        headerColor="#A9ADAA"\n        [expanded]="true">\n        <ion-grid>\n          <ion-row>\n            <ion-col col-6 *ngFor="let photo of moldinoutModel?.Images; let id = index">\n              <ion-card class="card-background-page">\n                <img style="height: 150px" [src]="photo?.photo" (tap)="tapViewPhoto(photo?.photo)" alt="" />\n                <ion-card-content>\n                  <p class="gps">{{ photo?.latitude }}, {{ photo?.longitude }}</p>\n                </ion-card-content>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-grid> \n      </accordion-list>\n      \n    </ion-list>\n\n    <ion-list *ngSwitchCase="\'In\'">\n      \n    </ion-list>\n  </div>\n</ion-content>\n\n<ion-footer>\n  <ion-item no-padding class="btn-setting-fav">\n    <ion-row class="two-col-button">\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 class="cancel-btn" (click)="backToPage()">{{ \'%btn.survey.back%\' | translate }}</button>\n      </ion-col>\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 (click)="ConfirmSaveData()">{{ \'%btn.survey.save%\' | translate }}</button>\n      </ion-col>\n    </ion-row>\n  </ion-item>\n</ion-footer>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-preview/moldinout-preview.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_5__providers_moldinout_provider_moldinout_provider__["a" /* MoldinoutProvider */]])
    ], MoldinoutPreviewPage);
    return MoldinoutPreviewPage;
}(__WEBPACK_IMPORTED_MODULE_0__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=moldinout-preview.js.map

/***/ })

});
//# sourceMappingURL=14.js.map