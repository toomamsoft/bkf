webpackJsonp([18],{

/***/ 428:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoldinoutConfirmInPageModule", function() { return MoldinoutConfirmInPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__moldinout_confirm_in__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MoldinoutConfirmInPageModule = /** @class */ (function () {
    function MoldinoutConfirmInPageModule() {
    }
    MoldinoutConfirmInPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_confirm_in__["a" /* MoldinoutConfirmInPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__moldinout_confirm_in__["a" /* MoldinoutConfirmInPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__moldinout_confirm_in__["a" /* MoldinoutConfirmInPage */]
            ]
        })
    ], MoldinoutConfirmInPageModule);
    return MoldinoutConfirmInPageModule;
}());

//# sourceMappingURL=moldinout-confirm-in.module.js.map

/***/ }),

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MoldinoutConfirmInPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_basepage__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_constant__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_moldinout_provider_moldinout_provider__ = __webpack_require__(54);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the MoldinoutConfirmInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MoldinoutConfirmInPage = /** @class */ (function (_super) {
    __extends(MoldinoutConfirmInPage, _super);
    function MoldinoutConfirmInPage(navCtrl, events, formBuilder, alertCtrl, _moldinoutProvider) {
        var _this = _super.call(this, events) || this;
        _this.navCtrl = navCtrl;
        _this.events = events;
        _this.formBuilder = formBuilder;
        _this.alertCtrl = alertCtrl;
        _this._moldinoutProvider = _moldinoutProvider;
        _this.form = _this.formBuilder.group({
            Remark: ['']
        });
        return _this;
    }
    MoldinoutConfirmInPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.moldinoutModel = this._moldinoutProvider.moldInOutModel;
                return [2 /*return*/];
            });
        });
    };
    MoldinoutConfirmInPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MoldinoutConfirmInPage');
    };
    MoldinoutConfirmInPage.prototype.onConfirmIn = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        this.moldinoutModel.ConfirmRemark = this.form.controls['Remark'].value;
                        if (!(this.moldinoutModel.ConfirmStatus == 'N')) return [3 /*break*/, 2];
                        return [4 /*yield*/, this._moldinoutProvider.ConfirmOrRejectOut()];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 2:
                        this.moldinoutModel.ConfirmStatus = 'Y';
                        return [4 /*yield*/, this._moldinoutProvider.ConfirmIn()];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        this.alertCtrl.create({
                            enableBackdropDismiss: false,
                            title: __WEBPACK_IMPORTED_MODULE_4__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__["%message.title%"],
                            subTitle: __WEBPACK_IMPORTED_MODULE_4__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__["%message.save.success%"] : __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__["%message.save.success%"],
                            buttons: [{
                                    text: __WEBPACK_IMPORTED_MODULE_4__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__["%btn.close%"] : __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__["%btn.close%"],
                                    handler: function () {
                                        _this.popToPage('MoldinoutMenuPage', _this.navCtrl);
                                    }
                                }]
                        }).present();
                        return [3 /*break*/, 6];
                    case 5:
                        ex_1 = _a.sent();
                        this._moldinoutProvider.submitErrorEvent(this._moldinoutProvider.moldInOutModel2);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    MoldinoutConfirmInPage.prototype.backPage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: __WEBPACK_IMPORTED_MODULE_4__shared_constant__["a" /* CONSTANT */].APP_LANGUAGE == 'th' ? __WEBPACK_IMPORTED_MODULE_5__assets_i18n_th_json__["%message.title%"] : __WEBPACK_IMPORTED_MODULE_6__assets_i18n_en_json__["%message.title%"],
                    subTitle: 'คุณต้องการยกเลิกรายการนี้ใช่ไหม',
                    buttons: [{
                            text: 'ยกลเลิก',
                            handler: function () {
                                console.log('cancel click');
                            }
                        }, {
                            text: 'ยืนยัน',
                            handler: function () {
                                _this.popToPage('MoldinoutMenuPage', _this.navCtrl);
                            }
                        }]
                }).present();
                return [2 /*return*/];
            });
        });
    };
    MoldinoutConfirmInPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-moldinout-confirm-in',template:/*ion-inline-start:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-confirm-in/moldinout-confirm-in.html"*/'<!--\n  Generated template for the MoldinoutConfirmInPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>moldinout-confirm-reject</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]="form">\n    <ion-item>\n      <ion-label floating>Remark</ion-label>\n      <ion-textarea formControlName="Remark" rows="6" autosize></ion-textarea>\n    </ion-item>\n  </form>\n</ion-content>\n\n<ion-footer>\n  <ion-item no-padding class="btn-setting-fav">\n    <ion-row class="two-col-button">\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 class="cancel-btn" (click)="backPage()">{{ \'%btn.back%\' | translate }}</button>\n      </ion-col>\n      <ion-col col-6>\n        <button no-margin ion-button full large col-12 [disabled]="!form.valid" (click)="onConfirmIn()">{{ \'%btn.survey.save%\' | translate }}</button>\n      </ion-col>\n    </ion-row>\n  </ion-item>\n</ion-footer>\n'/*ion-inline-end:"/Users/ToomTam/Desktop/project/BKF/src/pages/moldinout-confirm-in/moldinout-confirm-in.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_moldinout_provider_moldinout_provider__["a" /* MoldinoutProvider */]])
    ], MoldinoutConfirmInPage);
    return MoldinoutConfirmInPage;
}(__WEBPACK_IMPORTED_MODULE_0__shared_basepage__["a" /* BasePage */]));

//# sourceMappingURL=moldinout-confirm-in.js.map

/***/ })

});
//# sourceMappingURL=18.js.map